﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace SiteWare.Entity.Entities
{
    [DataContract]
    public class Plugin_Reserach_ArticleEntity
    {
       
        [DataMember] public long            ArticleID           { get; set; }
        [DataMember] public string          ArticleTitle        { get; set; }
        [DataMember] public string          ArticleImage        { get; set; }
        [DataMember] public DateTime        ArticleDate         { get; set; }
        [DataMember] public string          ArticleKeyWords     { get; set; }
        [DataMember] public string          ArticleSummery      { get; set; }
        [DataMember] public string          ArticleAuthor       { get; set; }
        [DataMember] public string          ArticleCategory     { get; set; }
        [DataMember] public string          ArticleDetails      { get; set; }
        [DataMember] public long            Order               { get; set; }
        [DataMember] public DateTime        PublishedDate       { get; set; }
        [DataMember] public bool            IsPublished         { get; set; }
        [DataMember] public bool            IsDelete            { get; set; }
        [DataMember] public int             LanguageID          { get; set; }
        [DataMember] public DateTime        AddDate             { get; set; }
        [DataMember] public string          AddUser             { get; set; }
        [DataMember] public DateTime        EditDate            { get; set; }
        [DataMember] public string          EditUser            { get; set; }
        [DataMember] public string LanguageName { get; set; }
        [DataMember] public long  ViewCount { get; set; }
        [DataMember] public bool IsShowSlider { get; set; }

    }
}


