﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.Entity.Entities
{
    public class Plugin_Reserach_AuthorImageEntity
    {
        [DataMember]
        public long AuthorImageID { get; set; }
        [DataMember]
        public long AuthorID { get; set; }
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public string Path { get; set; }
        [DataMember]
        public int Width { get; set; }
        [DataMember]
        public int Height { get; set; }
        [DataMember]
        public bool IsDeleted { get; set; }
        [DataMember]
        public DateTime AddDate { get; set; }
        [DataMember]
        public DateTime DeleteDate { get; set; }
    }
}
