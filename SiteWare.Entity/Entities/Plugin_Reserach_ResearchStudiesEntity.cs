﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace SiteWare.Entity.Entities
{
    [DataContract]
    public class Plugin_Reserach_ResearchStudiesEntity
    {
        [DataMember] public long        ResearchStudiesID       { get; set; }
        [DataMember] public string      ResearchStudiesTitle    { get; set; }
        [DataMember] public DateTime    ResearchStudiesDate     { get; set; }
        [DataMember] public string      ResearchStudiesAuthor   { get; set; }
        [DataMember] public string      ResearchStudiesVersion  { get; set; }
        [DataMember] public string      ResearchStudiesSummery  { get; set; }
        [DataMember] public string      ResearchStudiesDetails  { get; set; }
        [DataMember] public string      ResearchStudiesCatogory { get; set; }
        [DataMember] public string      ResearchStudiesImage    { get; set; }
        [DataMember] public long        Order                   { get; set; }
        [DataMember] public DateTime    PublishedDate           { get; set; }
        [DataMember] public bool        IsPublished             { get; set; }
        [DataMember] public bool        IsDelete                { get; set; }
        [DataMember] public int         LanguageID              { get; set; }
        [DataMember] public DateTime    AddDate                 { get; set; }
        [DataMember] public string      AddUser                 { get; set; }
        [DataMember] public DateTime    EditDate                { get; set; }
        [DataMember] public string      EditUser                { get; set; }
        [DataMember] public string      LanguageName            { get; set; }
        [DataMember] public long        ViewCount               { get; set; }
        [DataMember] public string      ResearchStudiesTitle2   { get; set; }
        [DataMember] public string      ResearchStudiesImage2   { get; set; }

        [DataMember] public string MedialLink { get; set; }
        [DataMember] public string Target { get; set; }
    }
}

 
