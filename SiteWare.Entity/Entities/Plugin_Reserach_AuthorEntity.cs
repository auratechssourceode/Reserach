﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace SiteWare.Entity.Entities
{
    [DataContract]
    public class Plugin_Reserach_AuthorEntity
    {
        [DataMember] public long        AuthorsID           { get; set; }
        [DataMember] public string      AuthorName          { get; set; }
        [DataMember] public string      AuthorImage         { get; set; }
        [DataMember] public string      AuthorTitle         { get; set; }
        [DataMember] public string      AuthorSummery       { get; set; }
        [DataMember] public string      AuthorDetails       { get; set; }
        [DataMember] public long        Order               { get; set; }
        [DataMember] public DateTime    PublishedDate       { get; set; }
        [DataMember] public bool        IsPublished         { get; set; }
        [DataMember] public bool        IsDelete            { get; set; }
        [DataMember] public int         LanguageID          { get; set; }
        [DataMember] public DateTime    AddDate             { get; set; }
        [DataMember] public string      AddUser             { get; set; }
        [DataMember] public DateTime    EditDate            { get; set; }
        [DataMember] public string      EditUser            { get; set; }
        [DataMember] public string      LanguageName        { get; set; }


    }
}

