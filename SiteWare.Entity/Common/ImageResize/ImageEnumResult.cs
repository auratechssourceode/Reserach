﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.Entity.Common.ImageResize
{


    public enum AuthenticationResults
    {
        Denied = 1,
        Disabled = 2,
        Allowed = 3,
        MaxNumberOfAttemptsReached = 4,
        WorngUsernameOrPassword = 5
    }

    class ImageEnumResult
    {
    }

    public class NewsImageSize
    {
        public static Tuple<int, int> ImgSize1 = new Tuple<int, int>(242, 126);
        public static Tuple<int, int> ImgSize2 = new Tuple<int, int>(692, 333);
        public static Tuple<int, int> GetImgSize(int size)
        {
            Tuple<int, int> iSize = new Tuple<int, int>(0, 0);
            switch (size)
            {
                case 1:
                    iSize = ImgSize1;
                    break;
                case 2:
                    iSize = ImgSize2;
                    break;
                default:
                    iSize = ImgSize1;
                    break;
            }
            return iSize;
        }
    }


    public class PillarsImageSize
    {
        public static Tuple<int, int> ImgSize1 = new Tuple<int, int>(114, 264);
        
        public static Tuple<int, int> GetImgSize(int size)
        {
            Tuple<int, int> iSize = new Tuple<int, int>(0, 0);
            switch (size)
            {
                case 1:
                    iSize = ImgSize1;
                    break;             
                default:
                    iSize = ImgSize1;
                    break;
            }
            return iSize;
        }
    }

    public class ThinkTankImageSize
    {
        public static Tuple<int, int> ImgSize1 = new Tuple<int, int>(251, 251);

        public static Tuple<int, int> GetImgSize(int size)
        {
            Tuple<int, int> iSize = new Tuple<int, int>(0, 0);
            switch (size)
            {
                case 1:
                    iSize = ImgSize1;
                    break;
                default:
                    iSize = ImgSize1;
                    break;
            }
            return iSize;
        }
    }

    public class SuppportersImageSize
    {
        public static Tuple<int, int> ImgSize1 = new Tuple<int, int>(204, 88);
        public static Tuple<int, int> ImgSize2 = new Tuple<int, int>(335, 144);
        public static Tuple<int, int> GetImgSize(int size)
        {
            Tuple<int, int> iSize = new Tuple<int, int>(0, 0);
            switch (size)
            {
                case 1:
                    iSize = ImgSize1;
                    break;
                case 2:
                    iSize = ImgSize2;
                    break;
                default:
                    iSize = ImgSize1;
                    break;
            }
            return iSize;
        }
    }

    public class AuthorImageSize
    {
        public static Tuple<int, int> ImgSize1 = new Tuple<int, int>(350, 350);

        public static Tuple<int, int> GetImgSize(int size)
        {
            Tuple<int, int> iSize = new Tuple<int, int>(0, 0);
            switch (size)
            {
                case 1:
                    iSize = ImgSize1;
                    break;
                default:
                    iSize = ImgSize1;
                    break;
            }
            return iSize;
        }
    }

}
