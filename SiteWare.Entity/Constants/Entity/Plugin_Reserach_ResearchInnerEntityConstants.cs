﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.Entity.Constants.Entity
{
    public static class Plugin_Reserach_ResearchInnerEntityConstants
    {
        public const string ResearchInnerID        = "ResearchInnerID";
        public const string ResearchInnerTitle     = "ResearchInnerTitle";
        public const string ResearchInnerSummery   = "ResearchInnerSummery";        
        public const string ResearchInnerCatogory  = "ResearchInnerCatogory";
        public const string ResearchInnerImage     = "ResearchInnerImage";
        public const string Order                  = "Order";
        public const string PublishedDate          = "PublishedDate";
        public const string IsPublished            = "IsPublished";
        public const string IsDelete               = "IsDelete";
        public const string LanguageID             = "LanguageID";
        public const string AddDate                = "AddDate";
        public const string AddUser                = "AddUser";
        public const string EditDate               = "EditDate";
        public const string EditUser               = "EditUser";
        public const string LanguageName           = "LanguageName";
        public const string ViewCount              = "ViewCount";
        public const string ResearchInnerTitle2    = "ResearchInnerTitle2";
        public const string MedialLink             = "MedialLink";
        public const string Target                 = "Target";
        public const string ResearchInnerDate      = "ResearchInnerDate";

    }
}
