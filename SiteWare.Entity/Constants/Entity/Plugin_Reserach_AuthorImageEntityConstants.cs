﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.Entity.Constants.Entity
{
    public static class Plugin_Reserach_AuthorImageEntityConstants
    {
        public const string AuthorImageID = "AuthorImageID";
        public const string AuthorID = "AuthorID";
        public const string FileName = "FileName";
        public const string Path = "Path";
        public const string Width = "Width";
        public const string Height = "Height";
        public const string IsDeleted = "IsDeleted";
        public const string AddDate = "AddDate";
        public const string DeleteDate = "DeleteDate";
    }
}
