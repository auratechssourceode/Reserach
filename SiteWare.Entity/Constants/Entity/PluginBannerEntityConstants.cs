﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.Entity.Constants.Entity
{
    public static class PluginBannerEntityConstants
    {
        public const string ID = "ID";
        public const string Title = "Title";
        public const string Sammery = "Sammery"; 
        public const string BannerImage = "BannerImage";
        public const string IconImage = "IconImage";
        public const string CategoryID = "CategoryID";
        public const string LanguageID = "LanguageID";
        public const string LanguageName = "LanguageName";
        public const string LinkTitle = "LinkTitle";
        public const string Link = "Link";
        public const string Target = "Target";
        public const string ImageTitle = "ImageTitle";
        public const string AltIamge = "AltIamge";
        public const string IsDeleted = "IsDeleted";
        public const string AddDate = "AddDate";
        public const string AddUser = "AddUser";
        public const string EditDate = "EditDate";
        public const string EditUser = "EditUser";
        public const string BannerOrder = "BannerOrder";
        public const string IsPublished = "IsPublished";
        public const string PublishDate = "PublishDate";
    }
}
