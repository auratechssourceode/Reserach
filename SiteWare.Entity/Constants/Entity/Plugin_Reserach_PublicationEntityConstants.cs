﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.Entity.Constants.Entity
{
    public static class Plugin_Reserach_PublicationEntityConstants
    {
        public const string PublicationsID = "PublicationsID";
        public const string PublicationsTitle = "PublicationsTitle";
        public const string PublicationsDate = "PublicationsDate";
        public const string PublicationsAuthor = "PublicationsAuthor";
        public const string PublicationsVersion = "PublicationsVersion";
        public const string PublicationsSummery = "PublicationsSummery";
        public const string PublicationsDetails = "PublicationsDetails";
        public const string PublicationsCatogory = "PublicationsCatogory";
        public const string PublicationsImage = "PublicationsImage";
        public const string Order = "Order";
        public const string PublishedDate = "PublishedDate";
        public const string IsPublished = "IsPublished";
        public const string IsDelete = "IsDelete";
        public const string LanguageID = "LanguageID";
        public const string AddDate = "AddDate";
        public const string AddUser = "AddUser";
        public const string EditDate = "EditDate";
        public const string EditUser = "EditUser";
        public const string LanguageName = "LanguageName";

        public const string ViewCount = "ViewCount";
        public const string PublicationsTitle2 = "PublicationsTitle2";
        public const string PublicationsImage2 = "PublicationsImage2";
    }
}
