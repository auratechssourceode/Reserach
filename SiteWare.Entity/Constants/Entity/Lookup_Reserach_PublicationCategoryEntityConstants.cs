﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.Entity.Constants.Entity
{
    public static class Lookup_Reserach_PublicationCategoryEntityConstants
    {
        public const string PublicationCategoryID = "PublicationCategoryID";
        public const string name = "name";
        public const string IsPublished = "IsPublished";
        public const string IsDeleted = "IsDeleted";
        public const string AddDate = "AddDate";
        public const string AddUser = "AddUser";
        public const string EditDate = "EditDate";
        public const string EditUser = "EditUser";
    }
}
