﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.Entity.Constants.Entity
{
    public static class Plugin_Reserach_AuthorEntityConstants
    {
        public const string AuthorsID     = "AuthorsID";
        public const string AuthorName    = "AuthorName";
        public const string AuthorImage   = "AuthorImage";
        public const string AuthorTitle   = "AuthorTitle";
        public const string AuthorSummery = "AuthorSummery";
        public const string AuthorDetails = "AuthorDetails";
        public const string Order = "Order";
        public const string PublishedDate = "PublishedDate";
        public const string IsPublished = "IsPublished";
        public const string IsDelete = "IsDelete";
        public const string LanguageID = "LanguageID";
        public const string AddDate = "AddDate";
        public const string AddUser = "AddUser";
        public const string EditDate = "EditDate";
        public const string EditUser = "EditUser";
        public const string LanguageName = "LanguageName";
    }
}
