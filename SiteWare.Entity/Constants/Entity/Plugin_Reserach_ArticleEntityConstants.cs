﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.Entity.Constants.Entity
{
    public static class Plugin_Reserach_ArticleEntityConstants
    {
        public const string ArticleID       = "ArticleID";
        public const string ArticleTitle    = "ArticleTitle";
        public const string ArticleImage    = "ArticleImage";
        public const string ArticleDate     = "ArticleDate";
        public const string ArticleKeyWords = "ArticleKeyWords";
        public const string ArticleSummery  = "ArticleSummery";
        public const string ArticleAuthor   = "ArticleAuthor";
        public const string ArticleCategory = "ArticleCategory";
        public const string ArticleDetails  = "ArticleDetails";
        public const string Order           = "Order";
        public const string PublishedDate   = "PublishedDate";
        public const string IsPublished     = "IsPublished";
        public const string IsDelete        = "IsDelete";
        public const string LanguageID      = "LanguageID";
        public const string AddDate         = "AddDate";
        public const string AddUser         = "AddUser";
        public const string EditDate        = "EditDate";
        public const string EditUser        = "EditUser";
        public const string LanguageName = "LanguageName";
        public const string ViewCount = "ViewCount";
        public const string IsShowSlider = "IsShowSlider";


    }                              
}
