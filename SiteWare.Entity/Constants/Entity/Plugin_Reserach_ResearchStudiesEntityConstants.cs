﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.Entity.Constants.Entity
{
    public static class Plugin_Reserach_ResearchStudiesEntityConstants
    {
        public const string ResearchStudiesID       = "ResearchStudiesID";
        public const string ResearchStudiesTitle    = "ResearchStudiesTitle";
        public const string ResearchStudiesDate     = "ResearchStudiesDate";
        public const string ResearchStudiesAuthor   = "ResearchStudiesAuthor";
        public const string ResearchStudiesVersion  = "ResearchStudiesVersion";
        public const string ResearchStudiesSummery  = "ResearchStudiesSummery";
        public const string ResearchStudiesDetails  = "ResearchStudiesDetails";
        public const string ResearchStudiesCatogory = "ResearchStudiesCatogory";
        public const string ResearchStudiesImage    = "ResearchStudiesImage";
        public const string Order = "Order";
        public const string PublishedDate = "PublishedDate";
        public const string IsPublished = "IsPublished";
        public const string IsDelete = "IsDelete";
        public const string LanguageID = "LanguageID";
        public const string AddDate = "AddDate";
        public const string AddUser = "AddUser";
        public const string EditDate = "EditDate";
        public const string EditUser = "EditUser";
        public const string LanguageName = "LanguageName";
        public const string ViewCount               = "ViewCount";
        public const string ResearchStudiesTitle2   = "ResearchStudiesTitle2";        
        public const string ResearchStudiesImage2   = "ResearchStudiesImage2";

        public const string MedialLink = "MedialLink";
        public const string Target = "Target";

    }
}
