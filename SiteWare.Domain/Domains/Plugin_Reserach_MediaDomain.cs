﻿using SiteWare.DataAccess.Repositories;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.Domain.Domains
{
    public class Plugin_Reserach_MediaDomain
    {

        public async static Task<ResultList<Plugin_Reserach_MediaEntity>> GetAll()
        {
            ResultList<Plugin_Reserach_MediaEntity> result = new ResultList<Plugin_Reserach_MediaEntity>();

            result = await Plugin_Reserach_MediaRepository.SelectAll();

            return result;
        }
        public static ResultList<Plugin_Reserach_MediaEntity> GetAllNotAsync()
        {
            ResultList<Plugin_Reserach_MediaEntity> result = new ResultList<Plugin_Reserach_MediaEntity>();

            result = Plugin_Reserach_MediaRepository.SelectAllNotAsync();

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_MediaEntity>> InsertRecord(Plugin_Reserach_MediaEntity entity)
        {
            ResultEntity<Plugin_Reserach_MediaEntity> result = new ResultEntity<Plugin_Reserach_MediaEntity>();

            result = await Plugin_Reserach_MediaRepository.Insert(entity);

            return result;
        }
        public static ResultEntity<Plugin_Reserach_MediaEntity> InsertRecordNotAsync(Plugin_Reserach_MediaEntity entity)
        {
            ResultEntity<Plugin_Reserach_MediaEntity> result = new ResultEntity<Plugin_Reserach_MediaEntity>();

            result = Plugin_Reserach_MediaRepository.InsertNotAsync(entity);

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_MediaEntity>> GetByID(long ID)
        {
            ResultEntity<Plugin_Reserach_MediaEntity> result = new ResultEntity<Plugin_Reserach_MediaEntity>();

            result = await Plugin_Reserach_MediaRepository.SelectByID(ID);

            return result;
        }
        public static ResultEntity<Plugin_Reserach_MediaEntity> GetByIDNotAsync(long ID)
        {
            ResultEntity<Plugin_Reserach_MediaEntity> result = new ResultEntity<Plugin_Reserach_MediaEntity>();

            result = Plugin_Reserach_MediaRepository.SelectByIDNotAsync(ID);

            return result;
        }

        public static ResultEntity<Plugin_Reserach_MediaEntity> UpdateRecordNotAsync(Plugin_Reserach_MediaEntity entity)
        {
            ResultEntity<Plugin_Reserach_MediaEntity> result = new ResultEntity<Plugin_Reserach_MediaEntity>();

            result = Plugin_Reserach_MediaRepository.UpdateNotAsync(entity);

            return result;
        }
        public async static Task<ResultEntity<Plugin_Reserach_MediaEntity>> UpdateRecord(Plugin_Reserach_MediaEntity entity)
        {
            ResultEntity<Plugin_Reserach_MediaEntity> result = new ResultEntity<Plugin_Reserach_MediaEntity>();

            result = await Plugin_Reserach_MediaRepository.Update(entity);

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_MediaEntity>> DeleteRecord(long ID)
        {
            ResultEntity<Plugin_Reserach_MediaEntity> result = new ResultEntity<Plugin_Reserach_MediaEntity>();

            result = await Plugin_Reserach_MediaRepository.Delete(ID);

            return result;
        }


        public async static Task<ResultEntity<Plugin_Reserach_MediaEntity>> UpdateNewsViewCount(long NewsID)
        {
            ResultEntity<Plugin_Reserach_MediaEntity> result = new ResultEntity<Plugin_Reserach_MediaEntity>();

            result = await Plugin_Reserach_MediaRepository.UpdateViewCount(NewsID);

            return result;
        }
    }
}
