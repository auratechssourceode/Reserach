﻿using SiteWare.DataAccess.Repositories;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.Domain.Domains
{
    public static class Lookup_Reserach_ArticleCategoryDomain
    {
        public async static Task<ResultList<Lookup_Reserach_ArticleCategoryEntity>> GetLookupAreaAll()
        {
            ResultList<Lookup_Reserach_ArticleCategoryEntity> result = new ResultList<Lookup_Reserach_ArticleCategoryEntity>();

            result = await Lookup_Reserach_ArticleCategoryRepository.SelectAll();

            return result;
        }

        public static ResultList<Lookup_Reserach_ArticleCategoryEntity> GetLookupAreaAllNotAsync()
        {
            ResultList<Lookup_Reserach_ArticleCategoryEntity> result = new ResultList<Lookup_Reserach_ArticleCategoryEntity>();

            result = Lookup_Reserach_ArticleCategoryRepository.SelectAllNotAsync();

            return result;
        }

        //public async static Task<ResultList<Lookup_Reserach_ArticleCategoryEntity>> GetLookupAreaByGovernateID(int GovernateID)
        //{
        //    ResultList<Lookup_Reserach_ArticleCategoryEntity> result = new ResultList<Lookup_Reserach_ArticleCategoryEntity>();

        //    result = await Lookup_Reserach_ArticleCategoryRepository.Lookup_Area_ByGovernateID(GovernateID);

        //    return result;
        //}

        //public static ResultList<Lookup_Reserach_ArticleCategoryEntity> GetLookupAreaByGovernateIDNotAsync(int GovernateID)
        //{
        //    ResultList<Lookup_Reserach_ArticleCategoryEntity> result = new ResultList<Lookup_Reserach_ArticleCategoryEntity>();

        //    result = Lookup_Reserach_ArticleCategoryRepository.Lookup_Area_ByGovernateIDNotAsync(GovernateID);

        //    return result;
        //}


        public async static Task<ResultEntity<Lookup_Reserach_ArticleCategoryEntity>> GetByID(int ID)
        {
            ResultEntity<Lookup_Reserach_ArticleCategoryEntity> result = new ResultEntity<Lookup_Reserach_ArticleCategoryEntity>();

            result = await Lookup_Reserach_ArticleCategoryRepository.SelectByID(ID);

            return result;
        }
        public static ResultEntity<Lookup_Reserach_ArticleCategoryEntity> GetByIDNotAsync(int ID)
        {
            ResultEntity<Lookup_Reserach_ArticleCategoryEntity> result = new ResultEntity<Lookup_Reserach_ArticleCategoryEntity>();

            result = Lookup_Reserach_ArticleCategoryRepository.SelectByIDNotAsync(ID);

            return result;
        }
    }
}
