﻿using SiteWare.DataAccess.Repositories;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.Domain.Domains
{
    public class Plugin_Reserach_ResearchInnerDomain
    {
        public async static Task<ResultList<Plugin_Reserach_ResearchInnerEntity>> GetAll()
        {
            ResultList<Plugin_Reserach_ResearchInnerEntity> result = new ResultList<Plugin_Reserach_ResearchInnerEntity>();

            result = await Plugin_Reserach_ResearchInnerRepository.SelectAll();

            return result;
        }
        public static ResultList<Plugin_Reserach_ResearchInnerEntity> GetAllNotAsync()
        {
            ResultList<Plugin_Reserach_ResearchInnerEntity> result = new ResultList<Plugin_Reserach_ResearchInnerEntity>();

            result = Plugin_Reserach_ResearchInnerRepository.SelectAllNotAsync();

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_ResearchInnerEntity>> InsertRecord(Plugin_Reserach_ResearchInnerEntity entity)
        {
            ResultEntity<Plugin_Reserach_ResearchInnerEntity> result = new ResultEntity<Plugin_Reserach_ResearchInnerEntity>();

            result = await Plugin_Reserach_ResearchInnerRepository.Insert(entity);

            return result;
        }
        public static ResultEntity<Plugin_Reserach_ResearchInnerEntity> InsertRecordNotAsync(Plugin_Reserach_ResearchInnerEntity entity)
        {
            ResultEntity<Plugin_Reserach_ResearchInnerEntity> result = new ResultEntity<Plugin_Reserach_ResearchInnerEntity>();

            result = Plugin_Reserach_ResearchInnerRepository.InsertNotAsync(entity);

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_ResearchInnerEntity>> GetByID(long ID)
        {
            ResultEntity<Plugin_Reserach_ResearchInnerEntity> result = new ResultEntity<Plugin_Reserach_ResearchInnerEntity>();

            result = await Plugin_Reserach_ResearchInnerRepository.SelectByID(ID);

            return result;
        }
        public static ResultEntity<Plugin_Reserach_ResearchInnerEntity> GetByIDNotAsync(long ID)
        {
            ResultEntity<Plugin_Reserach_ResearchInnerEntity> result = new ResultEntity<Plugin_Reserach_ResearchInnerEntity>();

            result = Plugin_Reserach_ResearchInnerRepository.SelectByIDNotAsync(ID);

            return result;
        }

        public static ResultEntity<Plugin_Reserach_ResearchInnerEntity> UpdateRecordNotAsync(Plugin_Reserach_ResearchInnerEntity entity)
        {
            ResultEntity<Plugin_Reserach_ResearchInnerEntity> result = new ResultEntity<Plugin_Reserach_ResearchInnerEntity>();

            result = Plugin_Reserach_ResearchInnerRepository.UpdateNotAsync(entity);

            return result;
        }
        public async static Task<ResultEntity<Plugin_Reserach_ResearchInnerEntity>> UpdateRecord(Plugin_Reserach_ResearchInnerEntity entity)
        {
            ResultEntity<Plugin_Reserach_ResearchInnerEntity> result = new ResultEntity<Plugin_Reserach_ResearchInnerEntity>();

            result = await Plugin_Reserach_ResearchInnerRepository.Update(entity);

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_ResearchInnerEntity>> DeleteRecord(long ID)
        {
            ResultEntity<Plugin_Reserach_ResearchInnerEntity> result = new ResultEntity<Plugin_Reserach_ResearchInnerEntity>();

            result = await Plugin_Reserach_ResearchInnerRepository.Delete(ID);

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_ResearchInnerEntity>> UpdateNewsViewCount(long NewsID)
        {
            ResultEntity<Plugin_Reserach_ResearchInnerEntity> result = new ResultEntity<Plugin_Reserach_ResearchInnerEntity>();

            result = await Plugin_Reserach_ResearchInnerRepository.UpdateViewCount(NewsID);

            return result;
        }
    }
}
