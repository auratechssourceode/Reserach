﻿using SiteWare.DataAccess.Repositories;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.Domain.Domains
{
    public class Plugin_Reserach_ResearchStudiesDomain
    {
        public async static Task<ResultList<Plugin_Reserach_ResearchStudiesEntity>> GetAll()
        {
            ResultList<Plugin_Reserach_ResearchStudiesEntity> result = new ResultList<Plugin_Reserach_ResearchStudiesEntity>();

            result = await Plugin_Reserach_ResearchStudiesRepository.SelectAll();

            return result;
        }
        public static ResultList<Plugin_Reserach_ResearchStudiesEntity> GetAllNotAsync()
        {
            ResultList<Plugin_Reserach_ResearchStudiesEntity> result = new ResultList<Plugin_Reserach_ResearchStudiesEntity>();

            result = Plugin_Reserach_ResearchStudiesRepository.SelectAllNotAsync();

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_ResearchStudiesEntity>> InsertRecord(Plugin_Reserach_ResearchStudiesEntity entity)
        {
            ResultEntity<Plugin_Reserach_ResearchStudiesEntity> result = new ResultEntity<Plugin_Reserach_ResearchStudiesEntity>();

            result = await Plugin_Reserach_ResearchStudiesRepository.Insert(entity);

            return result;
        }
        public static ResultEntity<Plugin_Reserach_ResearchStudiesEntity> InsertRecordNotAsync(Plugin_Reserach_ResearchStudiesEntity entity)
        {
            ResultEntity<Plugin_Reserach_ResearchStudiesEntity> result = new ResultEntity<Plugin_Reserach_ResearchStudiesEntity>();

            result = Plugin_Reserach_ResearchStudiesRepository.InsertNotAsync(entity);

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_ResearchStudiesEntity>> GetByID(long ID)
        {
            ResultEntity<Plugin_Reserach_ResearchStudiesEntity> result = new ResultEntity<Plugin_Reserach_ResearchStudiesEntity>();

            result = await Plugin_Reserach_ResearchStudiesRepository.SelectByID(ID);

            return result;
        }
        public static ResultEntity<Plugin_Reserach_ResearchStudiesEntity> GetByIDNotAsync(long ID)
        {
            ResultEntity<Plugin_Reserach_ResearchStudiesEntity> result = new ResultEntity<Plugin_Reserach_ResearchStudiesEntity>();

            result = Plugin_Reserach_ResearchStudiesRepository.SelectByIDNotAsync(ID);

            return result;
        }

        public static ResultEntity<Plugin_Reserach_ResearchStudiesEntity> UpdateRecordNotAsync(Plugin_Reserach_ResearchStudiesEntity entity)
        {
            ResultEntity<Plugin_Reserach_ResearchStudiesEntity> result = new ResultEntity<Plugin_Reserach_ResearchStudiesEntity>();

            result = Plugin_Reserach_ResearchStudiesRepository.UpdateNotAsync(entity);

            return result;
        }
        public async static Task<ResultEntity<Plugin_Reserach_ResearchStudiesEntity>> UpdateRecord(Plugin_Reserach_ResearchStudiesEntity entity)
        {
            ResultEntity<Plugin_Reserach_ResearchStudiesEntity> result = new ResultEntity<Plugin_Reserach_ResearchStudiesEntity>();

            result = await Plugin_Reserach_ResearchStudiesRepository.Update(entity);

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_ResearchStudiesEntity>> DeleteRecord(long ID)
        {
            ResultEntity<Plugin_Reserach_ResearchStudiesEntity> result = new ResultEntity<Plugin_Reserach_ResearchStudiesEntity>();

            result = await Plugin_Reserach_ResearchStudiesRepository.Delete(ID);

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_ResearchStudiesEntity>> UpdateNewsViewCount(long NewsID)
        {
            ResultEntity<Plugin_Reserach_ResearchStudiesEntity> result = new ResultEntity<Plugin_Reserach_ResearchStudiesEntity>();

            result = await Plugin_Reserach_ResearchStudiesRepository.UpdateViewCount(NewsID);

            return result;
        }
    }
}
