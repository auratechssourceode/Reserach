﻿using SiteWare.DataAccess.Repositories;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.Domain.Domains
{
    public static class Lookup_Reserach_PublicationCategoryDomain
    {
        public async static Task<ResultList<Lookup_Reserach_PublicationCategoryEntity>> GetLookupAreaAll()
        {
            ResultList<Lookup_Reserach_PublicationCategoryEntity> result = new ResultList<Lookup_Reserach_PublicationCategoryEntity>();

            result = await Lookup_Reserach_PublicationCategoryRepository.SelectAll();

            return result;
        }

        public static ResultList<Lookup_Reserach_PublicationCategoryEntity> GetLookupAreaAllNotAsync()
        {
            ResultList<Lookup_Reserach_PublicationCategoryEntity> result = new ResultList<Lookup_Reserach_PublicationCategoryEntity>();

            result = Lookup_Reserach_PublicationCategoryRepository.SelectAllNotAsync();

            return result;
        }

        //public async static Task<ResultList<Lookup_Reserach_PublicationCategoryEntity>> GetLookupAreaByGovernateID(int GovernateID)
        //{
        //    ResultList<Lookup_Reserach_PublicationCategoryEntity> result = new ResultList<Lookup_Reserach_PublicationCategoryEntity>();

        //    result = await Lookup_Reserach_PublicationCategoryRepository.Lookup_Area_ByGovernateID(GovernateID);

        //    return result;
        //}

        //public static ResultList<Lookup_Reserach_PublicationCategoryEntity> GetLookupAreaByGovernateIDNotAsync(int GovernateID)
        //{
        //    ResultList<Lookup_Reserach_PublicationCategoryEntity> result = new ResultList<Lookup_Reserach_PublicationCategoryEntity>();

        //    result = Lookup_Reserach_PublicationCategoryRepository.Lookup_Area_ByGovernateIDNotAsync(GovernateID);

        //    return result;
        //}

        public async static Task<ResultEntity<Lookup_Reserach_PublicationCategoryEntity>> GetByID(int ID)
        {
            ResultEntity<Lookup_Reserach_PublicationCategoryEntity> result = new ResultEntity<Lookup_Reserach_PublicationCategoryEntity>();

            result = await Lookup_Reserach_PublicationCategoryRepository.SelectByID(ID);

            return result;
        }
        public static ResultEntity<Lookup_Reserach_PublicationCategoryEntity> GetByIDNotAsync(int ID)
        {
            ResultEntity<Lookup_Reserach_PublicationCategoryEntity> result = new ResultEntity<Lookup_Reserach_PublicationCategoryEntity>();

            result = Lookup_Reserach_PublicationCategoryRepository.SelectByIDNotAsync(ID);

            return result;
        }

    }
}
