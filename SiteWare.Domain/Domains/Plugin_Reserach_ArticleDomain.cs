﻿using SiteWare.DataAccess.Repositories;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.Domain.Domains
{
    public class Plugin_Reserach_ArticleDomain
    {
        public async static Task<ResultList<Plugin_Reserach_ArticleEntity>> GetAll()
        {
            ResultList<Plugin_Reserach_ArticleEntity> result = new ResultList<Plugin_Reserach_ArticleEntity>();

            result = await Plugin_Reserach_ArticleRepository.SelectAll();

            return result;
        }
        public static ResultList<Plugin_Reserach_ArticleEntity> GetAllNotAsync()
        {
            ResultList<Plugin_Reserach_ArticleEntity> result = new ResultList<Plugin_Reserach_ArticleEntity>();

            result = Plugin_Reserach_ArticleRepository.SelectAllNotAsync();

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_ArticleEntity>> InsertRecord(Plugin_Reserach_ArticleEntity entity)
        {
            ResultEntity<Plugin_Reserach_ArticleEntity> result = new ResultEntity<Plugin_Reserach_ArticleEntity>();

            result = await Plugin_Reserach_ArticleRepository.Insert(entity);

            return result;
        }
        public static ResultEntity<Plugin_Reserach_ArticleEntity> InsertRecordNotAsync(Plugin_Reserach_ArticleEntity entity)
        {
            ResultEntity<Plugin_Reserach_ArticleEntity> result = new ResultEntity<Plugin_Reserach_ArticleEntity>();

            result = Plugin_Reserach_ArticleRepository.InsertNotAsync(entity);

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_ArticleEntity>> GetByID(long ID)
        {
            ResultEntity<Plugin_Reserach_ArticleEntity> result = new ResultEntity<Plugin_Reserach_ArticleEntity>();

            result = await Plugin_Reserach_ArticleRepository.SelectByID(ID);

            return result;
        }
        public static ResultEntity<Plugin_Reserach_ArticleEntity> GetByIDNotAsync(long ID)
        {
            ResultEntity<Plugin_Reserach_ArticleEntity> result = new ResultEntity<Plugin_Reserach_ArticleEntity>();

            result = Plugin_Reserach_ArticleRepository.SelectByIDNotAsync(ID);

            return result;
        }

        public static ResultEntity<Plugin_Reserach_ArticleEntity> UpdateRecordNotAsync(Plugin_Reserach_ArticleEntity entity)
        {
            ResultEntity<Plugin_Reserach_ArticleEntity> result = new ResultEntity<Plugin_Reserach_ArticleEntity>();

            result = Plugin_Reserach_ArticleRepository.UpdateNotAsync(entity);

            return result;
        }
        public async static Task<ResultEntity<Plugin_Reserach_ArticleEntity>> UpdateRecord(Plugin_Reserach_ArticleEntity entity)
        {
            ResultEntity<Plugin_Reserach_ArticleEntity> result = new ResultEntity<Plugin_Reserach_ArticleEntity>();

            result = await Plugin_Reserach_ArticleRepository.Update(entity);

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_ArticleEntity>> DeleteRecord(long ID)
        {
            ResultEntity<Plugin_Reserach_ArticleEntity> result = new ResultEntity<Plugin_Reserach_ArticleEntity>();

            result = await Plugin_Reserach_ArticleRepository.Delete(ID);
            
            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_ArticleEntity>> UpdateNewsViewCount(long NewsID)
        {
            ResultEntity<Plugin_Reserach_ArticleEntity> result = new ResultEntity<Plugin_Reserach_ArticleEntity>();

            result = await Plugin_Reserach_ArticleRepository.UpdateViewCount(NewsID);

            return result;
        }
    }
}
