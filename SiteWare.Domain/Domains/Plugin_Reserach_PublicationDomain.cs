﻿using SiteWare.DataAccess.Repositories;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.Domain.Domains
{
    public class Plugin_Reserach_PublicationDomain
    {
        public async static Task<ResultList<Plugin_Reserach_PublicationEntity>> GetAll()
        {
            ResultList<Plugin_Reserach_PublicationEntity> result = new ResultList<Plugin_Reserach_PublicationEntity>();

            result = await Plugin_Reserach_PublicationRepository.SelectAll();

            return result;
        }
        public static ResultList<Plugin_Reserach_PublicationEntity> GetAllNotAsync()
        {
            ResultList<Plugin_Reserach_PublicationEntity> result = new ResultList<Plugin_Reserach_PublicationEntity>();

            result = Plugin_Reserach_PublicationRepository.SelectAllNotAsync();

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_PublicationEntity>> InsertRecord(Plugin_Reserach_PublicationEntity entity)
        {
            ResultEntity<Plugin_Reserach_PublicationEntity> result = new ResultEntity<Plugin_Reserach_PublicationEntity>();

            result = await Plugin_Reserach_PublicationRepository.Insert(entity);

            return result;
        }
        public static ResultEntity<Plugin_Reserach_PublicationEntity> InsertRecordNotAsync(Plugin_Reserach_PublicationEntity entity)
        {
            ResultEntity<Plugin_Reserach_PublicationEntity> result = new ResultEntity<Plugin_Reserach_PublicationEntity>();

            result = Plugin_Reserach_PublicationRepository.InsertNotAsync(entity);

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_PublicationEntity>> GetByID(long ID)
        {
            ResultEntity<Plugin_Reserach_PublicationEntity> result = new ResultEntity<Plugin_Reserach_PublicationEntity>();

            result = await Plugin_Reserach_PublicationRepository.SelectByID(ID);

            return result;
        }
        public static ResultEntity<Plugin_Reserach_PublicationEntity> GetByIDNotAsync(long ID)
        {
            ResultEntity<Plugin_Reserach_PublicationEntity> result = new ResultEntity<Plugin_Reserach_PublicationEntity>();

            result = Plugin_Reserach_PublicationRepository.SelectByIDNotAsync(ID);

            return result;
        }

        public static ResultEntity<Plugin_Reserach_PublicationEntity> UpdateRecordNotAsync(Plugin_Reserach_PublicationEntity entity)
        {
            ResultEntity<Plugin_Reserach_PublicationEntity> result = new ResultEntity<Plugin_Reserach_PublicationEntity>();

            result = Plugin_Reserach_PublicationRepository.UpdateNotAsync(entity);

            return result;
        }
        public async static Task<ResultEntity<Plugin_Reserach_PublicationEntity>> UpdateRecord(Plugin_Reserach_PublicationEntity entity)
        {
            ResultEntity<Plugin_Reserach_PublicationEntity> result = new ResultEntity<Plugin_Reserach_PublicationEntity>();

            result = await Plugin_Reserach_PublicationRepository.Update(entity);

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_PublicationEntity>> DeleteRecord(long ID)
        {
            ResultEntity<Plugin_Reserach_PublicationEntity> result = new ResultEntity<Plugin_Reserach_PublicationEntity>();

            result = await Plugin_Reserach_PublicationRepository.Delete(ID);

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_PublicationEntity>> UpdateNewsViewCount(long NewsID)
        {
            ResultEntity<Plugin_Reserach_PublicationEntity> result = new ResultEntity<Plugin_Reserach_PublicationEntity>();

            result = await Plugin_Reserach_PublicationRepository.UpdateViewCount(NewsID);

            return result;
        }
    }
}
