﻿using SiteWare.DataAccess.Repositories;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.Domain.Domains
{
    public class Plugin_Reserach_AuthorImageDomain
    {
        public async static Task<ResultEntity<Plugin_Reserach_AuthorImageEntity>> InsertRecord(Plugin_Reserach_AuthorImageEntity entity)
        {
            ResultEntity<Plugin_Reserach_AuthorImageEntity> result = new ResultEntity<Plugin_Reserach_AuthorImageEntity>();

            result = await Plugin_Reserach_AuthorImageRepository.Insert(entity);

            return result;
        }

        public static ResultList<Plugin_Reserach_AuthorImageEntity> GetAllPillarsImagesByPillarsIDWidthHeight(int ID, int Width, int Height)
        {
            ResultList<Plugin_Reserach_AuthorImageEntity> result = new ResultList<Plugin_Reserach_AuthorImageEntity>();

            result = Plugin_Reserach_AuthorImageRepository.SelectAllByPillarsIDWidthHeight(ID, Width, Height);

            return result;
        }
    }
}
