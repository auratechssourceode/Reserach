﻿using SiteWare.DataAccess.Repositories;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.Domain.Domains
{
    public class Plugin_Reserach_AuthorDomain
    {
        public async static Task<ResultList<Plugin_Reserach_AuthorEntity>> GetAll()
        {
            ResultList<Plugin_Reserach_AuthorEntity> result = new ResultList<Plugin_Reserach_AuthorEntity>();

            result = await Plugin_Reserach_AuthorRepository.SelectAll();

            return result;
        }
        public static ResultList<Plugin_Reserach_AuthorEntity> GetAllNotAsync()
        {
            ResultList<Plugin_Reserach_AuthorEntity> result = new ResultList<Plugin_Reserach_AuthorEntity>();

            result = Plugin_Reserach_AuthorRepository.SelectAllNotAsync();

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_AuthorEntity>> InsertRecord(Plugin_Reserach_AuthorEntity entity)
        {
            ResultEntity<Plugin_Reserach_AuthorEntity> result = new ResultEntity<Plugin_Reserach_AuthorEntity>();

            result = await Plugin_Reserach_AuthorRepository.Insert(entity);

            return result;
        }
        public static ResultEntity<Plugin_Reserach_AuthorEntity> InsertRecordNotAsync(Plugin_Reserach_AuthorEntity entity)
        {
            ResultEntity<Plugin_Reserach_AuthorEntity> result = new ResultEntity<Plugin_Reserach_AuthorEntity>();

            result = Plugin_Reserach_AuthorRepository.InsertNotAsync(entity);

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_AuthorEntity>> GetByID(long ID)
        {
            ResultEntity<Plugin_Reserach_AuthorEntity> result = new ResultEntity<Plugin_Reserach_AuthorEntity>();

            result = await Plugin_Reserach_AuthorRepository.SelectByID(ID);

            return result;
        }
        public static ResultEntity<Plugin_Reserach_AuthorEntity> GetByIDNotAsync(long ID)
        {
            ResultEntity<Plugin_Reserach_AuthorEntity> result = new ResultEntity<Plugin_Reserach_AuthorEntity>();

            result = Plugin_Reserach_AuthorRepository.SelectByIDNotAsync(ID);

            return result;
        }

        public static ResultEntity<Plugin_Reserach_AuthorEntity> UpdateRecordNotAsync(Plugin_Reserach_AuthorEntity entity)
        {
            ResultEntity<Plugin_Reserach_AuthorEntity> result = new ResultEntity<Plugin_Reserach_AuthorEntity>();

            result = Plugin_Reserach_AuthorRepository.UpdateNotAsync(entity);

            return result;
        }
        public async static Task<ResultEntity<Plugin_Reserach_AuthorEntity>> UpdateRecord(Plugin_Reserach_AuthorEntity entity)
        {
            ResultEntity<Plugin_Reserach_AuthorEntity> result = new ResultEntity<Plugin_Reserach_AuthorEntity>();

            result = await Plugin_Reserach_AuthorRepository.Update(entity);

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_AuthorEntity>> DeleteRecord(long ID)
        {
            ResultEntity<Plugin_Reserach_AuthorEntity> result = new ResultEntity<Plugin_Reserach_AuthorEntity>();

            result = await Plugin_Reserach_AuthorRepository.Delete(ID);

            return result;
        }
    }
}
