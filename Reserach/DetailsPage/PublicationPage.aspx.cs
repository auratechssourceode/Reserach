﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using SiteWare.Entity.Entities;
using SiteWare.Entity.Common.Enums;
using SiteWare.Domain.Domains;
using SiteWare.Entity.Common.Entities;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using System.Configuration;
using System.Globalization;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class DetailsPage_PublicationPage : SiteBasePage
{
    public DateTime currentDate = DateTime.Now;
    public int LangID = 1;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Convert.ToInt32(Session["CurrentLanguage"]) == (int)EnumLanguage.English)
            {
                LangID = 1;
            }
            else
            {
                LangID = 2;
            }
            if (!IsPostBack)
            {
                FillData();
                FillMedia();
                FillResearchandstudies();
            }
        }
        catch (Exception ex)
        {
            string lang = string.Empty;
            if (Convert.ToInt32(Session["CurrentLanguage"]) == Convert.ToInt32(EnumLanguage.Arabic))
            {
                lang = "/ar";
            }
            else
            {
                lang = "/en";
            }
            Response.Redirect(lang + "/home", false);
        }
    }

    protected async void FillData()
    {
        try
        {
            // int NewsID = 1;
            long PublicationID = Convert.ToInt64(Page.RouteData.Values["ID"].ToString());
            ResultEntity<PagesEntity> PageResult = new ResultEntity<PagesEntity>();
            //ResultEntity<NewsEntity> Result = new ResultEntity<NewsEntity>();
            ResultEntity<Plugin_Reserach_PublicationEntity> Result = new ResultEntity<Plugin_Reserach_PublicationEntity>();
            byte LangID = Convert.ToByte(Session["CurrentLanguage"]);
            int PageID = 0;
            string PageName = string.Empty;

            string Alias = string.Empty;
            if (LangID == Convert.ToInt32(EnumLanguage.Arabic))
            {
                PageID = 3;
                //PageID = 3;
            }
            else
            {
                PageID = 3;
            }

            PageResult = PagesDomain.GetPagesByPageIDNotAsync(PageID);
            PageName = Convert.ToString(PageResult.Entity.Name);

            if (PageResult.Status == ErrorEnums.Success)
            {
                if (PageResult.Entity.LanguageID == Convert.ToInt32(EnumLanguage.English))
                {
                    lnkParentName.NavigateUrl = PageResult.Entity.AliasPath;
                    lnkParentName.Text = PageResult.Entity.Name;
                }
                else
                {
                    lnkParentName.NavigateUrl = PageResult.Entity.AliasPath;
                    lnkParentName.Text = PageResult.Entity.Name;
                }
            }
            else
            {
                lstParent.Visible = false;
            }

            Result = await Plugin_Reserach_PublicationDomain.GetByID(PublicationID);
            //Result = await NewsDomain.GetPagesByNewsID(NewsID);
            if (Result.Status == ErrorEnums.Success)
            {

                if (string.IsNullOrEmpty(PageName))
                {
                    if (LangID == (byte)EnumLanguage.English)
                    {
                        PageName = "Home";
                    }
                    else
                    {
                        PageName = "الصفحة الرئيسية";
                    }
                }


                //===================================

                string WebsiteName = "";
                ResultList<SettingEntity> WebResult = new ResultList<SettingEntity>();
                WebResult = SettingDomain.GetSettingAllWithoutAsync();
                if (WebResult.Status == ErrorEnums.Success)
                {
                    //Page.Title = WebResult.List[0].PageName;
                    WebsiteName = WebResult.List[0].PageName;

                }
                //===================================

                Page.MetaDescription = Regex.Replace(Result.Entity.PublicationsDetails.ToString(), @"<[^>]+>|&nbsp;", "").Trim();
                //Page.Title = Result.Entity.PublicationsTitle.ToString() + " - " + WebsiteName;
                //lblDetails.Text = FetchLinksFromSource(Result.Entity.PublicationsDetails.ToString());



                //Page.MetaDescription = Regex.Replace(Result.Entity.ArticleDetails.ToString(), @"<[^>]+>|&nbsp;", "").Trim();
                Page.Title = Result.Entity.PublicationsTitle.ToString() + " - " + WebsiteName;
                lblTitle.Text = Result.Entity.PublicationsTitle.ToString();
                //lblAuthor.Text = Result.Entity.PublicationsAuthor.ToString();
                //lblKeyword.Text = Result.Entity..ToString();
                lblDate.Text = Result.Entity.PublicationsDate.ToString("dd/MM/yyyy");
                lblViewCount.Text = Result.Entity.ViewCount.ToString();
                lblDetails.Text = FetchLinksFromSource(Result.Entity.PublicationsDetails.ToString());

                //lblNewsTitle.Text = Result.Entity.Headline;

                //var date = Convert.ToDateTime(Result.Entity.NewsDate);
                //if (LangID == Convert.ToInt32(EnumLanguage.Arabic))
                //{
                //    //Get month and convert to arabic
                //    var m = date.ToString("MMMM");
                //    var month = date.ToString("MMMM", new CultureInfo("ar-AE"));

                //    var Date = date.ToString("dd", new CultureInfo("ar-AE"));
                //    var year = date.ToString("yyyy", new CultureInfo("ar-AE"));
                //    lblNewsDate.Text = month + " " + Date + " " + year;
                //}
                //else
                //{
                //    //Convert NewsDate in given dateformat
                //    var m = date.ToString("MMMM");
                //    var month = date.ToString("MMMM", new CultureInfo("en-US"));

                //    var Date = date.ToString("dd", new CultureInfo("en-US"));
                //    var year = date.ToString("yyyy", new CultureInfo("en-US"));
                //    lblNewsDate.Text = month + " " + Date + " " + year;
                //}

                //lblNewsDate.Text = Result.Entity.NewsDate.ToString();
                //lblViewCount.Text = Result.Entity.ViewCount.ToString();
                //newsImage.ImageUrl = ConfigurationManager.AppSettings["ImagePath"].ToString() + Result.Entity.NewsImage;


                lblChildName.Text = Result.Entity.PublicationsTitle;
                //lblPageTitle.Text = PageName;



                //NewsEntity entity = new NewsEntity();
                //entity.NewsID = NewsID;
                //entity.ViewCount = Result.Entity.ViewCount;
                //var UpdateCount = await NewsDomain.UpdateNewsViewCount(entity.NewsID);


                Plugin_Reserach_PublicationEntity entity = new Plugin_Reserach_PublicationEntity();
                entity.PublicationsID = PublicationID;
                entity.ViewCount = Result.Entity.ViewCount;
                var UpdateCount = await Plugin_Reserach_PublicationDomain.UpdateNewsViewCount(entity.PublicationsID);
            }
            else
            {
                string lang = string.Empty;
                if (LangID == (byte)EnumLanguage.English)
                {
                    lang = "/en/";
                }
                else
                {
                    lang = "/ar/";
                }
                Response.Redirect(lang + "home", false);
            }
        }
        catch (Exception e)
        {
            throw;
        }
    }
    public string FetchLinksFromSource(string htmlSource)
    {
        HtmlDocument doc = new HtmlDocument();
        doc.LoadHtml(htmlSource);
        if (doc.DocumentNode.SelectNodes("//img") != null)
        {
            foreach (var img in doc.DocumentNode.SelectNodes("//img"))
            {
                string orig = img.Attributes["src"].Value;
                string newsrc = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + orig;
                img.SetAttributeValue("src", newsrc);
            }
        }
        try
        {
            if (doc.DocumentNode.SelectNodes("//table") != null)
            {
                foreach (var tab in doc.DocumentNode.SelectNodes("//table"))
                {
                    tab.SetAttributeValue("class", "Tblres");
                }
            }
        }
        catch
        {
        }
        return doc.DocumentNode.OuterHtml;
    }

    #region --> Get Media
    protected async void FillMedia()
    {
        try
        {
            Random rand = new Random();
            ResultList<Plugin_Reserach_MediaEntity> Result = new ResultList<Plugin_Reserach_MediaEntity>();
            Result = await Plugin_Reserach_MediaDomain.GetAll();
            if (Result.Status == ErrorEnums.Success)
            {
                lstMedia.DataSource = Result.List.Where(s => s.IsDelete == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishedDate <= currentDate).OrderBy(s => rand.Next()).Take(1).ToList();
                lstMedia.DataBind();
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void lstMedia_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {

            Image imgMedia = (Image)e.Item.FindControl("imgMedia");

            if (!string.IsNullOrEmpty(imgMedia.ImageUrl))
            {
                imgMedia.ImageUrl = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + imgMedia.ImageUrl.Replace("~/", "~/Siteware/");
            }
        }
    }

    #endregion

    #region --> Get Research and studies
    protected void FillResearchandstudies()

    {
        try
        {
            ResultList<Plugin_Reserach_ResearchInnerEntity> Result = new ResultList<Plugin_Reserach_ResearchInnerEntity>();
            Result = Plugin_Reserach_ResearchInnerDomain.GetAllNotAsync();
            if (Result.Status == ErrorEnums.Success)
            {
                lstResearchAndPublication.DataSource = Result.List.Where(s => s.IsDelete == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishedDate <= currentDate).OrderBy(s => s.Order).Take(1).ToList();
                lstResearchAndPublication.DataBind();


            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void lstResearchAndPublication_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            Image imgPublication = (Image)e.Item.FindControl("imgPublication");
            HiddenField hdnCategory = (HiddenField)e.Item.FindControl("hdnCategory");
            HiddenField hdnID = (HiddenField)e.Item.FindControl("hdnID");
            Literal lblCategory = (Literal)e.Item.FindControl("lblCategory");
            Literal lblTitle = (Literal)e.Item.FindControl("lblTitle");

            if (!string.IsNullOrEmpty(imgPublication.ImageUrl))
            {
                imgPublication.ImageUrl = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + imgPublication.ImageUrl.Replace("~/", "~/Siteware/");
            }

            int Cateid = Convert.ToInt32(hdnCategory.Value);

            var getcatedata = Lookup_Reserach_PublicationCategoryDomain.GetByIDNotAsync(Cateid);
            if (getcatedata.Status == ErrorEnums.Success)
            {
                lblCategory.Text = getcatedata.Entity.name;
            }

        }
    }

    #endregion

    protected async void LinkButtonID_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)(sender);
        string yourValue = btn.CommandArgument;

        long MediaId = Convert.ToInt64(btn.CommandArgument);

        Plugin_Reserach_MediaEntity entity = new Plugin_Reserach_MediaEntity();
        entity.MediaID = MediaId;
        //entity.ViewCount = Result.Entity.ViewCount;
        var UpdateCount = await Plugin_Reserach_MediaDomain.UpdateNewsViewCount(entity.MediaID);

        FillMedia();
    }
}