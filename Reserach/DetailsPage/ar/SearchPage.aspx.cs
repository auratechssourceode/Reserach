﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SiteWare.Entity.Entities;
using SiteWare.Entity.Common.Enums;
using SiteWare.Domain.Domains;
using SiteWare.Entity.Common.Entities;
using System.Text.RegularExpressions;
using System.Configuration;

public partial class Pages_SearchPage : SiteBasePage
{
    public DateTime currentDate = DateTime.Now;
    public int LangID = 2;
    public int Count = 0;
    public int Counter = 0;
   
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                FillData();
                FillBage();


               // lblPageName.Text = "Search";
                lblChildName.Text = "البحث";

            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }


    protected void FillData()
    {
        try
        {
            //string LanguageId = "1";
            Session["Category"] = "Search";
            //if (LanguageId == Convert.ToInt32(EnumLanguage.Arabic).ToString())
            //{
            //    Page.Title = "البحث في الموقع";
            //    Page.MetaDescription = "البحث في الموقع ";
            //    Page.Title = Page.Title = "البحث في الموقع ";
            //}
            //else
            //{
                Page.Title = "نتائج البحث";
                Page.MetaDescription = "نتائج البحث";
            //}

           // imgInnerBanner.ImageUrl = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + "/Siteware/Siteware_File/image/Arabella/about-banner.jpg";
           // imgSearchLogo.ImageUrl = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + "/Siteware/Siteware_File/image/Arabella/ser-1.jpg";

        }
        catch (Exception)
        {

            throw;
        }

    }
    protected async void FillBage()
    {

        try
        {
            DataPager pager;
            string keyword = Request.QueryString["keyword"].ToString();
            if (!string.IsNullOrEmpty(keyword))
            {

                byte LanguageId = Convert.ToByte(Session["CurrentLanguage"]);

                ResultList<PagesEntity> Result = new ResultList<PagesEntity>();
                //ResultList<Plugin_RadiologyEntity> radiologyResult = new ResultList<Plugin_RadiologyEntity>();
                //ResultList<PluginServiceEntity> serviceResult = new ResultList<PluginServiceEntity>();
                //ResultList<Plugin_DoctorEntity> doctorResult = new ResultList<Plugin_DoctorEntity>();
                ResultList<NewsEntity> newsResult = new ResultList<NewsEntity>();
                ResultList<Plugin_Reserach_ArticleEntity> ArticleResult = new ResultList<Plugin_Reserach_ArticleEntity>();
                ResultList<Plugin_Reserach_PublicationEntity> PublicationResult = new ResultList<Plugin_Reserach_PublicationEntity>();

                Result = await PagesDomain.GetPagesByKyeword(keyword, LanguageId);

                //radiologyResult = Plugin_RadiologyDomain.GetAllNotAsync();
                //serviceResult = PluginServiceDomain.GetDataPointAllNotAsync();
                //doctorResult = Plugin_DoctorDomain.GetAllNotAsync();
                newsResult = NewsDomain.GetNewsAllNotAsync();
                ArticleResult = Plugin_Reserach_ArticleDomain.GetAllNotAsync();
                PublicationResult = Plugin_Reserach_PublicationDomain.GetAllNotAsync();

                string lang = string.Empty;
                if (Convert.ToInt32(Session["CurrentLanguage"]) == Convert.ToInt32(EnumLanguage.Arabic))
                {
                    lang = "/ar";
                }
                else
                {
                    lang = "/en";
                }



                //if (Result.Status == ErrorEnums.Success || newsResult.Status == ErrorEnums.Success || serviceResult.Status == ErrorEnums.Success || announceResult.Status == ErrorEnums.Success)
                if (Result.Status == ErrorEnums.Success || newsResult.Status == ErrorEnums.Success)

                {
                    if (newsResult.Status == ErrorEnums.Success)
                    {
                        //Response.Write("<script>alert('12')</script>");
                        newsResult.List = newsResult.List.Where(s => s.Headline.Contains(keyword) || s.Summary.Contains(keyword) || s.Description.Contains(keyword) && (s.IsPublished && s.LanguageID == Convert.ToByte(Session["CurrentLanguage"]) && !s.IsDeleted && s.PublishDate.Date <= currentDate.Date)).ToList();
                        if (newsResult.List.Count > 0)
                        {
                            //Response.Write("<script>alert('13')</script>");
                            foreach (NewsEntity entity in newsResult.List)
                            {
                                PagesEntity result1 = new PagesEntity();
                                string title = Regex.Replace(entity.Headline, @"[\\:/*#%]+", " ");
                                long ID = entity.NewsID;
                                string newsLink = lang + "/NewsPage/" + title.Trim() + "/" + ID.ToString();
                                result1.Name = entity.Headline;
                                result1.ContentHTML = entity.Summary;
                                result1.LivePath = newsLink;
                                result1.IsPublished = entity.IsPublished;
                                Result.List.Add(result1);
                            }
                        }
                    }

                    if (ArticleResult.Status == ErrorEnums.Success)
                    {
                        //Response.Write("<script>alert('12')</script>");
                        ArticleResult.List = ArticleResult.List.Where(s => s.ArticleTitle.Contains(keyword) || s.ArticleSummery.Contains(keyword) || s.ArticleDetails.Contains(keyword) && (s.IsPublished && s.LanguageID == Convert.ToByte(Session["CurrentLanguage"]) && !s.IsDelete && s.PublishedDate.Date <= currentDate.Date)).ToList();
                        if (ArticleResult.List.Count > 0)
                        {
                            //Response.Write("<script>alert('13')</script>");
                            foreach (Plugin_Reserach_ArticleEntity entity in ArticleResult.List)
                            {
                                PagesEntity result1 = new PagesEntity();
                                string title = Regex.Replace(entity.ArticleTitle, @"[\\:/*#%]+", " ");
                                long ID = entity.ArticleID;
                                string newsLink = lang + "/ArticlePage/" + title.Trim() + "/" + ID.ToString();
                                result1.Name = entity.ArticleTitle;
                                result1.ContentHTML = entity.ArticleSummery;
                                result1.LivePath = newsLink;
                                result1.IsPublished = entity.IsPublished;
                                Result.List.Add(result1);
                            }
                        }
                    }

                    if (PublicationResult.Status == ErrorEnums.Success)
                    {
                        //Response.Write("<script>alert('12')</script>");
                        PublicationResult.List = PublicationResult.List.Where(s => s.PublicationsTitle.Contains(keyword) || s.PublicationsSummery.Contains(keyword) || s.PublicationsDetails.Contains(keyword) && (s.IsPublished && s.LanguageID == Convert.ToByte(Session["CurrentLanguage"]) && !s.IsDelete && s.PublishedDate.Date <= currentDate.Date)).ToList();
                        if (PublicationResult.List.Count > 0)
                        {
                            //Response.Write("<script>alert('13')</script>");
                            foreach (Plugin_Reserach_PublicationEntity entity in PublicationResult.List)
                            {
                                PagesEntity result1 = new PagesEntity();
                                string title = Regex.Replace(entity.PublicationsTitle, @"[\\:/*#%]+", " ");
                                long ID = entity.PublicationsID;
                                string newsLink = lang + "/PublicationPage/" + title.Trim() + "/" + ID.ToString();
                                result1.Name = entity.PublicationsTitle;
                                result1.ContentHTML = entity.PublicationsSummery;
                                result1.LivePath = newsLink;
                                result1.IsPublished = entity.IsPublished;
                                Result.List.Add(result1);
                            }
                        }
                    }

                    //if (radiologyResult.Status == ErrorEnums.Success)
                    //{
                    //    radiologyResult.List = radiologyResult.List.Where(s => s.Title.Trim().ToLower().Contains(keyword.ToLower()) || s.Summary.ToLower().Contains(keyword.ToLower()) && (s.IsPublished && s.LanguageID == Convert.ToByte(Session["CurrentLanguage"]) && !s.IsDeleted && s.PublishedDate <= currentDate)).ToList();
                    //    if (radiologyResult.List.Count > 0)
                    //    {
                    //        foreach (Plugin_RadiologyEntity entity in radiologyResult.List)
                    //        {
                    //            PagesEntity result1 = new PagesEntity();
                    //            string title = Regex.Replace(entity.Title, @"[\\:/*#%]+", " ");
                    //            long ID = entity.RadiologyID;
                    //            string radiologyLink = lang + "/RadiologyPage/" + title.Trim() + "/" + ID.ToString();
                    //            result1.Name = entity.Title;
                    //            result1.ContentHTML = entity.Summary;
                    //            result1.LivePath = radiologyLink;
                    //            result1.IsPublished = entity.IsPublished;
                    //            Result.List.Add(result1);
                    //        }
                    //    }

                    //    serviceResult.List = serviceResult.List.Where(x => x.ServiceName.Trim().ToLower().Contains(keyword.ToLower()) || x.Title.ToLower().Contains(keyword.ToLower()) && (x.IsPublished && x.LanguageID == Convert.ToByte(Session["CurrentLanguage"]) && !x.IsDeleted && x.PublishDate <= currentDate)).ToList();
                    //    foreach (PluginServiceEntity entity in serviceResult.List)
                    //    {
                    //        PagesEntity result1 = new PagesEntity();
                    //        string title = Regex.Replace(entity.Title, @"[\\:/*#%]+", " ");
                    //        long ID = entity.ID;
                    //        string serviceLink = lang + "/DepartmentPage/" + title.Trim() + "/" + ID.ToString();
                    //        result1.Name = entity.Title;
                    //        result1.ContentHTML = entity.ServiceName;
                    //        result1.LivePath = serviceLink;// string.Empty;
                    //        result1.IsPublished = entity.IsPublished;
                    //        Result.List.Add(result1);
                    //    }

                    //    doctorResult.List = doctorResult.List.Where(x => x.Title1.Trim().ToLower().Contains(keyword.ToLower()) || x.DrName.ToLower().Contains(keyword.ToLower()) && (x.IsPublished && x.LanguageID == Convert.ToByte(Session["CurrentLanguage"]) && !x.IsDeleted && x.PublishedDate <= currentDate)).ToList();
                    //    foreach (Plugin_DoctorEntity entity in doctorResult.List)
                    //    {
                    //        PagesEntity result1 = new PagesEntity();
                    //        string title = Regex.Replace(entity.DrName, @"[\\:/*#%]+", " ");
                    //        long ID = entity.DoctorID;
                    //        string ancLink = lang + "/DoctorDetails/" + title.Trim() + "/" + ID.ToString();
                    //        result1.Name = entity.DrName;
                    //        result1.ContentHTML = entity.Title1;
                    //        result1.LivePath = ancLink;//entity.Link;
                    //        result1.IsPublished = entity.IsPublished;
                    //        Result.List.Add(result1);
                    //    }

                    //}


                    Counter = Result.List.Where(s => s.IsPublished == true && s.IsDeleted == false).Count();
                    ArKeyword.Text = " كلمة البحث : " + keyword;
                    lstArSearchData.DataSource = Result.List.Where(a => a.IsDeleted == false).ToList();
                    lstArSearchData.DataBind();

                    lblArSearchCount.Text = Counter.ToString();


                    if (Convert.ToInt32(Session["CurrentLanguage"]) == Convert.ToInt32(EnumLanguage.Arabic))
                    {
                        pager = lstArSearchData.FindControl("DataPager1") as DataPager;
                    }
                    else
                    {
                        pager = lstArSearchData.FindControl("DataPager1") as DataPager;
                    }

                    pager.PageSize = 10; // Convert.ToInt32(ConfigurationManager.AppSettings["SearchPageSize"]);

                }


                else
                {
                    ArKeyword.Text = keyword + " : كلمة البحث ";
                    if (Result.List.Count == 0)
                    {
                        Counter = Result.List.Where(s => s.IsPublished == true && s.IsDeleted == false).Count();
                        lstArSearchData.DataSource = Result.List.Where(a => a.IsDeleted == false).ToList();
                        lstArSearchData.DataBind();
                        lblArSearchCount.Text = "لم يتم العثور على نتائج";
                    }
                }

               

            }
        }
        catch (Exception e)
        {

        }



        //try
        //{
        //    DataPager pager;
        //    string keyword = Request.QueryString["keyword"].ToString();
        //    if (!string.IsNullOrEmpty(keyword))
        //    {

        //        byte LanguageId = 1;

        //        ResultList<PagesEntity> Result = new ResultList<PagesEntity>();
        //        Result = await PagesDomain.GetPagesByKyeword(keyword, LanguageId);
        //        if (Result.Status == ErrorEnums.Success)
        //        {

        //            EnKeyword.Text = "Word Search : " + keyword;
        //            lstEnSearchData.DataSource = Result.List.Where(a => a.IsDeleted == false).ToList();
        //            lstEnSearchData.DataBind();

        //            if (lstEnSearchData.Items.Count > 0)
        //            {
        //                Counter = Counter + lstEnSearchData.Items.Count;
        //            }
        //            lblEnSearchCount.Text = Counter.ToString();

        //        }
        //        else
        //        {

        //            EnKeyword.Text = "Word Search : " + keyword;
        //            if (Result.List.Count == 0)
        //            {
        //                lstEnSearchData.DataSource = Result.List.Where(a => a.IsDeleted == false).ToList();
        //                lstEnSearchData.DataBind();
        //                lblEnSearchCount.Text = "No result found";
        //            }

        //        }

        //        pager = lstEnSearchData.FindControl("DataPager1") as DataPager;

        //        //pager.PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["SearchPageSize"]);

        //        int  CountPagezise = Convert.ToInt32(ConfigurationManager.AppSettings["SearchPageSizeNew"]);

        //        //pager.PageSize = CountPagezise;// Convert.ToInt32(ConfigurationManager.AppSettings["SearchPageSize"]);

        //    }
        //}
        //catch (Exception e)
        //{

        //}

    }
    protected void lstData_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            Label lblContent = (Label)e.Item.FindControl("lblContent");
            lblContent.Text = Regex.Replace(lblContent.Text, "<[^>]*>", string.Empty);
            if (lblContent.Text.Length > 440)
            {
                lblContent.Text = lblContent.Text.Substring(0, 440) + "..";
            }
        }

    }
    //protected void OnPagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
    //{
    //    DataPager pager1;

    //    pager1 = lstArSearchData.FindControl("DataPager1") as DataPager;

    //    (pager1).SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
    //    this.FillBage();
    //    //-------For call hightlight jquery function--------//
    //    string keyword = Request.QueryString["keyword"].ToString();
    //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "highlightWord('.example','" + keyword + "', 'highlight');", true);
    //}


    protected void OnPagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
    {
        DataPager pager1;

        pager1 = lstArSearchData.FindControl("DataPager1") as DataPager;

        (pager1).SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
        this.FillBage();
        //-------For call hightlight jquery function--------//
        string keyword = Request.QueryString["keyword"].ToString();
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "highlightWord('.example','" + keyword + "', 'highlight');", true);
    }


    protected void lstData_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
    {
        try
        {
            DataPager pager1;
            pager1 = lstArSearchData.FindControl("DataPager1") as DataPager;
            (pager1).SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            this.FillBage();
            Update.Focus();
        }
        catch (Exception ex)
        {

        }
    }
}