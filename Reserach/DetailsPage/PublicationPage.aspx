﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PublicationPage.aspx.cs" Inherits="DetailsPage_PublicationPage" Async="true" %>

<%@ Import Namespace="SiteWare.Entity.Common.Enums" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="bread_crumb">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb">

                        <li><a href="<%= (Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "/ar" : "/en") %>/Home">
                            <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "الرئيسية" : "Home") %>
                        </a></li>
                        <li runat="server" id="lstParent" visible="false" class="breadcrumb-item">
                            <asp:HyperLink ID="lnkParentName" runat="server" NavigateUrl='<%# Bind("AliasPath") %>' Target='<%# Bind("Target") %>' Text='<%# Bind("MenuName") %>'></asp:HyperLink>
                        </li>
                        <li class="active">
                            <asp:Literal ID="lblChildName" runat="server" Text='<%# Bind("MenuName") %>'></asp:Literal>
                        </li>

                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="comnpadding midal_part">
        <div class="container">
            <div class="row">

                <div class="col-lg-9 col-md-8 col-sm-8">

                    <div id="printpage" class="pas">
                        <div class="row">
                            <div class="col-lg-9 col-md-12 col-sm-12">
                                <div class="defense_policy">
                                    <h1 class="comntitle">
                                        <asp:Literal runat="server" ID="lblTitle"></asp:Literal></h1>
                                </div>
                            </div>
                            <%-- <div class="col-lg-3 col-md-12 col-sm-12">
                                <div class="downloadlink">
                                    <aside>
                                        <a href="javascript:void">قراءة / تحميل</a>
                                        <span>قراءة نص الورقة كاملا</span>
                                    </aside>
                                    <img src="<%=ResolveUrl("~/") %>App_Themes/ThemeAr/img/pdf.jpg" alt="">
                                </div>
                            </div>--%>
                        </div>
                        <div class="view_other">
                            <div class="view_cal">
                                <span>
                                    <asp:Literal runat="server" ID="lblDate"></asp:Literal>
                                    <i class="fa fa-calendar"></i></span>
                                <aside>
                                    <i class="fa fa-eye"></i>
                                    <span>
                                        <asp:Literal runat="server" ID="lblViewCount"></asp:Literal></span>
                                    <small>مشاهدة</small>
                                </aside>
                            </div>
                            <div class="print_social">
                                <%--<a href="#" id="PrintThis">
                                    <i class="fa fa-print"></i>
                                    طباعة المقال
                                </a>--%>
                                <aside>
                                    <span class="Share_Now">
                                        <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "مشاركة المقال" : "Share article") %>
                                    </span>
                                    <div class="addthis_inline_share_toolbox"></div>
                                    <div class="addthis_inline_follow_toolbox"></div>
                                </aside>
                            </div>
                        </div>

                        <div class="Introduction">
                            <asp:Literal runat="server" ID="lblDetails"></asp:Literal>
                        </div>
                        <div class="downloadlink downloadlink2" style="display: none">
                            <img src="<%=ResolveUrl("~/") %>App_Themes/ThemeAr/img/pdf.jpg" alt="">
                            <aside>
                                <a href="javascript:void">قراءة / تحميل</a>
                                <span>قراءة نص الورقة كاملا</span>
                            </aside>
                        </div>
                        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5fbf86e5431dd6f9"></script>

                    </div>




                </div>

                <div class="col-lg-3 col-md-4 col-sm-4">
                    <div class="sidebar">
                       <%-- <div class="title">--%>
                        <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "<div class='title'>" : "<div class='title' style='display:none'>") %>
                            <h1>
                                <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "دراسات ذات صلة" : "Related studies") %>
                            </h1>
                            <%--<a class="Seeall" href="<%= (Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "/ar/Home/أبحاث-ودراسات" : "/en/Home/Studies") %>/home">
                            <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "شاهد الجميع" : "See All") %>
                            <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "<i class='fa fa-angle-left'></i>" : "<i class='fa fa-angle-rigth'></i>") %>
                        </a>--%>
                        <a href="/ar/Home/أبحاث-ودراسات"  class="Seeall">
                            <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "شاهد الجميع" : "See All") %>
                            <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "<i class='fa fa-angle-left'></i>" : "<i class='fa fa-angle-rigth'></i>") %>
                        </a>

                            <%--<a href="javascript:void" class="Seeall">شاهد الجميع 
              <i class="fa fa-angle-left"></i>
                            </a>--%>
                        </div>


                        <%--<asp:ListView runat="server" ID="lstResearchAndPublication" OnItemDataBound="lstResearchAndPublication_ItemDataBound">
                            <ItemTemplate>
                                <div class="research_box">
                                    <div class="research_img">
                                        <asp:Image ID="imgPublication" runat="server" ImageUrl='<%# Bind("ResearchStudiesImage") %>' />
                                        <asp:HiddenField runat="server" ID="hdnID" Value='<%# Bind("ResearchStudiesID") %>' />
                                        <asp:HiddenField runat="server" ID="hdnCategory" Value='<%# Bind("ResearchStudiesCatogory") %>' />
                                        <span class="bg1">
                                            <asp:Literal runat="server" ID="lblCategory"></asp:Literal>
                                        </span>
                                    </div>
                                    <div class="research_text">
                                        <small class="LTR">
                                            <asp:Literal runat="server" ID="Literal1" Text='<%# Bind("ResearchStudiesDate","{0:dd/MM/yyyy}") %>'></asp:Literal></small>                                       
                                        <h3>
                                            <asp:HyperLink ID="lnkMore" runat="server" NavigateUrl='<%# Bind("MedialLink") %>' Target='<%# Bind("Target") %>'>
                                                <asp:Literal runat="server" ID="lblTitle" Text='<%# Bind("ResearchStudiesTitle") %>'></asp:Literal>
                                            </asp:HyperLink>

                                        </h3>
                                        <p>
                                            <asp:Literal runat="server" ID="lblSummary" Text='<%# Bind("ResearchStudiesSummery") %>'></asp:Literal>
                                        </p>
                                    </div>
                                </div>                                
                            </ItemTemplate>
                        </asp:ListView>--%>

                        <asp:ListView runat="server" ID="lstResearchAndPublication" OnItemDataBound="lstResearchAndPublication_ItemDataBound">
                            <ItemTemplate>
                                <div class="research_box">
                                    <div class="research_img">
                                        <asp:Image ID="imgPublication" runat="server" ImageUrl='<%# Bind("ResearchInnerImage") %>' />
                                        <asp:HiddenField runat="server" ID="hdnID" Value='<%# Bind("ResearchInnerID") %>' />
                                        <asp:HiddenField runat="server" ID="hdnCategory" Value='<%# Bind("ResearchInnerCatogory") %>' />
                                        <span class="bg1">
                                            <asp:Literal runat="server" ID="lblCategory"></asp:Literal>
                                        </span>
                                    </div>
                                    <div class="research_text">
                                        <small class="LTR">
                                            <asp:Literal runat="server" ID="Literal1" Text='<%# Bind("ResearchInnerDate","{0:dd/MM/yyyy}") %>'></asp:Literal></small>                                       
                                        <h3>
                                            <asp:Literal runat="server" ID="Literal2" Text='<%# Bind("ResearchInnerTitle") %>'></asp:Literal>
                                            <asp:HyperLink ID="lnkMore" runat="server" NavigateUrl='<%# Bind("MedialLink") %>' Target='<%# Bind("Target") %>'>
                                                <asp:Literal runat="server" ID="lblTitle" Text='<%# Bind("ResearchInnerTitle2") %>'></asp:Literal>
                                            </asp:HyperLink>
                                        </h3>
                                        <p>
                                            <asp:Literal runat="server" ID="lblSummary" Text='<%# Bind("ResearchInnerSummery") %>'></asp:Literal>
                                        </p>
                                    </div>
                                </div>                                
                            </ItemTemplate>
                        </asp:ListView>




                       <%-- <div class="research_box">
                            <div class="research_img">
                                <img src="<%=ResolveUrl("~/") %>App_Themes/ThemeAr/img/Research-1.jpg" alt="">
                                <span class="bg1">تقييم حالة</span>
                            </div>
                            <div class="research_text">
                                <small class="LTR">2020/08/31</small>
                                <h3>مراجعة كتاب: النكبة ونشوء الشتات الفلسطيني في الكويت
                <a href="javascript:void">ستيرلينغ جينسين </a></h3>
                                <p>استجابات المؤسسة العسكرية العراقية لجائحة فيروس كورونا متنوعة: خلق مزيد من التوتر في العلاقات العسكرية المدنية الشيعية، وبناء الثقة سيادتها الحزبية التي يغذّيها الخارج العسكرية العراقية لجائحة فيروس كورونا متنوعة.</p>
                            </div>
                        </div>--%>


                        <div class="title">
                            <h1>
                                <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "المرئيات" : "Media") %>
                            </h1>
                           <%-- <a href="javascript:void" class="Seeall">شاهد الجميع 
              <i class="fa fa-angle-left"></i>
                            </a>--%>
                        </div>

                        <asp:ListView runat="server" ID="lstMedia" OnItemDataBound="lstMedia_ItemDataBound">
                            <ItemTemplate>
                                <div class="item">
                                    <div class="videobox">
                                        <asp:HyperLink ID="lnkSubMenu" runat="server" NavigateUrl='<%# Bind("MedialLink") %>' Target='<%# Bind("Target") %>'>
                                       <%-- <asp:LinkButton ID="LinkButtonID" runat="server" CommandArgument='<%# Eval("MediaID") %>' OnClick="LinkButtonID_Click" >--%>
                                            <asp:Image ID="imgMedia" runat="server" ImageUrl='<%# Bind("MediaImage") %>' />
                                            </asp:HyperLink>
                                          <%--  </asp:LinkButton>--%>
                                            <h3>
                                                <asp:Literal runat="server" ID="lblTitl" Text='<%# Bind("Title") %>'></asp:Literal>
                                            </h3>
                                            <strong>
                                                <asp:Literal runat="server" ID="lblDate" Text='<%# Bind("PublishedDate","{0:dd/MM/yyyy}") %>'></asp:Literal>
                                            </strong>
                                            <ul class="list-unstyled">
                                                <li>
                                                    
                                                    <span><asp:Literal runat="server" ID="lblConubt" Text=<%# Eval("ViewCount") %>></asp:Literal></span>
                                                    <%--<i class="fa fa-eye"></i>
                                                    <span>3,277</span>--%>
                                                  <%--  <i class="fa fa-eye"></i>
                                                     <small>
                                                           <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "مشاهدة" : "Watch") %>
                                                    </small>--%>
                                                </li>
                                                <li>
                                                   <%-- <img src="<%=ResolveUrl("~/") %>App_Themes/ThemeAr/img/share.png" alt="">--%>

                                                </li>
                                            </ul>
                                        
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:ListView>

                        <div class="tweet" style="display: none">
                            <h1 class="comntitle">آخر التغريدات</h1>
                            <span>تابعونا <i class="fa fa-twitter"></i></span>
                            <img src="<%=ResolveUrl("~/") %>App_Themes/ThemeAr/img/twitter.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

