﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SiteWare.Entity.Entities;
using SiteWare.Entity.Common.Enums;
using SiteWare.Domain.Domains;
using SiteWare.Entity.Common.Entities;
using System.Data;
using System.Reflection;
using System.Configuration;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using System.Web.Routing;
using Siteware.Entity.Entities;
//using Siteware.Domain.Domains;
using SiteWare.Domain.Domains;


public partial class MasterPage : System.Web.UI.MasterPage
{
    public DateTime currentDate = DateTime.Now;
    public int LangID = 0;
    string lang = string.Empty;
    private byte CurrLangID;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                if (Session["CurrentLanguage"] == null)
                {
                    Response.Redirect("en/Home");
                }
                CurrLangID = Convert.ToByte(Session["CurrentLanguage"]);
                if (Convert.ToInt32(Session["CurrentLanguage"]) == (int)EnumLanguage.English)
                {
                    LangID = 1;
                    lang = "/en";
                   // lblLangID.Value = "2";
                    // lnkLanguage.Text = "عربى";
                }
                else
                {
                    LangID = 2;
                    lang = "/ar";
                    //lblLangID.Value = "1";
                    // lnkLanguage.Text = "English";
                }

                ResultList<SettingEntity> Result = new ResultList<SettingEntity>();
                Result = SettingDomain.GetSettingAllWithoutAsync();
                if (Result.Status == ErrorEnums.Success)
                {
                    Result.List = Result.List.Where(s => s.LanguageID == LangID && s.IsDeleted == false && s.IsPublished).ToList();
                    if (Result.List.Count >= 1)
                    {
                        //Page.Title = Result.List[0].PageName;
                        imgLogo.ImageUrl = ConfigurationManager.AppSettings["ImagePath"].ToString() + Result.List[0].Logo;
                        Mobilelogo.ImageUrl = ConfigurationManager.AppSettings["ImagePath"].ToString() + Result.List[0].Logo;
                        //imgFooterLogo.ImageUrl = ConfigurationManager.AppSettings["ImagePath"].ToString() + Result.List[0].FooterLogo;
                        lblCopyright.Text = FetchLinksFromSource(Result.List[0].CopyRights);
                    }

                    //imgLogo.ImageUrl = ConfigurationManager.AppSettings["ImagePath"].ToString() + Result.List[0].Logo;
                    //Mobilelogo.ImageUrl = ConfigurationManager.AppSettings["ImagePath"].ToString() + Result.List[0].Logo;
                    ////imgFooterLogo.ImageUrl = ConfigurationManager.AppSettings["ImagePath"].ToString() + Result.List[0].FooterLogo;
                    //lblCopyright.Text = FetchLinksFromSource(Result.List[0].CopyRights);
                }
                FillSecondNavigation();
                FillSocialIcon();
                FillFooterNavigation();
                FillAboutUs();
                FillContactUS1();
                FillMainNavigation();
                //FillLanguage();
            }
        }
        catch (Exception ex)
        {
        }
    }


    #region--> FillSecondNavigation
    protected void FillSecondNavigation()
    {
        try
        {
            ResultList<SecondNavigationEntity> Result = new ResultList<SecondNavigationEntity>();
            //Result = await SecondNavigationDomain.GetNavigationAll();
            Result =  SecondNavigationDomain.GetNavigationAllNotAsync();
            if (Result.Status == ErrorEnums.Success)
            {
                lstSecondNav.DataSource = Result.List.Where(s => s.IsPublished && !s.IsDeleted && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishDate <= currentDate).OrderBy(s => s.MenuOrder).Take(5).ToList();
                lstSecondNav.DataBind();

                MobilelstSecondNav.DataSource = Result.List.Where(s => s.IsPublished && !s.IsDeleted && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishDate <= currentDate).OrderBy(s => s.MenuOrder).Take(5).ToList();
                MobilelstSecondNav.DataBind();
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void lstSecondNav_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                HyperLink lnkSecondNav = (HyperLink)e.Item.FindControl("lnkSecondNav");

                if (lnkSecondNav.Target == "1")
                {
                    lnkSecondNav.Target = "_blank";
                }
                else
                {
                    lnkSecondNav.Target = "_parent";
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void MobilelstSecondNav_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                HyperLink lnkSecondNav = (HyperLink)e.Item.FindControl("lnkSecondNav");

                if (lnkSecondNav.Target == "1")
                {
                    lnkSecondNav.Target = "_blank";
                }
                else
                {
                    lnkSecondNav.Target = "_parent";
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    #endregion

    #region--> FillSocialIcon
    protected void FillSocialIcon()
    {
        try
        {
            ResultList<PluginSocialIconEntity> Result = new ResultList<PluginSocialIconEntity>();
            Result = PluginSocialIconDomain.GetPluginSocialIconAllNotAsync();

            if (Result.Status == ErrorEnums.Success)
            {
                lstSocialIcon.DataSource = Result.List.Where(q => q.LanguageID == CurrLangID && q.IsDeleted == false).OrderBy(c => c.ID);
                lstSocialIcon.DataBind();

                MobilelstSocialIcon.DataSource = Result.List.Where(q => q.LanguageID == CurrLangID && q.IsDeleted == false).OrderBy(c => c.ID);
                MobilelstSocialIcon.DataBind();
                //lstSocialIconFooter.DataSource = Result.List.Where(q => q.LanguageID == CurrLangID).OrderBy(c => c.ID);
                //lstSocialIconFooter.DataBind();


            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void lstSocialIcon_ItemDataBound(object sender, ListViewItemEventArgs e)
    {

        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            Image imgsocial = (Image)e.Item.FindControl("ImageSocialMedia");
            HiddenField HdnimgURL = (HiddenField)e.Item.FindControl("HdnimgURL");
            if (!string.IsNullOrEmpty(imgsocial.ImageUrl))
            {
                imgsocial.ImageUrl = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + imgsocial.ImageUrl.Replace("~/", "~/Siteware/");
            }
            if (HdnimgURL != null)
            {
                if (!string.IsNullOrEmpty(HdnimgURL.Value))
                {
                    string imageover = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + HdnimgURL.Value.Replace("~/", "~/Siteware/");
                    imgsocial.Attributes.Add("onmouseover", "this.src='" + imageover + "'"); imgsocial.Attributes.Add("onmouseout", "this.src='" + imgsocial.ImageUrl + "'");
                }
            }
        }
    }


    protected void MobilelstSocialIcon_ItemDataBound(object sender, ListViewItemEventArgs e)
    {

        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            Image imgsocial = (Image)e.Item.FindControl("ImageSocialMedia");
            HiddenField HdnimgURL = (HiddenField)e.Item.FindControl("HdnimgURL");
            if (!string.IsNullOrEmpty(imgsocial.ImageUrl))
            {
                imgsocial.ImageUrl = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + imgsocial.ImageUrl.Replace("~/", "~/Siteware/");
            }
            if (HdnimgURL != null)
            {
                if (!string.IsNullOrEmpty(HdnimgURL.Value))
                {
                    string imageover = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + HdnimgURL.Value.Replace("~/", "~/Siteware/");
                    imgsocial.Attributes.Add("onmouseover", "this.src='" + imageover + "'"); imgsocial.Attributes.Add("onmouseout", "this.src='" + imgsocial.ImageUrl + "'");
                }
            }
        }
    }

    #endregion

    #region--> FillFooterNavigation
    protected void FillFooterNavigation()
    {
        try
        {
            ResultList<FooterNavigationEntity> Result = new ResultList<FooterNavigationEntity>();
            Result = FooterNavigationDomain.GetFooterAllWithoutAsync();
            if (Result.Status == ErrorEnums.Success)
            {
                
                var AllResultList = Result.List.Where(s => s.ParentID == 0 && s.IsDeleted == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishDate <= currentDate).OrderBy(s => s.MenuOrder).ToList();

                lstFooterNav1.DataSource = AllResultList.Where(s => s.ParentID == 0 && s.IsDeleted == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishDate <= currentDate).OrderBy(s => s.MenuOrder).Take(1).ToList();
                lstFooterNav1.DataBind();

                lstFooterNav2.DataSource = AllResultList.Where(s => s.ParentID == 0 && s.IsDeleted == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishDate <= currentDate).OrderBy(s => s.MenuOrder).Skip(1).Take(1).ToList();
                lstFooterNav2.DataBind();
            }
        }
        catch (Exception e)
        {
        }
    }
    protected void lstFooterNav1_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                HiddenField hdnMenuID = (HiddenField)e.Item.FindControl("hdnMenuID");
                ListView lstSubFooterNav1 = (ListView)e.Item.FindControl("lstSubFooterNav1");

                long Perentid = Convert.ToInt64(hdnMenuID.Value);

                ResultList<FooterNavigationEntity> Result = new ResultList<FooterNavigationEntity>();
                Result = FooterNavigationDomain.GetFooterAllWithoutAsync();
                if (Result.Status == ErrorEnums.Success)
                {

                    lstSubFooterNav1.DataSource = Result.List.Where(s => s.ParentID == Perentid && s.IsDeleted == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishDate <= currentDate).OrderBy(s => s.MenuOrder).ToList();
                    lstSubFooterNav1.DataBind();
                }


            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void lstSubFooterNav1_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {

                HyperLink lnkNavi = (HyperLink)e.Item.FindControl("lnkNavi");

                if (string.IsNullOrEmpty(lnkNavi.NavigateUrl) || lnkNavi.NavigateUrl == "#")
                {
                    lnkNavi.NavigateUrl = "javascript:void(0);";
                }
                //if (lnkNavi.Target == "1")
                //{
                //    lnkNavi.Target = "_blank";
                //}
                //else
                //{
                //    lnkNavi.Target = "_parent";
                //}
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void lstFooterNav2_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                HiddenField hdnMenuID = (HiddenField)e.Item.FindControl("hdnMenuID");
                ListView lstSubFooterNav2 = (ListView)e.Item.FindControl("lstSubFooterNav2");

                long Perentid = Convert.ToInt64(hdnMenuID.Value);

                ResultList<FooterNavigationEntity> Result = new ResultList<FooterNavigationEntity>();
                Result = FooterNavigationDomain.GetFooterAllWithoutAsync();
                if (Result.Status == ErrorEnums.Success)
                {

                    lstSubFooterNav2.DataSource = Result.List.Where(s => s.ParentID == Perentid && s.IsDeleted == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishDate <= currentDate).OrderBy(s => s.MenuOrder).ToList();
                    lstSubFooterNav2.DataBind();
                }


            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void lstSubFooterNav2_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {

                HyperLink lnkNavi = (HyperLink)e.Item.FindControl("lnkNavi");

                if (string.IsNullOrEmpty(lnkNavi.NavigateUrl) || lnkNavi.NavigateUrl == "#")
                {
                    lnkNavi.NavigateUrl = "javascript:void(0);";
                }
                //if (lnkNavi.Target == "1")
                //{
                //    lnkNavi.Target = "_blank";
                //}
                //else
                //{
                //    lnkNavi.Target = "_parent";
                //}
            }
        }
        catch (Exception ex)
        {
        }
    }
    #endregion


    #region --> AboutUs


    protected void FillAboutUs()
    {
        try
        {
            ResultList<Plugin_AboutUsEntity> Result = new ResultList<Plugin_AboutUsEntity>();
            Result = Plugin_AboutUsDomain.GetAllNotAsync();
            if (Result.Status == ErrorEnums.Success)
            {


                lstAboutUS.DataSource = Result.List.Where(s => s.IsDeleted == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishedDate <= currentDate).OrderBy(s => s.ID).Take(1).ToList();
                lstAboutUS.DataBind();
            }


        }
        catch (Exception ex)
        {
        }
    }



    protected void lstAboutUS_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            Image aboutimg = (Image)e.Item.FindControl("aboutimg");

            if (!string.IsNullOrEmpty(aboutimg.ImageUrl))
            {
                aboutimg.ImageUrl = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + aboutimg.ImageUrl.Replace("~/", "~/Siteware/");
            }

        }
    }

    #endregion

    #region --> Contact US

    protected void FillContactUS1()
    {
        try
        {

            ResultList<PluginContactUsEntity> Result = new ResultList<PluginContactUsEntity>();
            //Result = await PluginContactUsDomain.GetPluginContactAll();
            Result = PluginContactUsDomain.GetPluginContactAllNotAsync();
            if (Result.Status == ErrorEnums.Success)
            {
                lstContactUSLocation.DataSource = Result.List.Where(s => s.IsDeleted == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishedDate <= currentDate).OrderBy(s => s.ID).Take(1).ToList();
                lstContactUSLocation.DataBind();


                lstContactUSLocation2.DataSource = Result.List.Where(s => s.IsDeleted == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishedDate <= currentDate).OrderBy(s => s.ID).Skip(1).Take(1).ToList();
                lstContactUSLocation2.DataBind();

                lstContactUSLocation3.DataSource = Result.List.Where(s => s.IsDeleted == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishedDate <= currentDate).OrderBy(s => s.ID).Skip(2).Take(1).ToList();
                lstContactUSLocation3.DataBind();
            }




        }
        catch (Exception ex)
        {
        }
    }

    protected void lstContactUSLocation_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            Image img = (Image)e.Item.FindControl("img");

            if (!string.IsNullOrEmpty(img.ImageUrl))
            {
                img.ImageUrl = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + img.ImageUrl.Replace("~/", "~/Siteware/");
            }

        }
    }

    protected void lstContactUSLocation2_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            Image img = (Image)e.Item.FindControl("img");
            ListView lstSocialIconFooter = (ListView)e.Item.FindControl("lstSocialIconFooter");

            if (!string.IsNullOrEmpty(img.ImageUrl))
            {
                img.ImageUrl = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + img.ImageUrl.Replace("~/", "~/Siteware/");
            }


            ResultList<PluginSocialIconEntity> Result = new ResultList<PluginSocialIconEntity>();
            Result = PluginSocialIconDomain.GetPluginSocialIconAllNotAsync();

            if (Result.Status == ErrorEnums.Success)
            {

                lstSocialIconFooter.DataSource = Result.List.Where(q => q.LanguageID == CurrLangID).OrderBy(c => c.ID);
                lstSocialIconFooter.DataBind();


            }

        }
    }


    protected void lstContactUSLocation3_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            Image img = (Image)e.Item.FindControl("img");

            if (!string.IsNullOrEmpty(img.ImageUrl))
            {
                img.ImageUrl = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + img.ImageUrl.Replace("~/", "~/Siteware/");
            }

        }
    }


    #endregion

    #region --> Main NAvigation

    protected void FillMainNavigation()
    {
        try
        {
            ResultList<NavigationEntity> Result = new ResultList<NavigationEntity>();
            Result = NavigationDomain.GetNavigationWebsiteAllNotAsync();
            if (Result.Status == ErrorEnums.Success)
            {
                lstNavigation.DataSource = Result.List.Where(q => q.ParentID == 0 && q.IsPublished == true && q.LanguageID == CurrLangID && q.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && q.PublishDate <= currentDate).Take(7).ToList();
                lstNavigation.DataBind();

                MobilelstNavigation.DataSource = Result.List.Where(q => q.ParentID == 0 && q.IsPublished == true && q.LanguageID == CurrLangID && q.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && q.PublishDate <= currentDate).Take(7).ToList();
                MobilelstNavigation.DataBind();
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void lstNavigation_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        ResultList<NavigationEntity> Result = new ResultList<NavigationEntity>();
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {

            HiddenField ID = (HiddenField)e.Item.FindControl("hdnId");
            HyperLink lnk = (HyperLink)e.Item.FindControl("lnkMenu");
            //HtmlGenericControl lstmenu = (HtmlGenericControl)e.Item.FindControl("lstmenu");
            HtmlGenericControl ulsubmenu = (HtmlGenericControl)e.Item.FindControl("ulsubmenu");

            if (lnk.Target == "1")
            { lnk.Target = "_blank"; }
            else
            { lnk.Target = "_parent"; }


            if (string.IsNullOrEmpty(lnk.NavigateUrl) || lnk.NavigateUrl == "#")
            {
                lnk.NavigateUrl = "javascript:void(0);";
                lnk.Target = "_parent";
            }

            //if (ID.Value == "1")
            //{
            //    lstmenu.Attributes.Add("class", "active");
            //}
            ListView lstSubNavgation = (ListView)e.Item.FindControl("lstSubNavgation");

            Result = NavigationDomain.GetNavigationByParentMenuID_Website(Convert.ToInt32(ID.Value));

            if (Result.List.Count == 0)
            {
                //lstmenu.Attributes.Add("class", "")
                if (string.IsNullOrEmpty(lnk.NavigateUrl) || lnk.NavigateUrl == "#")
                {
                    lnk.NavigateUrl = "javascript:void(0);";
                }
                ulsubmenu.Visible = false;
            }
            else
            {
                ulsubmenu.Attributes.Add("class", "dropdown-menu");
                lnk.Attributes.Add("class", "dropdown-toggle");
                lnk.Attributes.Add("data-toggle", "dropdown");
                lnk.Attributes.Add("role", "button");
                lnk.Attributes.Add("aria-haspopup", "true");
                lnk.Attributes.Add("aria-expanded", "false");

                //lstmenu.Attributes.Add("class", "nav-item dropdown");
                //lnk.Attributes.Add("class", "nav-link dropdown-toggle");
                //lnk.Attributes.Add("data-toggle", "dropdown");
                //lnk.Attributes.Add("aria-haspopup", "true");
                //lnk.Attributes.Add("aria-expanded", "false");
                //lnk.Attributes.Add("role", "button");
                lnk.NavigateUrl = "javascript:void(0);";
                lnk.Target = "_parent";
                lnk.Text = lnk.Text + "<span class='fa fa-angle-down'></span>";
                lstSubNavgation.DataSource = Result.List.Where(a => a.IsDeleted == false && a.IsPublished == true).OrderBy(a => a.MenuOrder);
                lstSubNavgation.DataBind();
            }
        }
    }


    protected void lstSubNavgation_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        ResultList<NavigationEntity> Result = new ResultList<NavigationEntity>();
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {


            HyperLink lnkSubMenu = (HyperLink)e.Item.FindControl("lnkSubMenu");


            if (lnkSubMenu.Target == "1")
            { lnkSubMenu.Target = "_blank"; }
            else
            { lnkSubMenu.Target = "_parent"; }

        }
    }


    protected void MobilelstNavigation_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        ResultList<NavigationEntity> Result = new ResultList<NavigationEntity>();
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            HiddenField ID = (HiddenField)e.Item.FindControl("hdnId");
            HyperLink lnk = (HyperLink)e.Item.FindControl("lnkMenu");
            HtmlGenericControl ulsubmenu = (HtmlGenericControl)e.Item.FindControl("ulsubmenu");
            if (lnk.Target == "1")
            { lnk.Target = "_blank"; }
            else
            { lnk.Target = "_parent"; }
            if (string.IsNullOrEmpty(lnk.NavigateUrl) || lnk.NavigateUrl == "#")
            {
                lnk.NavigateUrl = "javascript:void(0);";
                lnk.Target = "_parent";
            }
            ListView MobilelstSubNavgation = (ListView)e.Item.FindControl("MobilelstSubNavgation");
            Result = NavigationDomain.GetNavigationByParentMenuID_Website(Convert.ToInt32(ID.Value));
            if (Result.List.Count == 0)
            {
                //lstmenu.Attributes.Add("class", "")
                if (string.IsNullOrEmpty(lnk.NavigateUrl) || lnk.NavigateUrl == "#")
                {
                    lnk.NavigateUrl = "javascript:void(0);";
                }
                ulsubmenu.Visible = false;
            }
            else
            {
                ulsubmenu.Attributes.Add("class", "dropdown-menu");
                lnk.Attributes.Add("class", "dropdown-toggle");
                lnk.Attributes.Add("data-toggle", "dropdown");
                lnk.Attributes.Add("role", "button");
                lnk.Attributes.Add("aria-haspopup", "true");
                lnk.Attributes.Add("aria-expanded", "false");

                //lstmenu.Attributes.Add("class", "nav-item dropdown");
                //lnk.Attributes.Add("class", "nav-link dropdown-toggle");
                //lnk.Attributes.Add("data-toggle", "dropdown");
                //lnk.Attributes.Add("aria-haspopup", "true");
                //lnk.Attributes.Add("aria-expanded", "false");
                //lnk.Attributes.Add("role", "button");
                lnk.NavigateUrl = "javascript:void(0);";
                lnk.Target = "_parent";
                lnk.Text = lnk.Text + "<span class='fa fa-angle-down'></span>";
                MobilelstSubNavgation.DataSource = Result.List.Where(a => a.IsDeleted == false && a.IsPublished == true).OrderBy(a => a.MenuOrder);
                MobilelstSubNavgation.DataBind();
            }
        }
    }
    protected void MobilelstSubNavgation_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        ResultList<NavigationEntity> Result = new ResultList<NavigationEntity>();
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            HyperLink lnkSubMenu = (HyperLink)e.Item.FindControl("lnkSubMenu");
            if (lnkSubMenu.Target == "1")
            { lnkSubMenu.Target = "_blank"; }
            else
            { lnkSubMenu.Target = "_parent"; }

        }
    }

    #endregion

    #region--> Others
    protected void lnkSubmitForm_Click(object sender, EventArgs e)
    {
        //try
        //{
        //    Page.Validate("g1");
        //    if (Page.IsValid)
        //    {
        //        //if (isName && isInterest && isEmail)
        //        //{
        //        ResultEntity<ContactUsFormEntity> result = new ResultEntity<ContactUsFormEntity>();
        //        ContactUsFormEntity entity = new ContactUsFormEntity();
        //        entity.Name = txtName.Text;
        //        entity.Email = txtEmail.Text;
        //        entity.Title = txtInterest.Text;
        //        entity.Message = txtMessage.Text;
        //        entity.AddDate = DateTime.Now;
        //        entity.IsDeleted = false;
        //        entity.Contact = 0;

        //        result = ContactUsFormDomain.InsertContactUsForm(entity);
        //        if (result.Status == ErrorEnums.Success)
        //        {
        //            txtName.Text = "";
        //            txtName.Style.Add("border", "0");
        //            txtEmail.Text = "";
        //            txtEmail.Style.Add("border", "0");
        //            txtInterest.Text = "";
        //            txtInterest.Style.Add("border", "0");
        //            txtMessage.Text = "";
        //            lblMessage.Text = "Your Inqiry submitted successfully";
        //            mpeInquiry.Show();
        //        }
                
        //    }
        //    else
        //    {

        //        if (!string.IsNullOrEmpty(txtName.Text))
        //        {

        //            txtName.Style.Add("border", "0");
        //        }
        //        else
        //        {
        //            txtName.Style.Add("border", "1px solid #ff0000");

        //        }
        //        if (!string.IsNullOrEmpty(txtInterest.Text))
        //        {

        //            txtInterest.Style.Add("border", "0");
        //        }
        //        else
        //        {
        //            txtInterest.Style.Add("border", "1px solid #ff0000");

        //        }

        //        if (!string.IsNullOrEmpty(txtEmail.Text))
        //        {
        //            bool isMatch = Regex.IsMatch(txtEmail.Text, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
        //            if (isMatch)
        //            {

        //                txtEmail.Style.Add("border", "0");
        //            }
        //            else
        //            {
        //                txtEmail.Style.Add("border", "1px solid #ff0000");
        //            }
        //        }
        //        else
        //        {
        //            txtEmail.Style.Add("border", "1px solid #ff0000");
        //        }
        //        lnkSubmitForm.Focus();
        //    }            
        //}
        //catch
        //{
        //}

    }

    protected void lnkLanguage_Click(object sender, EventArgs e)
    {
        //if (lblLangID.Value == "2")
        //{
        //    lblTitle.Text = "Arabic language is coming soon";
        //    lblMessage.Text = "الموقع باللغة العربية تحت الانشاء";
        //    mpeSuccess.Show();
        //}
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            //string SearchText = string.Empty;
            //string lang = string.Empty;
            //if (!string.IsNullOrEmpty(txtSearch.Text))
            //    SearchText = txtSearch.Text.Trim();
            //if (!string.IsNullOrEmpty(txtMobSearch.Text))
            //    SearchText = txtMobSearch.Text.Trim();
            //if (Convert.ToInt32(Session["CurrentLanguage"]) == 1)
            //{
            //    lang = "en";
            //}
            //else
            //{
            //    lang = "ar";
            //}

            //Response.Redirect("~/DetailsPage/" + lang + "/SearchPage.aspx?keyword=" + SearchText, false);
        }
        catch (Exception ex)
        {
        }
    }

    public string FetchLinksFromSource(string htmlSource)
    {
        HtmlDocument doc = new HtmlDocument();
        doc.LoadHtml(htmlSource);
        if (doc.DocumentNode.SelectNodes("//img") != null)
        {
            foreach (var img in doc.DocumentNode.SelectNodes("//img"))
            {
                string orig = img.Attributes["src"].Value;
                string newsrc = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + orig;
                img.SetAttributeValue("src", newsrc);
            }
        }
        return doc.DocumentNode.OuterHtml;
    }
    #endregion

    #region --> NewsLetter

    protected void btnNewsLetter_Click(object sender, EventArgs e)
    {
        Page.Validate("grp1");
        if (Page.IsValid)
        {
            Plugin_News_LetterEntity entity = new Plugin_News_LetterEntity();
            entity.EmailNewsLetter = txtNewsletter.Text;
            entity.SubscribeNewsLetter = true;
            entity.AddDate = DateTime.Now;
            entity.AddUser = "web";
            entity.EditDate = DateTime.Now;
            entity.EditUser = "web";

            var Result = Plugin_News_LetterDomain.InsertPlugin_News_LetterNonasync(entity);
            if (Result.Status == ErrorEnums.Success)
            {
                //btnSubscribe.Text = "";
                txtNewsletter.Text = "";
                if (Convert.ToInt32(Session["CurrentLanguage"]) == (int)EnumLanguage.Arabic)
                {
                    lblMessage.Text = "Your email has been successfully registered";
                    //btnOk.Text = "Ok";
                }
                else
                {
                    lblMessage.Text = "Your email has been successfully registered";
                    //btnOk.Text = "Ok";
                }
                txtNewsletter.Attributes.Add("style", "");
                mpeSuccess.Show();

                //mpeSubscribe.Show();

            }
        }
        else
        {

            txtNewsletter.Attributes.Add("style", "border: 1px solid red");
        }
    }
    #endregion

    #region Search
    protected void txtSearch_TextChanged(object sender, EventArgs e)
    {
        //try
        //{
        //    string SearchText = string.Empty;
        //    string lang = string.Empty;
        //    if (!string.IsNullOrEmpty(txtSearch.Text)) { SearchText = txtSearch.Text.Trim(); }
        //    if (Convert.ToInt32(Session["CurrentLanguage"]) == (int)EnumLanguage.Arabic)
        //    {
        //        LangID = 2;
        //        lang = "/ar";
        //    }
        //    else
        //    {
        //        LangID = 1;
        //        lang = "/en";
        //    }

        //    Response.Redirect("~/DetailsPage/" + lang + "/SearchPage.aspx?keyword=" + SearchText, false);
        //}
        //catch (Exception ex)
        //{ throw ex; }

        try
        {
            string SearchText = string.Empty;
            string lang = string.Empty;
            if (!string.IsNullOrEmpty(txtSearch.Text)) { SearchText = txtSearch.Text.Trim(); }
            if (Convert.ToInt32(Session["CurrentLanguage"]) == (int)EnumLanguage.Arabic)
            {

                LangID = 2;
                Session["CurrentLanguage"] = LangID.ToString();
                lang = "/ar";
            }
            else
            {
                LangID = 1;
                Session["CurrentLanguage"] = LangID.ToString();
                lang = "/en";

            }

            Session["CurrentLanguage"] = LangID.ToString();

            Response.Redirect("~/DetailsPage/" + lang + "/SearchPage.aspx?keyword=" + SearchText, false);
        }
        catch (Exception ex)
        { throw ex; }
    }
    #endregion

    protected async void btnLanguage_Click(object sender, EventArgs e)
    {
        //int LangID = Convert.ToByte(Session["CurrentLanguage"]);
        //if (LangID == (int)EnumLanguage.Arabic)
        //{


        //    Response.Redirect("~/en/home", false);

        //}
        //else
        //{

        //    Response.Redirect("~/ar/home", false);

        //}

        try
        {
            //int LangID = Convert.ToInt32(ddlLanguage.SelectedValue);
            //if (LangID == (int)EnumLanguage.Arabic)
            //{
            //    Session["CurrentLanguage"] = LangID.ToString();
            //    Response.Redirect("~/ar/home", false);
            //}
            //else
            //{
            //    Session["CurrentLanguage"] = LangID.ToString();
            //    Response.Redirect("~/en/home", false);
            //}

            int LangID = 1; // Convert.ToInt32(ddlLanguage.SelectedValue);
            //int LangID = 1; // Convert.ToByte(Session["CurrentLanguage"]);
            string redirectURL = string.Empty;
            //LinkButton btn = (LinkButton)(sender);
            //string id = btn.CommandArgument;
            //int LangID = Convert.ToInt32(btn.CommandArgument);
            int CurrentLangId = Convert.ToInt32(Session["CurrentLanguage"].ToString());

            //to disable Espaniol language - uncomment this
            //if (LangID == (int)EnumLanguage.Espaniol)
            //{
            //    Session["CurrentLanguage"] = CurrentLangId;
            //}
            //else
            //{
            Session["CurrentLanguage"] = LangID;
            //}

            //btn.Attributes.Add("class", "activeLang");
            string lang = string.Empty;

            if (Convert.ToInt32(Session["CurrentLanguage"]) == Convert.ToInt32(EnumLanguage.Arabic))
            {
                SessionManager.CurrentSessionManager.ThemeName = "ThemeAr";
                lang = "/ar";
                //Response.Redirect("http://riifs.org/", false);
            }
            //else (Convert.ToInt32(Session["CurrentLanguage"]) == Convert.ToInt32(EnumLanguage.English))
            //{
            //    SessionManager.CurrentSessionManager.ThemeName = "ThemeEn";
            //    lang = "/en";
            //}
            else
            {
                SessionManager.CurrentSessionManager.ThemeName = "ThemeEn";
                lang = "/en";
                //Response.Redirect("http://riifs.org/", false);
            }

            byte LanguageID = Convert.ToByte(Session["CurrentLanguage"]);
            string category = Convert.ToString(Session["Category"]);
            switch (category)
            {
                case "Home":

                    ResultEntity<PagesEntity> Result = new ResultEntity<PagesEntity>();
                    //Result = await PagesDomain.GetPagesByPageID(Convert.ToInt32(Session["pageid"].ToString()));
                    Result = PagesDomain.GetPagesByPageIDNotAsync(Convert.ToInt32(Session["pageid"].ToString()));
                    var url1 = Result.Entity.MappedPage1;
                    //var url2 = Result.Entity.MappedPage2;
                    //var Page1 = await PagesDomain.GetPagesByAliasPath(url1, Convert.ToByte(Session["CurrentLanguage"].ToString()));
                    var Page1 = PagesDomain.GetPagesByAliasPathNotAsync(url1, Convert.ToByte(Session["CurrentLanguage"].ToString()));
                    //var Page2 = await PagesDomain.GetPagesByAliasPath(url2, Convert.ToByte(Session["CurrentLanguage"].ToString()));

                    if (Page1.Status == ErrorEnums.Success)
                    {
                        redirectURL = Page1.Entity.AliasPath;
                    }
                    //else if (Page2.Status == ErrorEnums.Success && Page1.Status == ErrorEnums.Warning)
                    //    redirectURL = Page2.Entity.AliasPath;
                    else
                    {
                        redirectURL = lang + "/Home"; //~/Error404.aspx
                    }

                    break;

                case "Search":
                    //if (CurrentLangId == LangID)
                    //{
                    //    string keyword = Request.QueryString["keyword"].ToString();
                    //    redirectURL = "~/DetailsPage/" + lang + "/SearchPage.aspx?keyword=" + keyword;
                    //}
                    //else
                    //{
                    string keyword = Request.QueryString["keyword"].ToString();
                    if (LangID == Convert.ToInt32(EnumLanguage.Arabic))
                    {
                        redirectURL = "~/DetailsPage/ar/SearchPage.aspx?keyword=" + keyword;
                    }
                    else if (LangID == Convert.ToInt32(EnumLanguage.English))
                    {
                        redirectURL = "~/DetailsPage/en/SearchPage.aspx?keyword=" + keyword;
                    }
                    else if (LangID == Convert.ToInt32(EnumLanguage.Espaniol))
                    {
                        redirectURL = "~/DetailsPage/es/SearchPage.aspx?keyword=" + keyword;
                    }
                    else
                    {
                        redirectURL = lang + "/Home";
                    }
                    //}

                    break;


                case "News":

                    int NewsID = Convert.ToInt32(Page.RouteData.Values["ID"].ToString());
                    //byte LangID = Convert.ToByte(Session["CurrentLanguage"]);
                    ResultEntity<NewsEntity> NewsResult = new ResultEntity<NewsEntity>();
                    ResultEntity<NewsEntity> News1 = new ResultEntity<NewsEntity>();
                    ResultEntity<NewsEntity> News2 = new ResultEntity<NewsEntity>();
                    NewsResult = await NewsDomain.GetPagesByNewsID(NewsID);
                    //NewsResult =  NewsDomain(NewsID);
                    int NewsId1 = Convert.ToInt32(NewsResult.Entity.MappedNewsID1 == "" ? 0 : Convert.ToInt32(NewsResult.Entity.MappedNewsID1));
                    int NewsId2 = Convert.ToInt32(NewsResult.Entity.MappedNewsID2 == "" ? 0 : Convert.ToInt32(NewsResult.Entity.MappedNewsID2));

                    if (NewsId1 != 0) //&& NewsId1 != Convert.ToInt32(NewsResult.Entity.MappedNewsID1)
                        
                        News1 = await NewsDomain.GetPagesByNewsID(NewsId1);
                   // News1 = await NewsDomain.GetPagesByNewsID(NewsId1, 2);

                    redirectURL =   "en/NewsPage/" + News1.Entity.Headline.Trim() + "/" + News1.Entity.NewsID.ToString();
                    

                    if (NewsId2 != 0)
                        News2 = await NewsDomain.GetPagesByNewsID(NewsId2, LanguageID);

                    if (News1.Status == ErrorEnums.Success && NewsId1 != 0 && News2.Entity.NewsID == 0)
                    {
                        string title = Regex.Replace(News1.Entity.Headline, @"[\\:/*#]+", " ");
                        redirectURL = lang + "/NewsPage/" + title + "/" + News1.Entity.NewsID.ToString();
                    }
                    else if (News2.Status == ErrorEnums.Success && NewsId2 != 0 && News1.Entity.NewsID == 0)
                    {
                        string title = Regex.Replace(News2.Entity.Headline, @"[\\:/*#]+", " ");
                        redirectURL = lang + "/NewsPage/" + title + "/" + News2.Entity.NewsID.ToString();
                    }
                    else
                        redirectURL = lang + "/Home";
                    break;


                //When Book Ajay Patel 29-01-2020

                default:
                    redirectURL = lang + "/Home";
                    break;
            }

            Response.Redirect(redirectURL, false);
        }
        catch (Exception ex)
        {
        }


    }

    #region--> FillLanguage | Add | Ajay Patel | 07-12-2020
    //protected void FillLanguage()
    //{
    //    try
    //    {
    //        ddlLanguage.Items.Clear();
    //        //ddlMobLanguage.Items.Clear();

    //        ResultList<LanguageEntity> Result = new ResultList<LanguageEntity>();
    //        //Result = LanguageDomain.GetLanguagesAll();
    //        Result = LanguageDomain.GetLanguagesAllNotAsync();

    //        if (Result.Status == ErrorEnums.Success)
    //        {
    //            foreach (LanguageEntity item in Result.List)
    //            {
    //                ddlLanguage.Items.Add(new ListItem(item.Name.ToString(), item.ID.ToString()));
    //                //ddlMobLanguage.Items.Add(new ListItem(item.Name.ToString(), item.ID.ToString()));
    //            }
    //            int LangID = Convert.ToInt32(Session["CurrentLanguage"]);
    //            ddlLanguage.SelectedValue = LangID.ToString();
    //            //ddlMobLanguage.SelectedValue = LangID.ToString();
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //    }
    //}

    //protected void ddlMobLanguage_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        int LangID = Convert.ToInt32(ddlMobLanguage.SelectedValue);
    //        if (LangID == (int)EnumLanguage.Arabic)
    //        {
    //            Session["CurrentLanguage"] = LangID.ToString();
    //            Response.Redirect("~/ar/home", false);
    //        }
    //        else
    //        {
    //            Session["CurrentLanguage"] = LangID.ToString();
    //            Response.Redirect("~/en/home", false);
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //    }
    //}
    //protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        //int LangID = Convert.ToInt32(ddlLanguage.SelectedValue);
    //        //if (LangID == (int)EnumLanguage.Arabic)
    //        //{
    //        //    Session["CurrentLanguage"] = LangID.ToString();
    //        //    Response.Redirect("~/ar/home", false);
    //        //}
    //        //else
    //        //{
    //        //    Session["CurrentLanguage"] = LangID.ToString();
    //        //    Response.Redirect("~/en/home", false);
    //        //}

    //        int LangID = Convert.ToInt32(ddlLanguage.SelectedValue);

    //        string redirectURL = string.Empty;
    //        //LinkButton btn = (LinkButton)(sender);
    //        //string id = btn.CommandArgument;
    //        //int LangID = Convert.ToInt32(btn.CommandArgument);
    //        int CurrentLangId = Convert.ToInt32(Session["CurrentLanguage"].ToString());

    //        //to disable Espaniol language - uncomment this
    //        //if (LangID == (int)EnumLanguage.Espaniol)
    //        //{
    //        //    Session["CurrentLanguage"] = CurrentLangId;
    //        //}
    //        //else
    //        //{
    //        Session["CurrentLanguage"] = LangID;
    //        //}

    //        //btn.Attributes.Add("class", "activeLang");
    //        string lang = string.Empty;

    //        if (Convert.ToInt32(Session["CurrentLanguage"]) == Convert.ToInt32(EnumLanguage.Arabic))
    //        {
    //            SessionManager.CurrentSessionManager.ThemeName = "ThemeAr";
    //            lang = "/ar";
    //            //Response.Redirect("http://riifs.org/", false);
    //        }
    //        //else (Convert.ToInt32(Session["CurrentLanguage"]) == Convert.ToInt32(EnumLanguage.English))
    //        //{
    //        //    SessionManager.CurrentSessionManager.ThemeName = "ThemeEn";
    //        //    lang = "/en";
    //        //}
    //        else
    //        {
    //            SessionManager.CurrentSessionManager.ThemeName = "ThemeEn";
    //            lang = "/en";
    //            //Response.Redirect("http://riifs.org/", false);
    //        }

    //        byte LanguageID = Convert.ToByte(Session["CurrentLanguage"]);
    //        string category = Convert.ToString(Session["Category"]);
    //        switch (category)
    //        {
    //            case "Home":

    //                ResultEntity<PagesEntity> Result = new ResultEntity<PagesEntity>();
    //                //Result = await PagesDomain.GetPagesByPageID(Convert.ToInt32(Session["pageid"].ToString()));
    //                Result = PagesDomain.GetPagesByPageIDNotAsync(Convert.ToInt32(Session["pageid"].ToString()));
    //                var url1 = Result.Entity.MappedPage1;
    //                //var url2 = Result.Entity.MappedPage2;
    //                //var Page1 = await PagesDomain.GetPagesByAliasPath(url1, Convert.ToByte(Session["CurrentLanguage"].ToString()));
    //                var Page1 = PagesDomain.GetPagesByAliasPathNotAsync(url1, Convert.ToByte(Session["CurrentLanguage"].ToString()));
    //                //var Page2 = await PagesDomain.GetPagesByAliasPath(url2, Convert.ToByte(Session["CurrentLanguage"].ToString()));

    //                if (Page1.Status == ErrorEnums.Success)
    //                {
    //                    redirectURL = Page1.Entity.AliasPath;
    //                }
    //                //else if (Page2.Status == ErrorEnums.Success && Page1.Status == ErrorEnums.Warning)
    //                //    redirectURL = Page2.Entity.AliasPath;
    //                else
    //                {
    //                    redirectURL = lang + "/Home"; //~/Error404.aspx
    //                }

    //                break;

    //            case "Search":
    //                //if (CurrentLangId == LangID)
    //                //{
    //                //    string keyword = Request.QueryString["keyword"].ToString();
    //                //    redirectURL = "~/DetailsPage/" + lang + "/SearchPage.aspx?keyword=" + keyword;
    //                //}
    //                //else
    //                //{
    //                string keyword = Request.QueryString["keyword"].ToString();
    //                if (LangID == Convert.ToInt32(EnumLanguage.Arabic))
    //                {
    //                    redirectURL = "~/DetailsPage/ar/SearchPage.aspx?keyword=" + keyword;
    //                }
    //                else if (LangID == Convert.ToInt32(EnumLanguage.English))
    //                {
    //                    redirectURL = "~/DetailsPage/en/SearchPage.aspx?keyword=" + keyword;
    //                }
    //                else if (LangID == Convert.ToInt32(EnumLanguage.Espaniol))
    //                {
    //                    redirectURL = "~/DetailsPage/es/SearchPage.aspx?keyword=" + keyword;
    //                }
    //                else
    //                {
    //                    redirectURL = lang + "/Home";
    //                }
    //                //}

    //                break;


    //            //When Book Ajay Patel 29-01-2020

    //            default:
    //                redirectURL = lang + "/Home";
    //                break;
    //        }

    //        Response.Redirect(redirectURL, false);
    //    }
    //    catch (Exception ex)
    //    {
    //    }
    //}
    #endregion

    protected async void btnLanguageEng_Click(object sender, EventArgs e)
    {
        try
        {
            //int LangID = Convert.ToInt32(ddlLanguage.SelectedValue);
            //if (LangID == (int)EnumLanguage.Arabic)
            //{
            //    
            //    Response.Redirect("~/ar/home", false);
            //}
            //else
            //{
            //    Session["CurrentLanguage"] = LangID.ToString();
            //    Response.Redirect("~/en/home", false);
            //}

            int LangID = 2; // Convert.ToInt32(ddlLanguage.SelectedValue);
            //int LangID = 1; // Convert.ToByte(Session["CurrentLanguage"]);
            string redirectURL = string.Empty;
            //LinkButton btn = (LinkButton)(sender);
            //string id = btn.CommandArgument;
            //int LangID = Convert.ToInt32(btn.CommandArgument);
            Session["CurrentLanguage"] = LangID.ToString();
            int CurrentLangId = Convert.ToInt32(Session["CurrentLanguage"].ToString());

            //to disable Espaniol language - uncomment this
            //if (LangID == (int)EnumLanguage.Espaniol)
            //{
            //    Session["CurrentLanguage"] = CurrentLangId;
            //}
            //else
            //{
           
            //}

            //btn.Attributes.Add("class", "activeLang");
            string lang = string.Empty;

            if (Convert.ToInt32(Session["CurrentLanguage"]) == Convert.ToInt32(EnumLanguage.English))
            {
                SessionManager.CurrentSessionManager.ThemeName = "ThemeEn";
                lang = "/en";
                //Response.Redirect("http://riifs.org/", false);
            }
            else
            {

                SessionManager.CurrentSessionManager.ThemeName = "ThemeAr";
                lang = "/ar";
                
            }

            byte LanguageID = Convert.ToByte(Session["CurrentLanguage"]);
            string category = Convert.ToString(Session["Category"]);
            switch (category)
            {
                case "Home":

                    ResultEntity<PagesEntity> Result = new ResultEntity<PagesEntity>();
                    //Result = await PagesDomain.GetPagesByPageID(Convert.ToInt32(Session["pageid"].ToString()));
                    Result = PagesDomain.GetPagesByPageIDNotAsync(Convert.ToInt32(Session["pageid"].ToString()));
                    var url1 = Result.Entity.MappedPage1;
                    //var url2 = Result.Entity.MappedPage2;
                    //var Page1 = await PagesDomain.GetPagesByAliasPath(url1, Convert.ToByte(Session["CurrentLanguage"].ToString()));
                    var Page1 = PagesDomain.GetPagesByAliasPathNotAsync(url1, Convert.ToByte(Session["CurrentLanguage"].ToString()));
                    //var Page2 = await PagesDomain.GetPagesByAliasPath(url2, Convert.ToByte(Session["CurrentLanguage"].ToString()));

                    if (Page1.Status == ErrorEnums.Success)
                    {
                        redirectURL = Page1.Entity.AliasPath;
                    }
                    //else if (Page2.Status == ErrorEnums.Success && Page1.Status == ErrorEnums.Warning)
                    //    redirectURL = Page2.Entity.AliasPath;
                    else
                    {
                        redirectURL = lang + "/Home"; //~/Error404.aspx
                    }

                    break;

                case "Search":
                    //if (CurrentLangId == LangID)
                    //{
                    //    string keyword = Request.QueryString["keyword"].ToString();
                    //    redirectURL = "~/DetailsPage/" + lang + "/SearchPage.aspx?keyword=" + keyword;
                    //}
                    //else
                    //{
                    string keyword = Request.QueryString["keyword"].ToString();
                    if (LangID == Convert.ToInt32(EnumLanguage.Arabic))
                    {
                        redirectURL = "~/DetailsPage/ar/SearchPage.aspx?keyword=" + keyword;
                    }
                    else if (LangID == Convert.ToInt32(EnumLanguage.English))
                    {
                        redirectURL = "~/DetailsPage/en/SearchPage.aspx?keyword=" + keyword;
                    }
                    else if (LangID == Convert.ToInt32(EnumLanguage.Espaniol))
                    {
                        redirectURL = "~/DetailsPage/es/SearchPage.aspx?keyword=" + keyword;
                    }
                    else
                    {
                        redirectURL = lang + "/Home";
                    }
                    //}

                    break;

                case "News":

                    int NewsID = Convert.ToInt32(Page.RouteData.Values["ID"].ToString());
                    //byte LangID = Convert.ToByte(Session["CurrentLanguage"]);
                    ResultEntity<NewsEntity> NewsResult = new ResultEntity<NewsEntity>();
                    ResultEntity<NewsEntity> News1 = new ResultEntity<NewsEntity>();
                    ResultEntity<NewsEntity> News2 = new ResultEntity<NewsEntity>();
                    NewsResult = await NewsDomain.GetPagesByNewsID(NewsID);
                    //NewsResult =  NewsDomain(NewsID);
                    int NewsId1 = Convert.ToInt32(NewsResult.Entity.MappedNewsID1 == "" ? 0 : Convert.ToInt32(NewsResult.Entity.MappedNewsID1));
                    int NewsId2 = Convert.ToInt32(NewsResult.Entity.MappedNewsID2 == "" ? 0 : Convert.ToInt32(NewsResult.Entity.MappedNewsID2));

                    if (NewsId1 != 0) //&& NewsId1 != Convert.ToInt32(NewsResult.Entity.MappedNewsID1)

                        News1 = await NewsDomain.GetPagesByNewsID(NewsId1);
                    // News1 = await NewsDomain.GetPagesByNewsID(NewsId1, 2);

                    redirectURL = "en/NewsPage/" + News1.Entity.Headline.Trim() + "/" + News1.Entity.NewsID.ToString();


                    if (NewsId2 != 0)
                        News2 = await NewsDomain.GetPagesByNewsID(NewsId2, LanguageID);

                    if (News1.Status == ErrorEnums.Success && NewsId1 != 0 && News2.Entity.NewsID == 0)
                    {
                        string title = Regex.Replace(News1.Entity.Headline, @"[\\:/*#]+", " ");
                        redirectURL = lang + "/NewsPage/" + title + "/" + News1.Entity.NewsID.ToString();
                    }
                    else if (News2.Status == ErrorEnums.Success && NewsId2 != 0 && News1.Entity.NewsID == 0)
                    {
                        string title = Regex.Replace(News2.Entity.Headline, @"[\\:/*#]+", " ");
                        redirectURL = lang + "/NewsPage/" + title + "/" + News2.Entity.NewsID.ToString();
                    }
                    else
                        redirectURL = lang + "/Home";
                    break;


                //When Book Ajay Patel 29-01-2020

                default:
                    redirectURL = lang + "/Home";
                    break;
            }

            Response.Redirect(redirectURL, false);
        }
        catch (Exception ex)
        {
        }
    }
}
