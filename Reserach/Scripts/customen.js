$(document).ready(function(){
    $(".search i, .searchdiv i").click(function(){
        $(".searchbox").fadeToggle("");
    });
});

$(".menubar , .closebtn").click(function(){
    $(".menu").fadeToggle();
  })

// ============= Publications Slider Script

$('#Publications').owlCarousel({
    //loop: true,
    rtl:true,
    nav: true,
    navText: [
      "<i class='fa fa-caret-left'></i>",
      "<i class='fa fa-arrow-right'></i>"
    ],
    autoplay: true,
    autoplayHoverPause: true,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 2
      },
      768: {
        items: 2
      },
      1024: {
        items: 3
      },
      1300: {
        items: 4
      }
    }
  })

// ============= Articles_Analytics Slider Script

  $('#Articles_Analytics').owlCarousel({
    loop: false,
    margin:30,
    rtl:true,
    nav: true,
    navText: [
      "<i class='fa fa-arrow-right'></i>",
      "<i class='fa fa-arrow-left'></i>"
    ],
    autoplay: true,
    autoplayHoverPause: true,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 2
      },
      768: {
        items: 2
      },
      1024: {
        items: 3
      },
      1300: {
        items: 3
      }
    }
  })


  // ============= Visuals_Video Slider Script

  $('#Visuals_Video').owlCarousel({
    loop: false,
    margin:30,
    rtl:true,
    nav: true,
    navText: [
      "<i class='fa fa-arrow-right'></i>",
      "<i class='fa fa-arrow-left'></i>"
    ],
    autoplay: true,
    autoplayHoverPause: true,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      768: {
        items: 1
      },
      1024: {
        items: 3
      },
      1300: {
        items: 3
      }
    }
  })

// ============= accordion

  $('.panel-collapse').on('show.bs.collapse', function () {
   $(this).siblings('.panel-heading').addClass('active');
 });

 $('.panel-collapse').on('hide.bs.collapse', function () {
   $(this).siblings('.panel-heading').removeClass('active');
 });
