﻿using Microsoft.CSharp;
using System.Web;
using System;
public class SessionManager
{
    private const string _SiteID = "SiteNumber";

    private const string _ThemeName = "ThemeName";
    public int SiteID
    {
        get
        {
            if (HttpContext.Current.Session != null)
            {

                return Convert.ToInt32(HttpContext.Current.Session[_SiteID]);
            }
            else
            {
                return 0;
            }
           
        }
        set { HttpContext.Current.Session[_SiteID] = value; }
    }
    public  string ThemeName
    {
        get
        {
            if (HttpContext.Current.Session != null)
            { 
                return (string)HttpContext.Current.Session[_ThemeName];
            } 
            else
            {
                return null;
            }
           
        }
        set { HttpContext.Current.Session[_ThemeName] = value; }
    }
    public static SessionManager CurrentSessionManager
    {
        get { return new SessionManager(); }
    }
    			
}