﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Routing;
using Microsoft.AspNet.FriendlyUrls;

namespace WebSite6
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            // var settings = new FriendlyUrlSettings();
            //settings.AutoRedirectMode = RedirectMode.Permanent;
            //routes.EnableFriendlyUrls(settings);

            routes.Ignore("{resource}.axd/{*pathInfo}");
            routes.Ignore("Images/{*pathInfo}");

            //routes.MapPageRoute("Home","home","~/Default.aspx");
            routes.MapPageRoute("Home", "{Language}/home", "~/Default.aspx");

            //routes.MapPageRoute(
            // "Home",
            // "home/{*RequestedPage}",
            // "~/Default.aspx",
            //  true,
            //  new System.Web.Routing.RouteValueDictionary { { "RequestedPage", "home" } });
            routes.MapPageRoute(
         "All Event Pages",
         "{Language}/EventPage/{Title}/{ID}",
         "~/DetailsPage/EventPage.aspx");

            routes.MapPageRoute(
         "All Core Pages",
         "{Language}/CoreValuePage/{Title}/{ID}",
         "~/DetailsPage/CoreValuePage.aspx");

            routes.MapPageRoute(
      "All Entity Pages",
      "{Language}/EntityPage/{Title}/{ID}",
      "~/DetailsPage/EntityPage.aspx");

            routes.MapPageRoute(
           "All Product Pages",
           "{Language}/ProductPage/{Title}/{ID}",
           "~/DetailsPage/ProductPage.aspx");

            routes.MapPageRoute(
            "All News Pages",
            "{Language}/NewsPage/{Title}/{ID}",
            "~/DetailsPage/NewsPage.aspx");

            routes.MapPageRoute(
           "All  Article Pages",
           "{Language}/ArticlePage/{Title}/{ID}",
           "~/DetailsPage/ArticlePage.aspx");

            routes.MapPageRoute(
          "All  Publication Pages",
          "{Language}/PublicationPage/{Title}/{ID}",
          "~/DetailsPage/PublicationPage.aspx");

            

            routes.MapPageRoute(
        "All Timeline Pages",
        "{Language}/TimelinePage/{Title}/{ID}",
        "~/DetailsPage/TimelinePage.aspx");

            routes.MapPageRoute(
            "All Photos Pages",
            "{Language}/PhotoGalleryDetail/{Title}/{ID}",
            "~/DetailsPage/PhotoGalleryDetail.aspx");

            routes.MapPageRoute(
           "All Video Pages",
           "{Language}/VideoGalleryDetail/{Title}/{ID}",
           "~/DetailsPage/VideoGalleryDetail.aspx");

            // routes.MapPageRoute(
            // "EnSearch",
            // "{Language}/SearchPage/{Title}",
            // "~/DetailsPage/en/SearchPage.aspx",
            // true,
            // new System.Web.Routing.RouteValueDictionary { { "RequestedPage", "~/DetailsPage/en/SearchPage.aspx" } });

            // routes.MapPageRoute(
            //"ArSearch",
            //"{Language}/SearchPage/{Title}",
            //"~/DetailsPage/ar/SearchPage.aspx",
            //true,
            // new System.Web.Routing.RouteValueDictionary { { "RequestedPage", "~/DetailsPage/ar/SearchPage.aspx" } });

            routes.MapPageRoute(
            "EnSearch",
            "DetailsPage/{Language}/SearchPage/{Title}",
            "~/DetailsPage/en/SearchPage.aspx",
            true,
            new System.Web.Routing.RouteValueDictionary { { "RequestedPage", "~/DetailsPage/en/SearchPage.aspx" } });

            routes.MapPageRoute(
           "ArSearch",
           "DetailsPage/{Language}/SearchPage/{Title}",
           "~/DetailsPage/ar/SearchPage.aspx",
           true,
            new System.Web.Routing.RouteValueDictionary { { "RequestedPage", "~/DetailsPage/ar/SearchPage.aspx" } });

            routes.MapPageRoute(
            "All Pages",
            "{Language}/Home/{*RequestedPage}",
            "~/Pages/Page.aspx",
            true,
            new System.Web.Routing.RouteValueDictionary { { "RequestedPage", "home" } });

            routes.MapPageRoute(
            "404",
            "{Language}/{*url}",
            "~/Default.aspx");
        }
    }
}