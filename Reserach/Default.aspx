﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" Culture="en-US" UICulture="en-US" Async="true" EnableEventValidation="false" %>

<%@ Import Namespace="SiteWare.Entity.Common.Enums" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <%--<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/GrayGrids/LineIcons/LineIcons.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">--%>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Reserach</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scriptmanager1" runat="server"></asp:ScriptManager>


        <div class="searchbox">
            <div class="searchdiv">
                <i class="fa fa-close"></i>

                <asp:TextBox runat="server" ID="txtSearch" placeholder="البحث" OnTextChanged="txtSearch_TextChanged" AutoPostBack="true">
                </asp:TextBox>
                <button class="fa fa-close serchbtn2"></button>


                <%-- <input type="text" placeholder="البحث">
                <button class="fa fa-search"></button>--%>
            </div>
        </div>

        <header>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="logo">

                            <a href="<%= (Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "/ar" : "/en") %>/home">
                                <asp:Image ID="imgLogo" runat="server" ImageUrl='<%# Bind("Image") %>' />
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9">
                        <div class="top_other">
                            <div class="toplink">
                                <asp:ListView runat="server" ID="lstSecondNav" OnItemDataBound="lstSecondNav_ItemDataBound">
                                    <ItemTemplate>

                                        <asp:HyperLink runat="server" ID="lnkSecondNav" Text='<%# Bind("MenuName") %>' NavigateUrl='<%# Bind("URL") %>' Target='<%# Bind("TargetID") %>'>
                                        </asp:HyperLink>

                                    </ItemTemplate>
                                </asp:ListView>
                            </div>
                            <div class="language">
                                <%--<select>
                                    <option>English</option>
                                    <option>Arebic</option>
                                </select>--%>
                                <%--<asp:DropDownList runat="server" ID="ddlLanguage" OnSelectedIndexChanged="ddlLanguage_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>--%>
                                <asp:LinkButton runat="server" CssClass="langlnk" ID="btnLanguage" OnClick="btnLanguage_Click"> <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "English" : "العربية") %></asp:LinkButton>
                                <i class="fa fa-globe"></i>
                            </div>
                            <div class="topsocial">
                                <span>
                                   
                                    <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "تابعونا على" : "follow us on") %>
                                </span>
                                <asp:ListView runat="server" ID="lstSocialIcon" OnItemDataBound="lstSocialIcon_ItemDataBound">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkSocail" runat="server" NavigateUrl='<%# Bind("Link") %>' Target='<%# Bind("Target") %>'>
                                            <asp:Image ID="ImageSocialMedia" runat="server" ImageUrl='<%# Bind("Image") %>' />
                                            <asp:HiddenField ID="HdnimgURL" runat="server" Value='<%# Bind("Imageover") %>' />
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:ListView>

                            </div>
                        </div>
                        <nav class="navbar navbar-default">
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                    <asp:ListView runat="server" ID="lstNavigation" OnItemDataBound="lstNavigation_ItemDataBound">
                                        <ItemTemplate>
                                            <li>
                                                <asp:HiddenField runat="server" ID="hdnId" Value='<%# Bind("ID") %>' />
                                                <asp:HyperLink runat="server" ID="lnkMenu" Text='<%# Bind("MenuName") %>' NavigateUrl='<%# Bind("URL") %>' Target='<%# Bind("TargetID") %>'>
                                                </asp:HyperLink>
                                                <ul class="dropdown-menu" runat="server" id="ulsubmenu">
                                                    <asp:ListView runat="server" ID="lstSubNavgation" OnItemDataBound="lstSubNavgation_ItemDataBound">
                                                        <ItemTemplate>
                                                            <li>
                                                                <asp:HyperLink ID="lnkSubMenu" runat="server" NavigateUrl='<%# Bind("URL") %>' Target='<%# Bind("TargetID") %>' Text='<%# Bind("MenuName") %>'>                                                                
                                                                </asp:HyperLink>
                                                            </li>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </ul>
                                            </li>
                                        </ItemTemplate>
                                    </asp:ListView>

                                </ul>
                                <div class="search">
                                    <span>
                                         <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "البحث" : "Search") %>
                                        
                                        

                                    </span>
                                    <i class="fa fa-search"></i>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </header>

        <section class="mobile_menu">
            <div class="mobbar">

                <a href="<%= (Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "/ar" : "/en") %>/home">
                    <asp:Image ID="Mobilelogo" runat="server" ImageUrl='<%# Bind("Image") %>' class="moblogo" />
                </a>
                <i class="fa fa-bars menubar"></i>
            </div>
            <div class="menu">
                <i class="fa fa-close closebtn"></i>
                <nav class="navbar navbar-default">
                    <div class="navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <asp:ListView runat="server" ID="MobilelstNavigation" OnItemDataBound="MobilelstNavigation_ItemDataBound">
                                <ItemTemplate>
                                    <li>
                                        <asp:HiddenField runat="server" ID="hdnId" Value='<%# Bind("ID") %>' />
                                        <asp:HyperLink runat="server" ID="lnkMenu" Text='<%# Bind("MenuName") %>' NavigateUrl='<%# Bind("URL") %>' Target='<%# Bind("TargetID") %>'>
                                        </asp:HyperLink>
                                        <ul class="dropdown-menu" runat="server" id="ulsubmenu">
                                            <asp:ListView runat="server" ID="MobilelstSubNavgation" OnItemDataBound="MobilelstSubNavgation_ItemDataBound">
                                                <ItemTemplate>
                                                    <li>
                                                        <asp:HyperLink ID="lnkSubMenu" runat="server" NavigateUrl='<%# Bind("URL") %>' Target='<%# Bind("TargetID") %>' Text='<%# Bind("MenuName") %>'>                                                                
                                                        </asp:HyperLink>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </ul>
                                    </li>
                                </ItemTemplate>
                            </asp:ListView>
                        </ul>
                        <div class="search">
                            <span>
                                <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "البحث" : "Search") %>
                            </span>
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                </nav>
                <div class="top_other">
                    <div class="toplink">
                        <asp:ListView runat="server" ID="MobilelstSecondNav" OnItemDataBound="MobilelstSecondNav_ItemDataBound">
                            <ItemTemplate>

                                <asp:HyperLink runat="server" ID="lnkSecondNav" Text='<%# Bind("MenuName") %>' NavigateUrl='<%# Bind("URL") %>' Target='<%# Bind("TargetID") %>'>
                                </asp:HyperLink>

                            </ItemTemplate>
                        </asp:ListView>

                    </div>
                    <div class="language">
                        <%--<select>
                            <option>English</option>
                            <option>Arebic</option>
                        </select>--%>
                        <%--<asp:DropDownList runat="server" ID="ddlMobLanguage" OnSelectedIndexChanged="ddlMobLanguage_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>--%>
                        <asp:LinkButton runat="server" CssClass="langlnk" ID="LinkButton1" OnClick="btnLanguage_Click"> <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "English" : "العربية") %></asp:LinkButton>
                        <i class="fa fa-globe"></i>
                    </div>
                    <div class="topsocial">
                        <span>
                            
                             <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "تابعونا على" : "follow us on") %>
                        </span>
                        <asp:ListView runat="server" ID="MobilelstSocialIcon" OnItemDataBound="MobilelstSocialIcon_ItemDataBound">
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkSocail" runat="server" NavigateUrl='<%# Bind("Link") %>' Target='<%# Bind("Target") %>'>
                                    <asp:Image ID="ImageSocialMedia" runat="server" ImageUrl='<%# Bind("Image") %>' />
                                    <asp:HiddenField ID="HdnimgURL" runat="server" Value='<%# Bind("Imageover") %>' />
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:ListView>

                    </div>
                </div>
            </div>
        </section>

        <section class="banner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1">
                        <div id="carousel-example-generic"  class="carousel slide carousel-fade" data-ride="carousel">
                            <div class="carousel-inner" role="listbox">

                                <%--<asp:ListView runat="server" ID="lstBennerArticle" OnItemDataBound="lstBennerArticle_ItemDataBound">
                                    <ItemTemplate>
                                        <div id="itemdiv" runat="server">
                                            <div class="row" <%= (Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "" : "style='direction: ltr;font-family: Verdana;'") %>">
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <div class="banner_caption">
                                                        <aside>
                                                            <asp:HiddenField runat="server" ID="hdnID" Value='<%# Bind("ArticleID") %>' />
                                                             <asp:HiddenField runat="server" ID="hdnCategory" Value='<%# Bind("ArticleCategory") %>' />
                                                            <asp:Literal runat="server" ID="lblTitle" Text='<%# Bind("ArticleTitle") %>'></asp:Literal>
                                                        </aside>
                                                        <span>
                                                            <asp:Literal runat="server" ID="lblAuthorName" Text='<%# Bind("ArticleAuthor") %>'></asp:Literal></span>
                                                        <small>
                                                            <i class="fa fa-calendar"></i>
                                                            <asp:Literal runat="server" ID="lblDate" Text='<%# Bind("ArticleDate","{0:dd/MM/yyyy}") %>'></asp:Literal>
                                                        </small>
                                                        <div class="bluebox">
                                                            <p>
                                                                <asp:Literal runat="server" ID="lblSummery" Text='<%# Bind("ArticleSummery") %>'></asp:Literal>
                                                            </p>
                                                            <asp:HyperLink ID="lnkMore" runat="server" class="detail">
                                                                <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "التفـــاصيل" : "Details") %>
                                                            </asp:HyperLink>
                                                           

                                                            <strong>
                                                                <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "المواضيع المتناولة" : "Topics covered") %>
                                                            </strong>
                                                            
                                                            <a href="javascript:void">
                                                                <asp:Literal runat="server" ID="lblKeyword" Text='<%# Bind("ArticleKeyWords") %>'></asp:Literal></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <div class="bannerimg">
                                                        <span>
                                                            <asp:Literal runat="server" ID="lblCategory"></asp:Literal></span>
                                                        </span>
                                                       
                                                        <asp:Image ID="imgArticle" runat="server" ImageUrl='<%# Bind("ArticleImage") %>' />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:ListView>--%>


                                <asp:ListView runat="server" ID="lstBennerArticle" OnItemDataBound="lstBennerArticle_ItemDataBound">
                                    <ItemTemplate>
                                        <div id="itemdiv" runat="server">
                                            <div class="row" <%= (Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "" : "style='direction: ltr;font-family: Verdana;'") %>">
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <div class="banner_caption">
                                                        <aside>
                                                            <asp:HiddenField runat="server" ID="hdnID" Value='<%# Bind("ID") %>' />
                                                             <asp:HiddenField runat="server" ID="hdnDataType" Value='<%# Bind("sliderDataType") %>' />
                                                             <asp:HiddenField runat="server" ID="hdnCategory" Value='<%# Bind("Category") %>' />
                                                            <asp:Literal runat="server" ID="lblTitle" Text='<%# Bind("Title") %>'></asp:Literal>
                                                        </aside>
                                                        <span>
                                                            <asp:Literal runat="server" ID="lblAuthorName" Text='<%# Bind("Author") %>'></asp:Literal></span>
                                                        <small>
                                                            <i class="fa fa-calendar"></i>
                                                            <asp:Literal runat="server" ID="lblDate" Text='<%# Bind("Date","{0:dd/MM/yyyy}") %>'></asp:Literal>
                                                        </small>
                                                        <div class="bluebox">
                                                            <p>
                                                                <asp:Literal runat="server" ID="lblSummery" Text='<%# Bind("Summery") %>'></asp:Literal>
                                                            </p>
                                                            <asp:HyperLink ID="lnkMore" runat="server" class="detail">
                                                                <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "التفـــاصيل" : "Details") %>
                                                            </asp:HyperLink>
                                                           

                                                            <strong>
                                                                <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "المواضيع المتناولة" : "Topics covered") %>
                                                            </strong>
                                                            
                                                            <a href="javascript:void">
                                                                <asp:Literal runat="server" ID="lblKeyword" Text='<%# Bind("KeyWords") %>'></asp:Literal></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <div class="bannerimg">
                                                        <span>
                                                            <asp:Literal runat="server" ID="lblCategory"></asp:Literal></span>
                                                        </span>
                                                       
                                                        <asp:Image ID="imgArticle" runat="server" ImageUrl='<%# Bind("Image") %>' />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:ListView>

                            </div>
                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                <span class="fa fa-arrow-right" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                <span class="fa fa-arrow-left" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                   <%-- <div class="col-lg-12">--%>


                        <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "<div class='col-lg-12'>" : "<div class='col-lg-12' style='display:none;'>") %>
                        <ul class="list-unstyled banner_ul">

                            <asp:ListView runat="server" ID="lstSubArticle" OnItemDataBound="lstSubArticle_ItemDataBound">
                                    <ItemTemplate>
                                         
                                        <li>
                                            <asp:HyperLink ID="lnkMore" runat="server">
                                            <asp:HiddenField runat="server" ID="hdnID" Value='<%# Bind("ArticleID") %>' />
                                <h4><asp:Literal runat="server" ID="lblTitle" Text='<%# Bind("ArticleTitle") %>'></asp:Literal></h4>
                                                </asp:HyperLink>
                                <span> <asp:Literal runat="server" ID="Literal2" Text='<%# Bind("ArticleAuthor") %>'></asp:Literal>
                                </span>
                                <small><asp:Literal runat="server" ID="Literal3" Text='<%# Bind("ArticleDate","{0:dd/MM/yyyy}") %>'></asp:Literal></small>
                            </li>

                                        
                                    </ItemTemplate>
                                </asp:ListView>

                           <%-- <li>
                                <h4>إيران تتفاخر بتقنيات جديدة...</h4>
                                <span>ستيرلينغ جينسين  |  وليد الراوي
                                </span>
                                <small>31/08/2020</small>
                            </li>
                            <li>
                                <h4>إيران تتفاخر بتقنيات جديدة...</h4>
                                <span>ستيرلينغ جينسين  |  وليد الراوي
                                </span>
                                <small>31/08/2020</small>
                            </li>
                            <li>
                                <h4>إيران تتفاخر بتقنيات جديدة...</h4>
                                <span>ستيرلينغ جينسين  |  وليد الراوي
                                </span>
                                <small>31/08/2020</small>
                            </li>--%>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section class="comnpadding Researchstudies_InstituteNews">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-12 col-sm-12">
                        <h1 class="comntitle">
          
                           <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "منشورات ومراجعات" : "Commentaries & Analysis") %>
                            
                           <%-- <a href="javascript:void"> شاهد الجميع 

            <i class="fa fa-angle-left"></i>
          </a>--%>
                    <a href="<%= (Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "/ar/Home/المنشورات" : "/en/Home/Publications-Reviews") %>">
                       
                        <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "شاهد الجميع" : "See All")  %>
                         <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "<i class='fa fa-angle-left'></i>" : "<i class='fa fa-angle-right'></i>") %>
                                 
                            </a>
                            
                        
                        </h1>
                        <div class="Research_studies">
                            <ul class="list-unstyled">

                               <%-- <asp:ListView runat="server" ID="lstPublication" OnItemDataBound="lstPublication_ItemDataBound">
                                    <ItemTemplate>
                                        <li>
                                            <div class="research_img">
                                                <asp:Image ID="imgPublication" runat="server" ImageUrl='<%# Bind("PublicationsImage") %>' />
                                                <asp:HiddenField runat="server" ID="hdnID" Value='<%# Bind("PublicationsID") %>' />
                                                <asp:HiddenField runat="server" ID="hdnCategory" Value='<%# Bind("PublicationsCatogory") %>' />
                                                <span class="bg1">
                                                    <asp:Literal runat="server" ID="lblCategory"></asp:Literal></span>
                                            </div>
                                            <div class="research_text">
                                                <h3>
                                                    <asp:HyperLink ID="lnkMore" runat="server">
                                                        <asp:Literal runat="server" ID="lblTitle" Text='<%# Bind("PublicationsTitle") %>'></asp:Literal>
                                                    </asp:HyperLink>

                                                </h3>
                                                <strong>
                                                    <asp:Literal runat="server" ID="lblAuthor" Text='<%# Bind("PublicationsAuthor") %>'></asp:Literal>
                                                </strong>
                                                <small class="LTR">
                                                    <asp:Literal runat="server" ID="lblDate" Text='<%# Bind("PublicationsDate","{0:dd/MM/yyyy}") %>'></asp:Literal></small>
                                                <p>
                                                    <asp:Literal runat="server" ID="lblSummary" Text='<%# Bind("PublicationsSummery") %>'></asp:Literal>
                                                </p>
                                            </div>
                                        </li>
                                    </ItemTemplate>
                                </asp:ListView>--%>

                                <asp:ListView runat="server" ID="lstResearchAndPublication" OnItemDataBound="lstResearchAndPublication_ItemDataBound">
                                    <ItemTemplate>
                                        <li>
                                            <div class="research_img">
                                                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# Bind("MedialLink") %>' Target='<%# Bind("Target") %>' >
                                                <asp:Image ID="imgPublication" runat="server" ImageUrl='<%# Bind("ResearchStudiesImage") %>' />
                                                     </asp:HyperLink>
                                                <asp:HiddenField runat="server" ID="hdnID" Value='<%# Bind("ResearchStudiesID") %>' />
                                                <asp:HiddenField runat="server" ID="hdnCategory" Value='<%# Bind("ResearchStudiesCatogory") %>' />
                                                <span class="bg1">
                                                    <asp:Literal runat="server" ID="lblCategory"></asp:Literal></span>
                                            </div>
                                            <div class="research_text">
                                                <h3>
                                                    <asp:HyperLink ID="lnkMore" runat="server" NavigateUrl='<%# Bind("MedialLink") %>' Target='<%# Bind("Target") %>' >
                                                        <asp:Literal runat="server" ID="lblTitle" Text='<%# Bind("ResearchStudiesTitle") %>'></asp:Literal>
                                                    </asp:HyperLink>

                                                </h3>
                                                <strong>
                                                     <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl='<%# Bind("MedialLink") %>' Target='<%# Bind("Target") %>' >
                                                    <asp:Literal runat="server" ID="lblAuthor" Text='<%# Bind("ResearchStudiesTitle2") %>'></asp:Literal>
                                                            </asp:HyperLink>
                                                </strong>
                                                <%--<small class="LTR">
                                                    <asp:Literal runat="server" ID="lblDate" Text='<%# Bind("ResearchStudiesDate","{0:dd/MM/yyyy}") %>'></asp:Literal></small>--%>
                                                <p>
                                                    <asp:Literal runat="server" ID="lblSummary" Text='<%# Bind("ResearchStudiesSummery") %>'></asp:Literal>
                                                </p>
                                            </div>
                                        </li>
                                    </ItemTemplate>
                                </asp:ListView>


                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12">

                        <a href="/ar/Home/NewsList">
                        <h1 class="comntitle"> <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "أخبار المعهد" : "Institute News") %>                            
                        </h1>
                            </a>

                             <a href="<%= (Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "/ar" : "/en") %>/home" style="display:none">
                       
                        <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "شاهد الجميع" : "See All") %>
                                <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "<i class='fa fa-angle-left'></i>" : "<i class='fa fa-angle-right'></i>") %>
                                 <%--<i class="fa fa-angle-right"></i>--%>
                            </a>
                        <div class="Institute_News">
                            <ul class="list-unstyled">

                                <asp:ListView runat="server" ID="lstNews" OnItemDataBound="lstNews_ItemDataBound">
                                    <ItemTemplate>
                                        <li>
                                            <span>
                                                <asp:HiddenField runat="server" ID="hdnID" Value='<%# Bind("NewsID") %>' />
                                                <asp:HiddenField runat="server" ID="hdndate" Value='<%# Bind("NewsDate") %>' />
                                                <asp:Literal runat="server" ID="lbldate"></asp:Literal>
                                            </span>
                                            <p>
                                                <asp:HyperLink ID="lnkMore" runat="server">
                                                    <asp:Literal runat="server" ID="lblTitle" Text='<%# Bind("Headline") %>'></asp:Literal>
                                                </asp:HyperLink>
                                            </p>
                                            <%-- <asp:HyperLink ID="lnkMore" runat="server" >شاهد المزيد</asp:HyperLink>--%>
                                        </li>
                                    </ItemTemplate>
                                </asp:ListView>


                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="comnpadding Articles_Analytics">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="comntitle">
                           
                       <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? " مقالات وتحليلات" : "Articles and Analytics") %>
                        </h1>

                         <a class="Seeall" href="<%= (Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "/ar" : "/en") %>/home" style="display:none">
                       
                        <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "شاهد الجميع" : "See All") %>
                             <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "<i class='fa fa-angle-left'></i>" : "<i class='fa fa-angle-right'></i>") %>
                                <%-- <i class="fa fa-angle-right"></i>--%>
                            </a>

                        <%--<a href="javascript:void" class="Seeall">شاهد الجميع 
          
                            <i class="fa fa-angle-left"></i>
                        </a>--%>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="Articles_Analytics" class="owl-carousel">
                            <asp:ListView runat="server" ID="lstArticle" OnItemDataBound="lstArticle_ItemDataBound">
                                <ItemTemplate>
                                    <div class="item">
                                         <asp:HyperLink ID="lnkMore" runat="server" >
                                        <div class="Articlesbox">
                                            <asp:HiddenField runat="server" ID="hdnID" Value='<%# Bind("ArticleID") %>' />
                                            <asp:Image ID="imgArticle" runat="server" ImageUrl='<%# Bind("ArticleImage") %>' />

                                            <div class="Articles_caption">
                                                <asp:HiddenField runat="server" ID="hdnCategory" Value='<%# Bind("ArticleCategory") %>' />
                                                <span class="bg3">
                                                    <asp:Literal runat="server" ID="lblCategory"></asp:Literal></span>

                                                <h4>
                                                    <asp:Literal runat="server" ID="lblTitle" Text='<%# Bind("ArticleTitle") %>'></asp:Literal>
                                                    <small>
                                                        <asp:Literal runat="server" ID="lblDate" Text='<%# Bind("ArticleDate","{0:dd/MM/yyyy}") %>'></asp:Literal></small>
                                                </h4>
                                            </div>
                                            <div class="border bg3"></div>
                                        </div>

                                        </asp:HyperLink>
                                    </div>
                                </ItemTemplate>
                            </asp:ListView>
                        </div>
                    </div>
                </div>

            </div>
        </section>

        <section class="comnpadding Publications">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="comntitle">
                            

                            <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "منشورات" : "Publications") %>
                    </h1>
                            </div>
                </div>
            </div>
            <div class="Publications_bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-9 col-md-9 col-sm-9">
                            <div class="carousel-wrap">
                                <div id="Publications" class="owl-carousel">

                                    <asp:ListView runat="server" ID="lstPublicationBooks" OnItemDataBound="lstPublicationBooks_ItemDataBound">
                                        <ItemTemplate>
                                            <div class="item">
                                                <asp:Image ID="imgPublication" runat="server" ImageUrl='<%# Bind("PublicationsImage2") %>' />
                                                <small>
                                                    <asp:Literal runat="server" ID="lblVersion" Text='<%# Bind("PublicationsVersion") %>'></asp:Literal></small>
                                                <h3>
                                                    <asp:Literal runat="server" ID="lblTitle" Text='<%# Bind("PublicationsTitle2") %>'></asp:Literal></h3>
                                                <p>
                                                    <asp:Literal runat="server" ID="lblAuthor" Text='<%# Bind("PublicationsAuthor") %>'></asp:Literal>
                                                </p>
                                            </div>
                                        </ItemTemplate>
                                    </asp:ListView>



                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="bookimg">
                                <img src="<%=ResolveUrl("~/") %>App_Themes/ThemeAr/img/book.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="comnpadding Visuals_LatestTweets">
            <div class="container">
                <div class="row">


                    <div class="col-lg-12 col-md-12 col-sm-12">

                         <h1 class="comntitle">
                             <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "المرئيات" : "Media") %>
                            
                       </h1>

                        <div id="Visuals_Video" class="owl-carousel">

                            <asp:ListView runat="server" ID="lstMedia" OnItemDataBound="lstMedia_ItemDataBound">
                                <ItemTemplate>
                                    <div class="item">
                                        <div class="videobox">
                                            


                                            <%--<asp:LinkButton ID="LinkButtonID" runat="server" CommandArgument='<%# Eval("MediaID") %>' OnClick="LinkButtonID_Click" >--%>
                                                <asp:HyperLink ID="lnkSubMenu" runat="server" NavigateUrl='<%# Bind("MedialLink") %>' Target='<%# Bind("Target") %>'>

                                                <asp:Image ID="imgMedia" runat="server" ImageUrl='<%# Bind("MediaImage") %>' />
                                                     </asp:HyperLink>
                                            <%-- </asp:LinkButton>--%>
                                                <h3>
                                                    <asp:Literal runat="server" ID="lblTitl" Text='<%# Bind("Title") %>'></asp:Literal>
                                                </h3>                                               
                                           
                                             <ul class="list-unstyled">
                                                    <li>
                                                        <strong>
                                                                <asp:Literal runat="server" ID="lblDate" Text='<%# Bind("PublishedDate","{0:dd/MM/yyyy}") %>'></asp:Literal>
                                                        </strong>
                                                    </li>
                                                    <li>                                                        
                                                        <%--<i class="fa fa-eye"></i>
                                                        <span><asp:Literal runat="server" ID="lblConubt" Text=<%# Eval("ViewCount") %>></asp:Literal></span>
                                                         <small>
                                                        <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "مشاهدة" : "Watch") %>--%>
                                                    </small>
                                                    </li>
                                                    <li>
                                                    <%--<img src="<%=ResolveUrl("~/") %>App_Themes/ThemeAr/img/share.png" alt=""></li>--%>
                                                </ul>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:ListView>


                        </div>

                    </div>



                  <%--  <div class="col-lg-8 col-md-8 col-sm-6">
                        <h1 class="comntitle">
                             <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "المرئيات" : "Visuals") %>
                            
                       </h1> 

                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="tweet">
                            <h1 class="comntitle">
                               
                          <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? " آخر التغريدات" : "Latest Tweets") %>
                            </h1>  <span>
                                   <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "تابعونا" : "Follow us") %>
                                
                                <i class="fa fa-twitter"></i></span>
                            <img src="<%=ResolveUrl("~/") %>App_Themes/ThemeAr/img/twitter.jpg" alt="">
                        </div>
                    </div>--%>
                </div>
            </div>
        </section>


        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-12 col-sm-12">



                        <div class="footerlogo">
                            <asp:ListView runat="server" ID="lstAboutUS" OnItemDataBound="lstAboutUS_ItemDataBound">
                                <ItemTemplate>

                                    <asp:HyperLink ID="lnkNavi" runat="server" NavigateUrl='<%# Bind("Link") %>' Target='<%# Bind("Target") %>'>
                                        <asp:Image runat="server" ImageUrl='<%# Bind("Image") %>' ID="aboutimg" />
                                    </asp:HyperLink>
                                    <p>
                                        <asp:Literal runat="server" ID="lblP" Text='<%# Bind("Summary") %>'></asp:Literal>
                                    </p>

                                </ItemTemplate>
                            </asp:ListView>
                            <%--<a href="index.html">
                                <img src="<%=ResolveUrl("~/") %>App_Themes/ThemeAr/img/footer-logo.png" alt="">
                            </a>
                            <p>استجابات المؤسسة العسكرية العراقية لجائحة فيروس كورونا متنوعة: خلق مزيد من التوتر في العلاقات العسكرية المدنية الشيعية، وبناء الثقة سيادتها الحزبية التي يغذّيها الخارج</p>--%>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-8">
                        <div class="Main_links">
                            <asp:ListView runat="server" ID="lstFooterNav1" OnItemDataBound="lstFooterNav1_ItemDataBound">
                                <ItemTemplate>
                                    <h6>
                                        <asp:Literal runat="server" ID="lblTitle" Text='<%# Bind("Title") %>'></asp:Literal>
                                        <asp:HiddenField runat="server" ID="hdnMenuID" Value='<%# Bind("ID") %>' />
                                    </h6>
                                    <%-- <asp:HyperLink ID="lnkSocail" runat="server" NavigateUrl='<%# Bind("Link") %>' Target='<%# Bind("Target") %>'>
                                                    <asp:Image ID="ImageSocialMedia" runat="server" ImageUrl='<%# Bind("Image") %>' />
                                                    <asp:HiddenField ID="HdnimgURL" runat="server" Value='<%# Bind("Imageover") %>' />
                                                </asp:HyperLink>--%>


                                    <ul class="list-unstyled">
                                        <asp:ListView runat="server" ID="lstSubFooterNav1" OnItemDataBound="lstSubFooterNav1_ItemDataBound">
                                            <ItemTemplate>
                                                <li>
                                                    <asp:HyperLink ID="lnkNavi" runat="server" Text='<%# Bind("Title") %>' NavigateUrl='<%# Bind("URL") %>' Target='<%# Bind("Target") %>'>
                                                    
                                                    </asp:HyperLink>
                                                </li>

                                            </ItemTemplate>
                                        </asp:ListView>

                                    </ul>
                                </ItemTemplate>
                            </asp:ListView>



                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4">
                        <div class="Sub_Links">

                            <asp:ListView runat="server" ID="lstFooterNav2" OnItemDataBound="lstFooterNav2_ItemDataBound">
                                <ItemTemplate>
                                    <h6>
                                        <asp:Literal runat="server" ID="lblTitle" Text='<%# Bind("Title") %>'></asp:Literal>
                                        <asp:HiddenField runat="server" ID="hdnMenuID" Value='<%# Bind("ID") %>' />
                                    </h6>
                                    <ul class="list-unstyled">
                                        <asp:ListView runat="server" ID="lstSubFooterNav2" OnItemDataBound="lstSubFooterNav2_ItemDataBound">
                                            <ItemTemplate>
                                                <li>
                                                    <asp:HyperLink ID="lnkNavi" runat="server" Text='<%# Bind("Title") %>' NavigateUrl='<%# Bind("URL") %>' Target='<%# Bind("Target") %>'>
                                                    
                                                    </asp:HyperLink>
                                                </li>

                                            </ItemTemplate>
                                        </asp:ListView>

                                    </ul>
                                </ItemTemplate>
                            </asp:ListView>


                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="contact_information">
                            <h6>
                                
                                 <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "معلومات الاتصال" : "Contact Information") %>
                            </h6>

                            <asp:ListView runat="server" ID="lstContactUSLocation" OnItemDataBound="lstContactUSLocation_ItemDataBound">
                                <ItemTemplate>
                                    <div class="location">
                                        <asp:Image ID="img" runat="server" ImageUrl='<%# Bind("Image") %>' />
                                        <%--  <img src="<%=ResolveUrl("~/") %>App_Themes/ThemeAr/img/location.png" alt="">--%>
                                        <span>
                                            <asp:Literal runat="server" ID="lbltitle" Text='<%# Bind("Title") %>'></asp:Literal>
                                            <strong>
                                                <asp:Literal runat="server" ID="lblTilte2" Text='<%# Bind("Description") %>'></asp:Literal></strong></span>
                                    </div>

                                </ItemTemplate>
                            </asp:ListView>

                            <asp:ListView runat="server" ID="lstContactUSLocation2" OnItemDataBound="lstContactUSLocation2_ItemDataBound">
                                <ItemTemplate>
                                    <div class="call">
                                        <asp:Image ID="img" runat="server" ImageUrl='<%# Bind("Image") %>' />
                                        <aside>
                                            <div class="num">
                                                <asp:HyperLink ID="lnkNavi" runat="server" Text='<%# Bind("Title") %>' NavigateUrl='<%# Bind("URL") %>' Target='<%# Bind("Target") %>'>
                                                    
                                                </asp:HyperLink>
                                                <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Bind("Description") %>' NavigateUrl='<%# Bind("URL") %>' Target='<%# Bind("Target") %>'>
                                                    
                                                </asp:HyperLink>
                                                <%--<a href="tel:+96265152444">+962 6 5152444</a>
                                        <a href="tel:+96265152955">+962 6 5152955</a>--%>
                                            </div>
                                            <div class="topsocial">


                                                <asp:ListView runat="server" ID="lstSocialIconFooter" OnItemDataBound="lstSocialIcon_ItemDataBound">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="lnkSocail" runat="server" NavigateUrl='<%# Bind("Link") %>' Target='<%# Bind("Target") %>'>
                                                            <asp:Image ID="ImageSocialMedia" runat="server" ImageUrl='<%# Bind("Image") %>' />
                                                            <asp:HiddenField ID="HdnimgURL" runat="server" Value='<%# Bind("Imageover") %>' />
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:ListView>


                                            </div>
                                        </aside>

                                    </div>

                                </ItemTemplate>
                            </asp:ListView>

                            <asp:ListView runat="server" ID="lstContactUSLocation3" OnItemDataBound="lstContactUSLocation3_ItemDataBound">
                                <ItemTemplate>
                                    <div class="mail">
                                        <asp:Image ID="img" runat="server" ImageUrl='<%# Bind("Image") %>' />
                                        <asp:HyperLink ID="lnkNavi" runat="server" Text='<%# Bind("Title") %>' NavigateUrl='<%# Bind("URL") %>' Target='<%# Bind("Target") %>'>                                                    
                                        </asp:HyperLink>
                                    </div>
                                </ItemTemplate>
                            </asp:ListView>

                            <%--<div class="mail">
                                <img src="<%=ResolveUrl("~/") %>App_Themes/ThemeAr/img/mail.png" alt="">
                                <a href="mailto:info@domainname.com">info@domainname.com
                                </a>
                            </div>--%>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <section class="subscribe_now">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="newslatter">
                            <strong>
                                 <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "النشرة الدورية" : "Newsletter") %>
                                
                      </strong>   <%--   <span>أدخل عنوان بريدك للاشتراك في القائمة البريدية للمركز  والحصول على آخر الأبحاث والدراسات على بريدك الخاص</span>--%>
                            <span>
                                <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "أدخل عنوان بريدك للاشتراك في القائمة البريدية للمركز  والحصول على آخر الأبحاث والدراسات على بريدك الخاص" : "Enter your email address to subscribe to the center's mailing list and get the latest research and studies on your own mail") %>
                            </span>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">

                        <asp:UpdatePanel ID="up1" UpdateMode="Conditional" runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="pnlLogon" runat="server" DefaultButton="btnNewsLetter" class="subscribe">
                                    <asp:TextBox ID="txtNewsletter" runat="server" PlaceHolder="ادخل البريد الالكتروني"></asp:TextBox>
                                    <asp:RequiredFieldValidator runat="server" CssClass="validator" ErrorMessage="" ID="RequiredFieldValidator4"
                                        ControlToValidate="txtNewsletter" Font-Size="11" Display="Dynamic" Text=""
                                        ForeColor="Red" ValidationGroup="grp1" EnableClientScript="false" SetFocusOnError="true" />
                                    <asp:RegularExpressionValidator ID="RequiredFieldValidator16" runat="server"
                                        CssClass="validator"
                                        ControlToValidate="txtNewsletter"
                                        ErrorMessage=""
                                        ForeColor="red"
                                        ValidationGroup="grp1"
                                        Display="Dynamic"
                                        ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$"
                                        Style="color: Red; float: right; margin-top: 2%; margin-right: -1.5%;"></asp:RegularExpressionValidator>
                                    <asp:LinkButton runat="server" ID="btnNewsLetter" ValidationGroup="grp1" OnClick="btnNewsLetter_Click" >

                                         <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "اشترك الآن" : "Subscribe") %>
                                    </asp:LinkButton>
                                    


                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>


                        <%-- <input type="text" placeholder="ادخل البريد الالكتروني ">--%>
                        <%--<button>اشترك الآن</button>--%>
                    </div>
                </div>
            </div>
        </section>

        <div class="copyright">
            <div class="container">
                <div class="row">

                    <asp:Literal ID="lblCopyright" runat="server"></asp:Literal>

                    <%--<div class="col-lg-7 col-md-7 col-sm-12">
         جميع الحقوق الملكية محفوظة<span class="LTR">2020.</span> 
         <strong>معهد السياسات والمجتمع للابحاث والدراسات</strong>
      </div>
      <div class="col-lg-5 col-md-5 col-sm-12">
        <div class="copylink">
          تصميم وتطوير بواسطة
          <a href="javascript:void">Auratechs Solutions </a>  
        </div>
        
      </div>--%>
                </div>
            </div>
        </div>


        <%--<div class="modal" id="lightbox">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div id="demo7" class="carousel slide" data-ride="carousel">

                            <!-- Indicators -->
                            <ul class="carousel-indicators">
                                <li data-target="#demo7" data-slide-to="0" class="active"></li>
                                <li data-target="#demo7" data-slide-to="1"></li>
                                <li data-target="#demo7" data-slide-to="2"></li>
                            </ul>

                            <!-- The slideshow -->
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img src="<%=ResolveUrl("~/") %>App_Themes/ThemeEn/img/gallery-icon1.png" alt="#">
                                </div>
                                <div class="carousel-item">
                                    <img src="<%=ResolveUrl("~/") %>App_Themes/ThemeEn/img/gallery-icon1.png" alt="#">
                                </div>
                                <div class="carousel-item">
                                    <img src="<%=ResolveUrl("~/") %>App_Themes/ThemeEn/img/gallery-icon1.png" alt="#">
                                </div>
                            </div>

                            <a class="carousel-control-prev" href="#demo7" data-slide="prev"></a>
                            <a class="carousel-control-next" href="#demo7" data-slide="next"></a>

                        </div>
                    </div>
                    <!-- /.modal-body -->
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>--%>
        <!-- /.modal -->


        <%-- <div class="modal" id="lightbox1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <iframe width="100%" height="600"
                            src="https://www.youtube.com/embed/tgbNymZ7vqY"></iframe>
                    </div>
                    <!-- /.modal-body -->
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>--%>
        <!-- /.modal -->

        <asp:LinkButton ID="lnkSubscribe" runat="server"></asp:LinkButton>
        <cc1:ModalPopupExtender runat="server" ID="mpeSuccess"
            TargetControlID="lnkSubscribe" BehaviorID="mpeSuccess"
            BackgroundCssClass="modalBackground" PopupControlID="panelNewsLetter"
            CancelControlID="btnNewLetterClose">
        </cc1:ModalPopupExtender>
        <asp:Panel ID="panelNewsLetter" runat="server" CssClass="modalPopup d_model_popup l_modelpopup" Style="display: none">
            <div class="modal-dialog modal-md">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-body">
                        <asp:LinkButton CssClass="close" data-dismiss="modal" runat="server" ID="btnNewLetterClose">&times;</asp:LinkButton>
                        <img src="<%=ResolveUrl("~/") %>App_Themes/ThemeAr/img/logo.png" />
                        <div class="divContent">
                            <strong>
                                <asp:Literal runat="server" ID="lblTitle" Text="Message"></asp:Literal>
                            </strong>
                            <p>
                                <asp:Literal runat="server" ID="lblMessage">                             
                                </asp:Literal>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>

        <%--  <script type="text/javascript" src="/Scripts/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="/Scripts/popper.js"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
        <script type="text/javascript" src="/Scripts/bootstrap-4.js"></script>
        <script type="text/javascript" src="/Scripts/mobile-swipe.js"></script>--%>

        <script type="text/javascript" src="/Scripts/1.12.4.jquery.min.js"></script>
        <script type="text/javascript" src="/Scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="/Scripts/owl.carousel.min.js"></script>
        
         
        <%=(Session["CurrentLanguage"].ToString() == Convert.ToInt32(EnumLanguage.Arabic).ToString() ? "<script type='text/javascript' src='/Scripts/custom.js'></script>" : "<script type='text/javascript' src='/Scripts/customen.js'></script>") %>


        <!-- Global site tag (gtag.js) - Google Analytics -->
        <%--<script async src="https://www.googletagmanager.com/gtag/js?id=UA-122954879-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() { dataLayer.push(arguments); }
            gtag('js', new Date());

            gtag('config', 'UA-122954879-1');
        </script>--%>

        <script>
            function toRedirect() {
                window.location.href = "new.aspx";
            }
        </script>
    </form>
</body>
</html>
