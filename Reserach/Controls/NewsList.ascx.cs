﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SiteWare.Entity.Entities;
using SiteWare.Entity.Common.Enums;
using SiteWare.Domain.Domains;
using SiteWare.Entity.Common.Entities;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Configuration;
using Siteware.Entity.Entities;


public partial class Controls_NewsList : System.Web.UI.UserControl
{
    DateTime currentDate = DateTime.Now;
    DataPager pager1;
    string lang = string.Empty;
    int newsCount = 1;
    private byte CurrLangID;
    protected void Page_Load(object sender, EventArgs e)
    {
        CurrLangID = Convert.ToByte(Session["CurrentLanguage"]);
        if (!IsPostBack)
        {
            if (Convert.ToInt32(Session["CurrentLanguage"]) == Convert.ToInt32(EnumLanguage.Arabic))
            {
                lang = "/ar";
            }
            else
            {
                lang = "/en";
            }
            FillNews();
            

        }
        if (Convert.ToInt32(Session["CurrentLanguage"]) == Convert.ToInt32(EnumLanguage.English))
        {
            pager1 = lstNews.FindControl("DataPager1") as DataPager;

        }
        else
        {
            pager1 = lstNews.FindControl("DataPager1") as DataPager;

        }
        if (pager1 != null)
        {
            pager1.PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
        }
    }

    protected void FillNews()
    {
        try
        {
            ResultList<NewsEntity> result = new ResultList<NewsEntity>();
            result = NewsDomain.GetNewsAllNotAsync();
            if (result.Status == ErrorEnums.Success)
            {
                //var res = result.List;
                //res = res.Where(s => !s.IsDeleted && s.IsPublished && s.PublishDate.Date <= currentDate.Date && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"])).OrderBy(s => s.NewsOrder).ThenByDescending(s => s.NewsDate).Take(1).ToList();
                //lblFirstNewsTitle.Text = res[0].Headline;
                //lblFirstNewsSummery.Text = res[0].Summary;
                //hdnNeesid.Value = res[0].NewsID.ToString();
                ////  hdnFirstNewsTime.Value = res[0].NewsDate.ToString();
                //DateTime DateNews = Convert.ToDateTime(res[0].NewsDate);
                //string Day = DateNews.ToString("dd", new CultureInfo("en-US"));
                //string Month = DateNews.ToString("MMMM", new CultureInfo("en-US"));
                //string Year = DateNews.ToString("yyyy", new CultureInfo("en-US"));
                //long Views = Convert.ToInt64(res[0].ViewCount);
                //lblFirstNewsTime.Text = "<span class='date'> " + Month + " " + Day + " " + Year + "</span> <i class='fa fa-eye'></i> " + Views + "";
                //lnkNewsDetail1.NavigateUrl = "";
                //imgfirstNews.ImageUrl = ConfigurationManager.AppSettings["ImagePath"].ToString() + res[0].NewsImage;

                //string title = Regex.Replace(lblFirstNewsTitle.Text, @"[\\:/*#.]+", "");

                //int ID = Convert.ToInt32(hdnNeesid.Value.ToString());
                //lnkNewsDetail1.NavigateUrl = lang + "/NewsPage/" + title + "/" + ID.ToString();

                var newsList = result.List.Where(s => !s.IsDeleted && s.IsPublished && s.PublishDate.Date <= currentDate.Date && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"])).OrderBy(s => s.NewsOrder).ThenByDescending(s => s.NewsDate).ToList();
                lstNews.DataSource = newsList.ToList();
                lstNews.DataBind();


            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void lstRecentNews_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                string lang = string.Empty;
                if (Convert.ToInt32(Session["CurrentLanguage"]) == Convert.ToInt32(EnumLanguage.Arabic))
                {
                    lang = "/ar";
                }
                else
                {
                    lang = "/en";
                }

                ListViewDataItem i = (ListViewDataItem)e.Item;
                var c = i.DataItemIndex;
                //HiddenField hdNewsID = (HiddenField)e.Item.FindControl("hdNewsID");
                HiddenField hdnnewsTime = (HiddenField)e.Item.FindControl("hdnnewsTime");
                Literal lbldate = (Literal)e.Item.FindControl("lbldate");

                //Image imgNews = (Image)e.Item.FindControl("imgNews");
                //imgNews.ImageUrl = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + imgNews.ImageUrl.Replace("~/", "~/Siteware/");

                Literal lblNewsTime = (Literal)e.Item.FindControl("lblNewsTime");
                //HiddenField hdnViewCount = (HiddenField)e.Item.FindControl("hdnViewCount");
                DateTime DateNews = Convert.ToDateTime(hdnnewsTime.Value);
                string Day = DateNews.ToString("dd", new CultureInfo("en-US"));
                string Month = DateNews.ToString("MMMM", new CultureInfo("en-US"));
                string Year = DateNews.ToString("yyyy", new CultureInfo("en-US"));
                //lblNewsTime.Text = "<span class='date'> " + Month + " " + Day + " " + Year + "</span> <i class='fa fa-eye'></i> " + hdnViewCount.Value + "";
                //HyperLink lnkNewsDetail2 = (HyperLink)e.Item.FindControl("lnkNewsDetail2");

                //Literal lblNewsTitle = (Literal)e.Item.FindControl("lblNewsTitle");
                //string title = Regex.Replace(lblNewsTitle.Text, @"[\\:/*#.]+", "");
                //int ID = Convert.ToInt32(hdNewsID.Value.ToString());
                //lnkNewsDetail2.NavigateUrl = lang + "/NewsPage/" + title + "/" + ID.ToString();
                HiddenField hdndate = (HiddenField)e.Item.FindControl("hdndate");
                var date = Convert.ToDateTime(hdndate.Value);
                if (CurrLangID == Convert.ToInt32(EnumLanguage.Arabic))
                {
                    //Get month and convert to arabic
                    var m = date.ToString("MMM");
                    var month = date.ToString("MMM", new CultureInfo("ar-AE"));

                    var Date = date.ToString("dd", new CultureInfo("ar-AE"));
                    var year = date.ToString("yyyy", new CultureInfo("ar-AE"));
                    //lbldate.Text = Date + " " + month + " ، " + year + " ";
                    lbldate.Text = "<strong>" + Date + "</strong><small> " + month + " </small>";
                }
                else
                {
                    //Get month and convert to arabic
                    var m = date.ToString("MMM");
                    var month = date.ToString("MMM", new CultureInfo("es-ES"));

                    var Date = date.ToString("dd", new CultureInfo("es-ES"));
                    var year = date.ToString("yyyy", new CultureInfo("es-ES"));
                    //lbldate.Text = Date + " " + month + " ، " + year + " ";
                    lbldate.Text = "<strong>" + Date + "</strong><small> " + month + " </small>";

                    //Convert NewsDate in given dateformat
                    //lbldate.Text = date.ToString("dd MMM, yyyy", new CultureInfo("en-US"));
                }
                Literal lblTitle = (Literal)e.Item.FindControl("lblTitle");
                HiddenField hdnID = (HiddenField)e.Item.FindControl("hdnID");

                HyperLink lnkMore = (HyperLink)e.Item.FindControl("lnkMore");
                string title = Regex.Replace(lblTitle.Text, @"[\\:/*#]+", " ");
                int ID = Convert.ToInt32(hdnID.Value.ToString());
                lnkMore.NavigateUrl = lang + "/NewsPage/" + title.Trim() + "/" + ID.ToString();


            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void lstNewsRow_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
    {
        try
        {
            DataPager pager1;
            pager1 = lstNews.FindControl("DataPager1") as DataPager;
            (pager1).SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            this.FillNews();
            upanel1.Focus();
        }
        catch (Exception ex)
        {

        }
    }
}