﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SiteWare.Entity.Entities;
using SiteWare.Entity.Common.Enums;
using SiteWare.Domain.Domains;
using SiteWare.Entity.Common.Entities;
using HtmlAgilityPack;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Globalization;

public partial class Pages_Page : SiteBasePage
{
    public DateTime currentDate = DateTime.Now;
    public int LangID = 1;
    protected void Page_Load(object sender, EventArgs e)
    {

        FillData();
        FillMedia();
        FillResearchandstudies();

        //FillNews();
        if (Convert.ToInt32(Session["CurrentLanguage"]) == (int)EnumLanguage.English)
        {
            LangID = 1;
        }
        else
        {
            LangID = 2;
        }
    }
    protected async void FillData()
    {
        try
        {
            string urllang = Page.RouteData.Values["Language"].ToString();
            string AliasPath = Page.RouteData.Values["RequestedPage"].ToString();
            string Alias = "/" + urllang + ConfigurationManager.AppSettings["RoutePath"].ToString() + AliasPath;
            List<String> keywordList = new List<String>();
            ResultEntity<PagesEntity> Result = new ResultEntity<PagesEntity>();
            ResultEntity<NavigationEntity> Result1 = new ResultEntity<NavigationEntity>();
            ResultList<PagesKeywordEntity> KeywordResult = new ResultList<PagesKeywordEntity>();
            var CountResult = await PagesDomain.UpdateViewCountByAliasPath(Alias);

            byte lID = 2;
            if (Convert.ToInt32(Session["CurrentLanguage"]) == (int)EnumLanguage.English)
            {
                lID = 1;
            }
            else
            {
                lID = 2;
            }


            Result = await PagesDomain.GetPagesByAliasPath(Alias, lID);
            //Result1 = await NavigationDomain.GetNavigationByUrl(Alias, 1);
            if (Result.Status == ErrorEnums.Success)
            {

                Session["pageid"] = Result.Entity.PageID;
                Session["Category"] = "Home";

                if (Result.Entity.IsList)
                {
                    //if (Result.Entity.PageID == 3)
                    //{
                    //    divMain.Attributes.Add("class", "about-us-content section news_section");
                    //}
                    //if (Result.Entity.PageID == 3028)
                    //{
                    //    divMain.Attributes.Add("class", "about-us-content section Events");
                    //}
                    //if (Result.Entity.PageID == 4042)
                    //{
                    //    divMain.Attributes.Add("class", "about-us-content section photo_albums");
                    //}
                    //if (Result.Entity.PageID == 4043)
                    //{
                    //    divMain.Attributes.Add("class", "about-us-content section video_gallery");
                    //}
                    //if (Result.Entity.PageID == 4049)
                    //{
                    //    divMain.Attributes.Add("class", "about-us-content section timeline");
                    //}
                    //if (Result.Entity.PageID == 4069)
                    //{
                    //    divMain.Visible = false;
                    //}

                    var controlname = Result.Entity.ListLink.Split('/');
                    int cnt = controlname.Count();
                    Control control = LoadControl("/Controls/" + controlname[cnt - 1]);
                    if (!string.IsNullOrEmpty(Result.Entity.ListLink.ToString()))
                    {

                        //lblContentDetails.Controls.Add(control);


                        //if (Result.Entity.PageID == 4069)
                        //{
                        //    lblDetail1.Controls.Add(control);
                        //}
                        //else
                        //{
                        //    lblDetail1.Visible = false;
                        //    lblDetail.Controls.Add(control);
                        //}


                        if (Result.Entity.ContentHTML.Length > 0)
                        {
                            lblContentDetails.Text = FetchLinksFromSource(Result.Entity.ContentHTML.ToString());
                        }

                        lblDetail1.Controls.Add(control);
                        
                        lblDetail1.Visible = true;
                        lblDetail1.Style.Add("display", "block");
                    }


                }
                else
                {
                    //lblDetail.Visible = false;                    
                    if (Result.Entity.ContentHTML.Length > 0)
                    {
                        lblTitleID.Text = Result.Entity.Name.ToString();
                        lblDate.Text = Result.Entity.PublishDate.ToString("dd/MM/yyyy");
                        lblViewCount.Text = Result.Entity.ViewCount.ToString();
                        lblContentDetails.Text = FetchLinksFromSource(Result.Entity.ContentHTML.ToString());
                    }
                }
                if (Result.Entity.ParentID > 0)
                {
                    ResultEntity<PagesEntity> ParentResult = new ResultEntity<PagesEntity>();
                    ParentResult = await PagesDomain.GetPagesByPageID(Result.Entity.ParentID);
                    lnkParentName.Text = ParentResult.Entity.Name;
                    lnkParentName.NavigateUrl = ParentResult.Entity.AliasPath;
                    lstParent.Visible = true;
                }
                else
                {
                    lstParent.Visible = false;
                }
                lblChildName.Text = Result.Entity.Name;
                //lblPageTitle.Text = Result.Entity.Name;
                Page.Title = Result.Entity.MetaTitle;

                Page.MetaDescription = Result.Entity.MetaDescription.ToString();

                KeywordResult = await PagesKeywordDomain.GetKeywordByPageID(Result.Entity.PageID);
                string keywords = string.Empty;
                if (KeywordResult.Status == ErrorEnums.Success)
                {
                    foreach (PagesKeywordEntity item in KeywordResult.List)
                    {
                        if (keywords.Length == 0)
                        {
                            keywords = item.Keyword;
                        }
                        else
                        {
                            keywords = keywords + "," + item.Keyword;
                        }
                    }
                }

                Page.MetaKeywords = keywords;

                HtmlMeta htmlMeta = new HtmlMeta();
                htmlMeta.HttpEquiv = "author";
                htmlMeta.Name = "author";
                htmlMeta.Content = Result.Entity.SEOAttribute;
                this.Page.Header.Controls.Add(htmlMeta);
            }
            else
            {
                Response.Redirect("/" + urllang + "/home", false);
            }
        }
        catch (Exception e)
        {
            string lang = string.Empty;
            //if (Convert.ToInt32(Session["CurrentLanguage"]) == Convert.ToInt32(EnumLanguage.Arabic))
            //{
            //    lang = "/ar";
            //}
            //else
            //{
            //    lang = "/en";
            //}
            //Response.Redirect(lang + "/home", false);
            //throw e;
        }
    }
    public string FetchLinksFromSource(string htmlSource)
    {
        HtmlDocument doc = new HtmlDocument();
        doc.LoadHtml(htmlSource);
        if (doc.DocumentNode.SelectNodes("//img") != null)
        {
            foreach (var img in doc.DocumentNode.SelectNodes("//img"))
            {
                string orig = img.Attributes["src"].Value;
                if (orig.Contains("http://") || orig.Contains("https://"))
                {
                    img.SetAttributeValue("src", orig);
                }
                else
                {
                    string newsrc = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + orig;
                    img.SetAttributeValue("src", newsrc);
                }

            }
        }
        if (doc.DocumentNode.SelectNodes("//table") != null)
        {
            foreach (var table in doc.DocumentNode.SelectNodes("//table"))
            {
                table.SetAttributeValue("class", "Tblres");
            }
        }
        return doc.DocumentNode.OuterHtml;
    }

    #region --> Get Media
    protected async void FillMedia()
    {
        try
        {
            ResultList<Plugin_Reserach_MediaEntity> Result = new ResultList<Plugin_Reserach_MediaEntity>();
            Result = await Plugin_Reserach_MediaDomain.GetAll();
            if (Result.Status == ErrorEnums.Success)
            {
                Random rand = new Random();



                lstMedia.DataSource = Result.List.Where(s => s.IsDelete == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishedDate <= currentDate).OrderBy(s=> rand.Next()).Take(1).ToList();
                lstMedia.DataBind();
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void lstMedia_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {

            Image imgMedia = (Image)e.Item.FindControl("imgMedia");

            if (!string.IsNullOrEmpty(imgMedia.ImageUrl))
            {
                imgMedia.ImageUrl = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + imgMedia.ImageUrl.Replace("~/", "~/Siteware/");
            }
        }
    }

    #endregion


    #region --> Get Research and studies
    protected void FillResearchandstudies()

    {
        try
        {
            ResultList<Plugin_Reserach_ResearchInnerEntity> Result = new ResultList<Plugin_Reserach_ResearchInnerEntity>();
            Result = Plugin_Reserach_ResearchInnerDomain.GetAllNotAsync();
            if (Result.Status == ErrorEnums.Success)
            {
                lstResearchAndPublication.DataSource = Result.List.Where(s => s.IsDelete == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishedDate <= currentDate).OrderBy(s => s.Order).Take(1).ToList();
                lstResearchAndPublication.DataBind();


            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void lstResearchAndPublication_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            Image imgPublication = (Image)e.Item.FindControl("imgPublication");
            HiddenField hdnCategory = (HiddenField)e.Item.FindControl("hdnCategory");
            HiddenField hdnID = (HiddenField)e.Item.FindControl("hdnID");
            Literal lblCategory = (Literal)e.Item.FindControl("lblCategory");
            Literal lblTitle = (Literal)e.Item.FindControl("lblTitle");

            if (!string.IsNullOrEmpty(imgPublication.ImageUrl))
            {
                imgPublication.ImageUrl = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + imgPublication.ImageUrl.Replace("~/", "~/Siteware/");
            }

            int Cateid = Convert.ToInt32(hdnCategory.Value);

            var getcatedata = Lookup_Reserach_PublicationCategoryDomain.GetByIDNotAsync(Cateid);
            if (getcatedata.Status == ErrorEnums.Success)
            {
                lblCategory.Text = getcatedata.Entity.name;
            }

        }
    }

    #endregion

    protected async void LinkButtonID_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)(sender);
        string yourValue = btn.CommandArgument;

        long MediaId = Convert.ToInt64(btn.CommandArgument);

        Plugin_Reserach_MediaEntity entity = new Plugin_Reserach_MediaEntity();
        entity.MediaID = MediaId;
        //entity.ViewCount = Result.Entity.ViewCount;
        var UpdateCount = await Plugin_Reserach_MediaDomain.UpdateNewsViewCount(entity.MediaID);

        FillMedia();
    }
}