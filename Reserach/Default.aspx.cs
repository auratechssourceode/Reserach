﻿using SiteWare.Domain.Domains;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Common.Enums;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SiteWare.DataAccess.Repositories;
using HtmlAgilityPack;
using System.Web.Services;
using Siteware.Entity.Entities;
//using Siteware.Domain.Domains;
using Newtonsoft.Json.Linq;


public partial class _Default : SiteBasePage
{
    public DateTime currentDate = DateTime.Now;
    public int LangID = 0;
    string lang = string.Empty;
    private byte CurrLangID;


    protected void Page_Load(object sender, EventArgs e)
    {
        CurrLangID = Convert.ToByte(Session["CurrentLanguage"]);
        if (Convert.ToInt32(Session["CurrentLanguage"]) == (int)EnumLanguage.English)
        {
            LangID = 1;
            lang = "/en";
            //lblLangID.Value = "1";
            // lnkLanguage.Text = "عربى";
        }
        else
        {
            LangID = 2;
            lang = "/ar";
            //lblLangID.Value = "2";
            // lnkLanguage.Text = "English";
        }

        if (!IsPostBack)
        {
            if (RouteData.Values["language"] == null)
            {
                Response.Redirect("ar/Home");
            }
            ResultList<SettingEntity> Result = new ResultList<SettingEntity>();
            Result = SettingDomain.GetSettingAllWithoutAsync();
            if (Result.Status == ErrorEnums.Success)
            {
                Result.List = Result.List.Where(s => s.LanguageID == LangID && s.IsDeleted == false && s.IsPublished).ToList();
                if (Result.List.Count >= 1)
                {
                    Page.Title = Result.List[0].PageName;
                    imgLogo.ImageUrl = ConfigurationManager.AppSettings["ImagePath"].ToString() + Result.List[0].Logo;
                    Mobilelogo.ImageUrl = ConfigurationManager.AppSettings["ImagePath"].ToString() + Result.List[0].Logo;
                    //imgFooterLogo.ImageUrl = ConfigurationManager.AppSettings["ImagePath"].ToString() + Result.List[0].FooterLogo;
                    lblCopyright.Text = FetchLinksFromSource(Result.List[0].CopyRights);
                }


            }

            FillSecondNavigation();
            FillSocialIcon();
            FillFooterNavigation();
            FillAboutUs();
            FillContactUS1();
            FillMainNavigation();
            //FillTwitter();
            FillArticle();
            FillPublication();
            FillNews();
            FillMedia();
            //FillLanguage();
            FillResearchandstudies();
            FillSlider();

        }
        else
        { }
    }



    #region--> FillSecondNavigation
    protected async void FillSecondNavigation()
    {
        try
        {
            ResultList<SecondNavigationEntity> Result = new ResultList<SecondNavigationEntity>();
            Result = await SecondNavigationDomain.GetNavigationAll();

            if (Result.Status == ErrorEnums.Success)
            {
                lstSecondNav.DataSource = Result.List.Where(s => s.IsPublished && !s.IsDeleted && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishDate <= currentDate).OrderBy(s => s.MenuOrder).Take(5).ToList();
                lstSecondNav.DataBind();

                MobilelstSecondNav.DataSource = Result.List.Where(s => s.IsPublished && !s.IsDeleted && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishDate <= currentDate).OrderBy(s => s.MenuOrder).Take(5).ToList();
                MobilelstSecondNav.DataBind();
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void lstSecondNav_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                HyperLink lnkSecondNav = (HyperLink)e.Item.FindControl("lnkSecondNav");

                if (lnkSecondNav.Target == "1")
                {
                    lnkSecondNav.Target = "_blank";
                }
                else
                {
                    lnkSecondNav.Target = "_parent";
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void MobilelstSecondNav_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                HyperLink lnkSecondNav = (HyperLink)e.Item.FindControl("lnkSecondNav");

                if (lnkSecondNav.Target == "1")
                {
                    lnkSecondNav.Target = "_blank";
                }
                else
                {
                    lnkSecondNav.Target = "_parent";
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    #endregion

    #region--> FillSocialIcon
    protected void FillSocialIcon()
    {
        try
        {
            ResultList<PluginSocialIconEntity> Result = new ResultList<PluginSocialIconEntity>();
            Result = PluginSocialIconDomain.GetPluginSocialIconAllNotAsync();

            if (Result.Status == ErrorEnums.Success)
            {
                lstSocialIcon.DataSource = Result.List.Where(q => q.LanguageID == CurrLangID && q.IsDeleted == false).OrderBy(c => c.ID);
                lstSocialIcon.DataBind();

                MobilelstSocialIcon.DataSource = Result.List.Where(q => q.LanguageID == CurrLangID && q.IsDeleted == false).OrderBy(c => c.ID);
                MobilelstSocialIcon.DataBind();
                //lstSocialIconFooter.DataSource = Result.List.Where(q => q.LanguageID == CurrLangID).OrderBy(c => c.ID);
                //lstSocialIconFooter.DataBind();


            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void lstSocialIcon_ItemDataBound(object sender, ListViewItemEventArgs e)
    {

        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            Image imgsocial = (Image)e.Item.FindControl("ImageSocialMedia");
            HiddenField HdnimgURL = (HiddenField)e.Item.FindControl("HdnimgURL");
            if (!string.IsNullOrEmpty(imgsocial.ImageUrl))
            {
                imgsocial.ImageUrl = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + imgsocial.ImageUrl.Replace("~/", "~/Siteware/");
            }
            if (HdnimgURL != null)
            {
                if (!string.IsNullOrEmpty(HdnimgURL.Value))
                {
                    string imageover = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + HdnimgURL.Value.Replace("~/", "~/Siteware/");
                    imgsocial.Attributes.Add("onmouseover", "this.src='" + imageover + "'"); imgsocial.Attributes.Add("onmouseout", "this.src='" + imgsocial.ImageUrl + "'");
                }
            }
        }
    }


    protected void MobilelstSocialIcon_ItemDataBound(object sender, ListViewItemEventArgs e)
    {

        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            Image imgsocial = (Image)e.Item.FindControl("ImageSocialMedia");
            HiddenField HdnimgURL = (HiddenField)e.Item.FindControl("HdnimgURL");
            if (!string.IsNullOrEmpty(imgsocial.ImageUrl))
            {
                imgsocial.ImageUrl = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + imgsocial.ImageUrl.Replace("~/", "~/Siteware/");
            }
            if (HdnimgURL != null)
            {
                if (!string.IsNullOrEmpty(HdnimgURL.Value))
                {
                    string imageover = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + HdnimgURL.Value.Replace("~/", "~/Siteware/");
                    imgsocial.Attributes.Add("onmouseover", "this.src='" + imageover + "'"); imgsocial.Attributes.Add("onmouseout", "this.src='" + imgsocial.ImageUrl + "'");
                }
            }
        }
    }

    #endregion

    #region--> FillFooterNavigation
    protected void FillFooterNavigation()
    {
        try
        {
            ResultList<FooterNavigationEntity> Result = new ResultList<FooterNavigationEntity>();
            Result = FooterNavigationDomain.GetFooterAllWithoutAsync();
            if (Result.Status == ErrorEnums.Success)
            {
                //long _parentID = 0;

                //var record = Result.List.Where(s => s.ParentID == _parentID && s.IsDeleted == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishDate <= currentDate).OrderBy(s => s.MenuOrder).FirstOrDefault();
                //if (record != null)
                //{
                //    _parentID = record.ID;
                //    lblFooterNavTitle.Text = record.Title;
                //}

                //Result.List = Result.List.Where(s => s.ParentID == _parentID && s.IsDeleted == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishDate <= currentDate).OrderBy(s => s.MenuOrder).ToList();

                var AllResultList = Result.List.Where(s => s.ParentID == 0 && s.IsDeleted == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishDate <= currentDate).OrderBy(s => s.MenuOrder).ToList();

                lstFooterNav1.DataSource = AllResultList.Where(s => s.ParentID == 0 && s.IsDeleted == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishDate <= currentDate).OrderBy(s => s.MenuOrder).Take(1).ToList();
                lstFooterNav1.DataBind();

                lstFooterNav2.DataSource = AllResultList.Where(s => s.ParentID == 0 && s.IsDeleted == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishDate <= currentDate).OrderBy(s => s.MenuOrder).Skip(1).Take(1).ToList();
                lstFooterNav2.DataBind();
            }
        }
        catch (Exception e)
        {
        }
    }
    protected void lstFooterNav1_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                HiddenField hdnMenuID = (HiddenField)e.Item.FindControl("hdnMenuID");
                ListView lstSubFooterNav1 = (ListView)e.Item.FindControl("lstSubFooterNav1");

                long Perentid = Convert.ToInt64(hdnMenuID.Value);

                ResultList<FooterNavigationEntity> Result = new ResultList<FooterNavigationEntity>();
                Result = FooterNavigationDomain.GetFooterAllWithoutAsync();
                if (Result.Status == ErrorEnums.Success)
                {

                    lstSubFooterNav1.DataSource = Result.List.Where(s => s.ParentID == Perentid && s.IsDeleted == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishDate <= currentDate).OrderBy(s => s.MenuOrder).ToList();
                    lstSubFooterNav1.DataBind();
                }


            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void lstSubFooterNav1_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {

                HyperLink lnkNavi = (HyperLink)e.Item.FindControl("lnkNavi");

                if (string.IsNullOrEmpty(lnkNavi.NavigateUrl) || lnkNavi.NavigateUrl == "#")
                {
                    lnkNavi.NavigateUrl = "javascript:void(0);";
                }
                //if (lnkNavi.Target == "1")
                //{
                //    lnkNavi.Target = "_blank";
                //}
                //else
                //{
                //    lnkNavi.Target = "_parent";
                //}
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void lstFooterNav2_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                HiddenField hdnMenuID = (HiddenField)e.Item.FindControl("hdnMenuID");
                ListView lstSubFooterNav2 = (ListView)e.Item.FindControl("lstSubFooterNav2");

                long Perentid = Convert.ToInt64(hdnMenuID.Value);

                ResultList<FooterNavigationEntity> Result = new ResultList<FooterNavigationEntity>();
                Result = FooterNavigationDomain.GetFooterAllWithoutAsync();
                if (Result.Status == ErrorEnums.Success)
                {

                    lstSubFooterNav2.DataSource = Result.List.Where(s => s.ParentID == Perentid && s.IsDeleted == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishDate <= currentDate).OrderBy(s => s.MenuOrder).ToList();
                    lstSubFooterNav2.DataBind();
                }


            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void lstSubFooterNav2_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {

                HyperLink lnkNavi = (HyperLink)e.Item.FindControl("lnkNavi");

                if (string.IsNullOrEmpty(lnkNavi.NavigateUrl) || lnkNavi.NavigateUrl == "#")
                {
                    lnkNavi.NavigateUrl = "javascript:void(0);";
                }
                //if (lnkNavi.Target == "1")
                //{
                //    lnkNavi.Target = "_blank";
                //}
                //else
                //{
                //    lnkNavi.Target = "_parent";
                //}
            }
        }
        catch (Exception ex)
        {
        }
    }
    #endregion


    #region --> AboutUs    
    protected void FillAboutUs()
    {
        try
        {
            ResultList<Plugin_AboutUsEntity> Result = new ResultList<Plugin_AboutUsEntity>();
            Result = Plugin_AboutUsDomain.GetAllNotAsync();
            if (Result.Status == ErrorEnums.Success)
            {


                lstAboutUS.DataSource = Result.List.Where(s => s.IsDeleted == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishedDate <= currentDate).OrderBy(s => s.ID).Take(1).ToList();
                lstAboutUS.DataBind();
            }


        }
        catch (Exception ex)
        {
        }
    }
    protected void lstAboutUS_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            Image aboutimg = (Image)e.Item.FindControl("aboutimg");

            if (!string.IsNullOrEmpty(aboutimg.ImageUrl))
            {
                aboutimg.ImageUrl = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + aboutimg.ImageUrl.Replace("~/", "~/Siteware/");
            }

        }
    }

    #endregion


    #region --> Contact US

    protected async void FillContactUS1()
    {
        try
        {

            ResultList<PluginContactUsEntity> Result = new ResultList<PluginContactUsEntity>();

            Result = await PluginContactUsDomain.GetPluginContactAll();
            if (Result.Status == ErrorEnums.Success)
            {
                lstContactUSLocation.DataSource = Result.List.Where(s => s.IsDeleted == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishedDate <= currentDate).OrderBy(s => s.ID).Take(1).ToList();
                lstContactUSLocation.DataBind();


                lstContactUSLocation2.DataSource = Result.List.Where(s => s.IsDeleted == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishedDate <= currentDate).OrderBy(s => s.ID).Skip(1).Take(1).ToList();
                lstContactUSLocation2.DataBind();

                lstContactUSLocation3.DataSource = Result.List.Where(s => s.IsDeleted == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishedDate <= currentDate).OrderBy(s => s.ID).Skip(2).Take(1).ToList();
                lstContactUSLocation3.DataBind();
            }




        }
        catch (Exception ex)
        {
        }
    }

    protected void lstContactUSLocation_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            Image img = (Image)e.Item.FindControl("img");

            if (!string.IsNullOrEmpty(img.ImageUrl))
            {
                img.ImageUrl = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + img.ImageUrl.Replace("~/", "~/Siteware/");
            }

        }
    }

    protected void lstContactUSLocation2_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            Image img = (Image)e.Item.FindControl("img");
            ListView lstSocialIconFooter = (ListView)e.Item.FindControl("lstSocialIconFooter");

            if (!string.IsNullOrEmpty(img.ImageUrl))
            {
                img.ImageUrl = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + img.ImageUrl.Replace("~/", "~/Siteware/");
            }


            ResultList<PluginSocialIconEntity> Result = new ResultList<PluginSocialIconEntity>();
            Result = PluginSocialIconDomain.GetPluginSocialIconAllNotAsync();

            if (Result.Status == ErrorEnums.Success)
            {

                lstSocialIconFooter.DataSource = Result.List.Where(q => q.LanguageID == CurrLangID).OrderBy(c => c.ID);
                lstSocialIconFooter.DataBind();


            }

        }
    }


    protected void lstContactUSLocation3_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            Image img = (Image)e.Item.FindControl("img");

            if (!string.IsNullOrEmpty(img.ImageUrl))
            {
                img.ImageUrl = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + img.ImageUrl.Replace("~/", "~/Siteware/");
            }

        }
    }


    #endregion


    #region --> Main NAvigation

    protected async void FillMainNavigation()
    {
        try
        {
            ResultList<NavigationEntity> Result = new ResultList<NavigationEntity>();
            Result = NavigationDomain.GetNavigationWebsiteAllNotAsync();
            if (Result.Status == ErrorEnums.Success)
            {
                lstNavigation.DataSource = Result.List.Where(q => q.ParentID == 0 && q.IsPublished == true && q.LanguageID == CurrLangID && q.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && q.PublishDate <= currentDate).Take(7).ToList();
                lstNavigation.DataBind();

                MobilelstNavigation.DataSource = Result.List.Where(q => q.ParentID == 0 && q.IsPublished == true && q.LanguageID == CurrLangID && q.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && q.PublishDate <= currentDate).Take(7).ToList();
                MobilelstNavigation.DataBind();
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void lstNavigation_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        ResultList<NavigationEntity> Result = new ResultList<NavigationEntity>();
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {

            HiddenField ID = (HiddenField)e.Item.FindControl("hdnId");
            HyperLink lnk = (HyperLink)e.Item.FindControl("lnkMenu");
            //HtmlGenericControl lstmenu = (HtmlGenericControl)e.Item.FindControl("lstmenu");
            HtmlGenericControl ulsubmenu = (HtmlGenericControl)e.Item.FindControl("ulsubmenu");

            if (lnk.Target == "1")
            { lnk.Target = "_blank"; }
            else
            { lnk.Target = "_parent"; }


            if (string.IsNullOrEmpty(lnk.NavigateUrl) || lnk.NavigateUrl == "#")
            {
                lnk.NavigateUrl = "javascript:void(0);";
                lnk.Target = "_parent";
            }

            //if (ID.Value == "1")
            //{
            //    lstmenu.Attributes.Add("class", "active");
            //}
            ListView lstSubNavgation = (ListView)e.Item.FindControl("lstSubNavgation");

            Result = NavigationDomain.GetNavigationByParentMenuID_Website(Convert.ToInt32(ID.Value));

            if (Result.List.Count == 0)
            {
                //lstmenu.Attributes.Add("class", "")
                if (string.IsNullOrEmpty(lnk.NavigateUrl) || lnk.NavigateUrl == "#")
                {
                    lnk.NavigateUrl = "javascript:void(0);";
                }
                ulsubmenu.Visible = false;
            }
            else
            {
                ulsubmenu.Attributes.Add("class", "dropdown-menu");
                lnk.Attributes.Add("class", "dropdown-toggle");
                lnk.Attributes.Add("data-toggle", "dropdown");
                lnk.Attributes.Add("role", "button");
                lnk.Attributes.Add("aria-haspopup", "true");
                lnk.Attributes.Add("aria-expanded", "false");

                //lstmenu.Attributes.Add("class", "nav-item dropdown");
                //lnk.Attributes.Add("class", "nav-link dropdown-toggle");
                //lnk.Attributes.Add("data-toggle", "dropdown");
                //lnk.Attributes.Add("aria-haspopup", "true");
                //lnk.Attributes.Add("aria-expanded", "false");
                //lnk.Attributes.Add("role", "button");
                lnk.NavigateUrl = "javascript:void(0);";
                lnk.Target = "_parent";
                lnk.Text = lnk.Text + "<span class='fa fa-angle-down'></span>";
                lstSubNavgation.DataSource = Result.List.Where(a => a.IsDeleted == false && a.IsPublished == true).OrderBy(a => a.MenuOrder);
                lstSubNavgation.DataBind();
            }
        }
    }


    protected void lstSubNavgation_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        ResultList<NavigationEntity> Result = new ResultList<NavigationEntity>();
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {


            HyperLink lnkSubMenu = (HyperLink)e.Item.FindControl("lnkSubMenu");


            if (lnkSubMenu.Target == "1")
            { lnkSubMenu.Target = "_blank"; }
            else
            { lnkSubMenu.Target = "_parent"; }

        }
    }


    protected void MobilelstNavigation_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        ResultList<NavigationEntity> Result = new ResultList<NavigationEntity>();
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            HiddenField ID = (HiddenField)e.Item.FindControl("hdnId");
            HyperLink lnk = (HyperLink)e.Item.FindControl("lnkMenu");
            HtmlGenericControl ulsubmenu = (HtmlGenericControl)e.Item.FindControl("ulsubmenu");
            if (lnk.Target == "1")
            { lnk.Target = "_blank"; }
            else
            { lnk.Target = "_parent"; }
            if (string.IsNullOrEmpty(lnk.NavigateUrl) || lnk.NavigateUrl == "#")
            {
                lnk.NavigateUrl = "javascript:void(0);";
                lnk.Target = "_parent";
            }
            ListView MobilelstSubNavgation = (ListView)e.Item.FindControl("MobilelstSubNavgation");
            Result = NavigationDomain.GetNavigationByParentMenuID_Website(Convert.ToInt32(ID.Value));
            if (Result.List.Count == 0)
            {
                //lstmenu.Attributes.Add("class", "")
                if (string.IsNullOrEmpty(lnk.NavigateUrl) || lnk.NavigateUrl == "#")
                {
                    lnk.NavigateUrl = "javascript:void(0);";
                }
                ulsubmenu.Visible = false;
            }
            else
            {
                ulsubmenu.Attributes.Add("class", "dropdown-menu");
                lnk.Attributes.Add("class", "dropdown-toggle");
                lnk.Attributes.Add("data-toggle", "dropdown");
                lnk.Attributes.Add("role", "button");
                lnk.Attributes.Add("aria-haspopup", "true");
                lnk.Attributes.Add("aria-expanded", "false");

                //lstmenu.Attributes.Add("class", "nav-item dropdown");
                //lnk.Attributes.Add("class", "nav-link dropdown-toggle");
                //lnk.Attributes.Add("data-toggle", "dropdown");
                //lnk.Attributes.Add("aria-haspopup", "true");
                //lnk.Attributes.Add("aria-expanded", "false");
                //lnk.Attributes.Add("role", "button");
                lnk.NavigateUrl = "javascript:void(0);";
                lnk.Target = "_parent";
                lnk.Text = lnk.Text + "<span class='fa fa-angle-down'></span>";
                MobilelstSubNavgation.DataSource = Result.List.Where(a => a.IsDeleted == false && a.IsPublished == true).OrderBy(a => a.MenuOrder);
                MobilelstSubNavgation.DataBind();
            }
        }
    }
    protected void MobilelstSubNavgation_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        ResultList<NavigationEntity> Result = new ResultList<NavigationEntity>();
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            HyperLink lnkSubMenu = (HyperLink)e.Item.FindControl("lnkSubMenu");
            if (lnkSubMenu.Target == "1")
            { lnkSubMenu.Target = "_blank"; }
            else
            { lnkSubMenu.Target = "_parent"; }

        }
    }

    #endregion


    #region--> Others
    protected void lnkSubmitForm_Click(object sender, EventArgs e)
    {
        try
        {
            Page.Validate("g1");
            if (Page.IsValid)
            {

                //    ResultEntity<ContactUsFormEntity> result = new ResultEntity<ContactUsFormEntity>();
                //    ContactUsFormEntity entity = new ContactUsFormEntity();
                //    entity.Name = txtName.Text;
                //    entity.Email = txtEmail.Text;
                //    entity.Title = txtInterest.Text;
                //    entity.Message = txtMessage.Text;
                //    entity.AddDate = DateTime.Now;
                //    entity.IsDeleted = false;
                //    entity.Contact = 0;

                //    result = ContactUsFormDomain.InsertContactUsForm(entity);
                //    if (result.Status == ErrorEnums.Success)
                //    {
                //        txtName.Text = "";
                //        txtName.Style.Add("border", "0");
                //        txtEmail.Text = "";
                //        txtEmail.Style.Add("border", "0");
                //        txtInterest.Text = "";
                //        txtInterest.Style.Add("border", "0");
                //        txtMessage.Text = "";
                //        lblMessage.Text = "Your Inqiry submitted successfully";
                //        mpeInquiry.Show();
                //    }

                //}
                //else
                //{

                //    if (!string.IsNullOrEmpty(txtName.Text))
                //    {

                //        txtName.Style.Add("border", "0");
                //    }
                //    else
                //    {
                //        txtName.Style.Add("border", "1px solid #ff0000");

                //    }
                //    if (!string.IsNullOrEmpty(txtInterest.Text))
                //    {

                //        txtInterest.Style.Add("border", "0");
                //    }
                //    else
                //    {
                //        txtInterest.Style.Add("border", "1px solid #ff0000");

                //    }

                //    if (!string.IsNullOrEmpty(txtEmail.Text))
                //    {
                //        bool isMatch = Regex.IsMatch(txtEmail.Text, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
                //        if (isMatch)
                //        {

                //            txtEmail.Style.Add("border", "0");
                //        }
                //        else
                //        {
                //            txtEmail.Style.Add("border", "1px solid #ff0000");
                //        }
                //    }
                //    else
                //    {
                //        txtEmail.Style.Add("border", "1px solid #ff0000");
                //    }
                //    lnkSubmitForm.Focus();

            }

        }
        catch
        {
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            //string SearchText = string.Empty;
            //string lang = string.Empty;
            //if (!string.IsNullOrEmpty(txtSearch.Text))
            //    SearchText = txtSearch.Text.Trim();
            //if (!string.IsNullOrEmpty(txtMobSearch.Text))
            //    SearchText = txtMobSearch.Text.Trim();
            //if (LangID == 1)
            //{
            //    lang = "en";
            //}
            //else
            //{
            //    lang = "ar";
            //}

            //Response.Redirect("~/DetailsPage/" + lang + "/SearchPage.aspx?keyword=" + SearchText, false);
        }
        catch (Exception ex)
        {
        }
    }
    public string FetchLinksFromSource(string htmlSource)
    {
        HtmlDocument doc = new HtmlDocument();
        doc.LoadHtml(htmlSource);
        if (doc.DocumentNode.SelectNodes("//img") != null)
        {
            foreach (var img in doc.DocumentNode.SelectNodes("//img"))
            {
                string orig = img.Attributes["src"].Value;
                string newsrc = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + orig;
                img.SetAttributeValue("src", newsrc);
            }
        }
        return doc.DocumentNode.OuterHtml;
    }
    //protected void txtSearch_TextChanged(object sender, EventArgs e)
    //{
    //    try
    //    {

    //    }
    //    catch (Exception ex)
    //    {
    //    }
    //}
    protected void lnkLanguage_Click(object sender, EventArgs e)
    {
        //if (lblLangID.Value == "2")
        //{
        //    lblTitle.Text = "Arabic language is coming soon";
        //    lblMessage.Text = "الموقع باللغة العربية تحت الانشاء";
        //    mpeSuccess.Show();
        //}
    }
    #endregion

    protected void btnNewLetterClose_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Default.aspx");
    }

    #region--> Get Tweets | ADD | Jigar Patel | 02052019
    public class Tweets
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public string Link { get; set; }
        public List<string> hashTags { get; set; }
        public string UserName { get; set; }
        public string ScreenName { get; set; }
        public string PostDate { get; set; }
        public string PostTime { get; set; }
    }
    protected void FillTwitter()
    {
        try
        {
            List<Tweets> listTweet = new List<Tweets>();
            //--------------------- Get Twitter Tweets --------------------------------------
            var twitter = new Twitter
            {
                OAuthConsumerKey = ConfigurationManager.AppSettings["APIKey"].ToString(),
                OAuthConsumerSecret = ConfigurationManager.AppSettings["APISecret"].ToString()
            };
            IEnumerable<dynamic> twitts = twitter.GetAccessToken("KHF_NHF", 50).Result;

            foreach (var t in twitts)
            {
                Tweets tw = new Tweets();
                var tID = (JValue)t["id"];
                var tText = (JValue)t["text"];
                var hashTags = t["entities"]["hashtags"];
                var tName = t["entities"]["user_mentions"];
                var tDate = (JValue)t["created_at"];
                string[] dateTime = tDate.ToString().Split(' ');

                tw.PostDate = dateTime[2] + " " + dateTime[1] + " " + dateTime[5];
                tw.PostTime = dateTime[3];

                List<string> htag = new List<string>();
                foreach (var h in hashTags)
                {
                    try
                    {
                        string tags = string.Empty;
                        string t1 = Convert.ToString((JValue)h["text"]);
                        tags = "#" + t1 + " ";
                        htag.Add(tags);

                    }
                    catch (Exception ex)
                    {
                    }
                }
                tw.hashTags = htag;


                foreach (var name in tName)
                {
                    try
                    {
                        string n1 = Convert.ToString((JValue)name["screen_name"]);
                        string n2 = Convert.ToString((JValue)name["name"]);
                        tw.ScreenName = n1;
                        tw.UserName = n2;
                    }
                    catch
                    {
                    }
                }
                try
                {
                    var resultLink = t["extended_entities"];
                    var tLink = resultLink["media"][0]["url"];
                    if (tLink != null)
                        tw.Link = Convert.ToString(tLink);
                    else
                    {
                        tw.Link = "https://twitter.com/i/web/status/" + tID;
                    }
                }
                catch
                {
                    try
                    {
                        var resultLink1 = t["entities"];
                        var tLink1 = resultLink1["urls"][0]["expanded_url"];
                        if (tLink1 != null)
                            tw.Link = Convert.ToString(tLink1);
                    }
                    catch
                    {
                        tw.Link = "https://twitter.com/i/web/status/" + tID;
                    }
                }
                if (tID != null)
                    tw.Id = Convert.ToString(tID.Value);
                if (tText != null)
                    tw.Text = Convert.ToString(tText.Value);


                if (htag.Count > 0)
                {
                    foreach (var a in htag)
                    {
                        try
                        {
                            tw.Text = tw.Text.Replace(a, "<span>" + a + "</span>");
                        }
                        catch
                        {
                        }
                    }
                }

                // tw.hashTags.Add(tags);

                listTweet.Add(tw);
            }
            if (listTweet.Count >= 0)
            {
                //lstTweets.DataSource = listTweet.Take(2);
                //lstTweets.DataBind();
            }
        }
        catch (Exception ex)
        {
            //throw ex;
        }
    }
    protected void lstTweets_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                Literal lblTime = (Literal)e.Item.FindControl("lblTime");
                Literal lblName2 = (Literal)e.Item.FindControl("lblName2");
                var dateTime = DateTime.ParseExact(lblTime.Text, "H:mm:ss", null, System.Globalization.DateTimeStyles.None);
                lblTime.Text = dateTime.ToString("hh:mm tt");
                if (!string.IsNullOrEmpty(lblName2.Text))
                    lblName2.Text = "@" + lblName2.Text;
            }
        }
        catch (Exception ex)
        {
        }

    }
    #endregion


    #region --> Get Article
    protected void FillArticle()
    {
        try
        {
            ResultList<Plugin_Reserach_ArticleEntity> Result = new ResultList<Plugin_Reserach_ArticleEntity>();
            Result = Plugin_Reserach_ArticleDomain.GetAllNotAsync();
            if (Result.Status == ErrorEnums.Success)
            {
                //lstBennerArticle.DataSource = Result.List.Where(s => s.IsDelete == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishedDate <= currentDate && s.IsShowSlider == true).OrderBy(s => s.Order).ThenByDescending(s => s.ArticleDate).ToList();
                //lstBennerArticle.DataBind();

                lstArticle.DataSource = Result.List.Where(s => s.IsDelete == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishedDate <= currentDate && s.IsShowSlider == false).OrderBy(s => s.Order).Take(3).ToList();
                lstArticle.DataBind();

                lstSubArticle.DataSource = Result.List.Where(s => s.IsDelete == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishedDate <= currentDate && s.IsShowSlider == false).OrderBy(s => s.Order).ThenByDescending(s => s.ArticleDate).Take(3).ToList();
                lstSubArticle.DataBind();
            }


           


        }
        catch (Exception ex)
        {
        }
    }

    int loopArticle = 1;
    protected void lstBennerArticle_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {


            
           HiddenField hdnDataType = (HiddenField)e.Item.FindControl("hdnDataType");



            Image imgArticle = (Image)e.Item.FindControl("imgArticle");
            HtmlGenericControl itemdiv = (HtmlGenericControl)e.Item.FindControl("itemdiv");
            HiddenField hdnID = (HiddenField)e.Item.FindControl("hdnID");
            Literal lblTitle = (Literal)e.Item.FindControl("lblTitle");


            if (!string.IsNullOrEmpty(imgArticle.ImageUrl))
            {
                imgArticle.ImageUrl = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + imgArticle.ImageUrl.Replace("~/", "~/Siteware/");
            }

            if (loopArticle == 1)
            {
                itemdiv.Attributes.Add("class", "item active");
            }
            else
            {

                itemdiv.Attributes.Add("class", "item");

            }


           

            string type = hdnDataType.Value;
            if(type == "Article")
            {

                HyperLink lnkMore = (HyperLink)e.Item.FindControl("lnkMore");
                string title = Regex.Replace(lblTitle.Text, @"[\\:/*#]+", " ");
                long ID = Convert.ToInt64(hdnID.Value.ToString());
                lnkMore.NavigateUrl = lang + "/ArticlePage/" + title.Trim() + "/" + ID.ToString();


                HiddenField hdnCategory = (HiddenField)e.Item.FindControl("hdnCategory");
                Literal lblCategory = (Literal)e.Item.FindControl("lblCategory");
                int Cateid = Convert.ToInt32(hdnCategory.Value);
                var getcatedata = Lookup_Reserach_ArticleCategoryDomain.GetByIDNotAsync(Cateid);
                if (getcatedata.Status == ErrorEnums.Success)
                {
                    lblCategory.Text = getcatedata.Entity.name;
                }

            }

            if (type == "News")
            {

                HyperLink lnkMore = (HyperLink)e.Item.FindControl("lnkMore");
                string title = Regex.Replace(lblTitle.Text, @"[\\:/*#]+", " ");
                long ID = Convert.ToInt64(hdnID.Value.ToString());
                lnkMore.NavigateUrl = lang + "/NewsPage/" + title.Trim() + "/" + ID.ToString();

                Literal lblCategory = (Literal)e.Item.FindControl("lblCategory");

                if (lang == "/en") {
                    lblCategory.Text = "News";

                }
                else {

                    lblCategory.Text = "أخبار";
                }
                
               
            }

            if (type == "Slider")
            {

                Literal lblCategory = (Literal)e.Item.FindControl("lblCategory");


                if (lang == "/en")
                {
                    lblCategory.Text = "Slider";

                }
                else
                {

                    lblCategory.Text = "متفرقات";
                }

               

            }


            loopArticle++;

        }
    }

    protected void lstArticle_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            Image imgArticle = (Image)e.Item.FindControl("imgArticle");
            if (!string.IsNullOrEmpty(imgArticle.ImageUrl))
            {
                imgArticle.ImageUrl = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + imgArticle.ImageUrl.Replace("~/", "~/Siteware/");
            }
            HiddenField hdnCategory = (HiddenField)e.Item.FindControl("hdnCategory");
            Literal lblCategory = (Literal)e.Item.FindControl("lblCategory");
            int Cateid = Convert.ToInt32(hdnCategory.Value);
            var getcatedata = Lookup_Reserach_ArticleCategoryDomain.GetByIDNotAsync(Cateid);
            if (getcatedata.Status == ErrorEnums.Success)
            {
                lblCategory.Text = getcatedata.Entity.name;
            }

            Literal lblTitle = (Literal)e.Item.FindControl("lblTitle");
            HiddenField hdnID = (HiddenField)e.Item.FindControl("hdnID");

            HyperLink lnkMore = (HyperLink)e.Item.FindControl("lnkMore");
            string title = Regex.Replace(lblTitle.Text, @"[\\:/*#]+", " ");
            long ID = Convert.ToInt64(hdnID.Value.ToString());
            lnkMore.NavigateUrl = lang + "/ArticlePage/" + title.Trim() + "/" + ID.ToString();






        }
    }

    protected void lstSubArticle_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {


            HiddenField hdnID = (HiddenField)e.Item.FindControl("hdnID");
            Literal lblTitle = (Literal)e.Item.FindControl("lblTitle");

            HyperLink lnkMore = (HyperLink)e.Item.FindControl("lnkMore");
            string title = Regex.Replace(lblTitle.Text, @"[\\:/*#]+", " ");
            long ID = Convert.ToInt64(hdnID.Value.ToString());
            lnkMore.NavigateUrl = lang + "/ArticlePage/" + title.Trim() + "/" + ID.ToString();

            int lengthParentRext = Convert.ToInt32(lblTitle.Text.Length);
            if (lengthParentRext > 25)
            {
                lblTitle.Text = lblTitle.Text.Substring(0, 25) + "...";
            }
            else
            {
                lblTitle.Text = lblTitle.Text;
            }

            Literal Literal2 = (Literal)e.Item.FindControl("Literal2");

            int lengthParentRext2 = Convert.ToInt32(Literal2.Text.Length);
            if (lengthParentRext2 > 20)
            {
                Literal2.Text = Literal2.Text.Substring(0, 20) + "...";
            }
            else
            {
                Literal2.Text = Literal2.Text;
            }



        }
    }



    #endregion


    protected void FillSlider()
    {
        try
        {
            


            List<sliderData> sliderDataList = new List<sliderData>();

            

            ResultList<PluginSliderEntity> SliderResult = new ResultList<PluginSliderEntity>();
            SliderResult = PluginSliderDomain.GetSliderAllNotAsync();
            if (SliderResult.Status == ErrorEnums.Success)
            {
                SliderResult.List = SliderResult.List.Where(s => s.IsDeleted == false && s.IsPublish == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishDate <= currentDate).OrderByDescending(s => s.PublishDate).ToList();
                foreach (PluginSliderEntity SliderEntity in SliderResult.List)
                {
                    sliderData sl = new sliderData();

                    sl.sliderDataType = "Slider";
                    sl.ID = SliderEntity.ID;
                    sl.Title = SliderEntity.Title;
                    sl.Image = SliderEntity.Image;
                    sl.Date = SliderEntity.PublishDate;
                    sl.KeyWords = "";
                    sl.Summery = SliderEntity.ImageTitle;
                    sl.Author = "";
                    sl.Category = "";
                    sl.Details = SliderEntity.Sammery;
                    sl.Order = SliderEntity.Order;
                    sl.PublishedDate = SliderEntity.PublishDate;
                    sl.IsPublished = SliderEntity.IsPublish;
                    sl.IsDelete = SliderEntity.IsDeleted;
                    sl.LanguageID = SliderEntity.LanguageID;
                    sl.LanguageName = SliderEntity.LanguageName;
                    sl.ViewCount = 0;
                    sl.IsShowSlider = true;


                    sliderDataList.Add(sl);
                }
            }

            ResultList<NewsEntity> NewsResult = new ResultList<NewsEntity>();
            NewsResult = NewsDomain.GetNewsAllNotAsync();
            if (NewsResult.Status == ErrorEnums.Success)
            {

                //NewsResult.List = NewsResult.List.Where(s => s.IsDeleted == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishDate <= currentDate).OrderBy(s => s.PublishDate).Take(10).ToList();

                NewsResult.List =  NewsResult.List.Where(s => s.IsDeleted == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishDate <= currentDate).OrderByDescending(s => s.NewsDate).ToList();

                foreach (NewsEntity NewsEntity in NewsResult.List)
                {
                    sliderData sl = new sliderData();

                    sl.sliderDataType = "News";
                    sl.ID = NewsEntity.NewsID;
                    sl.Title = NewsEntity.Headline;
                    sl.Image = NewsEntity.NewsImage;
                    sl.Date = NewsEntity.NewsDate;
                    sl.KeyWords = "";
                    sl.Summery = NewsEntity.Summary;
                    sl.Author = "";
                    sl.Category = "";
                    sl.Details = NewsEntity.Description;
                    sl.Order = Convert.ToInt32(NewsEntity.NewsOrder);   // 0; //NewsEntity.NewsOrder;
                    sl.PublishedDate = NewsEntity.PublishDate;
                    sl.IsPublished = NewsEntity.IsPublished;
                    sl.IsDelete = NewsEntity.IsDeleted;
                    sl.LanguageID = Convert.ToInt32(NewsEntity.LanguageID);
                    sl.LanguageName = NewsEntity.LanguageName;
                    sl.ViewCount = 0;
                    sl.IsShowSlider = true;


                    sliderDataList.Add(sl);
                }


            }


            ResultList<Plugin_Reserach_ArticleEntity> ArticleResult = new ResultList<Plugin_Reserach_ArticleEntity>();
            ArticleResult = Plugin_Reserach_ArticleDomain.GetAllNotAsync();
            if (ArticleResult.Status == ErrorEnums.Success)
            {
                ArticleResult.List = ArticleResult.List.Where(s => s.IsDelete == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishedDate <= currentDate && s.IsShowSlider == true).OrderByDescending(s => s.ArticleDate).ToList();

                foreach (Plugin_Reserach_ArticleEntity ArticleEntity in ArticleResult.List)
                {
                    sliderData sl = new sliderData();

                    sl.sliderDataType = "Article";
                    sl.ID = ArticleEntity.ArticleID;
                    sl.Title = ArticleEntity.ArticleTitle;
                    sl.Image = ArticleEntity.ArticleImage;
                    sl.Date = ArticleEntity.ArticleDate;
                    sl.KeyWords = ArticleEntity.ArticleKeyWords;
                    sl.Summery = ArticleEntity.ArticleSummery;
                    sl.Author = ArticleEntity.ArticleAuthor;
                    sl.Category = ArticleEntity.ArticleCategory;
                    sl.Details = ArticleEntity.ArticleDetails;
                    sl.Order = ArticleEntity.Order; // 0; //NewsEntity.NewsOrder;
                    sl.PublishedDate = ArticleEntity.PublishedDate;
                    sl.IsPublished = ArticleEntity.IsPublished;
                    sl.IsDelete = ArticleEntity.IsDelete;
                    sl.LanguageID = ArticleEntity.LanguageID;
                    sl.LanguageName = ArticleEntity.LanguageName;
                    //sl.ViewCount = 0;
                    //sl.IsShowSlider = true;


                    sliderDataList.Add(sl);
                }
            }

            lstBennerArticle.DataSource = sliderDataList.OrderByDescending(s => s.Date).Take(10).ToList();
            lstBennerArticle.DataBind();


        }
        catch (Exception ex)
        {
        }
    }

    #region --> Get Publication
    protected void FillPublication()
    {
        try
        {
            ResultList<Plugin_Reserach_PublicationEntity> Result = new ResultList<Plugin_Reserach_PublicationEntity>();
            Result = Plugin_Reserach_PublicationDomain.GetAllNotAsync();
            if (Result.Status == ErrorEnums.Success)
            {
                //lstPublication.DataSource = Result.List.Where(s => s.IsDelete == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishedDate <= currentDate).OrderBy(s => s.Order).Take(3).ToList();
                //lstPublication.DataBind();

                lstPublicationBooks.DataSource = Result.List.Where(s => s.IsDelete == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishedDate <= currentDate).OrderBy(s => s.Order).ToList();
                lstPublicationBooks.DataBind();
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void lstPublication_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            Image imgPublication = (Image)e.Item.FindControl("imgPublication");
            HiddenField hdnCategory = (HiddenField)e.Item.FindControl("hdnCategory");
            HiddenField hdnID = (HiddenField)e.Item.FindControl("hdnID");
            Literal lblCategory = (Literal)e.Item.FindControl("lblCategory");
            Literal lblTitle = (Literal)e.Item.FindControl("lblTitle");

            if (!string.IsNullOrEmpty(imgPublication.ImageUrl))
            {
                imgPublication.ImageUrl = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + imgPublication.ImageUrl.Replace("~/", "~/Siteware/");
            }

            int Cateid = Convert.ToInt32(hdnCategory.Value);

            var getcatedata = Lookup_Reserach_PublicationCategoryDomain.GetByIDNotAsync(Cateid);
            if (getcatedata.Status == ErrorEnums.Success)
            {
                lblCategory.Text = getcatedata.Entity.name;
            }

            HyperLink lnkMore = (HyperLink)e.Item.FindControl("lnkMore");
            string title = Regex.Replace(lblTitle.Text, @"[\\:/*#]+", " ");
            long ID = Convert.ToInt64(hdnID.Value.ToString());
            lnkMore.NavigateUrl = lang + "/PublicationPage/" + title.Trim() + "/" + ID.ToString();
        }
    }

    protected void lstPublicationBooks_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            Image imgPublication = (Image)e.Item.FindControl("imgPublication");

            if (!string.IsNullOrEmpty(imgPublication.ImageUrl))
            {
                imgPublication.ImageUrl = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + imgPublication.ImageUrl.Replace("~/", "~/Siteware/");
            }

        }
    }

    #endregion

    #region --> Get Research and studies
    protected void FillResearchandstudies()

    {
        try
        {
            ResultList<Plugin_Reserach_ResearchStudiesEntity> Result = new ResultList<Plugin_Reserach_ResearchStudiesEntity>();
            Result = Plugin_Reserach_ResearchStudiesDomain.GetAllNotAsync();
            if (Result.Status == ErrorEnums.Success)
            {
                lstResearchAndPublication.DataSource = Result.List.Where(s => s.IsDelete == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishedDate <= currentDate).OrderBy(s => s.Order).Take(3).ToList();
                lstResearchAndPublication.DataBind();


            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void lstResearchAndPublication_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            Image imgPublication = (Image)e.Item.FindControl("imgPublication");
            HiddenField hdnCategory = (HiddenField)e.Item.FindControl("hdnCategory");
            HiddenField hdnID = (HiddenField)e.Item.FindControl("hdnID");
            Literal lblCategory = (Literal)e.Item.FindControl("lblCategory");
            Literal lblTitle = (Literal)e.Item.FindControl("lblTitle");

            if (!string.IsNullOrEmpty(imgPublication.ImageUrl))
            {
                imgPublication.ImageUrl = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + imgPublication.ImageUrl.Replace("~/", "~/Siteware/");
            }

            int Cateid = Convert.ToInt32(hdnCategory.Value);

            var getcatedata = Lookup_Reserach_PublicationCategoryDomain.GetByIDNotAsync(Cateid);
            if (getcatedata.Status == ErrorEnums.Success)
            {
                lblCategory.Text = getcatedata.Entity.name;
            }

            //HyperLink lnkMore = (HyperLink)e.Item.FindControl("lnkMore");
            //string title = Regex.Replace(lblTitle.Text, @"[\\:/*#]+", " ");
            //long ID = Convert.ToInt64(hdnID.Value.ToString());
            //lnkMore.NavigateUrl = lang + "/PublicationPage/" + title.Trim() + "/" + ID.ToString();
        }
    }



    #endregion


    #region --> Get News
    protected async void FillNews()
    {
        try
        {
            ResultList<NewsEntity> Result = new ResultList<NewsEntity>();
            Result = await NewsDomain.GetNewsAll();
            if (Result.Status == ErrorEnums.Success)
            {
                lstNews.DataSource = Result.List.Where(s => s.IsDeleted == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishDate <= currentDate).OrderByDescending(s => s.NewsDate).Take(5).ToList();
                lstNews.DataBind();
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void lstNews_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {

            HiddenField hdndate = (HiddenField)e.Item.FindControl("hdndate");
            Literal lbldate = (Literal)e.Item.FindControl("lbldate");
            Literal lblTitle = (Literal)e.Item.FindControl("lblTitle");
            HiddenField hdnID = (HiddenField)e.Item.FindControl("hdnID");

            var date = Convert.ToDateTime(hdndate.Value);
            if (CurrLangID == Convert.ToInt32(EnumLanguage.Arabic))
            {
                //Get month and convert to arabic
                var m = date.ToString("MMM");
                var month = date.ToString("MMM", new CultureInfo("ar-AE"));

                var Date = date.ToString("dd", new CultureInfo("ar-AE"));
                var year = date.ToString("yyyy", new CultureInfo("ar-AE"));
                //lbldate.Text = Date + " " + month + " ، " + year + " ";
                lbldate.Text = "<strong>" + Date + "</strong><small> " + month + " </small>";
            }
            else
            {
                //Get month and convert to arabic
                var m = date.ToString("MMM");
                var month = date.ToString("MMM", new CultureInfo("es-ES"));

                var Date = date.ToString("dd", new CultureInfo("es-ES"));
                var year = date.ToString("yyyy", new CultureInfo("es-ES"));
                //lbldate.Text = Date + " " + month + " ، " + year + " ";
                lbldate.Text = "<strong>" + Date + "</strong><small> " + month + " </small>";

                //Convert NewsDate in given dateformat
                //lbldate.Text = date.ToString("dd MMM, yyyy", new CultureInfo("en-US"));
            }

            HyperLink lnkMore = (HyperLink)e.Item.FindControl("lnkMore");
            string title = Regex.Replace(lblTitle.Text, @"[\\:/*#]+", " ");
            int ID = Convert.ToInt32(hdnID.Value.ToString());
            lnkMore.NavigateUrl = lang + "/NewsPage/" + title.Trim() + "/" + ID.ToString();


        }
    }

    #endregion

    #region --> Get Media
    protected async void FillMedia()
    {
        try
        {
            ResultList<Plugin_Reserach_MediaEntity> Result = new ResultList<Plugin_Reserach_MediaEntity>();
            Result = await Plugin_Reserach_MediaDomain.GetAll();
            if (Result.Status == ErrorEnums.Success)
            {
                lstMedia.DataSource = Result.List.Where(s => s.IsDelete == false && s.IsPublished == true && s.LanguageID == Convert.ToInt32(Session["CurrentLanguage"]) && s.PublishedDate <= currentDate).OrderBy(s => s.Order).Take(3).ToList();
                lstMedia.DataBind();
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void lstMedia_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {

            Image imgMedia = (Image)e.Item.FindControl("imgMedia");

            if (!string.IsNullOrEmpty(imgMedia.ImageUrl))
            {
                imgMedia.ImageUrl = (ConfigurationManager.AppSettings["ImagePath"]).ToString().Trim() + imgMedia.ImageUrl.Replace("~/", "~/Siteware/");
            }
        }
    }

    #endregion

    #region NewsLetter

    protected void btnNewsLetter_Click(object sender, EventArgs e)
    {
        Page.Validate("grp1");
        if (Page.IsValid)
        {
            Plugin_News_LetterEntity entity = new Plugin_News_LetterEntity();
            entity.EmailNewsLetter = txtNewsletter.Text;
            entity.SubscribeNewsLetter = true;
            entity.AddDate = DateTime.Now;
            entity.AddUser = "web";
            entity.EditDate = DateTime.Now;
            entity.EditUser = "web";

            var Result = Plugin_News_LetterDomain.InsertPlugin_News_LetterNonasync(entity);
            if (Result.Status == ErrorEnums.Success)
            {
                //btnSubscribe.Text = "";
                txtNewsletter.Text = "";
                if (Convert.ToInt32(Session["CurrentLanguage"]) == (int)EnumLanguage.Arabic)
                {
                    lblMessage.Text = "Your email has been successfully registered";
                    //btnOk.Text = "Ok";
                }
                else
                {
                    lblMessage.Text = "Your email has been successfully registered";
                    //btnOk.Text = "Ok";
                }
                txtNewsletter.Attributes.Add("style", "");
                mpeSuccess.Show();

                //mpeSubscribe.Show();

            }
        }
        else
        {

            txtNewsletter.Attributes.Add("style", "border: 1px solid red");
        }
    }
    #endregion

    #region Search
    protected void txtSearch_TextChanged(object sender, EventArgs e)
    {
        try
        {
            string SearchText = string.Empty;
            string lang = string.Empty;
            if (!string.IsNullOrEmpty(txtSearch.Text)) { SearchText = txtSearch.Text.Trim(); }
            if (Convert.ToInt32(Session["CurrentLanguage"]) == (int)EnumLanguage.Arabic)
            {

                LangID = 2;
                Session["CurrentLanguage"] = LangID.ToString();
                lang = "/ar";
                //RouteData.Values["language"] = lang;

                Response.Redirect("~/DetailsPage/ar/SearchPage.aspx?keyword=" + txtSearch.Text.Trim(), false);
            }
            else
            {
                LangID = 1;
                Session["CurrentLanguage"] = LangID.ToString();
                lang = "/en";
                //RouteData.Values["language"] = lang;
                Response.Redirect("~/DetailsPage/en/SearchPage.aspx?keyword=" + txtSearch.Text.Trim(), false);
            }
            //Session["CurrentLanguage"] = LangID.ToString();

            //Response.Redirect("~/DetailsPage/" + lang + "/SearchPage.aspx?keyword=" + SearchText, false);
        }
        catch (Exception ex)
        { throw ex; }
    }
    #endregion


    #region--> FillLanguage | Add | Ajay Patel | 07-12-2020
    //protected async void FillLanguage()
    //{
    //    try
    //    {
    //       // ddlLanguage.Items.Clear();
    //        ddlMobLanguage.Items.Clear();

    //        ResultList<LanguageEntity> Result = new ResultList<LanguageEntity>();
    //        Result = await LanguageDomain.GetLanguagesAll();

    //        if (Result.Status == ErrorEnums.Success)
    //        {
    //            foreach (LanguageEntity item in Result.List)
    //            {
    //               // ddlLanguage.Items.Add(new ListItem(item.Name.ToString(), item.ID.ToString()));
    //                ddlMobLanguage.Items.Add(new ListItem(item.Name.ToString(), item.ID.ToString()));
    //            }
    //            int LangID = Convert.ToInt32(Session["CurrentLanguage"]);

    //           // ddlLanguage.SelectedValue = LangID.ToString();
    //            ddlMobLanguage.SelectedValue = LangID.ToString();
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //    }
    //}

    //protected void ddlMobLanguage_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        int LangID = Convert.ToInt32(ddlMobLanguage.SelectedValue);
    //        if (LangID == (int)EnumLanguage.Arabic)
    //        {
    //            Session["CurrentLanguage"] = LangID.ToString();

    //            Response.Redirect("~/ar/home", false);
    //        }
    //        else
    //        {
    //            Session["CurrentLanguage"] = LangID.ToString();
    //            Response.Redirect("~/en/home", false);
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //    }
    //}
    //protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        int LangID = Convert.ToInt32(ddlLanguage.SelectedValue);
    //        if (LangID == (int)EnumLanguage.Arabic)
    //        {
    //            Session["CurrentLanguage"] = LangID.ToString();

    //            Response.Redirect("~/ar/home", false);
    //        }
    //        else
    //        {
    //            Session["CurrentLanguage"] = LangID.ToString();
    //            Response.Redirect("~/en/home", false);
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //    }
    //}
    #endregion




    protected async void LinkButtonID_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)(sender);
        string yourValue = btn.CommandArgument;

        long MediaId = Convert.ToInt64(btn.CommandArgument);

        Plugin_Reserach_MediaEntity entity = new Plugin_Reserach_MediaEntity();
        entity.MediaID = MediaId;
        //entity.ViewCount = Result.Entity.ViewCount;
        var UpdateCount = await Plugin_Reserach_MediaDomain.UpdateNewsViewCount(entity.MediaID);

        FillMedia();
    }

    protected void btnLanguage_Click(object sender, EventArgs e)
    {
        int LangID = Convert.ToByte(Session["CurrentLanguage"]);
        if (LangID == (int)EnumLanguage.Arabic)
        {


            Response.Redirect("~/en/home", false);

        }
        else
        {

            Response.Redirect("~/ar/home", false);

        }
    }

    public class sliderData
    {
        public string sliderDataType { get; set; }
        public long ID { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
        public DateTime Date { get; set; }
        public string KeyWords { get; set; }
        public string Summery { get; set; }
        public string Author { get; set; }
        public string Category { get; set; }
        public string Details { get; set; }
        public long Order { get; set; }
        public DateTime PublishedDate { get; set; }
        public bool IsPublished { get; set; }
        public bool IsDelete { get; set; }
        public int LanguageID { get; set; }
        public string LanguageName { get; set; }
        public long ViewCount { get; set; }
        public bool IsShowSlider { get; set; }
    }
}
