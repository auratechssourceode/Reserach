﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Siteware.Web.Models
{
    public class ImageResultVM
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
        public string ImageName { get; set; }
    }

    public class ImageSizeVM
    {
        public int Height { get; set; }
        public int Width { get; set; }
    }
}