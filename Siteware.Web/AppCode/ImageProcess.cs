﻿using Siteware.Web.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace Siteware.Web.AppCode
{
    public class ImageProcess
    {
        // set default size here
        public int Width { get; set; }

        public int Height { get; set; }

        //private readonly string UploadPath = "~/Image/";


        public ImageResultVM RenameUploadFile(HttpPostedFileBase file, string UploadPath, int weight, int height, int cNewsID)
        {
            Guid id = Guid.NewGuid();
            file.SaveAs(HttpContext.Current.Request.MapPath(UploadPath + id + "_" + cNewsID));
            string finalFileName = id + "_" + cNewsID;
            //Int64 counter = 0;

            //if (System.IO.File.Exists
            //    (HttpContext.Current.Request.MapPath(UploadPath + finalFileName)))
            //{
            //    //file exists => add country try again
            //    return RenameUploadFile(file, Convert.ToString(++counter));
            //}

            //file doesn't exist, upload item but validate first
            return UploadFile(file, finalFileName, UploadPath, weight, height);
        }

        private ImageResultVM UploadFile(HttpPostedFileBase file, string fileName, string UploadPath, int weight, int height)
        {
            ImageResultVM imageResult = new ImageResultVM { Success = true, ErrorMessage = null };

            var path = Path.Combine(HttpContext.Current.Request.MapPath(UploadPath), fileName);
            string extension = Path.GetExtension(file.FileName);

            try
            {

                file.SaveAs(path);

                Image imgOriginal = Image.FromFile(path);
                var fileLength = new FileInfo(path).Length;
                //if (fileLength > 600000 || imgOriginal.Height > 400 || imgOriginal.Width > 400)
                {
                    //pass in whatever value you want 
                    Image imgActual = Scale(imgOriginal, weight, height);
                    imgOriginal.Dispose();
                    imgActual.Save(path);
                    imgActual.Dispose();
                }


                //var uri = new Uri("file://" + path, UriKind.Absolute);
                //System.IO.File.Delete(uri.LocalPath);

                imageResult.ImageName = fileName;

                return imageResult;
            }
            catch (Exception ex)
            {
                // you might NOT want to show the exception error for the user
                // this is generaly logging or testing

                imageResult.Success = false;
                imageResult.ErrorMessage = ex.Message;
                return imageResult;
            }
        }

        public ImageResultVM ImageProcessUpload(long cNewsID, int weight, int height, string orgImgPath, string newUploadPath)
        {
            ImageResultVM imageResult = new ImageResultVM { Success = true, ErrorMessage = null };
            try
            {
                Guid id = Guid.NewGuid();
                string extension = Path.GetExtension(orgImgPath);
                string fileName = id + "_" + cNewsID + extension;
                string newFullPath = Path.Combine(HttpContext.Current.Request.MapPath(newUploadPath), fileName);
                //var path = Path.Combine(HttpContext.Current.Request.MapPath("/Siteware/Siteware_File/image/NES/"), "cal-2.jpg");
                //Image imgOriginal = Image.FromFile("D:/TFS19/NES/NES/Siteware.Web/Siteware/Siteware_File/image/NES/cal-2.jpg");
                Image imgOriginal = Image.FromFile(orgImgPath);
                //var fileLength = new FileInfo(path).Length;
                //if (fileLength > 600000 || imgOriginal.Height > 400 || imgOriginal.Width > 400)
                //{
                //var path = Path.Combine(HttpContext.Current.Request.MapPath("/Siteware/Siteware_File/image/NES/CalendarEvent"), "cal-2.jpg");            
                //pass in whatever value you want 
                Image imgActual = Scale(imgOriginal, weight, height);
                imgOriginal.Dispose();
                imgActual.Save(newFullPath);
                imgActual.Dispose();
                //}
                imageResult.ImageName = fileName;

                return imageResult;
            }
            catch (Exception ex)
            {
                imageResult.Success = false;
                imageResult.ErrorMessage = ex.Message;
                return imageResult;
            }
        }

        private Image Scale(Image imgPhoto, int weight, int height)
        {
            float sourceWidth = imgPhoto.Width;
            float sourceHeight = imgPhoto.Height;
            float destHeight = 0;
            float destWidth = 0;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            // force resize, might distort image

            #region Coomentedcoding
            //if (Width != 0 && Height != 0)
            //{
            //    destWidth = Width;
            //    destHeight = Height;
            //}
            //// change size proportially depending on width or height
            //else if (Height != 0)
            //{
            //    destWidth = (float)(Height * sourceWidth) / sourceHeight;
            //    destHeight = Height;
            //}
            //else
            //{
            //    destWidth = Width;
            //    destHeight = (float)(sourceHeight * Width / sourceWidth);
            //}
            #endregion


            #region new coding
            destWidth = weight;
            destHeight = height;
            #endregion

            Bitmap bmPhoto = new Bitmap((int)destWidth, (int)destHeight,
                                        PixelFormat.Format32bppPArgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, (int)destWidth, (int)destHeight),
                new Rectangle(sourceX, sourceY, (int)sourceWidth, (int)sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();

            return bmPhoto;
        }
    }

    public class MemoryPostedFile : HttpPostedFileBase
    {
        private readonly byte[] fileBytes;

        public MemoryPostedFile(byte[] fileBytes, string fileName = null)
        {
            this.fileBytes = fileBytes;
            this.FileName = fileName;
            this.InputStream = new MemoryStream(fileBytes);
        }

        public override int ContentLength => fileBytes.Length;

        public override string FileName { get; }

        public override Stream InputStream { get; }
    }
}