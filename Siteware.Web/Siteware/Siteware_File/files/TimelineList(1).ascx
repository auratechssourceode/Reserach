﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TimelineList.ascx.cs" Inherits="Controls_TimelineList" %>
<%@ Import Namespace="SiteWare.Entity.Common.Enums" %>

<div class="about-us-content section timeline">
    <div class="container">
        <%--<div class="row">
            <div class="col-12">
                <h2>our history</h2>
                <p>
                    His Majesty King Hussein led the Hashemite Kingdom of Jordan for 47 years between 1952 and 1999. Under his leadership,
                        Jordan became a regional model for achievements in education, healthcare, socio-economic development and political
                        liberalization.
                </p>
                <div class="timeline_logo">
                    <asp:Image ID="imgLogo" runat="server" ImageUrl='<%# Bind("Image") %>' />
                  
                </div>
            </div>
        </div>--%>
        <div class="clearfix"></div>
        <asp:HiddenField runat="server" ID="mylistcount" />
        <ul class="list-unstyled timeline_list">
            <asp:ListView runat="server" ID="lstTimeline" OnItemDataBound="lstTimeline_ItemDataBound">
                <ItemTemplate>
                    <li id="ListCounts" runat="server">
                        <asp:HiddenField runat="server" ID="hdncolors" Value='<%# Bind("PluginAreaTitle")%>' />
                        <asp:HiddenField runat="server" ID="hdntextHead" Value='<%# Bind("Title")%>' />
                        <asp:HiddenField runat="server" ID="hdntextYear" Value='<%# Bind("Year")%>' />
                        <aside>
                            <asp:Literal runat="server" ID="lblTextheadYear"></asp:Literal>
                        </aside>
                        <%-- <aside>KHF International <span>1999</span></aside>--%>
                        <p>
                            <asp:Literal runat="server" ID="lblSummery" Text='<%# Bind("Summary")%>'></asp:Literal>
                        </p>
                        <%-- <p>
                            His Majesty King Hussein led the Hashemite Kingdom of Jordan for 47 years between 1952 and 1999. Under his leadership,
                        Jordan became a regional model for achievements in education, healthcare, socio-economic development and political
                        liberalization.
                        </p>--%>
                    </li>
                </ItemTemplate>
            </asp:ListView>

            <%-- <li class="left_side color_1 wow slideInUp">
                    <aside>KHF International <span>1999</span></aside>
                    <p>
                        His Majesty King Hussein led the Hashemite Kingdom of Jordan for 47 years between 1952 and 1999. Under his leadership,
                        Jordan became a regional model for achievements in education, healthcare, socio-economic development and political
                        liberalization.
                    </p>
                </li>
                <li class="right_side color_2 wow slideInUp mar_top_100">
                    <aside>KHF International <span>1999</span></aside>
                    <p>
                        His Majesty King Hussein led the Hashemite Kingdom of Jordan for 47 years between 1952 and 1999. Under his
                        leadership,
                        Jordan became a regional model for achievements in education, healthcare, socio-economic development and
                        political
                        liberalization.
                    </p>
                </li>
                <li class="left_side color_3 wow slideInUp">
                    <aside>KHF International <span>1999</span></aside>
                    <p>
                        His Majesty King Hussein led the Hashemite Kingdom of Jordan for 47 years between 1952 and 1999. Under his
                        leadership,
                        Jordan became a regional model for achievements in education, healthcare, socio-economic development and
                        political
                        liberalization.
                    </p>
                </li>
                <li class="right_side color_4 wow slideInUp">
                    <aside>KHF International <span>1999</span></aside>
                    <p>
                        His Majesty King Hussein led the Hashemite Kingdom of Jordan for 47 years between 1952 and 1999. Under his
                        leadership,
                        Jordan became a regional model for achievements in education, healthcare, socio-economic development and
                        political
                        liberalization.
                    </p>
                </li>
                <li class="left_side color_5 wow slideInUp">
                    <aside>KHF International <span>1999</span></aside>
                    <p>
                        His Majesty King Hussein led the Hashemite Kingdom of Jordan for 47 years between 1952 and 1999. Under his
                        leadership,
                        Jordan became a regional model for achievements in education, healthcare, socio-economic development and
                        political
                        liberalization.
                    </p>
                </li>
                <li class="right_side color_6 wow slideInUp mar_top_50">
                    <aside>KHF International <span>1999</span></aside>
                    <p>
                        His Majesty King Hussein led the Hashemite Kingdom of Jordan for 47 years between 1952 and 1999. Under his
                        leadership,
                        Jordan became a regional model for achievements in education, healthcare, socio-economic development and
                        political
                        liberalization.
                    </p>
                </li>
                <li class="left_side color_3 wow slideInUp mar_top_50">
                    <aside>KHF International <span>1999</span></aside>
                    <p>
                        His Majesty King Hussein led the Hashemite Kingdom of Jordan for 47 years between 1952 and 1999. Under his
                        leadership,
                        Jordan became a regional model for achievements in education, healthcare, socio-economic development and
                        political
                        liberalization.
                    </p>
                </li>--%>
        </ul>
        <div class="timeline_logo">
             <asp:Image ID="imgbottom" runat="server"  />
          <%--  <img src="./assets/images/crown.png" alt="" />--%>
            <strong>2019</strong>
        </div>
    </div>
</div>



<script src="https://code.jquery.com/jquery-3.4.1.js"
    integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
            crossorigin="anonymous"></script>

<script type="text/javascript">
    debugger;
    $(document).ready(function () {
        debugger;

        var mylistcount = $("#<%=mylistcount.ClientID%>").val();
        //  listcount.val();
        for (var i = 1; i <= mylistcount; i++) {
            debugger;
            var j = i - 1;

            var abcd = $('#ContentPlaceHolder1_ctl00_lstTimeline_hdncolors' + i + '_' + j + ' ').val();
            debugger;

            $('<style>.color_' + i + ':before{color:' + abcd + ' }</style>').appendTo('head');
            $('<style>.color_' + i + '{color:' + abcd + ' }</style>').appendTo('head');
        }

    });



</script>
