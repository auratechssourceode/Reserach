﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SubmitCV.ascx.cs" Inherits="Controls_SubmitCV" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<section class="contact_part">
      <div class="message_help">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12">
                    <%--<asp:UpdatePanel runat="server" ID="upanelContact">
                        <ContentTemplate>--%>
                    <%--<div class="form-group">
                        <asp:TextBox runat="server" ID="txtName" placeholder="Add your full name" ClientIDMode="Static" CssClass="customClick"></asp:TextBox>
                    </div>--%>

                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6">
                            <div class="form-group">
                                <asp:TextBox runat="server" ID="txtName" placeholder="Add your full name" ClientIDMode="Static" CssClass="customClick"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6">
                            <div class="form-group">
                                <asp:TextBox runat="server" ID="txtEmail" placeholder="Add your email address" ClientIDMode="Static" CssClass="customClick"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6">
                            <div class="form-group">
                                <asp:TextBox runat="server" ID="txtMobile" placeholder="Enter your phone number" ClientIDMode="Static" CssClass="customClick form-mobile"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6">
                            <div class="form-group">
                               <label>Upload Your CV</label>
                                        <div class="file-upload-wrapper" data-text="Select your file!">
                                            <span runat="server" id="spanBro_1" name="spanBro_1"></span>
                                            <input name="fileuploadfield_1" id="fileuploadfield_1" type="file" class="file-upload-field" value="Browse" onchange="callme2(this,1)">

                                            <%-- <asp:FileUpload ID="FileUpload2" runat="server" onchange="callme2(this,1)" />--%>
                                            <button>Browse</button>
                                        </div>
                            </div>
                        </div>
                        
                    </div>

                    <div class="row">
                    <div class="form-group">
                         <asp:DropDownList ID="ddlInterest" runat="server" class="customClick" TabIndex="2" Width="100%" >
                             <asp:ListItem Value="1" Text="Full Time"></asp:ListItem>
                             <asp:ListItem Value="1" Text="Internship"></asp:ListItem>
                                            </asp:DropDownList>
                    </div>
                        </div>
                    <button class="send_msg_btn" onclick="validForm();" type="button">Send message</button>
                    <asp:Button runat="server" ID="lnkSubmitFrm" OnClick="lnkSubmitFrm_Click" Text="Send message" Style="display: none;" disbled="disabled"></asp:Button>
                    <%--</ContentTemplate>
                    </asp:UpdatePanel>--%>
                </div>
                </div>

              <div class="clearfix"></div>
        </div></div>
</section>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script>    
    $(".customClick").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            $("[id*=<%=lnkSubmitFrm.ClientID %>]").click();
        }
    });
    function validForm() {
        debugger;
        var vname = false;
        var vEmail = false;
        var vMobile = false;
        if ($("#txtName").val() != "") {
            $("#txtName").css('border', 'none');
            vname = true;
        }
        else {
            $("#txtName").css('border', '1px solid red');
        }
        if ($("#txtEmail").val() != "") {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!regex.test($("#txtEmail").val())) {
                $("#txtEmail").css('border', '1px solid red');
            } else {
                $("#txtEmail").css('border', 'none');
                vEmail = true;
            }
        }
        else {
            $("#txtEmail").css('border', '1px solid red');
        }
        if ($("#txtMobile").val() != "") {
            $("#txtMobile").css('border', 'none');
            vMobile = true;
        }
        else {
            $("#txtMobile").css('border', '1px solid red');
        }
        if (vname && vEmail && vMobile) {
            $("[id*=<%=lnkSubmitFrm.ClientID %>]").click();
        }
    }
</script>
