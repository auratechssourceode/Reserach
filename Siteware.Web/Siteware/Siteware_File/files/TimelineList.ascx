﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TimelineList.ascx.cs" Inherits="Controls_TimelineList" %>
<%@ Import Namespace="SiteWare.Entity.Common.Enums" %>

    <div class="about-us-content section timeline">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2>our history</h2>
                    <p>
                        His Majesty King Hussein led the Hashemite Kingdom of Jordan for 47 years between 1952 and 1999. Under his leadership,
                        Jordan became a regional model for achievements in education, healthcare, socio-economic development and political
                        liberalization.
                    </p>
                    <div class="timeline_logo">
                        <img src="./assets/images/timeline-logo.png" alt="" />
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <ul class="list-unstyled timeline_list">
                <li class="left_side color_1 wow slideInUp">
                    <aside>KHF International <span>1999</span></aside>
                    <p>
                        His Majesty King Hussein led the Hashemite Kingdom of Jordan for 47 years between 1952 and 1999. Under his leadership,
                        Jordan became a regional model for achievements in education, healthcare, socio-economic development and political
                        liberalization.
                    </p>
                </li>
                <li class="right_side color_2 wow slideInUp mar_top_100">
                    <aside>KHF International <span>1999</span></aside>
                    <p>
                        His Majesty King Hussein led the Hashemite Kingdom of Jordan for 47 years between 1952 and 1999. Under his
                        leadership,
                        Jordan became a regional model for achievements in education, healthcare, socio-economic development and
                        political
                        liberalization.
                    </p>
                </li>
                <li class="left_side color_3 wow slideInUp">
                    <aside>KHF International <span>1999</span></aside>
                    <p>
                        His Majesty King Hussein led the Hashemite Kingdom of Jordan for 47 years between 1952 and 1999. Under his
                        leadership,
                        Jordan became a regional model for achievements in education, healthcare, socio-economic development and
                        political
                        liberalization.
                    </p>
                </li>
                <li class="right_side color_4 wow slideInUp">
                    <aside>KHF International <span>1999</span></aside>
                    <p>
                        His Majesty King Hussein led the Hashemite Kingdom of Jordan for 47 years between 1952 and 1999. Under his
                        leadership,
                        Jordan became a regional model for achievements in education, healthcare, socio-economic development and
                        political
                        liberalization.
                    </p>
                </li>
                <li class="left_side color_5 wow slideInUp">
                    <aside>KHF International <span>1999</span></aside>
                    <p>
                        His Majesty King Hussein led the Hashemite Kingdom of Jordan for 47 years between 1952 and 1999. Under his
                        leadership,
                        Jordan became a regional model for achievements in education, healthcare, socio-economic development and
                        political
                        liberalization.
                    </p>
                </li>
                <li class="right_side color_6 wow slideInUp mar_top_50">
                    <aside>KHF International <span>1999</span></aside>
                    <p>
                        His Majesty King Hussein led the Hashemite Kingdom of Jordan for 47 years between 1952 and 1999. Under his
                        leadership,
                        Jordan became a regional model for achievements in education, healthcare, socio-economic development and
                        political
                        liberalization.
                    </p>
                </li>
                <li class="left_side color_3 wow slideInUp mar_top_50">
                    <aside>KHF International <span>1999</span></aside>
                    <p>
                        His Majesty King Hussein led the Hashemite Kingdom of Jordan for 47 years between 1952 and 1999. Under his
                        leadership,
                        Jordan became a regional model for achievements in education, healthcare, socio-economic development and
                        political
                        liberalization.
                    </p>
                </li>
            </ul>
            <div class="timeline_logo">
                <img src="./assets/images/crown.png" alt="" />
                <strong>2019</strong>
            </div>
        </div>
    </div>


