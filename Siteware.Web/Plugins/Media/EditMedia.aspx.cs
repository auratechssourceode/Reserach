﻿using Siteware.Web.AppCode;
using SiteWare.Domain.Domains;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Common.Enums;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Siteware.Web.Plugins.Media
{
    public partial class EditMedia : System.Web.UI.Page
    {
        string PageName = "Media";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!FunctionSecurity.TestUserPermissionPage(SessionManager.GetInstance.Users.UserID, PageName))
                {
                    Response.Redirect("~/DashBoard.aspx", false);
                }
                if (!IsPostBack)
                {
                    if (SessionManager.GetInstance.Users != null)
                    {
                        FillNavigation();
                        FillLanguages();
                        FillDetails();
                    }
                    else
                    {
                        Session.Abandon();
                        Session.Clear();
                        Response.Redirect("~/Login.aspx", false);
                    }
                }

            }
            catch
            {
                Session.Abandon();
                Session.Clear();
                Response.Redirect("~/Login.aspx", false);
            }
        }

        protected void FillNavigation()
        {
            var masterPage = this.Master;
            if (masterPage != null)
            {

                Session["IDSelectPage"] = "~/Plugins/Media/ManageMedia.aspx";

            }
        }
        protected void FillLanguages()
        {
            ddlLanguages.Items.Insert(0, new ListItem("Select Language", "0"));

            ResultList<LanguageEntity> Result = new ResultList<LanguageEntity>();
            Result = LanguageDomain.GetLanguagesAllNotAsync();

            if (Result.Status == ErrorEnums.Success)
            {
                foreach (LanguageEntity item in Result.List)
                {
                    ddlLanguages.Items.Add(new ListItem(item.Name.ToString(), item.ID.ToString()));
                }
            }
        }

        protected async void FillDetails()
        {
            try
            {
                if (Session["MediaSession"] != null)
                {
                    long CoreID = Convert.ToInt64(Session["MediaSession"]);

                    ResultEntity<Plugin_Reserach_MediaEntity> Result = new ResultEntity<Plugin_Reserach_MediaEntity>();

                    Result = await Plugin_Reserach_MediaDomain.GetByID(CoreID);
                    if (Result.Status == ErrorEnums.Success)
                    {

                        //lblHeadName1.Text = Result.Entity.ArticleTitle;
                        //lblHeadName2.Text = Result.Entity.ArticleTitle;
                        txtTitle.Text = Result.Entity.MedialLink;
                        txtMediaTitle.Text = Result.Entity.Title;

                        ImagePage.ImageUrl = Result.Entity.MediaImage;
                        newWinField.Value = Result.Entity.MediaImage;
                        ddlParentPage.SelectedValue = Result.Entity.Target;
                        txtorderr.Text = Result.Entity.Order.ToString();

                        ddlLanguages.SelectedValue = Result.Entity.LanguageID.ToString();
                        CBIsPublished.Checked = Result.Entity.IsPublished;
                        txtPublishDate.Value = Result.Entity.PublishedDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                        lblViewCount.Text = Result.Entity.ViewCount.ToString();

                    }
                }
            }
            catch (Exception ex)
            {
            }

        }

        protected async void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["MediaSession"] != null)
                {
                    Plugin_Reserach_MediaEntity entity = new Plugin_Reserach_MediaEntity();
                    entity.MediaID = Convert.ToInt64(Session["MediaSession"]);

                    entity.MedialLink = txtTitle.Text.Trim();
                    entity.Title = txtMediaTitle.Text.Trim();
                    entity.MediaImage = newWinField.Value;
                    entity.Target = ddlParentPage.SelectedValue;
                    entity.Order = Convert.ToInt64(txtorderr.Text);
                    entity.LanguageID = Convert.ToInt32(ddlLanguages.SelectedValue);
                    entity.IsPublished = CBIsPublished.Checked;
                    entity.PublishedDate = DateTime.ParseExact(txtPublishDate.Value.ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    entity.IsDelete = false;
                    entity.AddDate = Convert.ToDateTime(DateTime.Now);
                    entity.AddUser = SessionManager.GetInstance.Users.UserID.ToString();
                    entity.EditDate = Convert.ToDateTime(DateTime.Now);
                    entity.EditUser = SessionManager.GetInstance.Users.UserID.ToString();
                    entity.ViewCount = Convert.ToInt64(lblViewCount.Text);
                    var Result = await Plugin_Reserach_MediaDomain.UpdateRecord(entity);

                    if (Result.Status == ErrorEnums.Success)
                    {
                        mpeSuccess.Show();
                    }
                }
                else
                {
                    Response.Redirect("~/Plugins/Media/ManageMedia.aspx", false);
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Plugins/Media/ManageMedia.aspx", false);
        }
    }
}