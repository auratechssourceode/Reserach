﻿using Siteware.Web.AppCode;
using SiteWare.Domain.Domains;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Common.Enums;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Siteware.Web.Plugins.Media
{
    public partial class ManageMedia : System.Web.UI.Page
    {
        string PageName = "Media";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!FunctionSecurity.TestUserPermissionPage(SessionManager.GetInstance.Users.UserID, PageName))
                {
                    Response.Redirect("~/DashBoard.aspx", false);
                }

                if (!IsPostBack)
                {
                    if (SessionManager.GetInstance.Users != null)
                    {
                        FillNavigation();
                        Session["MediaSession"] = null;
                        FillData();
                    }
                    else
                    {
                        Session.Abandon();
                        Session.Clear();
                        Response.Redirect("~/Login.aspx", false);
                    }
                }

            }
            catch
            {
                Session.Abandon();
                Session.Clear();
                Response.Redirect("~/Login.aspx", false);

            }
        }

        protected void FillNavigation()
        {
            var masterPage = this.Master;
            if (masterPage != null)
            {
                Session["IDSelectPage"] = "~/Plugins/Media/ManageMedias.aspx";

            }
        }

        [WebMethod]
        public static LangDetails[] BindDatatoDropdown()
        {
            try
            {
                ResultList<LanguageEntity> Result = new ResultList<LanguageEntity>();
                List<LangDetails> details = new List<LangDetails>();
                Result = LanguageDomain.GetLanguagesAllNotAsync();
                if (Result.Status == ErrorEnums.Success)
                {
                    foreach (var item in Result.List)
                    {
                        LangDetails lang = new LangDetails();
                        lang.LangId = item.ID;
                        lang.LangName = item.Name;
                        details.Add(lang);
                    }
                }
                return details.ToArray();
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public class LangDetails
        {
            public int LangId { get; set; }
            public string LangName { get; set; }
        }

        protected async void FillData()
        {
            ResultList<Plugin_Reserach_MediaEntity> Result = new ResultList<Plugin_Reserach_MediaEntity>();

            Result = await Plugin_Reserach_MediaDomain.GetAll();
            if (Result.Status == ErrorEnums.Success)
            {
                lstCoreValue.DataSource = Result.List.ToList();
                lstCoreValue.DataBind();
            }
        }

        protected void lstCoreValue_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListViewItemType.DataItem)
                {
                    Label lblPublishDate = (Label)e.Item.FindControl("lblPublishDate");
                    lblPublishDate.Text = Convert.ToDateTime(lblPublishDate.Text).ToString("dd-MM-yyyy");

                }
            }
            catch (Exception ex) { }

        }

        protected void lstCoreValue_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "ViewEdit")
            {
                int ID = Convert.ToInt32(e.CommandArgument);

                Session["MediaSession"] = ID;

                Response.Redirect("~/Plugins/Media/EditMedia.aspx", false);
            }
            if (e.CommandName == "DeleteItem")
            {
                int ID = Convert.ToInt32(e.CommandArgument);
                Session["MediaSession"] = ID;
                mpeSuccess.Show();
            }
        }

        protected async void DeleteItem()
        {
            if (Session["MediaSession"] != null)
            {
                Plugin_Reserach_MediaEntity entity = new Plugin_Reserach_MediaEntity();

                entity.MediaID = Convert.ToInt64(Session["MediaSession"].ToString());
                entity.IsDelete = true;
                var Result = await Plugin_Reserach_AuthorDomain.DeleteRecord(entity.MediaID);
                if (Result.Status == ErrorEnums.Success)
                {
                    Session["MediaSession"] = null;
                }
            }
            foreach (ListViewItem item in lstCoreValue.Items)
            {
                CheckBox CBID = ((CheckBox)item.FindControl("CBID"));
                if (CBID.Checked)
                {
                    Label mediaID = ((Label)item.FindControl("lblID"));
                    Plugin_Reserach_MediaEntity entity = new Plugin_Reserach_MediaEntity();
                    entity.MediaID = Convert.ToInt64(mediaID.Text);
                    entity.IsDelete = true;

                    var Result = await Plugin_Reserach_MediaDomain.DeleteRecord(entity.MediaID);
                    if (Result.Status == ErrorEnums.Success)
                    {
                        // mpeSuccess.Show();
                    }
                }
            }

        }

        protected void btnAdd2_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Plugins/Media/AddMedia.aspx", false);
        }

        protected void BtnDelete2_Click(object sender, EventArgs e)
        {
            mpeSuccess.Show();
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            DeleteItem();
            Response.Redirect("~/Plugins/Media/ManageMedia.aspx", false);
        }
    }
}