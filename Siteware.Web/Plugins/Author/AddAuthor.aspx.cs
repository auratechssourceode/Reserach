﻿using Siteware.Web.AppCode;
using Siteware.Web.Models;
using SiteWare.Domain.Domains;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Common.Enums;
using SiteWare.Entity.Common.ImageResize;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Siteware.Web.Plugins.Author
{
    public partial class AddAuthor : System.Web.UI.Page
    {
        string PageName = "Author";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!FunctionSecurity.TestUserPermissionPage(SessionManager.GetInstance.Users.UserID, PageName))
                {
                    Response.Redirect("~/DashBoard.aspx", false);
                }
                if (!IsPostBack)
                {
                    if (SessionManager.GetInstance.Users != null)
                    {
                        FillNavigation();
                        FillLanguages();
                    }
                    else
                    {
                        Session.Abandon();
                        Session.Clear();
                        Response.Redirect("~/Login.aspx", false);
                    }
                }

            }
            catch
            {
                Session.Abandon();
                Session.Clear();
                Response.Redirect("~/Login.aspx", false);
            }
        }


        protected void FillNavigation()
        {
            var masterPage = this.Master;
            if (masterPage != null)
            {

                Session["IDSelectPage"] = "~/Plugins/Author/ManageAuthor.aspx";

            }
        }
        protected async void FillLanguages()
        {
            ddlLanguages.Items.Insert(0, new ListItem("Select Language", "0"));

            ResultList<LanguageEntity> Result = new ResultList<LanguageEntity>();
            Result = await LanguageDomain.GetLanguagesAll();

            if (Result.Status == ErrorEnums.Success)
            {
                foreach (LanguageEntity item in Result.List)
                {
                    ddlLanguages.Items.Add(new ListItem(item.Name.ToString(), item.ID.ToString()));
                }
            }
        }

        protected async void btnAdd2_Click(object sender, EventArgs e)
        {
            Plugin_Reserach_AuthorEntity entity = new Plugin_Reserach_AuthorEntity();

            entity.AuthorTitle = txtTitle.Text.Trim();
            entity.AuthorName = txtAuthorName.Text.Trim();            
            entity.AuthorSummery = txtSummary.Text;
            entity.AuthorImage = newWinField.Value;
            entity.AuthorDetails = txtDescription.Text;
            entity.Order = Convert.ToInt64(txtorderr.Text);
            entity.LanguageID = Convert.ToInt32(ddlLanguages.SelectedValue);
            entity.IsPublished = CBIsPublished.Checked;
            entity.PublishedDate = DateTime.ParseExact(txtPublishDate.Value.ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
            entity.IsDelete = false;
            entity.AddDate = Convert.ToDateTime(DateTime.Now);
            entity.AddUser = SessionManager.GetInstance.Users.UserID.ToString();
            entity.EditDate = Convert.ToDateTime(DateTime.Now);
            entity.EditUser = SessionManager.GetInstance.Users.UserID.ToString();

            var Result = await Plugin_Reserach_AuthorDomain.InsertRecord(entity);





            #region --> Image Processing | 20201125
            if (Result.Status == ErrorEnums.Success && Result.Entity.AuthorsID > 0)
            {
                //string localURL = ConfigurationManager.AppSettings["NewsImageURL"];
                string fileUploadPath = ConfigurationManager.AppSettings["AuthorFileUploadPath"]; //"/Siteware/Siteware_File/image/JoWomenomics/News/";
                                                                                                   //string orgFilePath = localURL + entity.Image;
                string orgFilePath = Server.MapPath(entity.AuthorImage);

                List<ImageSizeVM> lstWeightHeight = new List<ImageSizeVM>();

                Tuple<int, int> iSize1A = AuthorImageSize.GetImgSize(1);
                lstWeightHeight.Add(new ImageSizeVM() { Width = iSize1A.Item1, Height = iSize1A.Item2 });


                //Tuple<int, int> iSize1 = ImageSize.GetImgSize(1);
                //lstWeightHeight.Add(new ImageSizeVM() { Width = iSize1.Item1, Height = iSize1.Item2 });
                //Tuple<int, int> iSize2 = ImageSize.GetImgSize(2);
                //lstWeightHeight.Add(new ImageSizeVM() { Width = iSize2.Item1, Height = iSize2.Item2 });                
                //lstWeightHeight.Add(new ImageSizeVM() { Width = 570, Height = 694 });
                foreach (ImageSizeVM isz in lstWeightHeight)
                {
                    ImageProcess iu = new ImageProcess();
                    try
                    {
                        ImageResultVM result = iu.ImageProcessUpload(Result.Entity.AuthorsID, isz.Width, isz.Height, orgFilePath, fileUploadPath);
                        if (result.Success)
                        {
                            //JoWomenomics_Plugin_PillarsImagesEntity ip = new JoWomenomics_Plugin_PillarsImagesEntity();
                            Plugin_Reserach_AuthorImageEntity ip = new Plugin_Reserach_AuthorImageEntity();
                            ip.AuthorID = Result.Entity.AuthorsID;
                            ip.AddDate = DateTime.Now;
                            ip.FileName = result.ImageName;
                            ip.Path = fileUploadPath + result.ImageName;
                            ip.Width = isz.Width;
                            ip.Height = isz.Height;
                            ip.DeleteDate = DateTime.Now;
                            var rs = await Plugin_Reserach_AuthorImageDomain.InsertRecord(ip);
                            //var rs = await Plugin_NewsImagesDomain.InsertRecord(ip);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            #endregion



            if (Result.Status == ErrorEnums.Success)
            {
                mpeSuccess.Show();
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Plugins/Author/ManageAuthor.aspx", false);
        }
    }
}