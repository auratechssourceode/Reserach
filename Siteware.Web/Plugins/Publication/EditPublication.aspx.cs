﻿using Siteware.Web.AppCode;
using SiteWare.Domain.Domains;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Common.Enums;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Siteware.Web.Plugins.Publication
{
    public partial class EditPublication : System.Web.UI.Page
    {
        string PageName = "Publication";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!FunctionSecurity.TestUserPermissionPage(SessionManager.GetInstance.Users.UserID, PageName))
                {
                    Response.Redirect("~/DashBoard.aspx", false);
                }
                if (!IsPostBack)
                {
                    if (SessionManager.GetInstance.Users != null)
                    {
                        FillNavigation();
                        FillLanguages();
                        FillCategory();
                        FillDetails();
                    }
                    else
                    {
                        Session.Abandon();
                        Session.Clear();
                        Response.Redirect("~/Login.aspx", false);
                    }
                }

            }
            catch
            {
                Session.Abandon();
                Session.Clear();
                Response.Redirect("~/Login.aspx", false);
            }
        }

        protected void FillNavigation()
        {
            var masterPage = this.Master;
            if (masterPage != null)
            {

                Session["IDSelectPage"] = "~/Plugins/Publication/ManagePublication.aspx";

            }
        }
        protected void FillLanguages()
        {
            ddlLanguages.Items.Insert(0, new ListItem("Select Language", "0"));

            ResultList<LanguageEntity> Result = new ResultList<LanguageEntity>();
            Result = LanguageDomain.GetLanguagesAllNotAsync();

            if (Result.Status == ErrorEnums.Success)
            {
                foreach (LanguageEntity item in Result.List)
                {
                    ddlLanguages.Items.Add(new ListItem(item.Name.ToString(), item.ID.ToString()));
                }
            }
        }

        protected async void FillCategory()
        {
            ddlCatogory.Items.Insert(0, new ListItem("Select Category", "0"));

            ResultList<Lookup_Reserach_PublicationCategoryEntity> Result = new ResultList<Lookup_Reserach_PublicationCategoryEntity>();
            Result = await Lookup_Reserach_PublicationCategoryDomain.GetLookupAreaAll();

            if (Result.Status == ErrorEnums.Success)
            {
                foreach (Lookup_Reserach_PublicationCategoryEntity item in Result.List)
                {
                    ddlCatogory.Items.Add(new ListItem(item.name.ToString(), item.PublicationCategoryID.ToString()));
                }
            }
        }

        protected async void FillDetails()
        {
            try
            {
                if (Session["PublicationSession"] != null)
                {
                    long CoreID = Convert.ToInt64(Session["PublicationSession"]);

                    ResultEntity<Plugin_Reserach_PublicationEntity> Result = new ResultEntity<Plugin_Reserach_PublicationEntity>();

                    Result = await Plugin_Reserach_PublicationDomain.GetByID(CoreID);
                    if (Result.Status == ErrorEnums.Success)
                    {

                        
                        
                        //entity.PublicationsSummery = txtSummary.Text;
                        //entity.PublicationsDetails = txtDescription.Text;
                        //entity.PublicationsCatogory = txtPublicationCategory.Text.Trim();
                        //entity.PublicationsImage = newWinField.Value;


                        lblHeadName1.Text = Result.Entity.PublicationsTitle;
                        lblHeadName2.Text = Result.Entity.PublicationsTitle;
                        txtTitle.Text = Result.Entity.PublicationsTitle;
                        txtArticleDate.Value = Result.Entity.PublicationsDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                        txtVersion.Text = Result.Entity.PublicationsVersion;


                        txtSummary.Text = Result.Entity.PublicationsSummery;
                        txtAuthor.Text = Result.Entity.PublicationsAuthor;
                        //txtPublicationCategory.Text = Result.Entity.PublicationsCatogory;
                        ddlCatogory.SelectedValue = Result.Entity.PublicationsCatogory;
                        ImagePage.ImageUrl = Result.Entity.PublicationsImage;
                        newWinField.Value = Result.Entity.PublicationsImage;

                        txtDescription.Text = Result.Entity.PublicationsDetails;
                        txtorderr.Text = Result.Entity.Order.ToString();

                        ddlLanguages.SelectedValue = Result.Entity.LanguageID.ToString();
                        CBIsPublished.Checked = Result.Entity.IsPublished;
                        txtPublishDate.Value = Result.Entity.PublishedDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);


                        if (Result.Entity.PublicationsImage2 != null && Result.Entity.PublicationsImage2 != "")
                        {
                            ImagePage2.ImageUrl = Result.Entity.PublicationsImage2.ToString();
                            newWinField2.Value = Result.Entity.PublicationsImage2.ToString();
                        }

                        txtTitle2.Text = Result.Entity.PublicationsTitle2;
                        lblViewCount.Text = Result.Entity.ViewCount.ToString();

                    }
                }
            }
            catch (Exception ex)
            {
            }

        }

        protected async void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["PublicationSession"] != null)
                {
                    Plugin_Reserach_PublicationEntity entity = new Plugin_Reserach_PublicationEntity();
                    entity.PublicationsID = Convert.ToInt64(Session["PublicationSession"]);

                    entity.PublicationsTitle = txtTitle.Text.Trim();
                    entity.PublicationsDate = DateTime.ParseExact(txtArticleDate.Value.ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    entity.PublicationsAuthor = txtAuthor.Text.Trim();
                    entity.PublicationsVersion = txtVersion.Text;
                    entity.PublicationsSummery = txtSummary.Text;
                    entity.PublicationsDetails = txtDescription.Text;
                    // entity.PublicationsCatogory = txtPublicationCategory.Text.Trim();
                    entity.PublicationsCatogory = ddlCatogory.SelectedValue;
                    entity.PublicationsImage = newWinField.Value;

                    entity.Order = Convert.ToInt64(txtorderr.Text);

                    entity.LanguageID = Convert.ToInt32(ddlLanguages.SelectedValue);
                    entity.IsPublished = CBIsPublished.Checked;
                    entity.PublishedDate = DateTime.ParseExact(txtPublishDate.Value.ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    entity.IsDelete = false;
                    entity.AddDate = Convert.ToDateTime(DateTime.Now);
                    entity.AddUser = SessionManager.GetInstance.Users.UserID.ToString();
                    entity.EditDate = Convert.ToDateTime(DateTime.Now);
                    entity.EditUser = SessionManager.GetInstance.Users.UserID.ToString();

                    entity.ViewCount = Convert.ToInt64(lblViewCount.Text);
                    entity.PublicationsImage2 = newWinField2.Value.ToString();
                    entity.PublicationsTitle2 = txtTitle2.Text.Trim();

                    var Result = await Plugin_Reserach_PublicationDomain.UpdateRecord(entity);

                    if (Result.Status == ErrorEnums.Success)
                    {
                        mpeSuccess.Show();
                    }
                }
                else
                {
                    Response.Redirect("~/Plugins/Publication/ManagePublication.aspx", false);
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Plugins/Publication/ManagePublication.aspx", false);
        }
    }
}