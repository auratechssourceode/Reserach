﻿using Siteware.Web.AppCode;
using SiteWare.Domain.Domains;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Common.Enums;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Siteware.Web.Plugins.ResearchStudies
{
    public partial class EditResearchStudies : System.Web.UI.Page
    {
        string PageName = "ResearchStudies";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!FunctionSecurity.TestUserPermissionPage(SessionManager.GetInstance.Users.UserID, PageName))
                {
                    Response.Redirect("~/DashBoard.aspx", false);
                }
                if (!IsPostBack)
                {
                    if (SessionManager.GetInstance.Users != null)
                    {
                        FillNavigation();
                        FillLanguages();
                        FillCategory();
                        FillDetails();
                    }
                    else
                    {
                        Session.Abandon();
                        Session.Clear();
                        Response.Redirect("~/Login.aspx", false);
                    }
                }

            }
            catch
            {
                Session.Abandon();
                Session.Clear();
                Response.Redirect("~/Login.aspx", false);
            }
        }

        protected void FillNavigation()
        {
            var masterPage = this.Master;
            if (masterPage != null)
            {

                Session["IDSelectPage"] = "~/Plugins/ResearchStudies/ManageResearchStudies.aspx";

            }
        }
        protected void FillLanguages()
        {
            ddlLanguages.Items.Insert(0, new ListItem("Select Language", "0"));

            ResultList<LanguageEntity> Result = new ResultList<LanguageEntity>();
            Result = LanguageDomain.GetLanguagesAllNotAsync();

            if (Result.Status == ErrorEnums.Success)
            {
                foreach (LanguageEntity item in Result.List)
                {
                    ddlLanguages.Items.Add(new ListItem(item.Name.ToString(), item.ID.ToString()));
                }
            }
        }

        protected async void FillCategory()
        {
            ddlCatogory.Items.Insert(0, new ListItem("Select Category", "0"));

            ResultList<Lookup_Reserach_PublicationCategoryEntity> Result = new ResultList<Lookup_Reserach_PublicationCategoryEntity>();
            Result = await Lookup_Reserach_PublicationCategoryDomain.GetLookupAreaAll();

            if (Result.Status == ErrorEnums.Success)
            {
                foreach (Lookup_Reserach_PublicationCategoryEntity item in Result.List)
                {
                    ddlCatogory.Items.Add(new ListItem(item.name.ToString(), item.PublicationCategoryID.ToString()));
                }
            }
        }

        protected async void FillDetails()
        {
            try
            {
                if (Session["ResearchStudiesSession"] != null)
                {
                    long CoreID = Convert.ToInt64(Session["ResearchStudiesSession"]);

                    ResultEntity<Plugin_Reserach_ResearchStudiesEntity> Result = new ResultEntity<Plugin_Reserach_ResearchStudiesEntity>();

                    Result = await Plugin_Reserach_ResearchStudiesDomain.GetByID(CoreID);
                    if (Result.Status == ErrorEnums.Success)
                    {

                        lblHeadName1.Text = Result.Entity.ResearchStudiesTitle;
                        lblHeadName2.Text = Result.Entity.ResearchStudiesTitle;
                        txtTitle.Text = Result.Entity.ResearchStudiesTitle;
                        txtArticleDate.Value = Result.Entity.ResearchStudiesDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                        //txtVersion.Text = Result.Entity.ResearchStudiesVersion;
                        txtSummary.Text = Result.Entity.ResearchStudiesSummery;
                        //txtAuthor.Text = Result.Entity.ResearchStudiesAuthor;                       
                        ddlCatogory.SelectedValue = Result.Entity.ResearchStudiesCatogory;
                        ImagePage.ImageUrl = Result.Entity.ResearchStudiesImage;
                        newWinField.Value = Result.Entity.ResearchStudiesImage;
                        //txtDescription.Text = Result.Entity.ResearchStudiesDetails;
                        txtorderr.Text = Result.Entity.Order.ToString();
                        ddlLanguages.SelectedValue = Result.Entity.LanguageID.ToString();
                        CBIsPublished.Checked = Result.Entity.IsPublished;
                        txtPublishDate.Value = Result.Entity.PublishedDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                        if (Result.Entity.ResearchStudiesImage2 != null && Result.Entity.ResearchStudiesImage2 != "")
                        {
                            ImagePage2.ImageUrl = Result.Entity.ResearchStudiesImage2.ToString();
                            newWinField2.Value = Result.Entity.ResearchStudiesImage2.ToString();
                        }


                        txtTitle2.Text = Result.Entity.ResearchStudiesTitle2;
                        lblViewCount.Text = Result.Entity.ViewCount.ToString();

                        ddlParentPage.SelectedValue = Result.Entity.Target;
                        txtLink.Text = Result.Entity.MedialLink;

                    }
                }
            }
            catch (Exception ex)
            {
            }

        }

        protected async void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["ResearchStudiesSession"] != null)
                {
                    Plugin_Reserach_ResearchStudiesEntity entity = new Plugin_Reserach_ResearchStudiesEntity();

                    entity.ResearchStudiesID = Convert.ToInt64(Session["ResearchStudiesSession"]);

                    entity.ResearchStudiesTitle = txtTitle.Text.Trim();
                    entity.ResearchStudiesDate = DateTime.ParseExact(txtArticleDate.Value.ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    entity.ResearchStudiesAuthor = string.Empty; // txtAuthor.Text.Trim();
                    entity.ResearchStudiesVersion = string.Empty; // txtVersion.Text;
                    entity.ResearchStudiesSummery = txtSummary.Text;
                    entity.ResearchStudiesDetails = string.Empty; // txtDescription.Text;
                    // entity.ResearchStudiesCatogory = txtPublicationCategory.Text.Trim();
                    entity.ResearchStudiesCatogory = ddlCatogory.SelectedValue;
                    entity.ResearchStudiesImage = newWinField.Value;

                    entity.Order = Convert.ToInt64(txtorderr.Text);

                    entity.LanguageID = Convert.ToInt32(ddlLanguages.SelectedValue);
                    entity.IsPublished = CBIsPublished.Checked;
                    entity.PublishedDate = DateTime.ParseExact(txtPublishDate.Value.ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    entity.IsDelete = false;
                    entity.AddDate = Convert.ToDateTime(DateTime.Now);
                    entity.AddUser = SessionManager.GetInstance.Users.UserID.ToString();
                    entity.EditDate = Convert.ToDateTime(DateTime.Now);
                    entity.EditUser = SessionManager.GetInstance.Users.UserID.ToString();

                    entity.ViewCount = Convert.ToInt64(lblViewCount.Text);
                    entity.ResearchStudiesImage2 = newWinField2.Value.ToString();
                    entity.ResearchStudiesTitle2 = txtTitle2.Text.Trim();

                    entity.MedialLink = txtLink.Text;
                    entity.Target = ddlParentPage.SelectedValue;

                    var Result = await Plugin_Reserach_ResearchStudiesDomain.UpdateRecord(entity);

                    if (Result.Status == ErrorEnums.Success)
                    {
                        mpeSuccess.Show();
                    }
                }
                else
                {
                    Response.Redirect("~/Plugins/ResearchStudies/ManageResearchStudies.aspx", false);
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Plugins/ResearchStudies/ManageResearchStudies.aspx", false);
        }
    }


}