﻿using Siteware.Web.AppCode;
using SiteWare.Domain.Domains;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Common.Enums;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Siteware.Web.Plugins.ResearchStudies
{
    public partial class ManageResearchStudies : System.Web.UI.Page
    {
        string PageName = "ResearchStudies";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!FunctionSecurity.TestUserPermissionPage(SessionManager.GetInstance.Users.UserID, PageName))
                {
                    Response.Redirect("~/DashBoard.aspx", false);
                }

                if (!IsPostBack)
                {
                    if (SessionManager.GetInstance.Users != null)
                    {
                        FillNavigation();
                        Session["ResearchStudiesSession"] = null;
                        FillData();
                    }
                    else
                    {
                        Session.Abandon();
                        Session.Clear();
                        Response.Redirect("~/Login.aspx", false);
                    }
                }

            }
            catch
            {
                Session.Abandon();
                Session.Clear();
                Response.Redirect("~/Login.aspx", false);

            }
        }

        protected void FillNavigation()
        {
            var masterPage = this.Master;
            if (masterPage != null)
            {
                Session["IDSelectPage"] = "~/Plugins/ResearchStudies/ManageResearchStudies.aspx";

            }
        }

        [WebMethod]
        public static LangDetails[] BindDatatoDropdown()
        {
            try
            {
                ResultList<LanguageEntity> Result = new ResultList<LanguageEntity>();
                List<LangDetails> details = new List<LangDetails>();
                Result = LanguageDomain.GetLanguagesAllNotAsync();
                if (Result.Status == ErrorEnums.Success)
                {
                    foreach (var item in Result.List)
                    {
                        LangDetails lang = new LangDetails();
                        lang.LangId = item.ID;
                        lang.LangName = item.Name;
                        details.Add(lang);
                    }
                }
                return details.ToArray();
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public class LangDetails
        {
            public int LangId { get; set; }
            public string LangName { get; set; }
        }

        protected async void FillData()
        {
            ResultList<Plugin_Reserach_ResearchStudiesEntity> Result = new ResultList<Plugin_Reserach_ResearchStudiesEntity>();

            Result = await Plugin_Reserach_ResearchStudiesDomain.GetAll();
            if (Result.Status == ErrorEnums.Success)
            {
                lstCoreValue.DataSource = Result.List.ToList();
                lstCoreValue.DataBind();
            }
        }

        protected void lstCoreValue_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListViewItemType.DataItem)
                {
                    Label lblPublishDate = (Label)e.Item.FindControl("lblPublishDate");
                    lblPublishDate.Text = Convert.ToDateTime(lblPublishDate.Text).ToString("dd-MM-yyyy");

                }
            }
            catch (Exception ex) { }

        }

        protected void lstCoreValue_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "ViewEdit")
            {
                int ID = Convert.ToInt32(e.CommandArgument);

                Session["ResearchStudiesSession"] = ID;

                Response.Redirect("~/Plugins/ResearchStudies/EditResearchStudies.aspx", false);
            }
            if (e.CommandName == "DeleteItem")
            {
                int ID = Convert.ToInt32(e.CommandArgument);
                Session["ResearchStudiesSession"] = ID;
                mpeSuccess.Show();
            }
        }

        protected async void DeleteItem()
        {
            if (Session["ResearchStudiesSession"] != null)
            {
                Plugin_Reserach_ResearchStudiesEntity entity = new Plugin_Reserach_ResearchStudiesEntity();

                entity.ResearchStudiesID = Convert.ToInt64(Session["ResearchStudiesSession"].ToString());
                entity.IsDelete = true;
                var Result = await Plugin_Reserach_ResearchStudiesDomain.DeleteRecord(entity.ResearchStudiesID);
                if (Result.Status == ErrorEnums.Success)
                {
                    Session["ResearchStudiesSession"] = null;
                }
            }
            foreach (ListViewItem item in lstCoreValue.Items)
            {
                CheckBox CBID = ((CheckBox)item.FindControl("CBID"));
                if (CBID.Checked)
                {
                    Label articleID = ((Label)item.FindControl("lblID"));
                    Plugin_Reserach_ResearchStudiesEntity entity = new Plugin_Reserach_ResearchStudiesEntity();
                    entity.ResearchStudiesID = Convert.ToInt64(articleID.Text);
                    entity.IsDelete = true;

                    var Result = await Plugin_Reserach_ResearchStudiesDomain.DeleteRecord(entity.ResearchStudiesID);
                    if (Result.Status == ErrorEnums.Success)
                    {
                        // mpeSuccess.Show();
                    }
                }
            }

        }

        protected void btnAdd2_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Plugins/ResearchStudies/AddResearchStudies.aspx", false);
        }

        protected void BtnDelete2_Click(object sender, EventArgs e)
        {
            mpeSuccess.Show();
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            DeleteItem();
            Response.Redirect("~/Plugins/ResearchStudies/ManageResearchStudies.aspx", false);
        }
    }
}