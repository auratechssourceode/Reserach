﻿using Siteware.Web.AppCode;
using SiteWare.Domain.Domains;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Common.Enums;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Siteware.Web.Plugins.ResearchInner
{
    public partial class EditResearchInner : System.Web.UI.Page
    {
        string PageName = "ResearchInner";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!FunctionSecurity.TestUserPermissionPage(SessionManager.GetInstance.Users.UserID, PageName))
                {
                    Response.Redirect("~/DashBoard.aspx", false);
                }
                if (!IsPostBack)
                {
                    if (SessionManager.GetInstance.Users != null)
                    {
                        FillNavigation();
                        FillLanguages();
                        FillCategory();
                        FillDetails();
                    }
                    else
                    {
                        Session.Abandon();
                        Session.Clear();
                        Response.Redirect("~/Login.aspx", false);
                    }
                }

            }
            catch
            {
                Session.Abandon();
                Session.Clear();
                Response.Redirect("~/Login.aspx", false);
            }
        }

        protected void FillNavigation()
        {
            var masterPage = this.Master;
            if (masterPage != null)
            {

                Session["IDSelectPage"] = "~/Plugins/ResearchInner/ManageResearchInner.aspx";

            }
        }
        protected void FillLanguages()
        {
            ddlLanguages.Items.Insert(0, new ListItem("Select Language", "0"));

            ResultList<LanguageEntity> Result = new ResultList<LanguageEntity>();
            Result = LanguageDomain.GetLanguagesAllNotAsync();

            if (Result.Status == ErrorEnums.Success)
            {
                foreach (LanguageEntity item in Result.List)
                {
                    ddlLanguages.Items.Add(new ListItem(item.Name.ToString(), item.ID.ToString()));
                }
            }
        }

        protected async void FillCategory()
        {
            ddlCatogory.Items.Insert(0, new ListItem("Select Category", "0"));

            ResultList<Lookup_Reserach_PublicationCategoryEntity> Result = new ResultList<Lookup_Reserach_PublicationCategoryEntity>();
            Result = await Lookup_Reserach_PublicationCategoryDomain.GetLookupAreaAll();

            if (Result.Status == ErrorEnums.Success)
            {
                foreach (Lookup_Reserach_PublicationCategoryEntity item in Result.List)
                {
                    ddlCatogory.Items.Add(new ListItem(item.name.ToString(), item.PublicationCategoryID.ToString()));
                }
            }
        }

        protected async void FillDetails()
        {
            try
            {
                if (Session["ResearchInnserSession"] != null)
                {
                    long CoreID = Convert.ToInt64(Session["ResearchInnserSession"]);

                    ResultEntity<Plugin_Reserach_ResearchInnerEntity> Result = new ResultEntity<Plugin_Reserach_ResearchInnerEntity>();

                    Result = await Plugin_Reserach_ResearchInnerDomain.GetByID(CoreID);
                    if (Result.Status == ErrorEnums.Success)
                    {

                        lblHeadName1.Text = Result.Entity.ResearchInnerTitle;
                        lblHeadName2.Text = Result.Entity.ResearchInnerTitle;
                        txtTitle.Text = Result.Entity.ResearchInnerTitle;
                        //txtArticleDate.Value = Result.Entity.ResearchStudiesDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                        //txtVersion.Text = Result.Entity.ResearchStudiesVersion;
                        txtSummary.Text = Result.Entity.ResearchInnerSummery;
                        //txtAuthor.Text = Result.Entity.ResearchStudiesAuthor;                       
                        ddlCatogory.SelectedValue = Result.Entity.ResearchInnerCatogory;
                        ImagePage.ImageUrl = Result.Entity.ResearchInnerImage;
                        newWinField.Value = Result.Entity.ResearchInnerImage;
                        //txtDescription.Text = Result.Entity.ResearchStudiesDetails;
                        txtorderr.Text = Result.Entity.Order.ToString();
                        ddlLanguages.SelectedValue = Result.Entity.LanguageID.ToString();
                        CBIsPublished.Checked = Result.Entity.IsPublished;
                        txtPublishDate.Value = Result.Entity.PublishedDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                        //if (Result.Entity.im != null && Result.Entity.ResearchStudiesImage2 != "")
                        //{
                        //    ImagePage2.ImageUrl = Result.Entity.ResearchStudiesImage2.ToString();
                        //    newWinField2.Value = Result.Entity.ResearchStudiesImage2.ToString();
                        //}

                        txtArticleDate.Value = Result.Entity.ResearchInnerDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                        txtTitle2.Text = Result.Entity.ResearchInnerTitle2;
                        lblViewCount.Text = Result.Entity.ViewCount.ToString();

                        ddlParentPage.SelectedValue = Result.Entity.Target;
                        txtLink.Text = Result.Entity.MedialLink;

                    }
                }
            }
            catch (Exception ex)
            {
            }

        }

        protected async void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["ResearchInnserSession"] != null)
                {
                    Plugin_Reserach_ResearchInnerEntity entity = new Plugin_Reserach_ResearchInnerEntity();

                    entity.ResearchInnerID = Convert.ToInt64(Session["ResearchInnserSession"]);
                    entity.ResearchInnerTitle = txtTitle.Text.Trim();
                    //entity.ResearchStudiesDate = DateTime.ParseExact(txtArticleDate.Value.ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    
                    entity.ResearchInnerSummery = txtSummary.Text;
                    //entity.de = string.Empty; // txtDescription.Text;
                    // entity.ResearchStudiesCatogory = txtPublicationCategory.Text.Trim();
                    entity.ResearchInnerCatogory = ddlCatogory.SelectedValue;
                    entity.ResearchInnerImage = newWinField.Value;
                    entity.Order = Convert.ToInt64(txtorderr.Text);
                    entity.LanguageID = Convert.ToInt32(ddlLanguages.SelectedValue);
                    entity.IsPublished = CBIsPublished.Checked;
                    entity.PublishedDate = DateTime.ParseExact(txtPublishDate.Value.ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    entity.IsDelete = false;
                    entity.AddDate = Convert.ToDateTime(DateTime.Now);
                    entity.AddUser = SessionManager.GetInstance.Users.UserID.ToString();
                    entity.EditDate = Convert.ToDateTime(DateTime.Now);
                    entity.EditUser = SessionManager.GetInstance.Users.UserID.ToString();
                    entity.ViewCount = Convert.ToInt64(lblViewCount.Text);
                    //entity.ResearchInnerImage = newWinField2.Value.ToString();
                    entity.ResearchInnerTitle2 = txtTitle2.Text.Trim();
                    entity.MedialLink = txtLink.Text;
                    entity.Target = ddlParentPage.SelectedValue;
                    entity.ResearchInnerDate = DateTime.ParseExact(txtArticleDate.Value.ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture);

                    var Result = await Plugin_Reserach_ResearchInnerDomain.UpdateRecord(entity);

                    if (Result.Status == ErrorEnums.Success)
                    {
                        mpeSuccess.Show();
                    }
                }
                else
                {
                    Response.Redirect("~/Plugins/ResearchInner/ManageResearchInner.aspx", false);
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Plugins/ResearchInner/ManageResearchInner.aspx", false);
        }
    }
}