﻿using Siteware.Web.AppCode;
using SiteWare.Domain.Domains;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Common.Enums;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Siteware.Web.Plugins.Article
{
    public partial class EditArticle : System.Web.UI.Page
    {
        string PageName = "Article";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!FunctionSecurity.TestUserPermissionPage(SessionManager.GetInstance.Users.UserID, PageName))
                {
                    Response.Redirect("~/DashBoard.aspx", false);
                }
                if (!IsPostBack)
                {
                    if (SessionManager.GetInstance.Users != null)
                    {
                        FillNavigation();
                        FillLanguages();
                        FillCategory();
                        FillDetails();
                    }
                    else
                    {
                        Session.Abandon();
                        Session.Clear();
                        Response.Redirect("~/Login.aspx", false);
                    }
                }

            }
            catch
            {
                Session.Abandon();
                Session.Clear();
                Response.Redirect("~/Login.aspx", false);
            }
        }

        protected void FillNavigation()
        {
            var masterPage = this.Master;
            if (masterPage != null)
            {
               
                Session["IDSelectPage"] = "~/Plugins/Article/ManageArticle.aspx";

            }
        }
        protected void FillLanguages()
        {
            ddlLanguages.Items.Insert(0, new ListItem("Select Language", "0"));

            ResultList<LanguageEntity> Result = new ResultList<LanguageEntity>();
            Result = LanguageDomain.GetLanguagesAllNotAsync();

            if (Result.Status == ErrorEnums.Success)
            {
                foreach (LanguageEntity item in Result.List)
                {
                    ddlLanguages.Items.Add(new ListItem(item.Name.ToString(), item.ID.ToString()));
                }
            }
        }

        protected async void FillCategory()
        {
            ddlCatogory.Items.Insert(0, new ListItem("Select Category", "0"));

            ResultList<Lookup_Reserach_ArticleCategoryEntity> Result = new ResultList<Lookup_Reserach_ArticleCategoryEntity>();
            Result = await Lookup_Reserach_ArticleCategoryDomain.GetLookupAreaAll();

            if (Result.Status == ErrorEnums.Success)
            {
                foreach (Lookup_Reserach_ArticleCategoryEntity item in Result.List)
                {
                    ddlCatogory.Items.Add(new ListItem(item.name.ToString(), item.ArticleCategoryID.ToString()));
                }
            }
        }

        protected async void FillDetails()
        {
            try
            {
                if (Session["ArticleSession"] != null)
                {
                    long CoreID = Convert.ToInt64(Session["ArticleSession"]);

                    ResultEntity<Plugin_Reserach_ArticleEntity> Result = new ResultEntity<Plugin_Reserach_ArticleEntity>();

                    Result = await Plugin_Reserach_ArticleDomain.GetByID(CoreID);
                    if (Result.Status == ErrorEnums.Success)
                    {

                        lblHeadName1.Text = Result.Entity.ArticleTitle;
                        lblHeadName2.Text = Result.Entity.ArticleTitle;
                        txtTitle.Text = Result.Entity.ArticleTitle;
                        txtArticleDate.Value = Result.Entity.ArticleDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                        txtKeyword.Text = Result.Entity.ArticleKeyWords;

                        txtSummary.Text = Result.Entity.ArticleSummery;
                        txtAuthor.Text = Result.Entity.ArticleAuthor;
                        //txtArticleCategory.Text = Result.Entity.ArticleCategory;
                        ddlCatogory.SelectedValue = Result.Entity.ArticleCategory;
                        ImagePage.ImageUrl = Result.Entity.ArticleImage;
                        newWinField.Value = Result.Entity.ArticleImage;
                        lblViewCount.Text = Result.Entity.ViewCount.ToString();
                        txtDescription.Text = Result.Entity.ArticleDetails;
                        txtorderr.Text = Result.Entity.Order.ToString();

                        ddlLanguages.SelectedValue = Result.Entity.LanguageID.ToString();
                        CBIsPublished.Checked = Result.Entity.IsPublished;
                        txtPublishDate.Value = Result.Entity.PublishedDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                        checkShowSlider.Checked = Result.Entity.IsShowSlider;

                    }
                }
            }
            catch (Exception ex)
            {
            }

        }

        protected async void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["ArticleSession"] != null)
                {
                    Plugin_Reserach_ArticleEntity entity = new Plugin_Reserach_ArticleEntity();
                    entity.ArticleID = Convert.ToInt64(Session["ArticleSession"]);

                    entity.ArticleTitle = txtTitle.Text.Trim();
                    entity.ArticleDate = DateTime.ParseExact(txtArticleDate.Value.ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    entity.ArticleKeyWords = txtKeyword.Text;
                    entity.ArticleSummery = txtSummary.Text;
                    entity.ArticleAuthor = txtAuthor.Text.Trim();
                    entity.ArticleCategory = ddlCatogory.SelectedValue;
                    //entity.ArticleCategory = txtArticleCategory.Text.Trim();
                    entity.ViewCount = Convert.ToInt64(lblViewCount.Text);
                    entity.ArticleImage = newWinField.Value;

                    entity.ArticleDetails = txtDescription.Text;
                    entity.Order = Convert.ToInt64(txtorderr.Text);

                    entity.LanguageID = Convert.ToInt32(ddlLanguages.SelectedValue);
                    entity.IsPublished = CBIsPublished.Checked;
                    entity.PublishedDate = DateTime.ParseExact(txtPublishDate.Value.ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    entity.IsDelete = false;
                    entity.AddDate = Convert.ToDateTime(DateTime.Now);
                    entity.AddUser = SessionManager.GetInstance.Users.UserID.ToString();
                    entity.EditDate = Convert.ToDateTime(DateTime.Now);
                    entity.EditUser = SessionManager.GetInstance.Users.UserID.ToString();
                    entity.IsShowSlider = checkShowSlider.Checked;
                    var Result = await Plugin_Reserach_ArticleDomain.UpdateRecord(entity);

                    if (Result.Status == ErrorEnums.Success)
                    {
                        mpeSuccess.Show();
                    }
                }
                else
                {
                    Response.Redirect("~/Plugins/CoreValue/ManageCoreValue.aspx", false);
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Plugins/Article/ManageArticle.aspx", false);
        }

       

        
    }
}