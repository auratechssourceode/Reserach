﻿using Siteware.Web.AppCode;
using SiteWare.Domain.Domains;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Common.Enums;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Siteware.Web.Plugins.Article
{
    public partial class AddArticle : System.Web.UI.Page
    {
        string PageName = "Article";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!FunctionSecurity.TestUserPermissionPage(SessionManager.GetInstance.Users.UserID, PageName))
                {
                    Response.Redirect("~/DashBoard.aspx", false);
                }
                if (!IsPostBack)
                {
                    if (SessionManager.GetInstance.Users != null)
                    {
                        FillNavigation();
                        FillLanguages();
                        FillCategory();
                    }
                    else
                    {
                        Session.Abandon();
                        Session.Clear();
                        Response.Redirect("~/Login.aspx", false);
                    }
                }

            }
            catch
            {
                Session.Abandon();
                Session.Clear();
                Response.Redirect("~/Login.aspx", false);
            }
        }

        protected void FillNavigation()
        {
            var masterPage = this.Master;
            if (masterPage != null)
            {
               
                Session["IDSelectPage"] = "~/Plugins/Article/ManageArticle.aspx";

            }
        }
        protected async void FillLanguages()
        {
            ddlLanguages.Items.Insert(0, new ListItem("Select Language", "0"));

            ResultList<LanguageEntity> Result = new ResultList<LanguageEntity>();
            Result = await LanguageDomain.GetLanguagesAll();

            if (Result.Status == ErrorEnums.Success)
            {
                foreach (LanguageEntity item in Result.List)
                {
                    ddlLanguages.Items.Add(new ListItem(item.Name.ToString(), item.ID.ToString()));
                }
            }
        }

        protected async void FillCategory()
        {
            ddlCatogory.Items.Insert(0, new ListItem("Select Category", "0"));

            ResultList<Lookup_Reserach_ArticleCategoryEntity> Result = new ResultList<Lookup_Reserach_ArticleCategoryEntity>();
            Result = await Lookup_Reserach_ArticleCategoryDomain.GetLookupAreaAll();

            if (Result.Status == ErrorEnums.Success)
            {
                foreach (Lookup_Reserach_ArticleCategoryEntity item in Result.List)
                {
                    ddlCatogory.Items.Add(new ListItem(item.name.ToString(), item.ArticleCategoryID.ToString()));
                }
            }
        }

        protected async void btnAdd2_Click(object sender, EventArgs e)
        {
            Plugin_Reserach_ArticleEntity entity = new Plugin_Reserach_ArticleEntity();

           

            entity.ArticleTitle = txtTitle.Text.Trim();
            entity.ArticleDate = DateTime.ParseExact(txtArticleDate.Value.ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
            entity.ArticleKeyWords = txtKeyword.Text;
            entity.ArticleSummery = txtSummary.Text;
            entity.ArticleAuthor = txtAuthor.Text.Trim();
           // entity.ArticleCategory = txtArticleCategory.Text.Trim();
            entity.ArticleCategory = ddlCatogory.SelectedValue;
            entity.ViewCount = 0;
            entity.ArticleImage = newWinField.Value;
           
            entity.ArticleDetails = txtDescription.Text;
            entity.Order = Convert.ToInt64(txtorderr.Text);
            
            entity.LanguageID = Convert.ToInt32(ddlLanguages.SelectedValue);
            entity.IsPublished = CBIsPublished.Checked;
            entity.PublishedDate = DateTime.ParseExact(txtPublishDate.Value.ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
            entity.IsDelete = false;
            entity.AddDate = Convert.ToDateTime(DateTime.Now);
            entity.AddUser = SessionManager.GetInstance.Users.UserID.ToString();
            entity.EditDate = Convert.ToDateTime(DateTime.Now);
            entity.EditUser = SessionManager.GetInstance.Users.UserID.ToString();
            entity.IsShowSlider = checkShowSlider.Checked;

            var Result = await Plugin_Reserach_ArticleDomain.InsertRecord(entity);

            if (Result.Status == ErrorEnums.Success)
            {
                mpeSuccess.Show();
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Plugins/Article/ManageArticle.aspx", false);
        }

       

        
    }
}