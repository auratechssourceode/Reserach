﻿using SiteWare.DataAccess.Common.Constants;
using SiteWare.DataAccess.RepositorieConstants;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Common.Enums;
using SiteWare.Entity.Constants.Entity;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.DataAccess.Repositories
{
    public static class Plugin_Reserach_AuthorRepository
    {
        public async static Task<ResultList<Plugin_Reserach_AuthorEntity>> SelectAll()
        {
            ResultList<Plugin_Reserach_AuthorEntity> result = new ResultList<Plugin_Reserach_AuthorEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_AuthorRepositoryConstants.SP_SelectAll, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            List<Plugin_Reserach_AuthorEntity> list = new List<Plugin_Reserach_AuthorEntity>();

            try
            {
                sqlConnection.Open();

                SqlDataReader reader = await sqlCommand.ExecuteReaderAsync();

                while (reader.Read())
                {
                    Plugin_Reserach_AuthorEntity entity = EntityHelper(reader, false);
                    list.Add(entity);
                }

                if (list.Count > 0)
                {
                    reader.Close();

                    result.List = list;

                }
                else
                {
                    result.Status = ErrorEnums.Information;
                    result.Details = MessageConstants.CannotFindAllMessage;
                    result.Message = MessageConstants.CannotFindAllDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
                result.Message = ex.Message;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
        public static ResultList<Plugin_Reserach_AuthorEntity> SelectAllNotAsync()
        {
            ResultList<Plugin_Reserach_AuthorEntity> result = new ResultList<Plugin_Reserach_AuthorEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_AuthorRepositoryConstants.SP_SelectAll, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            List<Plugin_Reserach_AuthorEntity> list = new List<Plugin_Reserach_AuthorEntity>();

            try
            {
                sqlConnection.Open();

                SqlDataReader reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    Plugin_Reserach_AuthorEntity entity = EntityHelper(reader, false);
                    list.Add(entity);
                }

                if (list.Count > 0)
                {
                    reader.Close();

                    result.List = list;

                }
                else
                {
                    result.Status = ErrorEnums.Information;
                    result.Details = MessageConstants.CannotFindAllMessage;
                    result.Message = MessageConstants.CannotFindAllDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
                result.Message = ex.Message;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_AuthorEntity>> Insert(Plugin_Reserach_AuthorEntity entity)
        {

            ResultEntity<Plugin_Reserach_AuthorEntity> result = new ResultEntity<Plugin_Reserach_AuthorEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_AuthorRepositoryConstants.SP_Insert, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                //sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AuthorsID, entity.AuthorsID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AuthorName, entity.AuthorName);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AuthorImage, entity.AuthorImage);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AuthorTitle, entity.AuthorTitle);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AuthorSummery, entity.AuthorSummery);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AuthorDetails, entity.AuthorDetails);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.Order, entity.Order);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.PublishedDate, entity.PublishedDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.IsPublished, entity.IsPublished);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.IsDelete, entity.IsDelete);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.LanguageID, entity.LanguageID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AddDate, entity.AddDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AddUser, entity.AddUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.EditDate, entity.EditDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.EditUser, entity.EditUser);


                sqlCommand.Parameters.Add(Plugin_Reserach_AuthorRepositoryConstants.AuthorsID, SqlDbType.BigInt).Direction = ParameterDirection.Output;

                SqlDataReader reader = await sqlCommand.ExecuteReaderAsync();
                result.Entity = entity;
                entity.AuthorsID = Convert.ToInt64(sqlCommand.Parameters[Plugin_Reserach_AuthorRepositoryConstants.AuthorsID].Value);

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.InsertSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
        public static ResultEntity<Plugin_Reserach_AuthorEntity> InsertNotAsync(Plugin_Reserach_AuthorEntity entity)
        {

            ResultEntity<Plugin_Reserach_AuthorEntity> result = new ResultEntity<Plugin_Reserach_AuthorEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_AuthorRepositoryConstants.SP_Insert, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                //sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AuthorsID, entity.AuthorsID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AuthorName, entity.AuthorName);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AuthorImage, entity.AuthorImage);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AuthorTitle, entity.AuthorTitle);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AuthorSummery, entity.AuthorSummery);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AuthorDetails, entity.AuthorDetails);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.Order, entity.Order);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.PublishedDate, entity.PublishedDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.IsPublished, entity.IsPublished);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.IsDelete, entity.IsDelete);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.LanguageID, entity.LanguageID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AddDate, entity.AddDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AddUser, entity.AddUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.EditDate, entity.EditDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.EditUser, entity.EditUser);
                sqlCommand.Parameters.Add(Plugin_Reserach_AuthorRepositoryConstants.AuthorsID, SqlDbType.BigInt).Direction = ParameterDirection.Output;

                SqlDataReader reader = sqlCommand.ExecuteReader();
                result.Entity = entity;
                entity.AuthorsID = Convert.ToInt64(sqlCommand.Parameters[Plugin_Reserach_AuthorRepositoryConstants.AuthorsID].Value);

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.InsertSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_AuthorEntity>> SelectByID(long ID)
        {

            ResultEntity<Plugin_Reserach_AuthorEntity> result = new ResultEntity<Plugin_Reserach_AuthorEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_AuthorRepositoryConstants.SP_SelectByID, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.Add(new SqlParameter(Plugin_Reserach_AuthorRepositoryConstants.AuthorsID, ID));
                SqlDataReader reader = await sqlCommand.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    reader.Read();
                    result.Entity = EntityHelper(reader, true);
                }
                else
                {
                    result.Status = ErrorEnums.Warning;
                    result.Message = MessageConstants.NotFoundMessage;
                    result.Details = MessageConstants.NotFoundDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
        public static ResultEntity<Plugin_Reserach_AuthorEntity> SelectByIDNotAsync(long ID)
        {

            ResultEntity<Plugin_Reserach_AuthorEntity> result = new ResultEntity<Plugin_Reserach_AuthorEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_AuthorRepositoryConstants.SP_SelectByID, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.Add(new SqlParameter(Plugin_Reserach_AuthorRepositoryConstants.AuthorsID, ID));
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    result.Entity = EntityHelper(reader, true);
                }
                else
                {
                    result.Status = ErrorEnums.Warning;
                    result.Message = MessageConstants.NotFoundMessage;
                    result.Details = MessageConstants.NotFoundDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_AuthorEntity>> Update(Plugin_Reserach_AuthorEntity entity)
        {

            ResultEntity<Plugin_Reserach_AuthorEntity> result = new ResultEntity<Plugin_Reserach_AuthorEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_AuthorRepositoryConstants.SP_Update, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();

                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AuthorsID, entity.AuthorsID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AuthorName, entity.AuthorName);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AuthorImage, entity.AuthorImage);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AuthorTitle, entity.AuthorTitle);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AuthorSummery, entity.AuthorSummery);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AuthorDetails, entity.AuthorDetails);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.Order, entity.Order);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.PublishedDate, entity.PublishedDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.IsPublished, entity.IsPublished);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.IsDelete, entity.IsDelete);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.LanguageID, entity.LanguageID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AddDate, entity.AddDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AddUser, entity.AddUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.EditDate, entity.EditDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.EditUser, entity.EditUser);
                await sqlCommand.ExecuteNonQueryAsync();
                result.Entity = entity;

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.UpdateSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
        public static ResultEntity<Plugin_Reserach_AuthorEntity> UpdateNotAsync(Plugin_Reserach_AuthorEntity entity)
        {

            ResultEntity<Plugin_Reserach_AuthorEntity> result = new ResultEntity<Plugin_Reserach_AuthorEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_AuthorRepositoryConstants.SP_Update, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AuthorsID, entity.AuthorsID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AuthorName, entity.AuthorName);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AuthorImage, entity.AuthorImage);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AuthorTitle, entity.AuthorTitle);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AuthorSummery, entity.AuthorSummery);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AuthorDetails, entity.AuthorDetails);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.Order, entity.Order);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.PublishedDate, entity.PublishedDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.IsPublished, entity.IsPublished);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.IsDelete, entity.IsDelete);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.LanguageID, entity.LanguageID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AddDate, entity.AddDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AddUser, entity.AddUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.EditDate, entity.EditDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.EditUser, entity.EditUser);

                sqlCommand.ExecuteNonQueryAsync();
                result.Entity = entity;

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.UpdateSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_AuthorEntity>> Delete(long ID)
        {

            ResultEntity<Plugin_Reserach_AuthorEntity> result = new ResultEntity<Plugin_Reserach_AuthorEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_AuthorRepositoryConstants.SP_Delete, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorRepositoryConstants.AuthorsID, ID);

                await sqlCommand.ExecuteReaderAsync();

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.DeleteSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        static Plugin_Reserach_AuthorEntity EntityHelper(SqlDataReader reader, bool isFull)
        {
            Plugin_Reserach_AuthorEntity entity = new Plugin_Reserach_AuthorEntity();

            try
            {
                entity.AuthorsID = Convert.ToInt64(reader[Plugin_Reserach_AuthorEntityConstants.AuthorsID].ToString());
                entity.AuthorName = reader[Plugin_Reserach_AuthorEntityConstants.AuthorName] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_AuthorEntityConstants.AuthorName].ToString();
                entity.AuthorImage = reader[Plugin_Reserach_AuthorEntityConstants.AuthorImage] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_AuthorEntityConstants.AuthorImage].ToString();

                
                entity.AuthorTitle = reader[Plugin_Reserach_AuthorEntityConstants.AuthorTitle] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_AuthorEntityConstants.AuthorTitle].ToString();
                entity.AuthorSummery = reader[Plugin_Reserach_AuthorEntityConstants.AuthorSummery] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_AuthorEntityConstants.AuthorSummery].ToString();
                entity.AuthorDetails = reader[Plugin_Reserach_AuthorEntityConstants.AuthorDetails] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_AuthorEntityConstants.AuthorDetails].ToString();
                
                entity.Order = reader[Plugin_Reserach_AuthorEntityConstants.Order] == DBNull.Value ? 0 : Convert.ToInt64(reader[Plugin_Reserach_AuthorEntityConstants.Order].ToString());

                entity.PublishedDate = reader[Plugin_Reserach_AuthorEntityConstants.PublishedDate] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(reader[Plugin_Reserach_AuthorEntityConstants.PublishedDate].ToString());
                entity.IsPublished = reader[Plugin_Reserach_AuthorEntityConstants.IsPublished] == DBNull.Value ? false : Convert.ToBoolean(reader[Plugin_Reserach_AuthorEntityConstants.IsPublished].ToString());
                entity.IsDelete = reader[Plugin_Reserach_AuthorEntityConstants.IsDelete] == DBNull.Value ? false : Convert.ToBoolean(reader[Plugin_Reserach_AuthorEntityConstants.IsDelete].ToString());
                entity.LanguageID = reader[Plugin_Reserach_AuthorEntityConstants.LanguageID] == DBNull.Value ? 1 : Convert.ToInt32(reader[Plugin_Reserach_AuthorEntityConstants.LanguageID].ToString());
                entity.AddDate = reader[Plugin_Reserach_AuthorEntityConstants.AddDate] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(reader[Plugin_Reserach_AuthorEntityConstants.AddDate].ToString());
                entity.EditDate = reader[Plugin_Reserach_AuthorEntityConstants.EditDate] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(reader[Plugin_Reserach_AuthorEntityConstants.EditDate].ToString());
                entity.AddUser = reader[Plugin_Reserach_AuthorEntityConstants.AddUser].ToString();
                entity.EditUser = reader[Plugin_Reserach_AuthorEntityConstants.EditUser].ToString();
                bool ColumnExists = false;
                try
                {
                    int columnOrdinal = reader.GetOrdinal("LanguageName");
                    ColumnExists = true;
                }
                catch (IndexOutOfRangeException)
                {
                    ColumnExists = false;
                }

                if (ColumnExists)
                {
                    entity.LanguageName = reader[Plugin_Reserach_AuthorEntityConstants.LanguageName] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_AuthorEntityConstants.LanguageName].ToString();
                }
            }
            catch (Exception ex)
            {

            }

            return entity;
        }
    }
}
