﻿using SiteWare.DataAccess.Common.Constants;
using SiteWare.DataAccess.RepositorieConstants;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using SiteWare.Entity.Common.Enums;
using SiteWare.Entity.Constants.Entity;

namespace SiteWare.DataAccess.Repositories
{
    public static  class Lookup_Reserach_PublicationCategoryRepository
    {
        public async static Task<ResultList<Lookup_Reserach_PublicationCategoryEntity>> SelectAll()
        {
            ResultList<Lookup_Reserach_PublicationCategoryEntity> result = new ResultList<Lookup_Reserach_PublicationCategoryEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Lookup_Reserach_PublicationCategoryRepositoryConstants.SP_SelectAll, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            List<Lookup_Reserach_PublicationCategoryEntity> list = new List<Lookup_Reserach_PublicationCategoryEntity>();

            try
            {
                sqlConnection.Open();

                SqlDataReader reader = await sqlCommand.ExecuteReaderAsync();

                while (reader.Read())
                {
                    Lookup_Reserach_PublicationCategoryEntity entity = EntityHelper(reader, false);
                    list.Add(entity);
                }

                if (list.Count > 0)
                {
                    reader.Close();

                    result.List = list;

                }
                else
                {
                    result.Status = ErrorEnums.Information;
                    result.Details = MessageConstants.CannotFindAllMessage;
                    result.Message = MessageConstants.CannotFindAllDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
                result.Message = ex.Message;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        public static ResultList<Lookup_Reserach_PublicationCategoryEntity> SelectAllNotAsync()
        {
            ResultList<Lookup_Reserach_PublicationCategoryEntity> result = new ResultList<Lookup_Reserach_PublicationCategoryEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Lookup_Reserach_PublicationCategoryRepositoryConstants.SP_SelectAll, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            List<Lookup_Reserach_PublicationCategoryEntity> list = new List<Lookup_Reserach_PublicationCategoryEntity>();

            try
            {
                sqlConnection.Open();

                SqlDataReader reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    Lookup_Reserach_PublicationCategoryEntity entity = EntityHelper(reader, false);
                    list.Add(entity);
                }

                if (list.Count > 0)
                {
                    reader.Close();

                    result.List = list;

                }
                else
                {
                    result.Status = ErrorEnums.Information;
                    result.Details = MessageConstants.CannotFindAllMessage;
                    result.Message = MessageConstants.CannotFindAllDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
                result.Message = ex.Message;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        //public async static Task<ResultList<Lookup_Reserach_PublicationCategoryEntity>> Lookup_Area_ByGovernateID(int PublicationCategoryID)
        //{

        //    ResultList<Lookup_Reserach_PublicationCategoryEntity> result = new ResultList<Lookup_Reserach_PublicationCategoryEntity>();

        //    SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
        //    SqlCommand sqlCommand = new SqlCommand(Lookup_Reserach_PublicationCategoryRepositoryConstants.SP_SelectByGovID, sqlConnection);
        //    sqlCommand.CommandType = CommandType.StoredProcedure;
        //    List<Lookup_Reserach_PublicationCategoryEntity> list = new List<Lookup_Reserach_PublicationCategoryEntity>();

        //    try
        //    {
        //        sqlConnection.Open();
        //        sqlCommand.Parameters.Add(new SqlParameter(Lookup_Reserach_PublicationCategoryRepositoryConstants.PublicationCategoryID, PublicationCategoryID));
        //        SqlDataReader reader = await sqlCommand.ExecuteReaderAsync();
        //        while (reader.Read())
        //        {
        //            Lookup_Reserach_PublicationCategoryEntity entity = EntityHelper(reader, false);
        //            list.Add(entity);
        //        }

        //        if (list.Count > 0)
        //        {
        //            reader.Close();
        //            result.List = list;
        //        }
        //        else
        //        {
        //            result.Status = ErrorEnums.Information;
        //            result.Details = MessageConstants.CannotFindAllMessage;
        //            result.Message = MessageConstants.CannotFindAllDetails;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        result.Status = ErrorEnums.Exception;
        //        result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
        //    }
        //    finally
        //    {
        //        sqlConnection.Close();
        //        sqlConnection.Dispose();
        //        sqlCommand.Dispose();
        //    }

        //    return result;
        //}

        //public static ResultList<Lookup_Reserach_PublicationCategoryEntity> Lookup_Area_ByGovernateIDNotAsync(int PublicationCategoryID)
        //{

        //    ResultList<Lookup_Reserach_PublicationCategoryEntity> result = new ResultList<Lookup_Reserach_PublicationCategoryEntity>();

        //    SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
        //    SqlCommand sqlCommand = new SqlCommand(Lookup_Reserach_PublicationCategoryRepositoryConstants.SP_SelectByGovID, sqlConnection);
        //    sqlCommand.CommandType = CommandType.StoredProcedure;
        //    List<Lookup_Reserach_PublicationCategoryEntity> list = new List<Lookup_Reserach_PublicationCategoryEntity>();

        //    try
        //    {
        //        sqlConnection.Open();
        //        sqlCommand.Parameters.Add(new SqlParameter(Lookup_Reserach_PublicationCategoryRepositoryConstants.PublicationCategoryID, PublicationCategoryID));
        //        SqlDataReader reader = sqlCommand.ExecuteReader();
        //        while (reader.Read())
        //        {
        //            Lookup_Reserach_PublicationCategoryEntity entity = EntityHelper(reader, false);
        //            list.Add(entity);
        //        }

        //        if (list.Count > 0)
        //        {
        //            reader.Close();
        //            result.List = list;
        //        }
        //        else
        //        {
        //            result.Status = ErrorEnums.Information;
        //            result.Details = MessageConstants.CannotFindAllMessage;
        //            result.Message = MessageConstants.CannotFindAllDetails;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        result.Status = ErrorEnums.Exception;
        //        result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
        //    }
        //    finally
        //    {
        //        sqlConnection.Close();
        //        sqlConnection.Dispose();
        //        sqlCommand.Dispose();
        //    }

        //    return result;
        //}


        public async static Task<ResultEntity<Lookup_Reserach_PublicationCategoryEntity>> SelectByID(int ID)
        {

            ResultEntity<Lookup_Reserach_PublicationCategoryEntity> result = new ResultEntity<Lookup_Reserach_PublicationCategoryEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Lookup_Reserach_PublicationCategoryRepositoryConstants.SP_SelectByGovID, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.Add(new SqlParameter(Lookup_Reserach_PublicationCategoryRepositoryConstants.PublicationCategoryID, ID));
                SqlDataReader reader = await sqlCommand.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    reader.Read();
                    result.Entity = EntityHelper(reader, true);
                }
                else
                {
                    result.Status = ErrorEnums.Warning;
                    result.Message = MessageConstants.NotFoundMessage;
                    result.Details = MessageConstants.NotFoundDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
        public static ResultEntity<Lookup_Reserach_PublicationCategoryEntity> SelectByIDNotAsync(int ID)
        {

            ResultEntity<Lookup_Reserach_PublicationCategoryEntity> result = new ResultEntity<Lookup_Reserach_PublicationCategoryEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Lookup_Reserach_PublicationCategoryRepositoryConstants.SP_SelectByGovID, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.Add(new SqlParameter(Lookup_Reserach_PublicationCategoryRepositoryConstants.PublicationCategoryID, ID));
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    result.Entity = EntityHelper(reader, true);
                }
                else
                {
                    result.Status = ErrorEnums.Warning;
                    result.Message = MessageConstants.NotFoundMessage;
                    result.Details = MessageConstants.NotFoundDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        static Lookup_Reserach_PublicationCategoryEntity EntityHelper(SqlDataReader reader, bool isFull)
        {
            Lookup_Reserach_PublicationCategoryEntity entity = new Lookup_Reserach_PublicationCategoryEntity();

            entity.PublicationCategoryID = Convert.ToInt32(reader[Lookup_Reserach_PublicationCategoryEntityConstants.PublicationCategoryID].ToString());
            entity.name = reader[Lookup_Reserach_PublicationCategoryEntityConstants.name].ToString();
            entity.IsPublished = Convert.ToBoolean(reader[Lookup_Reserach_PublicationCategoryEntityConstants.IsPublished].ToString());
            entity.IsDeleted = Convert.ToBoolean(reader[Lookup_Reserach_PublicationCategoryEntityConstants.IsDeleted].ToString());
            entity.AddDate = Convert.ToDateTime(reader[Lookup_Reserach_PublicationCategoryEntityConstants.AddDate].ToString());
            entity.AddUser = Convert.ToInt32(reader[Lookup_Reserach_PublicationCategoryEntityConstants.AddUser].ToString());
            entity.EditDate = Convert.ToDateTime(reader[Lookup_Reserach_PublicationCategoryEntityConstants.EditDate].ToString());
            entity.EditUser = Convert.ToInt32(reader[Lookup_Reserach_PublicationCategoryEntityConstants.EditUser].ToString());

            return entity;
        }
    }
}
