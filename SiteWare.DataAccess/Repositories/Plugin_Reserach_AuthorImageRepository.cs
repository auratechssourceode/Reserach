﻿using SiteWare.DataAccess.Common.Constants;
using SiteWare.DataAccess.RepositorieConstants;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Common.Enums;
using SiteWare.Entity.Constants.Entity;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.DataAccess.Repositories
{
    public static class Plugin_Reserach_AuthorImageRepository
    {
        public static ResultList<Plugin_Reserach_AuthorImageEntity> SelectAllByPillarsIDWidthHeight(int ID, int Width, int Height)
        {

            ResultList<Plugin_Reserach_AuthorImageEntity> result = new ResultList<Plugin_Reserach_AuthorImageEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_AuthorImageRepositoryConstants.SP_SelectAllByIDWidthHeight, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            List<Plugin_Reserach_AuthorImageEntity> list = new List<Plugin_Reserach_AuthorImageEntity>();

            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorImageRepositoryConstants.AuthorID, ID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorImageRepositoryConstants.Width, Width);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorImageRepositoryConstants.Height, Height);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    Plugin_Reserach_AuthorImageEntity entity = EntityHelper(reader, false);
                    list.Add(entity);
                }

                if (list.Count > 0)
                {
                    reader.Close();

                    result.List = list;

                }
                else
                {
                    result.Status = ErrorEnums.Information;
                    result.Details = MessageConstants.CannotFindAllMessage;
                    result.Message = MessageConstants.CannotFindAllDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
                result.Message = ex.Message;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_AuthorImageEntity>> Insert(Plugin_Reserach_AuthorImageEntity entity)
        {

            ResultEntity<Plugin_Reserach_AuthorImageEntity> result = new ResultEntity<Plugin_Reserach_AuthorImageEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_AuthorImageRepositoryConstants.SP_Insert, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorImageRepositoryConstants.AuthorID, entity.AuthorID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorImageRepositoryConstants.FileName, entity.FileName);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorImageRepositoryConstants.Path, entity.Path);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorImageRepositoryConstants.Width, entity.Width);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorImageRepositoryConstants.Height, entity.Height);

                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorImageRepositoryConstants.IsDeleted, entity.IsDeleted);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorImageRepositoryConstants.AddDate, entity.AddDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_AuthorImageRepositoryConstants.DeleteDate, entity.DeleteDate);

                SqlDataReader reader = await sqlCommand.ExecuteReaderAsync();
                result.Entity = entity;
                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.InsertSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        static Plugin_Reserach_AuthorImageEntity EntityHelper(SqlDataReader reader, bool isFull)
        {
            Plugin_Reserach_AuthorImageEntity entity = new Plugin_Reserach_AuthorImageEntity();
            try
            {
                entity.AuthorImageID = Convert.ToInt64(reader[Plugin_Reserach_AuthorImageEntityConstants.AuthorImageID].ToString());
                entity.AuthorID = Convert.ToInt32(reader[Plugin_Reserach_AuthorImageEntityConstants.AuthorID].ToString());
                entity.FileName = reader[Plugin_Reserach_AuthorImageEntityConstants.FileName].ToString();
                entity.Path = reader[Plugin_Reserach_AuthorImageEntityConstants.Path].ToString();
                entity.Width = Convert.ToInt32(reader[Plugin_Reserach_AuthorImageEntityConstants.Width].ToString());
                entity.Height = Convert.ToInt32(reader[Plugin_Reserach_AuthorImageEntityConstants.Height].ToString());
                entity.IsDeleted = Convert.ToBoolean(reader[Plugin_Reserach_AuthorImageEntityConstants.IsDeleted].ToString());
                entity.AddDate = Convert.ToDateTime(reader[Plugin_Reserach_AuthorImageEntityConstants.AddDate].ToString());
                entity.DeleteDate = Convert.ToDateTime(reader[Plugin_Reserach_AuthorImageEntityConstants.DeleteDate].ToString());

            }
            catch
            {
            }

            return entity;
        }
    }
}
