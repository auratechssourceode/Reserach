﻿using SiteWare.DataAccess.Common.Constants;
using SiteWare.DataAccess.RepositorieConstants;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Common.Enums;
using SiteWare.Entity.Constants.Entity;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.DataAccess.Repositories
{
    public static class Plugin_Reserach_PublicationRepository
    {

        public async static Task<ResultList<Plugin_Reserach_PublicationEntity>> SelectAll()
        {
            ResultList<Plugin_Reserach_PublicationEntity> result = new ResultList<Plugin_Reserach_PublicationEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_PublicationRepositoryConstants.SP_SelectAll, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            List<Plugin_Reserach_PublicationEntity> list = new List<Plugin_Reserach_PublicationEntity>();

            try
            {
                sqlConnection.Open();

                SqlDataReader reader = await sqlCommand.ExecuteReaderAsync();

                while (reader.Read())
                {
                    Plugin_Reserach_PublicationEntity entity = EntityHelper(reader, false);
                    list.Add(entity);
                }

                if (list.Count > 0)
                {
                    reader.Close();

                    result.List = list;

                }
                else
                {
                    result.Status = ErrorEnums.Information;
                    result.Details = MessageConstants.CannotFindAllMessage;
                    result.Message = MessageConstants.CannotFindAllDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
                result.Message = ex.Message;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
        public static ResultList<Plugin_Reserach_PublicationEntity> SelectAllNotAsync()
        {
            ResultList<Plugin_Reserach_PublicationEntity> result = new ResultList<Plugin_Reserach_PublicationEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_PublicationRepositoryConstants.SP_SelectAll, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            List<Plugin_Reserach_PublicationEntity> list = new List<Plugin_Reserach_PublicationEntity>();

            try
            {
                sqlConnection.Open();

                SqlDataReader reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    Plugin_Reserach_PublicationEntity entity = EntityHelper(reader, false);
                    list.Add(entity);
                }

                if (list.Count > 0)
                {
                    reader.Close();

                    result.List = list;

                }
                else
                {
                    result.Status = ErrorEnums.Information;
                    result.Details = MessageConstants.CannotFindAllMessage;
                    result.Message = MessageConstants.CannotFindAllDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
                result.Message = ex.Message;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_PublicationEntity>> Insert(Plugin_Reserach_PublicationEntity entity)
        {

            ResultEntity<Plugin_Reserach_PublicationEntity> result = new ResultEntity<Plugin_Reserach_PublicationEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_PublicationRepositoryConstants.SP_Insert, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                //sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsID, entity.PublicationsID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsTitle, entity.PublicationsTitle);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsDate, entity.PublicationsDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsAuthor, entity.PublicationsAuthor);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsVersion, entity.PublicationsVersion);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsSummery, entity.PublicationsSummery);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsDetails, entity.PublicationsDetails);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsCatogory, entity.PublicationsCatogory);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsImage, entity.PublicationsImage);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.Order, entity.Order);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublishedDate, entity.PublishedDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.IsPublished, entity.IsPublished);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.IsDelete, entity.IsDelete);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.LanguageID, entity.LanguageID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.AddDate, entity.AddDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.AddUser, entity.AddUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.EditDate, entity.EditDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.EditUser, entity.EditUser);

                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.ViewCount, entity.ViewCount);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsTitle2, entity.PublicationsTitle2);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsImage2, entity.PublicationsImage2);


                sqlCommand.Parameters.Add(Plugin_Reserach_PublicationRepositoryConstants.PublicationsID, SqlDbType.BigInt).Direction = ParameterDirection.Output;

                SqlDataReader reader = await sqlCommand.ExecuteReaderAsync();
                result.Entity = entity;
                entity.PublicationsID = Convert.ToInt64(sqlCommand.Parameters[Plugin_Reserach_PublicationRepositoryConstants.PublicationsID].Value);

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.InsertSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
        public static ResultEntity<Plugin_Reserach_PublicationEntity> InsertNotAsync(Plugin_Reserach_PublicationEntity entity)
        {

            ResultEntity<Plugin_Reserach_PublicationEntity> result = new ResultEntity<Plugin_Reserach_PublicationEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_PublicationRepositoryConstants.SP_Insert, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                //sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsID, entity.PublicationsID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsTitle, entity.PublicationsTitle);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsDate, entity.PublicationsDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsAuthor, entity.PublicationsAuthor);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsVersion, entity.PublicationsVersion);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsSummery, entity.PublicationsSummery);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsDetails, entity.PublicationsDetails);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsCatogory, entity.PublicationsCatogory);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsImage, entity.PublicationsImage);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.Order, entity.Order);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublishedDate, entity.PublishedDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.IsPublished, entity.IsPublished);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.IsDelete, entity.IsDelete);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.LanguageID, entity.LanguageID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.AddDate, entity.AddDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.AddUser, entity.AddUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.EditDate, entity.EditDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.EditUser, entity.EditUser);

                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.ViewCount, entity.ViewCount);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsTitle2, entity.PublicationsTitle2);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsImage2, entity.PublicationsImage2);

                sqlCommand.Parameters.Add(Plugin_Reserach_PublicationRepositoryConstants.PublicationsID, SqlDbType.BigInt).Direction = ParameterDirection.Output;

                SqlDataReader reader = sqlCommand.ExecuteReader();
                result.Entity = entity;
                entity.PublicationsID = Convert.ToInt64(sqlCommand.Parameters[Plugin_Reserach_PublicationRepositoryConstants.PublicationsID].Value);

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.InsertSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_PublicationEntity>> SelectByID(long ID)
        {

            ResultEntity<Plugin_Reserach_PublicationEntity> result = new ResultEntity<Plugin_Reserach_PublicationEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_PublicationRepositoryConstants.SP_SelectByID, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.Add(new SqlParameter(Plugin_Reserach_PublicationRepositoryConstants.PublicationsID, ID));
                SqlDataReader reader = await sqlCommand.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    reader.Read();
                    result.Entity = EntityHelper(reader, true);
                }
                else
                {
                    result.Status = ErrorEnums.Warning;
                    result.Message = MessageConstants.NotFoundMessage;
                    result.Details = MessageConstants.NotFoundDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
        public static ResultEntity<Plugin_Reserach_PublicationEntity> SelectByIDNotAsync(long ID)
        {

            ResultEntity<Plugin_Reserach_PublicationEntity> result = new ResultEntity<Plugin_Reserach_PublicationEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_PublicationRepositoryConstants.SP_SelectByID, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.Add(new SqlParameter(Plugin_Reserach_PublicationRepositoryConstants.PublicationsID, ID));
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    result.Entity = EntityHelper(reader, true);
                }
                else
                {
                    result.Status = ErrorEnums.Warning;
                    result.Message = MessageConstants.NotFoundMessage;
                    result.Details = MessageConstants.NotFoundDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_PublicationEntity>> Update(Plugin_Reserach_PublicationEntity entity)
        {

            ResultEntity<Plugin_Reserach_PublicationEntity> result = new ResultEntity<Plugin_Reserach_PublicationEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_PublicationRepositoryConstants.SP_Update, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();

                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsTitle, entity.PublicationsTitle);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsDate, entity.PublicationsDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsAuthor, entity.PublicationsAuthor);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsVersion, entity.PublicationsVersion);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsSummery, entity.PublicationsSummery);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsDetails, entity.PublicationsDetails);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsCatogory, entity.PublicationsCatogory);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsImage, entity.PublicationsImage);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.Order, entity.Order);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublishedDate, entity.PublishedDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.IsPublished, entity.IsPublished);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.IsDelete, entity.IsDelete);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.LanguageID, entity.LanguageID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.AddDate, entity.AddDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.AddUser, entity.AddUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.EditDate, entity.EditDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.EditUser, entity.EditUser);

                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.ViewCount, entity.ViewCount);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsTitle2, entity.PublicationsTitle2);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsImage2, entity.PublicationsImage2);

                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsID, entity.PublicationsID);
                await sqlCommand.ExecuteNonQueryAsync();
                result.Entity = entity;

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.UpdateSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
        public static ResultEntity<Plugin_Reserach_PublicationEntity> UpdateNotAsync(Plugin_Reserach_PublicationEntity entity)
        {

            ResultEntity<Plugin_Reserach_PublicationEntity> result = new ResultEntity<Plugin_Reserach_PublicationEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_PublicationRepositoryConstants.SP_Update, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsTitle, entity.PublicationsTitle);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsDate, entity.PublicationsDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsAuthor, entity.PublicationsAuthor);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsVersion, entity.PublicationsVersion);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsSummery, entity.PublicationsSummery);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsDetails, entity.PublicationsDetails);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsCatogory, entity.PublicationsCatogory);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsImage, entity.PublicationsImage);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.Order, entity.Order);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublishedDate, entity.PublishedDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.IsPublished, entity.IsPublished);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.IsDelete, entity.IsDelete);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.LanguageID, entity.LanguageID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.AddDate, entity.AddDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.AddUser, entity.AddUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.EditDate, entity.EditDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.EditUser, entity.EditUser);

                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.ViewCount, entity.ViewCount);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsTitle2, entity.PublicationsTitle2);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsImage2, entity.PublicationsImage2);

                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsID, entity.PublicationsID);

                sqlCommand.ExecuteNonQueryAsync();
                result.Entity = entity;

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.UpdateSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_PublicationEntity>> Delete(long ID)
        {

            ResultEntity<Plugin_Reserach_PublicationEntity> result = new ResultEntity<Plugin_Reserach_PublicationEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_PublicationRepositoryConstants.SP_Delete, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsID, ID);

                await sqlCommand.ExecuteReaderAsync();

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.DeleteSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        static Plugin_Reserach_PublicationEntity EntityHelper(SqlDataReader reader, bool isFull)
        {
            Plugin_Reserach_PublicationEntity entity = new Plugin_Reserach_PublicationEntity();

            try
            {
                entity.PublicationsID = Convert.ToInt64(reader[Plugin_Reserach_PublicationEntityConstants.PublicationsID].ToString());
                entity.PublicationsTitle = reader[Plugin_Reserach_PublicationEntityConstants.PublicationsTitle] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_PublicationEntityConstants.PublicationsTitle].ToString();
                entity.PublicationsDate = reader[Plugin_Reserach_PublicationEntityConstants.PublicationsDate] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(reader[Plugin_Reserach_PublicationEntityConstants.PublicationsDate].ToString());
                entity.PublicationsAuthor = reader[Plugin_Reserach_PublicationEntityConstants.PublicationsAuthor] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_PublicationEntityConstants.PublicationsAuthor].ToString();
                entity.PublicationsVersion = reader[Plugin_Reserach_PublicationEntityConstants.PublicationsVersion] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_PublicationEntityConstants.PublicationsVersion].ToString();
                entity.PublicationsSummery = reader[Plugin_Reserach_PublicationEntityConstants.PublicationsSummery] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_PublicationEntityConstants.PublicationsSummery].ToString();
                entity.PublicationsDetails = reader[Plugin_Reserach_PublicationEntityConstants.PublicationsDetails] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_PublicationEntityConstants.PublicationsDetails].ToString();
                entity.PublicationsCatogory = reader[Plugin_Reserach_PublicationEntityConstants.PublicationsCatogory] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_PublicationEntityConstants.PublicationsCatogory].ToString();
                entity.PublicationsImage = reader[Plugin_Reserach_PublicationEntityConstants.PublicationsImage] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_PublicationEntityConstants.PublicationsImage].ToString();
                entity.Order = reader[Plugin_Reserach_PublicationEntityConstants.Order] == DBNull.Value ? 0 : Convert.ToInt64(reader[Plugin_Reserach_PublicationEntityConstants.Order].ToString());
                entity.PublishedDate = reader[Plugin_Reserach_PublicationEntityConstants.PublishedDate] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(reader[Plugin_Reserach_PublicationEntityConstants.PublishedDate].ToString());
                entity.IsPublished = reader[Plugin_Reserach_PublicationEntityConstants.IsPublished] == DBNull.Value ? false : Convert.ToBoolean(reader[Plugin_Reserach_PublicationEntityConstants.IsPublished].ToString());
                entity.IsDelete = reader[Plugin_Reserach_PublicationEntityConstants.IsDelete] == DBNull.Value ? false : Convert.ToBoolean(reader[Plugin_Reserach_PublicationEntityConstants.IsDelete].ToString());
                entity.LanguageID = reader[Plugin_Reserach_PublicationEntityConstants.LanguageID] == DBNull.Value ? 1 : Convert.ToInt32(reader[Plugin_Reserach_PublicationEntityConstants.LanguageID].ToString());
                entity.AddDate = reader[Plugin_Reserach_PublicationEntityConstants.AddDate] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(reader[Plugin_Reserach_PublicationEntityConstants.AddDate].ToString());
                entity.EditDate = reader[Plugin_Reserach_PublicationEntityConstants.EditDate] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(reader[Plugin_Reserach_PublicationEntityConstants.EditDate].ToString());
                entity.AddUser = reader[Plugin_Reserach_PublicationEntityConstants.AddUser].ToString();
                entity.EditUser = reader[Plugin_Reserach_PublicationEntityConstants.EditUser].ToString();

                bool ColumnExists = false;
                try
                {
                    int columnOrdinal = reader.GetOrdinal("LanguageName");
                    ColumnExists = true;
                }
                catch (IndexOutOfRangeException)
                {
                    ColumnExists = false;
                }

                if (ColumnExists)
                {
                    entity.LanguageName = reader[Plugin_Reserach_PublicationEntityConstants.LanguageName] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_PublicationEntityConstants.LanguageName].ToString();
                }


                try
                {
                    int columnOrdinal = reader.GetOrdinal("ViewCount");
                    ColumnExists = true;
                }
                catch (IndexOutOfRangeException)
                {
                    ColumnExists = false;
                }

                if (ColumnExists)
                {
                    entity.ViewCount = reader[Plugin_Reserach_PublicationEntityConstants.ViewCount] == DBNull.Value ? 1 : Convert.ToInt64(reader[Plugin_Reserach_PublicationEntityConstants.ViewCount].ToString());
                }


                try
                {
                    int columnOrdinal = reader.GetOrdinal("PublicationsTitle2");
                    ColumnExists = true;
                }
                catch (IndexOutOfRangeException)
                {
                    ColumnExists = false;
                }

                if (ColumnExists)
                {
                    entity.PublicationsTitle2 = reader[Plugin_Reserach_PublicationEntityConstants.PublicationsTitle2] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_PublicationEntityConstants.PublicationsTitle2].ToString();
                }


                try
                {
                    int columnOrdinal = reader.GetOrdinal("PublicationsImage2");
                    ColumnExists = true;
                }
                catch (IndexOutOfRangeException)
                {
                    ColumnExists = false;
                }

                if (ColumnExists)
                {
                    entity.PublicationsImage2 = reader[Plugin_Reserach_PublicationEntityConstants.PublicationsImage2] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_PublicationEntityConstants.PublicationsImage2].ToString();
                }
            }
            catch (Exception ex)
            {

            }

            return entity;
        }


        public async static Task<ResultEntity<Plugin_Reserach_PublicationEntity>> UpdateViewCount(long PublicationsID)
        {

            ResultEntity<Plugin_Reserach_PublicationEntity> result = new ResultEntity<Plugin_Reserach_PublicationEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_PublicationRepositoryConstants.SP_UpdateViewCount, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_PublicationRepositoryConstants.PublicationsID, PublicationsID);

                await sqlCommand.ExecuteNonQueryAsync();

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.UpdateSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
    }
}
