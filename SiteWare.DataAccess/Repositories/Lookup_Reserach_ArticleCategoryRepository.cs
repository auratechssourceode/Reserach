﻿using SiteWare.DataAccess.Common.Constants;
using SiteWare.DataAccess.RepositorieConstants;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using SiteWare.Entity.Common.Enums;
using SiteWare.Entity.Constants.Entity;

namespace SiteWare.DataAccess.Repositories
{
    public static  class Lookup_Reserach_ArticleCategoryRepository
    {
        public async static Task<ResultList<Lookup_Reserach_ArticleCategoryEntity>> SelectAll()
        {
            ResultList<Lookup_Reserach_ArticleCategoryEntity> result = new ResultList<Lookup_Reserach_ArticleCategoryEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Lookup_Reserach_ArticleCategoryRepositoryConstants.SP_SelectAll, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            List<Lookup_Reserach_ArticleCategoryEntity> list = new List<Lookup_Reserach_ArticleCategoryEntity>();

            try
            {
                sqlConnection.Open();

                SqlDataReader reader = await sqlCommand.ExecuteReaderAsync();

                while (reader.Read())
                {
                    Lookup_Reserach_ArticleCategoryEntity entity = EntityHelper(reader, false);
                    list.Add(entity);
                }

                if (list.Count > 0)
                {
                    reader.Close();

                    result.List = list;

                }
                else
                {
                    result.Status = ErrorEnums.Information;
                    result.Details = MessageConstants.CannotFindAllMessage;
                    result.Message = MessageConstants.CannotFindAllDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
                result.Message = ex.Message;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        public static ResultList<Lookup_Reserach_ArticleCategoryEntity> SelectAllNotAsync()
        {
            ResultList<Lookup_Reserach_ArticleCategoryEntity> result = new ResultList<Lookup_Reserach_ArticleCategoryEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Lookup_Reserach_ArticleCategoryRepositoryConstants.SP_SelectAll, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            List<Lookup_Reserach_ArticleCategoryEntity> list = new List<Lookup_Reserach_ArticleCategoryEntity>();

            try
            {
                sqlConnection.Open();

                SqlDataReader reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    Lookup_Reserach_ArticleCategoryEntity entity = EntityHelper(reader, false);
                    list.Add(entity);
                }

                if (list.Count > 0)
                {
                    reader.Close();

                    result.List = list;

                }
                else
                {
                    result.Status = ErrorEnums.Information;
                    result.Details = MessageConstants.CannotFindAllMessage;
                    result.Message = MessageConstants.CannotFindAllDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
                result.Message = ex.Message;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }



        public async static Task<ResultEntity<Lookup_Reserach_ArticleCategoryEntity>> SelectByID(int ID)
        {

            ResultEntity<Lookup_Reserach_ArticleCategoryEntity> result = new ResultEntity<Lookup_Reserach_ArticleCategoryEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Lookup_Reserach_ArticleCategoryRepositoryConstants.SP_SelectByGovID, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.Add(new SqlParameter(Lookup_Reserach_ArticleCategoryRepositoryConstants.ArticleCategoryID, ID));
                SqlDataReader reader = await sqlCommand.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    reader.Read();
                    result.Entity = EntityHelper(reader, true);
                }
                else
                {
                    result.Status = ErrorEnums.Warning;
                    result.Message = MessageConstants.NotFoundMessage;
                    result.Details = MessageConstants.NotFoundDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
        public static ResultEntity<Lookup_Reserach_ArticleCategoryEntity> SelectByIDNotAsync(int ID)
        {

            ResultEntity<Lookup_Reserach_ArticleCategoryEntity> result = new ResultEntity<Lookup_Reserach_ArticleCategoryEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Lookup_Reserach_ArticleCategoryRepositoryConstants.SP_SelectByGovID, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.Add(new SqlParameter(Lookup_Reserach_ArticleCategoryRepositoryConstants.ArticleCategoryID, ID));
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    result.Entity = EntityHelper(reader, true);
                }
                else
                {
                    result.Status = ErrorEnums.Warning;
                    result.Message = MessageConstants.NotFoundMessage;
                    result.Details = MessageConstants.NotFoundDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        //public async static Task<ResultList<Lookup_Reserach_ArticleCategoryEntity>> Lookup_Area_ByGovernateID(int ArticleCategoryID)
        //{

        //    ResultList<Lookup_Reserach_ArticleCategoryEntity> result = new ResultList<Lookup_Reserach_ArticleCategoryEntity>();

        //    SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
        //    SqlCommand sqlCommand = new SqlCommand(Lookup_Reserach_ArticleCategoryRepositoryConstants.SP_SelectByGovID, sqlConnection);
        //    sqlCommand.CommandType = CommandType.StoredProcedure;
        //    List<Lookup_Reserach_ArticleCategoryEntity> list = new List<Lookup_Reserach_ArticleCategoryEntity>();

        //    try
        //    {
        //        sqlConnection.Open();
        //        sqlCommand.Parameters.Add(new SqlParameter(Lookup_Reserach_ArticleCategoryRepositoryConstants.ArticleCategoryID, ArticleCategoryID));
        //        SqlDataReader reader = await sqlCommand.ExecuteReaderAsync();
        //        while (reader.Read())
        //        {
        //            Lookup_Reserach_ArticleCategoryEntity entity = EntityHelper(reader, false);
        //            list.Add(entity);
        //        }

        //        if (list.Count > 0)
        //        {
        //            reader.Close();
        //            result.List = list;
        //        }
        //        else
        //        {
        //            result.Status = ErrorEnums.Information;
        //            result.Details = MessageConstants.CannotFindAllMessage;
        //            result.Message = MessageConstants.CannotFindAllDetails;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        result.Status = ErrorEnums.Exception;
        //        result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
        //    }
        //    finally
        //    {
        //        sqlConnection.Close();
        //        sqlConnection.Dispose();
        //        sqlCommand.Dispose();
        //    }

        //    return result;
        //}

        //public static ResultList<Lookup_Reserach_ArticleCategoryEntity> Lookup_Area_ByGovernateIDNotAsync(int ArticleCategoryID)
        //{

        //    ResultList<Lookup_Reserach_ArticleCategoryEntity> result = new ResultList<Lookup_Reserach_ArticleCategoryEntity>();

        //    SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
        //    SqlCommand sqlCommand = new SqlCommand(Lookup_Reserach_ArticleCategoryRepositoryConstants.SP_SelectByGovID, sqlConnection);
        //    sqlCommand.CommandType = CommandType.StoredProcedure;
        //    List<Lookup_Reserach_ArticleCategoryEntity> list = new List<Lookup_Reserach_ArticleCategoryEntity>();

        //    try
        //    {
        //        sqlConnection.Open();
        //        sqlCommand.Parameters.Add(new SqlParameter(Lookup_Reserach_ArticleCategoryRepositoryConstants.ArticleCategoryID, ArticleCategoryID));
        //        SqlDataReader reader = sqlCommand.ExecuteReader();
        //        while (reader.Read())
        //        {
        //            Lookup_Reserach_ArticleCategoryEntity entity = EntityHelper(reader, false);
        //            list.Add(entity);
        //        }

        //        if (list.Count > 0)
        //        {
        //            reader.Close();
        //            result.List = list;
        //        }
        //        else
        //        {
        //            result.Status = ErrorEnums.Information;
        //            result.Details = MessageConstants.CannotFindAllMessage;
        //            result.Message = MessageConstants.CannotFindAllDetails;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        result.Status = ErrorEnums.Exception;
        //        result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
        //    }
        //    finally
        //    {
        //        sqlConnection.Close();
        //        sqlConnection.Dispose();
        //        sqlCommand.Dispose();
        //    }

        //    return result;
        //}

        static Lookup_Reserach_ArticleCategoryEntity EntityHelper(SqlDataReader reader, bool isFull)
        {
            Lookup_Reserach_ArticleCategoryEntity entity = new Lookup_Reserach_ArticleCategoryEntity();

            entity.ArticleCategoryID = Convert.ToInt32(reader[Lookup_Reserach_ArticleCategoryEntityConstants.ArticleCategoryID].ToString());
            entity.name = reader[Lookup_Reserach_ArticleCategoryEntityConstants.name].ToString();
            entity.IsPublished = Convert.ToBoolean(reader[Lookup_Reserach_ArticleCategoryEntityConstants.IsPublished].ToString());
            entity.IsDeleted = Convert.ToBoolean(reader[Lookup_Reserach_ArticleCategoryEntityConstants.IsDeleted].ToString());
            entity.AddDate = Convert.ToDateTime(reader[Lookup_Reserach_ArticleCategoryEntityConstants.AddDate].ToString());
            entity.AddUser = Convert.ToInt32(reader[Lookup_Reserach_ArticleCategoryEntityConstants.AddUser].ToString());
            entity.EditDate = Convert.ToDateTime(reader[Lookup_Reserach_ArticleCategoryEntityConstants.EditDate].ToString());
            entity.EditUser = Convert.ToInt32(reader[Lookup_Reserach_ArticleCategoryEntityConstants.EditUser].ToString());

            return entity;
        }
    }
}
