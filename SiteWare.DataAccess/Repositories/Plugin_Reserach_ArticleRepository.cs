﻿using SiteWare.DataAccess.Common.Constants;
using SiteWare.DataAccess.RepositorieConstants;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Common.Enums;
using SiteWare.Entity.Constants.Entity;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.DataAccess.Repositories
{
    public static class Plugin_Reserach_ArticleRepository
    {
        public async static Task<ResultList<Plugin_Reserach_ArticleEntity>> SelectAll()
        {
            ResultList<Plugin_Reserach_ArticleEntity> result = new ResultList<Plugin_Reserach_ArticleEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ArticleRepositoryConstants.SP_SelectAll, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            List<Plugin_Reserach_ArticleEntity> list = new List<Plugin_Reserach_ArticleEntity>();

            try
            {
                sqlConnection.Open();

                SqlDataReader reader = await sqlCommand.ExecuteReaderAsync();

                while (reader.Read())
                {
                    Plugin_Reserach_ArticleEntity entity = EntityHelper(reader, false);
                    list.Add(entity);
                }

                if (list.Count > 0)
                {
                    reader.Close();

                    result.List = list;

                }
                else
                {
                    result.Status = ErrorEnums.Information;
                    result.Details = MessageConstants.CannotFindAllMessage;
                    result.Message = MessageConstants.CannotFindAllDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
                result.Message = ex.Message;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
        public static ResultList<Plugin_Reserach_ArticleEntity> SelectAllNotAsync()
        {
            ResultList<Plugin_Reserach_ArticleEntity> result = new ResultList<Plugin_Reserach_ArticleEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ArticleRepositoryConstants.SP_SelectAll, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            List<Plugin_Reserach_ArticleEntity> list = new List<Plugin_Reserach_ArticleEntity>();

            try
            {
                sqlConnection.Open();

                SqlDataReader reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    Plugin_Reserach_ArticleEntity entity = EntityHelper(reader, false);
                    list.Add(entity);
                }

                if (list.Count > 0)
                {
                    reader.Close();

                    result.List = list;

                }
                else
                {
                    result.Status = ErrorEnums.Information;
                    result.Details = MessageConstants.CannotFindAllMessage;
                    result.Message = MessageConstants.CannotFindAllDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
                result.Message = ex.Message;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_ArticleEntity>> Insert(Plugin_Reserach_ArticleEntity entity)
        {

            ResultEntity<Plugin_Reserach_ArticleEntity> result = new ResultEntity<Plugin_Reserach_ArticleEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ArticleRepositoryConstants.SP_Insert, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                //sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleID, entity.ArticleID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleTitle, entity.ArticleTitle);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleImage, entity.ArticleImage);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleDate, entity.ArticleDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleKeyWords, entity.ArticleKeyWords);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleSummery, entity.ArticleSummery);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleAuthor, entity.ArticleAuthor);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleCategory, entity.ArticleCategory);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleDetails, entity.ArticleDetails);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.Order, entity.Order);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.PublishedDate, entity.PublishedDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.IsPublished, entity.IsPublished);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.IsDelete, entity.IsDelete);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.LanguageID, entity.LanguageID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.AddDate, entity.AddDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.AddUser, entity.AddUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.EditDate, entity.EditDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.EditUser, entity.EditUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ViewCount, entity.ViewCount);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.IsShowSlider, entity.IsShowSlider);
                


                sqlCommand.Parameters.Add(Plugin_Reserach_ArticleRepositoryConstants.ArticleID, SqlDbType.BigInt).Direction = ParameterDirection.Output;

                SqlDataReader reader = await sqlCommand.ExecuteReaderAsync();
                result.Entity = entity;
                entity.ArticleID = Convert.ToInt64(sqlCommand.Parameters[Plugin_Reserach_ArticleRepositoryConstants.ArticleID].Value);

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.InsertSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
        public static ResultEntity<Plugin_Reserach_ArticleEntity> InsertNotAsync(Plugin_Reserach_ArticleEntity entity)
        {

            ResultEntity<Plugin_Reserach_ArticleEntity> result = new ResultEntity<Plugin_Reserach_ArticleEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ArticleRepositoryConstants.SP_Insert, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                //sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleID, entity.ArticleID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleTitle, entity.ArticleTitle);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleImage, entity.ArticleImage);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleDate, entity.ArticleDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleKeyWords, entity.ArticleKeyWords);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleSummery, entity.ArticleSummery);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleAuthor, entity.ArticleAuthor);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleCategory, entity.ArticleCategory);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleDetails, entity.ArticleDetails);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.Order, entity.Order);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.PublishedDate, entity.PublishedDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.IsPublished, entity.IsPublished);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.IsDelete, entity.IsDelete);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.LanguageID, entity.LanguageID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.AddDate, entity.AddDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.AddUser, entity.AddUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.EditDate, entity.EditDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.EditUser, entity.EditUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ViewCount, entity.ViewCount);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.IsShowSlider, entity.IsShowSlider);
                sqlCommand.Parameters.Add(Plugin_Reserach_ArticleRepositoryConstants.ArticleID, SqlDbType.BigInt).Direction = ParameterDirection.Output;

                SqlDataReader reader = sqlCommand.ExecuteReader();
                result.Entity = entity;
                entity.ArticleID = Convert.ToInt64(sqlCommand.Parameters[Plugin_Reserach_ArticleRepositoryConstants.ArticleID].Value);

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.InsertSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_ArticleEntity>> SelectByID(long ID)
        {

            ResultEntity<Plugin_Reserach_ArticleEntity> result = new ResultEntity<Plugin_Reserach_ArticleEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ArticleRepositoryConstants.SP_SelectByID, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.Add(new SqlParameter(Plugin_Reserach_ArticleRepositoryConstants.ArticleID, ID));
                SqlDataReader reader = await sqlCommand.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    reader.Read();
                    result.Entity = EntityHelper(reader, true);
                }
                else
                {
                    result.Status = ErrorEnums.Warning;
                    result.Message = MessageConstants.NotFoundMessage;
                    result.Details = MessageConstants.NotFoundDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
        public static ResultEntity<Plugin_Reserach_ArticleEntity> SelectByIDNotAsync(long ID)
        {

            ResultEntity<Plugin_Reserach_ArticleEntity> result = new ResultEntity<Plugin_Reserach_ArticleEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ArticleRepositoryConstants.SP_SelectByID, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.Add(new SqlParameter(Plugin_Reserach_ArticleRepositoryConstants.ArticleID, ID));
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    result.Entity = EntityHelper(reader, true);
                }
                else
                {
                    result.Status = ErrorEnums.Warning;
                    result.Message = MessageConstants.NotFoundMessage;
                    result.Details = MessageConstants.NotFoundDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_ArticleEntity>> Update(Plugin_Reserach_ArticleEntity entity)
        {

            ResultEntity<Plugin_Reserach_ArticleEntity> result = new ResultEntity<Plugin_Reserach_ArticleEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ArticleRepositoryConstants.SP_Update, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleTitle, entity.ArticleTitle);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleImage, entity.ArticleImage);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleDate, entity.ArticleDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleKeyWords, entity.ArticleKeyWords);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleSummery, entity.ArticleSummery);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleAuthor, entity.ArticleAuthor);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleCategory, entity.ArticleCategory);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleDetails, entity.ArticleDetails);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.Order, entity.Order);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.PublishedDate, entity.PublishedDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.IsPublished, entity.IsPublished);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.IsDelete, entity.IsDelete);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.LanguageID, entity.LanguageID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.AddDate, entity.AddDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.AddUser, entity.AddUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.EditDate, entity.EditDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.EditUser, entity.EditUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ViewCount, entity.ViewCount);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleID, entity.ArticleID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.IsShowSlider, entity.IsShowSlider);
                await sqlCommand.ExecuteNonQueryAsync();
                result.Entity = entity;

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.UpdateSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
        public static ResultEntity<Plugin_Reserach_ArticleEntity> UpdateNotAsync(Plugin_Reserach_ArticleEntity entity)
        {

            ResultEntity<Plugin_Reserach_ArticleEntity> result = new ResultEntity<Plugin_Reserach_ArticleEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ArticleRepositoryConstants.SP_Update, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleTitle, entity.ArticleTitle);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleImage, entity.ArticleImage);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleDate, entity.ArticleDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleKeyWords, entity.ArticleKeyWords);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleSummery, entity.ArticleSummery);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleAuthor, entity.ArticleAuthor);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleCategory, entity.ArticleCategory);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleDetails, entity.ArticleDetails);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.Order, entity.Order);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.PublishedDate, entity.PublishedDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.IsPublished, entity.IsPublished);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.IsDelete, entity.IsDelete);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.LanguageID, entity.LanguageID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.AddDate, entity.AddDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.AddUser, entity.AddUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.EditDate, entity.EditDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.EditUser, entity.EditUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ViewCount, entity.ViewCount);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.IsShowSlider, entity.IsShowSlider);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleID, entity.ArticleID);

                sqlCommand.ExecuteNonQueryAsync();
                result.Entity = entity;

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.UpdateSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_ArticleEntity>> Delete(long ID)
        {

            ResultEntity<Plugin_Reserach_ArticleEntity> result = new ResultEntity<Plugin_Reserach_ArticleEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ArticleRepositoryConstants.SP_Delete, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleID, ID);

                await sqlCommand.ExecuteReaderAsync();

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.DeleteSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        static Plugin_Reserach_ArticleEntity EntityHelper(SqlDataReader reader, bool isFull)
        {
            Plugin_Reserach_ArticleEntity entity = new Plugin_Reserach_ArticleEntity();

            try
            {
                entity.ArticleID = Convert.ToInt64(reader[Plugin_Reserach_ArticleEntityConstants.ArticleID].ToString());
                entity.ArticleTitle = reader[Plugin_Reserach_ArticleEntityConstants.ArticleTitle] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ArticleEntityConstants.ArticleTitle].ToString();
                entity.ArticleImage = reader[Plugin_Reserach_ArticleEntityConstants.ArticleImage] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ArticleEntityConstants.ArticleImage].ToString();

                entity.ArticleDate = reader[Plugin_Reserach_ArticleEntityConstants.ArticleDate] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(reader[Plugin_Reserach_ArticleEntityConstants.ArticleDate].ToString());
                entity.ArticleKeyWords = reader[Plugin_Reserach_ArticleEntityConstants.ArticleKeyWords] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ArticleEntityConstants.ArticleKeyWords].ToString();
                entity.ArticleSummery = reader[Plugin_Reserach_ArticleEntityConstants.ArticleSummery] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ArticleEntityConstants.ArticleSummery].ToString();
                entity.ArticleAuthor = reader[Plugin_Reserach_ArticleEntityConstants.ArticleAuthor] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ArticleEntityConstants.ArticleAuthor].ToString();
                entity.ArticleCategory = reader[Plugin_Reserach_ArticleEntityConstants.ArticleCategory] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ArticleEntityConstants.ArticleCategory].ToString();
                entity.ArticleDetails = reader[Plugin_Reserach_ArticleEntityConstants.ArticleDetails] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ArticleEntityConstants.ArticleDetails].ToString();


                entity.Order = reader[Plugin_Reserach_ArticleEntityConstants.Order] == DBNull.Value ? 0 : Convert.ToInt64(reader[Plugin_Reserach_ArticleEntityConstants.Order].ToString());

                entity.PublishedDate = reader[Plugin_Reserach_ArticleEntityConstants.PublishedDate] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(reader[Plugin_Reserach_ArticleEntityConstants.PublishedDate].ToString());
                entity.IsPublished = reader[Plugin_Reserach_ArticleEntityConstants.IsPublished] == DBNull.Value ? false : Convert.ToBoolean(reader[Plugin_Reserach_ArticleEntityConstants.IsPublished].ToString());
                entity.IsDelete = reader[Plugin_Reserach_ArticleEntityConstants.IsDelete] == DBNull.Value ? false : Convert.ToBoolean(reader[Plugin_Reserach_ArticleEntityConstants.IsDelete].ToString());
                entity.LanguageID = reader[Plugin_Reserach_ArticleEntityConstants.LanguageID] == DBNull.Value ? 1 : Convert.ToInt32(reader[Plugin_Reserach_ArticleEntityConstants.LanguageID].ToString());
                entity.AddDate = reader[Plugin_Reserach_ArticleEntityConstants.AddDate] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(reader[Plugin_Reserach_ArticleEntityConstants.AddDate].ToString());
                entity.EditDate = reader[Plugin_Reserach_ArticleEntityConstants.EditDate] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(reader[Plugin_Reserach_ArticleEntityConstants.EditDate].ToString());
                entity.AddUser = reader[Plugin_Reserach_ArticleEntityConstants.AddUser].ToString();
                entity.EditUser = reader[Plugin_Reserach_ArticleEntityConstants.EditUser].ToString();
                bool ColumnExists = false;
                try
                {
                    int columnOrdinal = reader.GetOrdinal("LanguageName");
                    ColumnExists = true;
                }
                catch (IndexOutOfRangeException)
                {
                    ColumnExists = false;
                }

                if (ColumnExists)
                {
                    entity.LanguageName = reader[Plugin_Reserach_ArticleEntityConstants.LanguageName] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ArticleEntityConstants.LanguageName].ToString();
                }

                try
                {
                    int columnOrdinal = reader.GetOrdinal("ViewCount");
                    ColumnExists = true;
                }
                catch (IndexOutOfRangeException)
                {
                    ColumnExists = false;
                }

                if (ColumnExists)
                {
                    entity.ViewCount = reader[Plugin_Reserach_ArticleEntityConstants.ViewCount] == DBNull.Value ? 0 : Convert.ToInt64(reader[Plugin_Reserach_ArticleEntityConstants.ViewCount].ToString());
                    
                }


                try
                {
                    int columnOrdinal = reader.GetOrdinal("IsShowSlider");
                    ColumnExists = true;
                }
                catch (IndexOutOfRangeException)
                {
                    ColumnExists = false;
                }

                if (ColumnExists)
                {
                    entity.IsShowSlider = reader[Plugin_Reserach_ArticleEntityConstants.IsShowSlider] == DBNull.Value ? false : Convert.ToBoolean(reader[Plugin_Reserach_ArticleEntityConstants.IsShowSlider].ToString());                    
                }

            }
            catch (Exception ex)
            {

            }

            return entity;
        }


        public async static Task<ResultEntity<Plugin_Reserach_ArticleEntity>> UpdateViewCount(long ArticleID)
        {

            ResultEntity<Plugin_Reserach_ArticleEntity> result = new ResultEntity<Plugin_Reserach_ArticleEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ArticleRepositoryConstants.SP_UpdateViewCount, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ArticleRepositoryConstants.ArticleID, ArticleID);

                await sqlCommand.ExecuteNonQueryAsync();

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.UpdateSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
    }
}

