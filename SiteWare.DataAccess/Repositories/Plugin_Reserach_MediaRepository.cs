﻿using SiteWare.DataAccess.Common.Constants;
using SiteWare.DataAccess.RepositorieConstants;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Common.Enums;
using SiteWare.Entity.Constants.Entity;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.DataAccess.Repositories
{
    public static class Plugin_Reserach_MediaRepository
    {
        public async static Task<ResultList<Plugin_Reserach_MediaEntity>> SelectAll()
        {
            ResultList<Plugin_Reserach_MediaEntity> result = new ResultList<Plugin_Reserach_MediaEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_MediaRepositoryConstants.SP_SelectAll, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            List<Plugin_Reserach_MediaEntity> list = new List<Plugin_Reserach_MediaEntity>();

            try
            {
                sqlConnection.Open();

                SqlDataReader reader = await sqlCommand.ExecuteReaderAsync();

                while (reader.Read())
                {
                    Plugin_Reserach_MediaEntity entity = EntityHelper(reader, false);
                    list.Add(entity);
                }

                if (list.Count > 0)
                {
                    reader.Close();

                    result.List = list;

                }
                else
                {
                    result.Status = ErrorEnums.Information;
                    result.Details = MessageConstants.CannotFindAllMessage;
                    result.Message = MessageConstants.CannotFindAllDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
                result.Message = ex.Message;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
        public static ResultList<Plugin_Reserach_MediaEntity> SelectAllNotAsync()
        {
            ResultList<Plugin_Reserach_MediaEntity> result = new ResultList<Plugin_Reserach_MediaEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_MediaRepositoryConstants.SP_SelectAll, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            List<Plugin_Reserach_MediaEntity> list = new List<Plugin_Reserach_MediaEntity>();

            try
            {
                sqlConnection.Open();

                SqlDataReader reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    Plugin_Reserach_MediaEntity entity = EntityHelper(reader, false);
                    list.Add(entity);
                }

                if (list.Count > 0)
                {
                    reader.Close();

                    result.List = list;

                }
                else
                {
                    result.Status = ErrorEnums.Information;
                    result.Details = MessageConstants.CannotFindAllMessage;
                    result.Message = MessageConstants.CannotFindAllDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
                result.Message = ex.Message;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_MediaEntity>> Insert(Plugin_Reserach_MediaEntity entity)
        {

            ResultEntity<Plugin_Reserach_MediaEntity> result = new ResultEntity<Plugin_Reserach_MediaEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_MediaRepositoryConstants.SP_Insert, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                //sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.MediaID, entity.MediaID);                
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.MediaImage, entity.MediaImage);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.MedialLink, entity.MedialLink);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.Target, entity.Target);                
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.Order, entity.Order);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.PublishedDate, entity.PublishedDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.IsPublished, entity.IsPublished);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.IsDelete, entity.IsDelete);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.LanguageID, entity.LanguageID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.AddDate, entity.AddDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.AddUser, entity.AddUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.EditDate, entity.EditDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.EditUser, entity.EditUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.Title, entity.Title);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.ViewCount, entity.ViewCount);

                

                sqlCommand.Parameters.Add(Plugin_Reserach_MediaRepositoryConstants.MediaID, SqlDbType.BigInt).Direction = ParameterDirection.Output;

                SqlDataReader reader = await sqlCommand.ExecuteReaderAsync();
                result.Entity = entity;
                entity.MediaID = Convert.ToInt64(sqlCommand.Parameters[Plugin_Reserach_MediaRepositoryConstants.MediaID].Value);

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.InsertSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
        public static ResultEntity<Plugin_Reserach_MediaEntity> InsertNotAsync(Plugin_Reserach_MediaEntity entity)
        {

            ResultEntity<Plugin_Reserach_MediaEntity> result = new ResultEntity<Plugin_Reserach_MediaEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_MediaRepositoryConstants.SP_Insert, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                //sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.MediaID, entity.MediaID);                
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.MediaImage, entity.MediaImage);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.MedialLink, entity.MedialLink);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.Target, entity.Target);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.Order, entity.Order);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.PublishedDate, entity.PublishedDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.IsPublished, entity.IsPublished);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.IsDelete, entity.IsDelete);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.LanguageID, entity.LanguageID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.AddDate, entity.AddDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.AddUser, entity.AddUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.EditDate, entity.EditDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.EditUser, entity.EditUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.Title, entity.Title);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.ViewCount, entity.ViewCount);
                sqlCommand.Parameters.Add(Plugin_Reserach_MediaRepositoryConstants.MediaID, SqlDbType.BigInt).Direction = ParameterDirection.Output;

                SqlDataReader reader = sqlCommand.ExecuteReader();
                result.Entity = entity;
                entity.MediaID = Convert.ToInt64(sqlCommand.Parameters[Plugin_Reserach_MediaRepositoryConstants.MediaID].Value);

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.InsertSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_MediaEntity>> SelectByID(long ID)
        {

            ResultEntity<Plugin_Reserach_MediaEntity> result = new ResultEntity<Plugin_Reserach_MediaEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_MediaRepositoryConstants.SP_SelectByID, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.Add(new SqlParameter(Plugin_Reserach_MediaRepositoryConstants.MediaID, ID));
                SqlDataReader reader = await sqlCommand.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    reader.Read();
                    result.Entity = EntityHelper(reader, true);
                }
                else
                {
                    result.Status = ErrorEnums.Warning;
                    result.Message = MessageConstants.NotFoundMessage;
                    result.Details = MessageConstants.NotFoundDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
        public static ResultEntity<Plugin_Reserach_MediaEntity> SelectByIDNotAsync(long ID)
        {

            ResultEntity<Plugin_Reserach_MediaEntity> result = new ResultEntity<Plugin_Reserach_MediaEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_MediaRepositoryConstants.SP_SelectByID, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.Add(new SqlParameter(Plugin_Reserach_MediaRepositoryConstants.MediaID, ID));
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    result.Entity = EntityHelper(reader, true);
                }
                else
                {
                    result.Status = ErrorEnums.Warning;
                    result.Message = MessageConstants.NotFoundMessage;
                    result.Details = MessageConstants.NotFoundDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_MediaEntity>> Update(Plugin_Reserach_MediaEntity entity)
        {

            ResultEntity<Plugin_Reserach_MediaEntity> result = new ResultEntity<Plugin_Reserach_MediaEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_MediaRepositoryConstants.SP_Update, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();

                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.MediaID, entity.MediaID);
                //sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.MediaID, entity.MediaID);                
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.MediaImage, entity.MediaImage);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.MedialLink, entity.MedialLink);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.Target, entity.Target);
                
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.Order, entity.Order);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.PublishedDate, entity.PublishedDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.IsPublished, entity.IsPublished);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.IsDelete, entity.IsDelete);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.LanguageID, entity.LanguageID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.AddDate, entity.AddDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.AddUser, entity.AddUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.EditDate, entity.EditDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.EditUser, entity.EditUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.Title, entity.Title);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.ViewCount, entity.ViewCount);
                await sqlCommand.ExecuteNonQueryAsync();
                result.Entity = entity;

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.UpdateSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
        public static ResultEntity<Plugin_Reserach_MediaEntity> UpdateNotAsync(Plugin_Reserach_MediaEntity entity)
        {

            ResultEntity<Plugin_Reserach_MediaEntity> result = new ResultEntity<Plugin_Reserach_MediaEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_MediaRepositoryConstants.SP_Update, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.MediaID, entity.MediaID);
                //sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.MediaID, entity.MediaID);                
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.MediaImage, entity.MediaImage);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.MedialLink, entity.MedialLink);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.Target, entity.Target);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.Order, entity.Order);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.PublishedDate, entity.PublishedDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.IsPublished, entity.IsPublished);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.IsDelete, entity.IsDelete);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.LanguageID, entity.LanguageID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.AddDate, entity.AddDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.AddUser, entity.AddUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.EditDate, entity.EditDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.EditUser, entity.EditUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.Title, entity.Title);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.ViewCount, entity.ViewCount);

                sqlCommand.ExecuteNonQueryAsync();
                result.Entity = entity;

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.UpdateSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_MediaEntity>> Delete(long ID)
        {

            ResultEntity<Plugin_Reserach_MediaEntity> result = new ResultEntity<Plugin_Reserach_MediaEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_MediaRepositoryConstants.SP_Delete, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.MediaID, ID);

                await sqlCommand.ExecuteReaderAsync();

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.DeleteSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        static Plugin_Reserach_MediaEntity EntityHelper(SqlDataReader reader, bool isFull)
        {
            Plugin_Reserach_MediaEntity entity = new Plugin_Reserach_MediaEntity();

            try
            {
                entity.MediaID      = Convert.ToInt64(reader[Plugin_Reserach_MediaEntityConstants.MediaID].ToString());
                entity.MediaImage   = reader[Plugin_Reserach_MediaEntityConstants.MediaImage] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_MediaEntityConstants.MediaImage].ToString();
                entity.MedialLink   = reader[Plugin_Reserach_MediaEntityConstants.MedialLink] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_MediaEntityConstants.MedialLink].ToString();
                entity.Target       = reader[Plugin_Reserach_MediaEntityConstants.Target] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_MediaEntityConstants.Target].ToString();                
                entity.Order = reader[Plugin_Reserach_MediaEntityConstants.Order] == DBNull.Value ? 0 : Convert.ToInt64(reader[Plugin_Reserach_MediaEntityConstants.Order].ToString());
                entity.PublishedDate = reader[Plugin_Reserach_MediaEntityConstants.PublishedDate] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(reader[Plugin_Reserach_MediaEntityConstants.PublishedDate].ToString());
                entity.IsPublished = reader[Plugin_Reserach_MediaEntityConstants.IsPublished] == DBNull.Value ? false : Convert.ToBoolean(reader[Plugin_Reserach_MediaEntityConstants.IsPublished].ToString());
                entity.IsDelete = reader[Plugin_Reserach_MediaEntityConstants.IsDelete] == DBNull.Value ? false : Convert.ToBoolean(reader[Plugin_Reserach_MediaEntityConstants.IsDelete].ToString());
                entity.LanguageID = reader[Plugin_Reserach_MediaEntityConstants.LanguageID] == DBNull.Value ? 1 : Convert.ToInt32(reader[Plugin_Reserach_MediaEntityConstants.LanguageID].ToString());
                entity.AddDate = reader[Plugin_Reserach_MediaEntityConstants.AddDate] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(reader[Plugin_Reserach_MediaEntityConstants.AddDate].ToString());
                entity.EditDate = reader[Plugin_Reserach_MediaEntityConstants.EditDate] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(reader[Plugin_Reserach_MediaEntityConstants.EditDate].ToString());
                entity.AddUser = reader[Plugin_Reserach_MediaEntityConstants.AddUser].ToString();
                entity.EditUser = reader[Plugin_Reserach_MediaEntityConstants.EditUser].ToString();
                bool ColumnExists = false;
                try
                {
                    int columnOrdinal = reader.GetOrdinal("LanguageName");
                    ColumnExists = true;
                }
                catch (IndexOutOfRangeException)
                {
                    ColumnExists = false;
                }

                if (ColumnExists)
                {
                    entity.LanguageName = reader[Plugin_Reserach_MediaEntityConstants.LanguageName] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_MediaEntityConstants.LanguageName].ToString();
                }

                try
                {
                    int columnOrdinal = reader.GetOrdinal("Title");
                    ColumnExists = true;
                }
                catch (IndexOutOfRangeException)
                {
                    ColumnExists = false;
                }

                if (ColumnExists)
                {
                    entity.Title = reader[Plugin_Reserach_MediaEntityConstants.Title] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_MediaEntityConstants.Title].ToString();
                }


                try
                {
                    int columnOrdinal = reader.GetOrdinal("ViewCount");
                    ColumnExists = true;
                }
                catch (IndexOutOfRangeException)
                {
                    ColumnExists = false;
                }

                if (ColumnExists)
                {
                    entity.ViewCount = reader[Plugin_Reserach_MediaEntityConstants.ViewCount] == DBNull.Value ? 1 : Convert.ToInt64(reader[Plugin_Reserach_MediaEntityConstants.ViewCount].ToString());
                }
            }
            catch (Exception ex)
            {

            }

            return entity;
        }

        public async static Task<ResultEntity<Plugin_Reserach_MediaEntity>> UpdateViewCount(long MediaID)
        {

            ResultEntity<Plugin_Reserach_MediaEntity> result = new ResultEntity<Plugin_Reserach_MediaEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_MediaRepositoryConstants.SP_UpdateViewCount, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_MediaRepositoryConstants.MediaID, MediaID);

                await sqlCommand.ExecuteNonQueryAsync();

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.UpdateSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
    }
}
