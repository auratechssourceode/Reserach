﻿using SiteWare.DataAccess.Common.Constants;
using SiteWare.DataAccess.RepositorieConstants;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Common.Enums;
using SiteWare.Entity.Constants.Entity;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.DataAccess.Repositories
{
    public static class Plugin_Reserach_ResearchInnerRepository
    {
        public async static Task<ResultList<Plugin_Reserach_ResearchInnerEntity>> SelectAll()
        {
            ResultList<Plugin_Reserach_ResearchInnerEntity> result = new ResultList<Plugin_Reserach_ResearchInnerEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ResearchInnerRepositoryConstants.SP_SelectAll, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            List<Plugin_Reserach_ResearchInnerEntity> list = new List<Plugin_Reserach_ResearchInnerEntity>();

            try
            {
                sqlConnection.Open();

                SqlDataReader reader = await sqlCommand.ExecuteReaderAsync();

                while (reader.Read())
                {
                    Plugin_Reserach_ResearchInnerEntity entity = EntityHelper(reader, false);
                    list.Add(entity);
                }

                if (list.Count > 0)
                {
                    reader.Close();

                    result.List = list;

                }
                else
                {
                    result.Status = ErrorEnums.Information;
                    result.Details = MessageConstants.CannotFindAllMessage;
                    result.Message = MessageConstants.CannotFindAllDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
                result.Message = ex.Message;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
        public static ResultList<Plugin_Reserach_ResearchInnerEntity> SelectAllNotAsync()
        {
            ResultList<Plugin_Reserach_ResearchInnerEntity> result = new ResultList<Plugin_Reserach_ResearchInnerEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ResearchInnerRepositoryConstants.SP_SelectAll, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            List<Plugin_Reserach_ResearchInnerEntity> list = new List<Plugin_Reserach_ResearchInnerEntity>();

            try
            {
                sqlConnection.Open();

                SqlDataReader reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    Plugin_Reserach_ResearchInnerEntity entity = EntityHelper(reader, false);
                    list.Add(entity);
                }

                if (list.Count > 0)
                {
                    reader.Close();

                    result.List = list;

                }
                else
                {
                    result.Status = ErrorEnums.Information;
                    result.Details = MessageConstants.CannotFindAllMessage;
                    result.Message = MessageConstants.CannotFindAllDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
                result.Message = ex.Message;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_ResearchInnerEntity>> Insert(Plugin_Reserach_ResearchInnerEntity entity)
        {

            ResultEntity<Plugin_Reserach_ResearchInnerEntity> result = new ResultEntity<Plugin_Reserach_ResearchInnerEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ResearchInnerRepositoryConstants.SP_Insert, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                //sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerID, entity.ResearchInnerID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerTitle, entity.ResearchInnerTitle);               
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerSummery, entity.ResearchInnerSummery);               
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerCatogory, entity.ResearchInnerCatogory);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerImage, entity.ResearchInnerImage);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.Order, entity.Order);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.PublishedDate, entity.PublishedDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.IsPublished, entity.IsPublished);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.IsDelete, entity.IsDelete);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.LanguageID, entity.LanguageID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.AddDate, entity.AddDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.AddUser, entity.AddUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.EditDate, entity.EditDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.EditUser, entity.EditUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ViewCount, entity.ViewCount);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerTitle2, entity.ResearchInnerTitle2);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.MedialLink, entity.MedialLink);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.Target, entity.Target);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerDate, entity.ResearchInnerDate);
                

                sqlCommand.Parameters.Add(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerID, SqlDbType.BigInt).Direction = ParameterDirection.Output;

                SqlDataReader reader = await sqlCommand.ExecuteReaderAsync();
                result.Entity = entity;
                entity.ResearchInnerID = Convert.ToInt64(sqlCommand.Parameters[Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerID].Value);

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.InsertSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
        public static ResultEntity<Plugin_Reserach_ResearchInnerEntity> InsertNotAsync(Plugin_Reserach_ResearchInnerEntity entity)
        {

            ResultEntity<Plugin_Reserach_ResearchInnerEntity> result = new ResultEntity<Plugin_Reserach_ResearchInnerEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ResearchInnerRepositoryConstants.SP_Insert, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                //sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerID, entity.ResearchInnerID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerTitle, entity.ResearchInnerTitle);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerSummery, entity.ResearchInnerSummery);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerCatogory, entity.ResearchInnerCatogory);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerImage, entity.ResearchInnerImage);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.Order, entity.Order);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.PublishedDate, entity.PublishedDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.IsPublished, entity.IsPublished);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.IsDelete, entity.IsDelete);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.LanguageID, entity.LanguageID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.AddDate, entity.AddDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.AddUser, entity.AddUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.EditDate, entity.EditDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.EditUser, entity.EditUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ViewCount, entity.ViewCount);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerTitle2, entity.ResearchInnerTitle2);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.MedialLink, entity.MedialLink);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.Target, entity.Target);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerDate, entity.ResearchInnerDate);

                sqlCommand.Parameters.Add(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerID, SqlDbType.BigInt).Direction = ParameterDirection.Output;

                SqlDataReader reader = sqlCommand.ExecuteReader();
                result.Entity = entity;
                entity.ResearchInnerID = Convert.ToInt64(sqlCommand.Parameters[Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerID].Value);

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.InsertSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_ResearchInnerEntity>> SelectByID(long ID)
        {

            ResultEntity<Plugin_Reserach_ResearchInnerEntity> result = new ResultEntity<Plugin_Reserach_ResearchInnerEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ResearchInnerRepositoryConstants.SP_SelectByID, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.Add(new SqlParameter(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerID, ID));
                SqlDataReader reader = await sqlCommand.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    reader.Read();
                    result.Entity = EntityHelper(reader, true);
                }
                else
                {
                    result.Status = ErrorEnums.Warning;
                    result.Message = MessageConstants.NotFoundMessage;
                    result.Details = MessageConstants.NotFoundDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
        public static ResultEntity<Plugin_Reserach_ResearchInnerEntity> SelectByIDNotAsync(long ID)
        {

            ResultEntity<Plugin_Reserach_ResearchInnerEntity> result = new ResultEntity<Plugin_Reserach_ResearchInnerEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ResearchInnerRepositoryConstants.SP_SelectByID, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.Add(new SqlParameter(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerID, ID));
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    result.Entity = EntityHelper(reader, true);
                }
                else
                {
                    result.Status = ErrorEnums.Warning;
                    result.Message = MessageConstants.NotFoundMessage;
                    result.Details = MessageConstants.NotFoundDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_ResearchInnerEntity>> Update(Plugin_Reserach_ResearchInnerEntity entity)
        {

            ResultEntity<Plugin_Reserach_ResearchInnerEntity> result = new ResultEntity<Plugin_Reserach_ResearchInnerEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ResearchInnerRepositoryConstants.SP_Update, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();

                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerTitle, entity.ResearchInnerTitle);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerSummery, entity.ResearchInnerSummery);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerCatogory, entity.ResearchInnerCatogory);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerImage, entity.ResearchInnerImage);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.Order, entity.Order);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.PublishedDate, entity.PublishedDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.IsPublished, entity.IsPublished);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.IsDelete, entity.IsDelete);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.LanguageID, entity.LanguageID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.AddDate, entity.AddDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.AddUser, entity.AddUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.EditDate, entity.EditDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.EditUser, entity.EditUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ViewCount, entity.ViewCount);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerTitle2, entity.ResearchInnerTitle2);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.MedialLink, entity.MedialLink);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.Target, entity.Target);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerDate, entity.ResearchInnerDate);

                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerID, entity.ResearchInnerID);
                await sqlCommand.ExecuteNonQueryAsync();
                result.Entity = entity;

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.UpdateSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
        public static ResultEntity<Plugin_Reserach_ResearchInnerEntity> UpdateNotAsync(Plugin_Reserach_ResearchInnerEntity entity)
        {

            ResultEntity<Plugin_Reserach_ResearchInnerEntity> result = new ResultEntity<Plugin_Reserach_ResearchInnerEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ResearchInnerRepositoryConstants.SP_Update, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerTitle, entity.ResearchInnerTitle);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerSummery, entity.ResearchInnerSummery);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerCatogory, entity.ResearchInnerCatogory);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerImage, entity.ResearchInnerImage);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.Order, entity.Order);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.PublishedDate, entity.PublishedDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.IsPublished, entity.IsPublished);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.IsDelete, entity.IsDelete);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.LanguageID, entity.LanguageID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.AddDate, entity.AddDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.AddUser, entity.AddUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.EditDate, entity.EditDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.EditUser, entity.EditUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ViewCount, entity.ViewCount);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerTitle2, entity.ResearchInnerTitle2);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.MedialLink, entity.MedialLink);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.Target, entity.Target);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerDate, entity.ResearchInnerDate);

                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerID, entity.ResearchInnerID);

                sqlCommand.ExecuteNonQueryAsync();
                result.Entity = entity;

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.UpdateSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_ResearchInnerEntity>> Delete(long ID)
        {

            ResultEntity<Plugin_Reserach_ResearchInnerEntity> result = new ResultEntity<Plugin_Reserach_ResearchInnerEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ResearchInnerRepositoryConstants.SP_Delete, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerID, ID);

                await sqlCommand.ExecuteReaderAsync();

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.DeleteSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        static Plugin_Reserach_ResearchInnerEntity EntityHelper(SqlDataReader reader, bool isFull)
        {
            Plugin_Reserach_ResearchInnerEntity entity = new Plugin_Reserach_ResearchInnerEntity();

            try
            {
                entity.ResearchInnerID = Convert.ToInt64(reader[Plugin_Reserach_ResearchInnerEntityConstants.ResearchInnerID].ToString());
                entity.ResearchInnerTitle = reader[Plugin_Reserach_ResearchInnerEntityConstants.ResearchInnerTitle] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ResearchInnerEntityConstants.ResearchInnerTitle].ToString();
                entity.ResearchInnerSummery = reader[Plugin_Reserach_ResearchInnerEntityConstants.ResearchInnerSummery] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ResearchInnerEntityConstants.ResearchInnerSummery].ToString();
                
                entity.ResearchInnerCatogory = reader[Plugin_Reserach_ResearchInnerEntityConstants.ResearchInnerCatogory] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ResearchInnerEntityConstants.ResearchInnerCatogory].ToString();
                entity.ResearchInnerImage = reader[Plugin_Reserach_ResearchInnerEntityConstants.ResearchInnerImage] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ResearchInnerEntityConstants.ResearchInnerImage].ToString();
                entity.Order = reader[Plugin_Reserach_ResearchInnerEntityConstants.Order] == DBNull.Value ? 0 : Convert.ToInt64(reader[Plugin_Reserach_ResearchInnerEntityConstants.Order].ToString());
                entity.PublishedDate = reader[Plugin_Reserach_ResearchInnerEntityConstants.PublishedDate] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(reader[Plugin_Reserach_ResearchInnerEntityConstants.PublishedDate].ToString());
                entity.IsPublished = reader[Plugin_Reserach_ResearchInnerEntityConstants.IsPublished] == DBNull.Value ? false : Convert.ToBoolean(reader[Plugin_Reserach_ResearchInnerEntityConstants.IsPublished].ToString());
                entity.IsDelete = reader[Plugin_Reserach_ResearchInnerEntityConstants.IsDelete] == DBNull.Value ? false : Convert.ToBoolean(reader[Plugin_Reserach_ResearchInnerEntityConstants.IsDelete].ToString());
                entity.LanguageID = reader[Plugin_Reserach_ResearchInnerEntityConstants.LanguageID] == DBNull.Value ? 1 : Convert.ToInt32(reader[Plugin_Reserach_ResearchInnerEntityConstants.LanguageID].ToString());
                entity.AddDate = reader[Plugin_Reserach_ResearchInnerEntityConstants.AddDate] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(reader[Plugin_Reserach_ResearchInnerEntityConstants.AddDate].ToString());
                entity.EditDate = reader[Plugin_Reserach_ResearchInnerEntityConstants.EditDate] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(reader[Plugin_Reserach_ResearchInnerEntityConstants.EditDate].ToString());
                entity.AddUser = reader[Plugin_Reserach_ResearchInnerEntityConstants.AddUser].ToString();
                entity.EditUser = reader[Plugin_Reserach_ResearchInnerEntityConstants.EditUser].ToString();

                bool ColumnExists = false;
                try
                {
                    int columnOrdinal = reader.GetOrdinal("LanguageName");
                    ColumnExists = true;
                }
                catch (IndexOutOfRangeException)
                {
                    ColumnExists = false;
                }

                if (ColumnExists)
                {
                    entity.LanguageName = reader[Plugin_Reserach_ResearchInnerEntityConstants.LanguageName] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ResearchInnerEntityConstants.LanguageName].ToString();
                }


                try
                {
                    int columnOrdinal = reader.GetOrdinal("ViewCount");
                    ColumnExists = true;
                }
                catch (IndexOutOfRangeException)
                {
                    ColumnExists = false;
                }

                if (ColumnExists)
                {
                    entity.ViewCount = reader[Plugin_Reserach_ResearchInnerEntityConstants.ViewCount] == DBNull.Value ? 1 : Convert.ToInt64(reader[Plugin_Reserach_ResearchInnerEntityConstants.ViewCount].ToString());
                }


                try
                {
                    int columnOrdinal = reader.GetOrdinal("ResearchInnerTitle2");
                    ColumnExists = true;
                }
                catch (IndexOutOfRangeException)
                {
                    ColumnExists = false;
                }

                if (ColumnExists)
                {
                    entity.ResearchInnerTitle2 = reader[Plugin_Reserach_ResearchInnerEntityConstants.ResearchInnerTitle2] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ResearchInnerEntityConstants.ResearchInnerTitle2].ToString();
                }


                try
                {
                    int columnOrdinal = reader.GetOrdinal("MedialLink");
                    ColumnExists = true;
                }
                catch (IndexOutOfRangeException)
                {
                    ColumnExists = false;
                }

                if (ColumnExists)
                {
                    entity.MedialLink = reader[Plugin_Reserach_ResearchInnerEntityConstants.MedialLink] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ResearchInnerEntityConstants.MedialLink].ToString();
                }

                try
                {
                    int columnOrdinal = reader.GetOrdinal("Target");
                    ColumnExists = true;
                }
                catch (IndexOutOfRangeException)
                {
                    ColumnExists = false;
                }

                if (ColumnExists)
                {
                    entity.Target = reader[Plugin_Reserach_ResearchInnerEntityConstants.Target] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ResearchInnerEntityConstants.Target].ToString();
                }

                try
                {
                    int columnOrdinal = reader.GetOrdinal("ResearchInnerDate");
                    ColumnExists = true;
                }
                catch (IndexOutOfRangeException)
                {
                    ColumnExists = false;
                }

                if (ColumnExists)
                {
                    entity.ResearchInnerDate = reader[Plugin_Reserach_ResearchInnerEntityConstants.ResearchInnerDate] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(reader[Plugin_Reserach_ResearchInnerEntityConstants.ResearchInnerDate].ToString());
                   
                }



            }
            catch (Exception ex)
            {

            }

            return entity;
        }


        public async static Task<ResultEntity<Plugin_Reserach_ResearchInnerEntity>> UpdateViewCount(long ResearchInnerID)
        {

            ResultEntity<Plugin_Reserach_ResearchInnerEntity> result = new ResultEntity<Plugin_Reserach_ResearchInnerEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ResearchInnerRepositoryConstants.SP_UpdateViewCount, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchInnerRepositoryConstants.ResearchInnerID, ResearchInnerID);

                await sqlCommand.ExecuteNonQueryAsync();

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.UpdateSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
    }
}
