﻿using SiteWare.DataAccess.Common.Constants;
using SiteWare.DataAccess.RepositorieConstants;
using SiteWare.Entity.Common.Entities;
using SiteWare.Entity.Common.Enums;
using SiteWare.Entity.Constants.Entity;
using SiteWare.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.DataAccess.Repositories
{
    public static class Plugin_Reserach_ResearchStudiesRepository
    {
        public async static Task<ResultList<Plugin_Reserach_ResearchStudiesEntity>> SelectAll()
        {
            ResultList<Plugin_Reserach_ResearchStudiesEntity> result = new ResultList<Plugin_Reserach_ResearchStudiesEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ResearchStudiesRepositoryConstants.SP_SelectAll, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            List<Plugin_Reserach_ResearchStudiesEntity> list = new List<Plugin_Reserach_ResearchStudiesEntity>();

            try
            {
                sqlConnection.Open();

                SqlDataReader reader = await sqlCommand.ExecuteReaderAsync();

                while (reader.Read())
                {
                    Plugin_Reserach_ResearchStudiesEntity entity = EntityHelper(reader, false);
                    list.Add(entity);
                }

                if (list.Count > 0)
                {
                    reader.Close();

                    result.List = list;

                }
                else
                {
                    result.Status = ErrorEnums.Information;
                    result.Details = MessageConstants.CannotFindAllMessage;
                    result.Message = MessageConstants.CannotFindAllDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
                result.Message = ex.Message;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
        public static ResultList<Plugin_Reserach_ResearchStudiesEntity> SelectAllNotAsync()
        {
            ResultList<Plugin_Reserach_ResearchStudiesEntity> result = new ResultList<Plugin_Reserach_ResearchStudiesEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ResearchStudiesRepositoryConstants.SP_SelectAll, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            List<Plugin_Reserach_ResearchStudiesEntity> list = new List<Plugin_Reserach_ResearchStudiesEntity>();

            try
            {
                sqlConnection.Open();

                SqlDataReader reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    Plugin_Reserach_ResearchStudiesEntity entity = EntityHelper(reader, false);
                    list.Add(entity);
                }

                if (list.Count > 0)
                {
                    reader.Close();

                    result.List = list;

                }
                else
                {
                    result.Status = ErrorEnums.Information;
                    result.Details = MessageConstants.CannotFindAllMessage;
                    result.Message = MessageConstants.CannotFindAllDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
                result.Message = ex.Message;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_ResearchStudiesEntity>> Insert(Plugin_Reserach_ResearchStudiesEntity entity)
        {

            ResultEntity<Plugin_Reserach_ResearchStudiesEntity> result = new ResultEntity<Plugin_Reserach_ResearchStudiesEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ResearchStudiesRepositoryConstants.SP_Insert, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                //sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesID, entity.ResearchStudiesID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesTitle, entity.ResearchStudiesTitle);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesDate, entity.ResearchStudiesDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesAuthor, entity.ResearchStudiesAuthor);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesVersion, entity.ResearchStudiesVersion);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesSummery, entity.ResearchStudiesSummery);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesDetails, entity.ResearchStudiesDetails);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesCatogory, entity.ResearchStudiesCatogory);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesImage, entity.ResearchStudiesImage);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.Order, entity.Order);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.PublishedDate, entity.PublishedDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.IsPublished, entity.IsPublished);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.IsDelete, entity.IsDelete);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.LanguageID, entity.LanguageID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.AddDate, entity.AddDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.AddUser, entity.AddUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.EditDate, entity.EditDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.EditUser, entity.EditUser);

                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ViewCount, entity.ViewCount);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesTitle2, entity.ResearchStudiesTitle2);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesImage2, entity.ResearchStudiesImage2);

                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.MedialLink, entity.MedialLink);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.Target, entity.Target);


                sqlCommand.Parameters.Add(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesID, SqlDbType.BigInt).Direction = ParameterDirection.Output;

                SqlDataReader reader = await sqlCommand.ExecuteReaderAsync();
                result.Entity = entity;
                entity.ResearchStudiesID = Convert.ToInt64(sqlCommand.Parameters[Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesID].Value);

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.InsertSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
        public static ResultEntity<Plugin_Reserach_ResearchStudiesEntity> InsertNotAsync(Plugin_Reserach_ResearchStudiesEntity entity)
        {

            ResultEntity<Plugin_Reserach_ResearchStudiesEntity> result = new ResultEntity<Plugin_Reserach_ResearchStudiesEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ResearchStudiesRepositoryConstants.SP_Insert, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                //sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesID, entity.ResearchStudiesID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesTitle, entity.ResearchStudiesTitle);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesDate, entity.ResearchStudiesDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesAuthor, entity.ResearchStudiesAuthor);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesVersion, entity.ResearchStudiesVersion);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesSummery, entity.ResearchStudiesSummery);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesDetails, entity.ResearchStudiesDetails);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesCatogory, entity.ResearchStudiesCatogory);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesImage, entity.ResearchStudiesImage);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.Order, entity.Order);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.PublishedDate, entity.PublishedDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.IsPublished, entity.IsPublished);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.IsDelete, entity.IsDelete);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.LanguageID, entity.LanguageID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.AddDate, entity.AddDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.AddUser, entity.AddUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.EditDate, entity.EditDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.EditUser, entity.EditUser);

                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ViewCount, entity.ViewCount);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesTitle2, entity.ResearchStudiesTitle2);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesImage2, entity.ResearchStudiesImage2);

                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.MedialLink, entity.MedialLink);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.Target, entity.Target);

                sqlCommand.Parameters.Add(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesID, SqlDbType.BigInt).Direction = ParameterDirection.Output;

                SqlDataReader reader = sqlCommand.ExecuteReader();
                result.Entity = entity;
                entity.ResearchStudiesID = Convert.ToInt64(sqlCommand.Parameters[Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesID].Value);

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.InsertSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_ResearchStudiesEntity>> SelectByID(long ID)
        {

            ResultEntity<Plugin_Reserach_ResearchStudiesEntity> result = new ResultEntity<Plugin_Reserach_ResearchStudiesEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ResearchStudiesRepositoryConstants.SP_SelectByID, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.Add(new SqlParameter(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesID, ID));
                SqlDataReader reader = await sqlCommand.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    reader.Read();
                    result.Entity = EntityHelper(reader, true);
                }
                else
                {
                    result.Status = ErrorEnums.Warning;
                    result.Message = MessageConstants.NotFoundMessage;
                    result.Details = MessageConstants.NotFoundDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
        public static ResultEntity<Plugin_Reserach_ResearchStudiesEntity> SelectByIDNotAsync(long ID)
        {

            ResultEntity<Plugin_Reserach_ResearchStudiesEntity> result = new ResultEntity<Plugin_Reserach_ResearchStudiesEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ResearchStudiesRepositoryConstants.SP_SelectByID, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.Add(new SqlParameter(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesID, ID));
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    result.Entity = EntityHelper(reader, true);
                }
                else
                {
                    result.Status = ErrorEnums.Warning;
                    result.Message = MessageConstants.NotFoundMessage;
                    result.Details = MessageConstants.NotFoundDetails;
                }
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_ResearchStudiesEntity>> Update(Plugin_Reserach_ResearchStudiesEntity entity)
        {

            ResultEntity<Plugin_Reserach_ResearchStudiesEntity> result = new ResultEntity<Plugin_Reserach_ResearchStudiesEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ResearchStudiesRepositoryConstants.SP_Update, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();

                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesTitle, entity.ResearchStudiesTitle);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesDate, entity.ResearchStudiesDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesAuthor, entity.ResearchStudiesAuthor);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesVersion, entity.ResearchStudiesVersion);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesSummery, entity.ResearchStudiesSummery);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesDetails, entity.ResearchStudiesDetails);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesCatogory, entity.ResearchStudiesCatogory);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesImage, entity.ResearchStudiesImage);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.Order, entity.Order);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.PublishedDate, entity.PublishedDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.IsPublished, entity.IsPublished);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.IsDelete, entity.IsDelete);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.LanguageID, entity.LanguageID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.AddDate, entity.AddDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.AddUser, entity.AddUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.EditDate, entity.EditDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.EditUser, entity.EditUser);

                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ViewCount, entity.ViewCount);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesTitle2, entity.ResearchStudiesTitle2);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesImage2, entity.ResearchStudiesImage2);

                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.MedialLink, entity.MedialLink);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.Target, entity.Target);

                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesID, entity.ResearchStudiesID);
                await sqlCommand.ExecuteNonQueryAsync();
                result.Entity = entity;

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.UpdateSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
        public static ResultEntity<Plugin_Reserach_ResearchStudiesEntity> UpdateNotAsync(Plugin_Reserach_ResearchStudiesEntity entity)
        {

            ResultEntity<Plugin_Reserach_ResearchStudiesEntity> result = new ResultEntity<Plugin_Reserach_ResearchStudiesEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ResearchStudiesRepositoryConstants.SP_Update, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesTitle, entity.ResearchStudiesTitle);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesDate, entity.ResearchStudiesDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesAuthor, entity.ResearchStudiesAuthor);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesVersion, entity.ResearchStudiesVersion);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesSummery, entity.ResearchStudiesSummery);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesDetails, entity.ResearchStudiesDetails);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesCatogory, entity.ResearchStudiesCatogory);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesImage, entity.ResearchStudiesImage);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.Order, entity.Order);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.PublishedDate, entity.PublishedDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.IsPublished, entity.IsPublished);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.IsDelete, entity.IsDelete);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.LanguageID, entity.LanguageID);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.AddDate, entity.AddDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.AddUser, entity.AddUser);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.EditDate, entity.EditDate);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.EditUser, entity.EditUser);

                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ViewCount, entity.ViewCount);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesTitle2, entity.ResearchStudiesTitle2);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesImage2, entity.ResearchStudiesImage2);

                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.MedialLink, entity.MedialLink);
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.Target, entity.Target);

                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesID, entity.ResearchStudiesID);

                sqlCommand.ExecuteNonQueryAsync();
                result.Entity = entity;

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.UpdateSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        public async static Task<ResultEntity<Plugin_Reserach_ResearchStudiesEntity>> Delete(long ID)
        {

            ResultEntity<Plugin_Reserach_ResearchStudiesEntity> result = new ResultEntity<Plugin_Reserach_ResearchStudiesEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ResearchStudiesRepositoryConstants.SP_Delete, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesID, ID);

                await sqlCommand.ExecuteReaderAsync();

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.DeleteSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }

        static Plugin_Reserach_ResearchStudiesEntity EntityHelper(SqlDataReader reader, bool isFull)
        {
            Plugin_Reserach_ResearchStudiesEntity entity = new Plugin_Reserach_ResearchStudiesEntity();

            try
            {
                entity.ResearchStudiesID = Convert.ToInt64(reader[Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesID].ToString());
                entity.ResearchStudiesTitle = reader[Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesTitle] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesTitle].ToString();
                entity.ResearchStudiesDate = reader[Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesDate] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(reader[Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesDate].ToString());
                entity.ResearchStudiesAuthor = reader[Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesAuthor] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesAuthor].ToString();
                entity.ResearchStudiesVersion = reader[Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesVersion] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesVersion].ToString();
                entity.ResearchStudiesSummery = reader[Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesSummery] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesSummery].ToString();
                entity.ResearchStudiesDetails = reader[Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesDetails] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesDetails].ToString();
                entity.ResearchStudiesCatogory = reader[Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesCatogory] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesCatogory].ToString();
                entity.ResearchStudiesImage = reader[Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesImage] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesImage].ToString();
                entity.Order = reader[Plugin_Reserach_ResearchStudiesEntityConstants.Order] == DBNull.Value ? 0 : Convert.ToInt64(reader[Plugin_Reserach_ResearchStudiesEntityConstants.Order].ToString());
                entity.PublishedDate = reader[Plugin_Reserach_ResearchStudiesEntityConstants.PublishedDate] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(reader[Plugin_Reserach_ResearchStudiesEntityConstants.PublishedDate].ToString());
                entity.IsPublished = reader[Plugin_Reserach_ResearchStudiesEntityConstants.IsPublished] == DBNull.Value ? false : Convert.ToBoolean(reader[Plugin_Reserach_ResearchStudiesEntityConstants.IsPublished].ToString());
                entity.IsDelete = reader[Plugin_Reserach_ResearchStudiesEntityConstants.IsDelete] == DBNull.Value ? false : Convert.ToBoolean(reader[Plugin_Reserach_ResearchStudiesEntityConstants.IsDelete].ToString());
                entity.LanguageID = reader[Plugin_Reserach_ResearchStudiesEntityConstants.LanguageID] == DBNull.Value ? 1 : Convert.ToInt32(reader[Plugin_Reserach_ResearchStudiesEntityConstants.LanguageID].ToString());
                entity.AddDate = reader[Plugin_Reserach_ResearchStudiesEntityConstants.AddDate] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(reader[Plugin_Reserach_ResearchStudiesEntityConstants.AddDate].ToString());
                entity.EditDate = reader[Plugin_Reserach_ResearchStudiesEntityConstants.EditDate] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(reader[Plugin_Reserach_ResearchStudiesEntityConstants.EditDate].ToString());
                entity.AddUser = reader[Plugin_Reserach_ResearchStudiesEntityConstants.AddUser].ToString();
                entity.EditUser = reader[Plugin_Reserach_ResearchStudiesEntityConstants.EditUser].ToString();

                bool ColumnExists = false;
                try
                {
                    int columnOrdinal = reader.GetOrdinal("LanguageName");
                    ColumnExists = true;
                }
                catch (IndexOutOfRangeException)
                {
                    ColumnExists = false;
                }

                if (ColumnExists)
                {
                    entity.LanguageName = reader[Plugin_Reserach_ResearchStudiesEntityConstants.LanguageName] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ResearchStudiesEntityConstants.LanguageName].ToString();
                }


                try
                {
                    int columnOrdinal = reader.GetOrdinal("ViewCount");
                    ColumnExists = true;
                }
                catch (IndexOutOfRangeException)
                {
                    ColumnExists = false;
                }

                if (ColumnExists)
                {
                    entity.ViewCount = reader[Plugin_Reserach_ResearchStudiesEntityConstants.ViewCount] == DBNull.Value ? 1 : Convert.ToInt64(reader[Plugin_Reserach_ResearchStudiesEntityConstants.ViewCount].ToString());
                }


                try
                {
                    int columnOrdinal = reader.GetOrdinal("ResearchStudiesTitle2");
                    ColumnExists = true;
                }
                catch (IndexOutOfRangeException)
                {
                    ColumnExists = false;
                }

                if (ColumnExists)
                {
                    entity.ResearchStudiesTitle2 = reader[Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesTitle2] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesTitle2].ToString();
                }


                try
                {
                    int columnOrdinal = reader.GetOrdinal("ResearchStudiesImage2");
                    ColumnExists = true;
                }
                catch (IndexOutOfRangeException)
                {
                    ColumnExists = false;
                }

                if (ColumnExists)
                {
                    entity.ResearchStudiesImage2 = reader[Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesImage2] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesImage2].ToString();
                }


                try
                {
                    int columnOrdinal = reader.GetOrdinal("MedialLink");
                    ColumnExists = true;
                }
                catch (IndexOutOfRangeException)
                {
                    ColumnExists = false;
                }

                if (ColumnExists)
                {
                    entity.MedialLink = reader[Plugin_Reserach_ResearchStudiesEntityConstants.MedialLink] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ResearchStudiesEntityConstants.MedialLink].ToString();
                }

                try
                {
                    int columnOrdinal = reader.GetOrdinal("Target");
                    ColumnExists = true;
                }
                catch (IndexOutOfRangeException)
                {
                    ColumnExists = false;
                }

                if (ColumnExists)
                {
                    entity.Target = reader[Plugin_Reserach_ResearchStudiesEntityConstants.Target] == DBNull.Value ? string.Empty : reader[Plugin_Reserach_ResearchStudiesEntityConstants.Target].ToString();
                }



            }
            catch (Exception ex)
            {

            }

            return entity;
        }


        public async static Task<ResultEntity<Plugin_Reserach_ResearchStudiesEntity>> UpdateViewCount(long ResearchStudiesID)
        {

            ResultEntity<Plugin_Reserach_ResearchStudiesEntity> result = new ResultEntity<Plugin_Reserach_ResearchStudiesEntity>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[CommonRepositoryConstants.SQLDBConnection].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand(Plugin_Reserach_ResearchStudiesRepositoryConstants.SP_UpdateViewCount, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                sqlCommand.Parameters.AddWithValue(Plugin_Reserach_ResearchStudiesRepositoryConstants.ResearchStudiesID, ResearchStudiesID);

                await sqlCommand.ExecuteNonQueryAsync();

                result.Status = ErrorEnums.Success;
                result.Message = MessageConstants.UpdateSuccessMessage;
            }
            catch (Exception ex)
            {
                result.Status = ErrorEnums.Exception;
                result.Details = ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                sqlCommand.Dispose();
            }

            return result;
        }
    }
}
