﻿using SiteWare.DataAccess.Common.Constants;
using SiteWare.Entity.Constants.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.DataAccess.RepositorieConstants
{
    public static class Lookup_Reserach_ArticleCategoryRepositoryConstants
    {
        public const string ArticleCategoryID = CommonRepositoryConstants.PreSQLParameter + Lookup_Reserach_ArticleCategoryEntityConstants.ArticleCategoryID;
        public const string name = CommonRepositoryConstants.PreSQLParameter + Lookup_Reserach_ArticleCategoryEntityConstants.name;
        public const string IsPublished = CommonRepositoryConstants.PreSQLParameter + Lookup_Reserach_ArticleCategoryEntityConstants.IsPublished;
        public const string IsDeleted = CommonRepositoryConstants.PreSQLParameter + Lookup_Reserach_ArticleCategoryEntityConstants.IsDeleted;
        public const string AddDate = CommonRepositoryConstants.PreSQLParameter + Lookup_Reserach_ArticleCategoryEntityConstants.AddDate;
        public const string AddUser = CommonRepositoryConstants.PreSQLParameter + Lookup_Reserach_ArticleCategoryEntityConstants.AddUser;
        public const string EditDate = CommonRepositoryConstants.PreSQLParameter + Lookup_Reserach_ArticleCategoryEntityConstants.EditDate;
        public const string EditUser = CommonRepositoryConstants.PreSQLParameter + Lookup_Reserach_ArticleCategoryEntityConstants.EditUser;
        
        public const string SP_SelectAll = "Lookup_Reserach_ArticleCategory_SelectAll";
        public const string SP_SelectByGovID = "Lookup_Reserach_ArticleCategory_SelectByID";
    }
}
