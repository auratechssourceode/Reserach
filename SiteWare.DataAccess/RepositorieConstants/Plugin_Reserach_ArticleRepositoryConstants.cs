﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiteWare.DataAccess.Common.Constants;
using SiteWare.Entity.Constants.Entity;

namespace SiteWare.DataAccess.RepositorieConstants
{
    public static class Plugin_Reserach_ArticleRepositoryConstants
    {
        public const string ArticleID = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ArticleEntityConstants.ArticleID;
        public const string ArticleTitle = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ArticleEntityConstants.ArticleTitle;
        public const string ArticleImage = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ArticleEntityConstants.ArticleImage;
        public const string ArticleDate = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ArticleEntityConstants.ArticleDate;
        public const string ArticleKeyWords = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ArticleEntityConstants.ArticleKeyWords;
        public const string ArticleSummery = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ArticleEntityConstants.ArticleSummery;
        public const string ArticleAuthor = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ArticleEntityConstants.ArticleAuthor;
        public const string ArticleCategory = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ArticleEntityConstants.ArticleCategory;
        public const string ArticleDetails = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ArticleEntityConstants.ArticleDetails;
        public const string Order = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ArticleEntityConstants.Order;
        public const string PublishedDate = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ArticleEntityConstants.PublishedDate;
        public const string IsPublished = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ArticleEntityConstants.IsPublished;
        public const string IsDelete = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ArticleEntityConstants.IsDelete;
        public const string LanguageID = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ArticleEntityConstants.LanguageID;
        public const string AddDate = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ArticleEntityConstants.AddDate;
        public const string AddUser = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ArticleEntityConstants.AddUser;
        public const string EditDate = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ArticleEntityConstants.EditDate;
        public const string EditUser = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ArticleEntityConstants.EditUser;
        public const string ViewCount = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ArticleEntityConstants.ViewCount;

        public const string IsShowSlider = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ArticleEntityConstants.IsShowSlider;

        public const string SP_SelectAll = "Plugin_Reserach_Article_SelectAll";
        public const string SP_Insert = "Plugin_Reserach_Article_Insert";
        public const string SP_SelectByID = "Plugin_Reserach_Article_SelectByID";
        public const string SP_Update = "Plugin_Reserach_Article_Update";
        public const string SP_Delete = "Plugin_Reserach_Article_Delete";
        public const string SP_UpdateViewCount = "Plugin_Reserach_Article_UpdateViewCount";

    }
}
