﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiteWare.DataAccess.Common.Constants;
using SiteWare.Entity.Constants.Entity;

namespace SiteWare.DataAccess.RepositorieConstants
{
    public static class Plugin_Reserach_ResearchStudiesRepositoryConstants
    {
        public const string ResearchStudiesID = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesID;
        public const string ResearchStudiesTitle = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesTitle;
        public const string ResearchStudiesDate = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesDate;
        public const string ResearchStudiesAuthor = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesAuthor;
        public const string ResearchStudiesVersion = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesVersion;
        public const string ResearchStudiesSummery = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesSummery;
        public const string ResearchStudiesDetails = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesDetails;
        public const string ResearchStudiesCatogory = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesCatogory;
        public const string ResearchStudiesImage = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesImage;
        public const string Order = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchStudiesEntityConstants.Order;
        public const string PublishedDate = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchStudiesEntityConstants.PublishedDate;
        public const string IsPublished = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchStudiesEntityConstants.IsPublished;
        public const string IsDelete = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchStudiesEntityConstants.IsDelete;
        public const string LanguageID = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchStudiesEntityConstants.LanguageID;
        public const string AddDate = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchStudiesEntityConstants.AddDate;
        public const string AddUser = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchStudiesEntityConstants.AddUser;
        public const string EditDate = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchStudiesEntityConstants.EditDate;
        public const string EditUser = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchStudiesEntityConstants.EditUser;
        public const string LanguageName = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchStudiesEntityConstants.LanguageName;
        public const string ViewCount = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchStudiesEntityConstants.ViewCount;
        public const string ResearchStudiesTitle2 = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesTitle2;
        public const string ResearchStudiesImage2 = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchStudiesEntityConstants.ResearchStudiesImage2;

        public const string MedialLink = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchStudiesEntityConstants.MedialLink;
        public const string Target = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchStudiesEntityConstants.Target;


        public const string SP_SelectAll = "Plugin_Reserach_ResearchStudies_SelectAll";
        public const string SP_Insert = "Plugin_Reserach_ResearchStudies_Insert";
        public const string SP_SelectByID = "Plugin_Reserach_ResearchStudies_SelectByID";
        public const string SP_Update = "Plugin_Reserach_ResearchStudies_Update";
        public const string SP_Delete = "Plugin_Reserach_ResearchStudies_Delete";
        public const string SP_UpdateViewCount = "Plugin_Reserach_ResearchStudies_UpdateViewCount";

    }
}
