﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiteWare.DataAccess.Common.Constants;
using SiteWare.Entity.Constants.Entity;

namespace SiteWare.DataAccess.RepositorieConstants
{
    public static class Plugin_Reserach_ResearchInnerRepositoryConstants
    {
        public const string ResearchInnerID = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchInnerEntityConstants.ResearchInnerID;
        public const string ResearchInnerTitle = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchInnerEntityConstants.ResearchInnerTitle;
        public const string ResearchInnerSummery = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchInnerEntityConstants.ResearchInnerSummery;        
        public const string ResearchInnerCatogory = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchInnerEntityConstants.ResearchInnerCatogory;
        public const string ResearchInnerImage = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchInnerEntityConstants.ResearchInnerImage;
        public const string Order = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchInnerEntityConstants.Order;
        public const string PublishedDate = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchInnerEntityConstants.PublishedDate;
        public const string IsPublished = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchInnerEntityConstants.IsPublished;
        public const string IsDelete = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchInnerEntityConstants.IsDelete;
        public const string LanguageID = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchInnerEntityConstants.LanguageID;
        public const string AddDate = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchInnerEntityConstants.AddDate;
        public const string AddUser = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchInnerEntityConstants.AddUser;
        public const string EditDate = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchInnerEntityConstants.EditDate;
        public const string EditUser = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchInnerEntityConstants.EditUser;
        public const string LanguageName = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchInnerEntityConstants.LanguageName;
        public const string ViewCount = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchInnerEntityConstants.ViewCount;
        public const string ResearchInnerTitle2 = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchInnerEntityConstants.ResearchInnerTitle2;
        public const string MedialLink = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchInnerEntityConstants.MedialLink;
        public const string Target = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchInnerEntityConstants.Target;
        public const string ResearchInnerDate = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_ResearchInnerEntityConstants.ResearchInnerDate;

         
        public const string SP_SelectAll = "Plugin_Reserach_ResearchInner_SelectAll";
        public const string SP_Insert = "Plugin_Reserach_ResearchInner_Insert";
        public const string SP_SelectByID = "Plugin_Reserach_ResearchInner_SelectByID";
        public const string SP_Update = "Plugin_Reserach_ResearchInner_Update";
        public const string SP_Delete = "Plugin_Reserach_ResearchInner_Delete";
        public const string SP_UpdateViewCount = "Plugin_Reserach_ResearchInner_UpdateViewCount";

    }
}
