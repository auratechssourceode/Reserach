﻿using SiteWare.DataAccess.Common.Constants;
using SiteWare.Entity.Constants.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.DataAccess.RepositorieConstants
{
    public static class Lookup_Reserach_PublicationCategoryRepositoryConstants
    {
        public const string PublicationCategoryID = CommonRepositoryConstants.PreSQLParameter + Lookup_Reserach_PublicationCategoryEntityConstants.PublicationCategoryID;
        public const string name = CommonRepositoryConstants.PreSQLParameter + Lookup_Reserach_PublicationCategoryEntityConstants.name;
        public const string IsPublished = CommonRepositoryConstants.PreSQLParameter + Lookup_Reserach_PublicationCategoryEntityConstants.IsPublished;
        public const string IsDeleted = CommonRepositoryConstants.PreSQLParameter + Lookup_Reserach_PublicationCategoryEntityConstants.IsDeleted;
        public const string AddDate = CommonRepositoryConstants.PreSQLParameter + Lookup_Reserach_PublicationCategoryEntityConstants.AddDate;
        public const string AddUser = CommonRepositoryConstants.PreSQLParameter + Lookup_Reserach_PublicationCategoryEntityConstants.AddUser;
        public const string EditDate = CommonRepositoryConstants.PreSQLParameter + Lookup_Reserach_PublicationCategoryEntityConstants.EditDate;
        public const string EditUser = CommonRepositoryConstants.PreSQLParameter + Lookup_Reserach_PublicationCategoryEntityConstants.EditUser;

        public const string SP_SelectAll = "Lookup_Reserach_PublicationCategory_SelectAll";
        public const string SP_SelectByGovID = "Lookup_Reserach_PublicationCategory_SelectByID";
    }
}
