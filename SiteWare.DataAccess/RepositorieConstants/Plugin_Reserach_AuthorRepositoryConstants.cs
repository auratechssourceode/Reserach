﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiteWare.DataAccess.Common.Constants;
using SiteWare.Entity.Constants.Entity;

namespace SiteWare.DataAccess.RepositorieConstants
{
    public static class Plugin_Reserach_AuthorRepositoryConstants
    {
        public const string AuthorsID = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_AuthorEntityConstants.AuthorsID;
        public const string AuthorName = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_AuthorEntityConstants.AuthorName;
        public const string AuthorImage = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_AuthorEntityConstants.AuthorImage;
        public const string AuthorTitle = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_AuthorEntityConstants.AuthorTitle;
        public const string AuthorSummery = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_AuthorEntityConstants.AuthorSummery;
        public const string AuthorDetails = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_AuthorEntityConstants.AuthorDetails;
        public const string Order = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_AuthorEntityConstants.Order;
        public const string PublishedDate = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_AuthorEntityConstants.PublishedDate;
        public const string IsPublished = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_AuthorEntityConstants.IsPublished;
        public const string IsDelete = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_AuthorEntityConstants.IsDelete;
        public const string LanguageID = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_AuthorEntityConstants.LanguageID;
        public const string AddDate = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_AuthorEntityConstants.AddDate;
        public const string AddUser = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_AuthorEntityConstants.AddUser;
        public const string EditDate = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_AuthorEntityConstants.EditDate;
        public const string EditUser = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_AuthorEntityConstants.EditUser;
        public const string LanguageName = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_AuthorEntityConstants.LanguageName;

        public const string SP_SelectAll = "Plugin_Reserach_Author_SelectAll";
        public const string SP_Insert = "Plugin_Reserach_Author_Insert";
        public const string SP_SelectByID = "Plugin_Reserach_Author_SelectByID";
        public const string SP_Update = "Plugin_Reserach_Author_Update";
        public const string SP_Delete = "Plugin_Reserach_Author_Delete";

    }
}
