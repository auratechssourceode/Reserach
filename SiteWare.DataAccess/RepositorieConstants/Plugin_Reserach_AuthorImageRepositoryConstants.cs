﻿using SiteWare.DataAccess.Common.Constants;
using SiteWare.Entity.Constants.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWare.DataAccess.RepositorieConstants
{
    class Plugin_Reserach_AuthorImageRepositoryConstants
    {
        public const string AuthorImageID = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_AuthorImageEntityConstants.AuthorImageID;
        public const string AuthorID = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_AuthorImageEntityConstants.AuthorID;
        public const string FileName = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_AuthorImageEntityConstants.FileName;
        public const string Path = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_AuthorImageEntityConstants.Path;
        public const string Width = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_AuthorImageEntityConstants.Width;
        public const string Height = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_AuthorImageEntityConstants.Height;
        public const string IsDeleted = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_AuthorImageEntityConstants.IsDeleted;
        public const string AddDate = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_AuthorImageEntityConstants.AddDate;
        public const string DeleteDate = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_AuthorImageEntityConstants.DeleteDate;

        public const string SP_Insert = "Plugin_Reserach_AuthorImageImages_Insert";

        //public const string SP_SelectAll = "";

        public const string SP_SelectAllByIDWidthHeight = "Plugin_Reserach_AuthorImage_SelectAllByAuthorID";
    }
}
