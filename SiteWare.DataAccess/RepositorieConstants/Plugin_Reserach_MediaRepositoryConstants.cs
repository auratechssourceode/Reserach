﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiteWare.DataAccess.Common.Constants;
using SiteWare.Entity.Constants.Entity;

namespace SiteWare.DataAccess.RepositorieConstants
{
    public static class Plugin_Reserach_MediaRepositoryConstants
    {
        public const string MediaID = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_MediaEntityConstants.MediaID;
        public const string MediaImage = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_MediaEntityConstants.MediaImage;
        public const string MedialLink = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_MediaEntityConstants.MedialLink;
        public const string Target = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_MediaEntityConstants.Target;
        public const string Order = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_MediaEntityConstants.Order;
        public const string PublishedDate = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_MediaEntityConstants.PublishedDate;
        public const string IsPublished = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_MediaEntityConstants.IsPublished;
        public const string IsDelete = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_MediaEntityConstants.IsDelete;
        public const string LanguageID = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_MediaEntityConstants.LanguageID;
        public const string AddDate = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_MediaEntityConstants.AddDate;
        public const string AddUser = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_MediaEntityConstants.AddUser;
        public const string EditDate = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_MediaEntityConstants.EditDate;
        public const string EditUser = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_MediaEntityConstants.EditUser;
        public const string Title = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_MediaEntityConstants.Title;
        public const string ViewCount = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_MediaEntityConstants.ViewCount;

        public const string SP_SelectAll = "Plugin_Reserach_Media_SelectAll";
        public const string SP_Insert = "Plugin_Reserach_Media_Insert";
        public const string SP_SelectByID = "Plugin_Reserach_Media_SelectByID";
        public const string SP_Update = "Plugin_Reserach_Media_Update";
        public const string SP_Delete = "Plugin_Reserach_Media_Update";
        public const string SP_UpdateViewCount = "Plugin_Reserach_Media_UpdateViewCount";
    }
}
