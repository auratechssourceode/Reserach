﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiteWare.DataAccess.Common.Constants;
using SiteWare.Entity.Constants.Entity;

namespace SiteWare.DataAccess.RepositorieConstants
{
    public static class Plugin_Reserach_PublicationRepositoryConstants
    {

        public const string PublicationsID = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_PublicationEntityConstants.PublicationsID;
        public const string PublicationsTitle = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_PublicationEntityConstants.PublicationsTitle;
        public const string PublicationsDate = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_PublicationEntityConstants.PublicationsDate;
        public const string PublicationsAuthor = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_PublicationEntityConstants.PublicationsAuthor;
        public const string PublicationsVersion = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_PublicationEntityConstants.PublicationsVersion;
        public const string PublicationsSummery = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_PublicationEntityConstants.PublicationsSummery;
        public const string PublicationsDetails = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_PublicationEntityConstants.PublicationsDetails;
        public const string PublicationsCatogory = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_PublicationEntityConstants.PublicationsCatogory;
        public const string PublicationsImage = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_PublicationEntityConstants.PublicationsImage;

        public const string Order = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_PublicationEntityConstants.Order;
        public const string PublishedDate = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_PublicationEntityConstants.PublishedDate;
        public const string IsPublished = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_PublicationEntityConstants.IsPublished;
        public const string IsDelete = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_PublicationEntityConstants.IsDelete;
        public const string LanguageID = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_PublicationEntityConstants.LanguageID;
        public const string AddDate = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_PublicationEntityConstants.AddDate;
        public const string AddUser = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_PublicationEntityConstants.AddUser;
        public const string EditDate = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_PublicationEntityConstants.EditDate;
        public const string EditUser = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_PublicationEntityConstants.EditUser;

        public const string ViewCount = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_PublicationEntityConstants.ViewCount;
        public const string PublicationsTitle2 = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_PublicationEntityConstants.PublicationsTitle2;
        public const string PublicationsImage2 = CommonRepositoryConstants.PreSQLParameter + Plugin_Reserach_PublicationEntityConstants.PublicationsImage2;

        public const string SP_SelectAll = "Plugin_Reserach_Publication_SelectAll";
        public const string SP_Insert = "Plugin_Reserach_Publication_Insert";
        public const string SP_SelectByID = "Plugin_Reserach_Publication_SelectByID";
        public const string SP_Update = "Plugin_Reserach_Publication_Update";
        public const string SP_Delete = "Plugin_Reserach_Publication_Delete";
        public const string SP_UpdateViewCount = "Plugin_Reserach_Publication_UpdateViewCount";
    }
}
